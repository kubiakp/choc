/*******************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on [TEMPLATE_FILE_NAME] Ver 1.0.0
 *
 *    @file       oc_[driver_name].h
 *
 *    @brief      File with interface functions for the [DRIVER_NAME] driver
 *
 *    @author     [NAME] [SURNAME] - (Created on: [DATE] - [TIME])
 *
 *    @note       Copyright (C) [YEAR] [NAME] [SURNAME] <[EMAIL]>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_[DRIVER_NAME]_LLD_H
#define _OC_[DRIVER_NAME]_LLD_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_[driver_name].h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

/* REQUIRED PROTOTYPES */
extern void              oC_[DRIVER_NAME]_LLD_TurnOnDriver                               ( void );
extern void              oC_[DRIVER_NAME]_LLD_TurnOffDriver                              ( void );

/* CONFIGURE PROTOTYPES */
extern oC_ErrorCode_t    oC_[DRIVER_NAME]_LLD_Configure                                  ( const oC_[DRIVER_NAME]_Config_t * Config );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* _OC_[DRIVER_NAME]_LLD_H */
