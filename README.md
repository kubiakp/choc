# Breaking News #

* TIMER - LLD is ready for review
 
# Tasks for approaching release (31.07.2015)#

*Next release version: [V.0.15.7.0 Bursa](wiki/V.0.15.7.0 Bursa)*

* [All for next release](https://bitbucket.org/kubiakp/choc/issues?status=open&status=new&version=V.0.15.7.0 Bursa)
* [Assigned to Patryk Kubiak](https://bitbucket.org/kubiakp/choc/issues?responsible=kubiakp&status=open&status=new&version=V.0.15.7.0 Bursa)
* [Assigned to Kamil Drobienko](https://bitbucket.org/kubiakp/choc/issues?responsible=drobny&status=open&status=new&version=V.0.15.7.0 Bursa&sort=-priority)
* [Assigned to Mikołaj Filar](https://bitbucket.org/kubiakp/choc/issues?responsible=miki234389&status=new&status=open&version=V.0.15.7.0 Bursa)
* [Assigned to Krzysztof Dworzyński](https://bitbucket.org/kubiakp/choc/issues?responsible=Dworniok&status=open&status=new&version=V.0.15.7.0 Bursa)
* [Assigned to Krzysztof Chmielewski](https://bitbucket.org/kubiakp/choc/issues?responsible=KrzysztofC&status=new&status=open&version=V.0.15.7.0 Bursa)