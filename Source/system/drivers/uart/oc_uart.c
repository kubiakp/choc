/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.c Ver 1.0.0
 *
 *    @file       oc_UART.h
 *
 *    @brief      File with interface of the UART driver
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-14 - 20:37:03)
 *
 *    @note       Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_uart_lld.h>
#include <oc_uart.h>
#include <oc_tranh.h>
#include <oc_os.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/

#define ADD( CHANNEL , SIGNAL , PIN , ...)  const oC_GPIO_PinLink_t * const oC_CAT_6( oC_GPIO_PinLink_ , CHANNEL , _ , SIGNAL , _ , PIN ) =  &PIN;
oC_UART_PinsList(ADD)
#undef ADD

#define VERSION         oC_DRIVER_VERSION(1,0,0,0)

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The macro helper for definition loop for each UART channel
 */
//==========================================================================================================================================
#define foreach_channel     for(oC_UART_Channel_t channel = 0 ; channel < oC_UART_Channel_NumberOfChannels ; channel++ )

//==========================================================================================================================================
/**
 * Reference to the transmission handler container
 */
//==========================================================================================================================================
#define TRANH(channel)      UART.TRANH[channel]

/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The flag if UART driver was initialized
 */
//==========================================================================================================================================
static bool DriverTurnedOn = false;

//==========================================================================================================================================
/**
 * The main container for storing the UART data.
 */
//==========================================================================================================================================
struct
{
    oC_TRANH_t*     TRANH[oC_UART_Channel_NumberOfChannels];
} UART;

//==========================================================================================================================================
/**
 * The interface configuration for TRANH library
 */
//==========================================================================================================================================
static const oC_TRANH_Config_t TRANH_Config     = {
                                                   .Send    = (oC_TRANH_Send_t)     oC_UART_LLD_Send ,
                                                   .Receive = (oC_TRANH_Receive_t)  oC_UART_LLD_Receive
};

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static inline oC_ErrorCode_t    PrepareTransmissionHandler           ( oC_UART_Channel_t Channel );
static inline oC_ErrorCode_t    DeleteTransmissionHandler            ( oC_UART_Channel_t Channel );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 *  Function called before system starts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_TurnOnDriver( void )
{
    oC_UART_LLD_TurnOnDriver( );

    foreach_channel
    {
        TRANH(channel)      = NULL;
    }

    DriverTurnedOn          = true;

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 *  Function called before system restarts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_TurnOffDriver( void )
{
    DriverTurnedOn          = false;

    foreach_channel
    {
        DeleteTransmissionHandler(channel);
    }

    oC_UART_LLD_TurnOffDriver( );

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * The function checks if UART driver is turned on (was initialized first).
 *
 * @return true if driver is turned on, and ready to work.
 */
//==========================================================================================================================================
bool oC_UART_IsTurnedOn( void )
{
    return DriverTurnedOn;
}

//==========================================================================================================================================
/**
 * The function for configuring the driver.
 *
 * @param Config        Reference to the configuration structure
 *
 * @return Code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Configure( const oC_UART_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , oC_OS_IsAddressCorrect( Config ) ? oC_ErrorCode_None : oC_ErrorCode_WrongAddress )
         && oC_AssignErrorCode( &errorCode , oC_UART_LLD_Configure( Config ) )
         && oC_AssignErrorCode( &errorCode , PrepareTransmissionHandler( Config->Channel ) )
            )
    {
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        oC_UART_Unconfigure( Config );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Removes configuration from configured list. The function is called before software system restart.
 *
 * @param Config        Reference to the configuration structure to remove from list
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Unconfigure( const oC_UART_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , oC_UART_LLD_Unconfigure( Config ))
         && oC_AssignErrorCode( &errorCode , DeleteTransmissionHandler( Config->Channel ))
            )
    {
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Checks if the channel was configured
 *
 * @param Channel       Channel to check
 *
 * @return true if configured, false if not, or if channel is not correct
 */
//==========================================================================================================================================
bool oC_UART_IsChannelConfigured( const oC_UART_Channel_t Channel )
{
    bool configured = false;

    if ( Channel < oC_UART_Channel_NumberOfChannels )
    {
        configured  = oC_UART_LLD_IsChannelTurnedOn(    Channel )
                   && oC_UART_LLD_IsChannelEnabled(     Channel );
    }

    return configured;
}

//==========================================================================================================================================
/**
 * The function to send data via the driver as a stream
 *
 * @param Config        Reference to the config structure
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_UART_SendViaStream( const oC_UART_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int sent_bytes = 0;

    if (
            oC_TRANH_LockSend( TRANH(Config->Channel) , Timeout_ms )
         && oC_TRANH_SetParameter( TRANH(Config->Channel) , Config )
         && oC_TRANH_Send( TRANH(Config->Channel) , (uint8_t*)Data , Length )
         && oC_TRANH_WaitForSendFinished( TRANH(Config->Channel) , Timeout_ms )
         )
    {
        sent_bytes  = Length;
    }

    return sent_bytes;
}

//==========================================================================================================================================
/**
 * The function to receive data via the driver as a stream
 *
 * @param Context       Reference to the context of the driver
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_UART_ReceiveViaStream( const oC_UART_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int received_bytes = 0;

    if (
            oC_TRANH_LockReceive( TRANH(Config->Channel) , Timeout_ms )
         && oC_TRANH_SetParameter( TRANH(Config->Channel) , Config )
         && oC_TRANH_Receive( TRANH(Config->Channel) , (uint8_t*)Data , Length )
         && oC_TRANH_WaitForReceiveFinished( TRANH(Config->Channel) , Timeout_ms )
         )
    {
        received_bytes = Length;
    }

    return received_bytes;
}

//==========================================================================================================================================
/**
 * Sets loopback for configuration
 *
 * @param Config
 * @param Loopback
 *
 * @return
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_SetLoopback( const oC_UART_Config_t * Config , bool Loopback )
{
    return oC_UART_LLD_SetLoopback( Config , Loopback );
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Prepares transmission handler object
 *
 * @param Channel       Channel to prepare the transmission handler
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t PrepareTransmissionHandler( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode;

    if( oC_TRANH_IsContextCorrect( TRANH(Channel) ) )
    {
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        TRANH(Channel)  = oC_TRANH_New( &TRANH_Config );

        if ( oC_TRANH_IsContextCorrect( TRANH(Channel) ) )
        {
            errorCode   = oC_ErrorCode_None;
        }
        else
        {
            errorCode   = oC_ErrorCode_UARTCannotPrepareTRAHN;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function deletes transmission handler object for selected channel
 *
 * @param Channel   Channel to delete transmission handler
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t DeleteTransmissionHandler( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Channel < oC_UART_Channel_NumberOfChannels )
    {
        oC_TRANH_Delete( &TRANH(Channel) );
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

/*==========================================================================================================================================
//
//     INTERRUPTS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Common handler for interrupts
 *
 * @param Channel   Number of channel which interrupt occur
 *
 * @return true if task woken
 */
//==========================================================================================================================================
bool oC_UART_INT_InterruptHandler( oC_UART_Channel_t Channel )
{
    bool taskWoken = false;

    if ( oC_TRANH_IsContextCorrect( TRANH(Channel) ) )
    {
        oC_TRANH_HandleRx( TRANH(Channel) , &taskWoken );
        oC_TRANH_HandleTx( TRANH(Channel) , &taskWoken );
    }

    return taskWoken;
}

/** ****************************************************************************************************************************************
 * The section with driver definition
 */
#define _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________

static const oC_Driver_Interface_t Interface = {
                                                .RequiredBootLevel              = oC_Boot_Level_DontRequireAnything ,
                                                .TurnOnFunction                 = oC_UART_TurnOnDriver ,
                                                .TurnOffFunction                = oC_UART_TurnOffDriver ,
                                                .IsTurnedOnFunction             = oC_UART_IsTurnedOn ,
                                                .ConfigureFunction              = oC_UART_Configure ,
                                                .UnconfigureFunction            = oC_UART_Unconfigure ,
                                                .SendViaStreamFunction          = oC_UART_SendViaStream ,
                                                .ReceiveViaStreamFunction       = oC_UART_ReceiveViaStream
};

oC_DRIVER_DEFINE(UART , "uart" , VERSION , Interface);

/* END OF SECTION */
#undef  _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________
