/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.h Ver 1.0.0
 *
 *    @file       oc_uart2.h
 *
 *    @brief      File with interface functions for the UART2 driver
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-14 - 20:37:04)
 *
 *    @note       Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_UART2_H
#define _OC_UART2_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_uart_defs.h>
#include <oc_gen_macros.h>
#include <oc_errors.h>
#include <oc_gpio.h>
#include <oc_driver.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The type storing channel
 */
//==========================================================================================================================================
typedef enum
{
#define ADD(CNAME)  oC_CAT_2( oC_UART_Channel_ , CNAME ) ,
    oC_UART_ChannelsList( ADD )
#undef ADD
    oC_UART_Channel_NumberOfChannels
} oC_UART_Channel_t;

//==========================================================================================================================================
/**
 * Store length of the word
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_WordLength_5Bits ,  //!< oC_UART_WordLength_5Bits
    oC_UART_WordLength_6Bits ,  //!< oC_UART_WordLength_6Bits
    oC_UART_WordLength_7Bits ,  //!< oC_UART_WordLength_7Bits
    oC_UART_WordLength_8Bits    //!< oC_UART_WordLength_8Bits
} oC_UART_WordLength_t;

//==========================================================================================================================================
/**
 * Parity checks
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_Parity_None ,//!< oC_UART_Parity_None
    oC_UART_Parity_Odd , //!< oC_UART_Parity_Odd
    oC_UART_Parity_Even  //!< oC_UART_Parity_Even
} oC_UART_Parity_t;

//==========================================================================================================================================
/**
 * How long should be stop bit
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_StopBit_1Bit ,   //!< oC_UART_StopBit_1Bit
    oC_UART_StopBit_1p5Bits ,//!< oC_UART_StopBit_1p5Bits
    oC_UART_StopBit_2Bits    //!< oC_UART_StopBit_2Bits
} oC_UART_StopBit_t;

//==========================================================================================================================================
/**
 * Order of bits in frame
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_BitOrder_LSBFirst ,//!< oC_UART_BitOrder_LSBFirst
    oC_UART_BitOrder_MSBFirst  //!< oC_UART_BitOrder_MSBFirst
} oC_UART_BitOrder_t;

//==========================================================================================================================================
/**
 * If uart should be inverted
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_Invert_NotInverted ,//!< oC_UART_Invert_NotInverted
    oC_UART_Invert_Inverted     //!< oC_UART_Invert_Inverted
} oC_UART_Invert_t;

//==========================================================================================================================================
/**
 * Pins needed for UART
 */
//==========================================================================================================================================
typedef struct
{
    const oC_GPIO_PinLink_t *           Rx;
    const oC_GPIO_PinLink_t *           Tx;
} oC_UART_Pins_t;

//==========================================================================================================================================
/**
 * Configuration of the UART2
 */
//==========================================================================================================================================
typedef struct
{
    oC_UART_Channel_t           Channel;
    oC_UART_WordLength_t        WordLength;
    uint32_t                    BitRate;
    oC_UART_Parity_t            Parity;
    oC_UART_StopBit_t           StopBit;
    oC_UART_BitOrder_t          BitOrder;
    oC_UART_Invert_t            Invert;
    oC_UART_Pins_t              Pins;
} oC_UART_Config_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/

#define ADD( CHANNEL , SIGNAL , PIN , ...)  extern const oC_GPIO_PinLink_t * const oC_CAT_6( oC_GPIO_PinLink_ , CHANNEL , _ , SIGNAL , _ , PIN );
oC_UART_PinsList(ADD)
#undef ADD

/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern oC_ErrorCode_t oC_UART_Configure                    ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t oC_UART_Unconfigure                  ( const oC_UART_Config_t * Config );
extern bool           oC_UART_IsChannelConfigured          ( const oC_UART_Channel_t Channel );
extern int            oC_UART_SendViaStream                ( const oC_UART_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern int            oC_UART_ReceiveViaStream             ( const oC_UART_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern oC_ErrorCode_t oC_UART_SetLoopback                  ( const oC_UART_Config_t * Config , bool Loopback );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

oC_DRIVER_DECLARE(UART);

#endif /* _OC_UART2_H */
