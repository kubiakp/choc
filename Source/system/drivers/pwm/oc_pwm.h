/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.h Ver 1.0.0
 *
 *    @file       oc_pwm.h
 *
 *    @brief      File with interface functions for the PWM driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-09 - 13:00:58)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_PWM_H
#define _OC_PWM_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_pwm_defs.h>
#include <oc_gen_macros.h>
#include <oc_errors.h>
#include <oc_gpio.h>
#include <stdint.h>
#include <oc_sys.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The type for store duty of the PWM signal. It store information in format:
 *
 *          000.00%
 *
 * To set 100% duty, set duty variable to value 10000
 */
//==========================================================================================================================================
typedef uint16_t oC_PWM_Duty_t;

//==========================================================================================================================================
/**
 * The way to generate PWM signal.
 */
//==========================================================================================================================================
typedef enum
{
    oC_PWM_Mode_Hardware ,                       //!< It will use PWM mode from micro controller timers
    oC_PWM_Mode_HardwareIfAvailable ,            //!< It will use #oC_PWM_Mode_Hardware mode if, there is a possibility, or use #oC_PWM_Mode_Software mode, if not.
    oC_PWM_Mode_BitBanging ,                     //!< It will simulate a PWM signal using cyclic functions. It will not be accurate
    oC_PWM_Mode_Software = oC_PWM_Mode_BitBanging//!< Look at #oC_PWM_Mode_BitBanging
} oC_PWM_Mode_t;

//==========================================================================================================================================
/**
 * Configuration of the PWM. The structure should be used for #oC_PWM_Configure function.
 */
//==========================================================================================================================================
typedef struct
{
    const oC_GPIO_PinLink_t *           PinLink;    /**< Reference to the pin, where PWM should be generated */
    oC_Frequency_t                  Frequency;  /**< Frequency of the PWM signal. */
    oC_PWM_Mode_t                       Mode;       /**< Mode of the generation - hardware if available, or software. */
} oC_PWM_Config_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern void           oC_PWM_TurnOnDriver                 ( void );
extern void           oC_PWM_TurnOffDriver                ( void );
extern oC_ErrorCode_t oC_PWM_Configure                    ( const oC_PWM_Config_t * Config );
extern oC_ErrorCode_t oC_PWM_Unconfigure                  ( const oC_PWM_Config_t * Config );
extern int 	          oC_PWM_SendViaStream				  ( const oC_PWM_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern int 	          oC_PWM_ReceiveViaStream             ( const oC_PWM_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern oC_ErrorCode_t oC_PWM_SetDuty                      ( const oC_PWM_Config_t * Config , oC_PWM_Duty_t Duty );
extern oC_PWM_Duty_t  oC_PWM_GetDuty                      ( const oC_PWM_Config_t * Config );
extern oC_ErrorCode_t oC_PWM_ReadDuty                     ( const oC_PWM_Config_t * Config , oC_PWM_Duty_t * Duty );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

#endif /* _OC_PWM_H */
