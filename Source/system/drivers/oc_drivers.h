/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_drivers.h
 *
 *    @brief      With the interface for the drivers managing
 *
 *    @author     Patryk Kubiak - (Created on: 6 pa� 2014 16:58:32)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef OC_DRIVERS_H_
#define OC_DRIVERS_H_

#include <oc_compiler.h>
#include <oc_driver.h>
#include <oc_drivers_cfg.h>
#include <oc_default_drivers_cfg.h>
#include <oc_errors.h>
#include <oc_spi.h>
#include <oc_gpio.h>
#include <oc_1wire.h>
#include <oc_7seg.h>
#include <oc_a4982.h>
#include <oc_acom.h>
#include <oc_adc.h>
#include <oc_bluet.h>
#include <oc_can.h>
#include <oc_dac.h>
#include <oc_ds18b20.h>
#include <oc_enc.h>
#include <oc_enet.h>
#include <oc_erom.h>
#include <oc_gprs.h>
#include <oc_gps.h>
#include <oc_gsm.h>
#include <oc_hd44780.h>
#include <oc_i2c.h>
#include <oc_led.h>
#include <oc_mkc.h>
#include <oc_pcm.h>
#include <oc_ps2.h>
#include <oc_pwm.h>
#include <oc_rfid.h>
#include <oc_rtc.h>
#include <oc_sdcc.h>
#include <oc_tft.h>
#include <oc_tsc.h>
#include <oc_usb.h>
#include <oc_uart.h>
#include <oc_wdg.h>
#include <oc_wifi.h>
#include <oc_xbee.h>
#include <oc_sys.h>
#include <oc_tim.h>

/** ****************************************************************************************************************************************
 * The section with interface macros
 */
#define _________________________________________INTEFACE_MACROS_SECTION____________________________________________________________________

#define oC_Drivers_IsDriverRegistered( DriverName )                 _oC_Drivers_IsDriverRegistered( &oC_DRIVER_REGISTRATION_NAME(DriverName) )
#define oC_Drivers_IsDriverTurnedOn( DriverName )                   _oC_Drivers_IsDriverTurnedOn( &oC_DRIVER_REGISTRATION_NAME(DriverName) )
#define oC_Drivers_ConfigureDriver( DriverName , Configuration )    _oC_Drivers_ConfigureDriver( &oC_DRIVER_REGISTRATION_NAME(DriverName) , Configuration )
#define oC_Drivers_UnconfigureDriver( DriverName , Configuration )  _oC_Drivers_ConfigureDriver( &oC_DRIVER_REGISTRATION_NAME(DriverName) , Configuration )
#define oC_Drivers_TurnOnDriver( DriverName )                       _oC_Drivers_TurnDriver( &oC_DRIVER_REGISTRATION_NAME(DriverName) , true )
#define oC_Drivers_TurnOffDriver( DriverName )                      _oC_Drivers_TurnDriver( &oC_DRIVER_REGISTRATION_NAME(DriverName) , false )
#define oC_Drivers_RestartDriver( DriverName )                      _oC_Drivers_TurnDriver( &oC_DRIVER_REGISTRATION_NAME(DriverName) , false );\
                                                                    _oC_Drivers_TurnDriver( &oC_DRIVER_REGISTRATION_NAME(DriverName) , true )
/* END OF SECTION */
#undef  _________________________________________INTEFACE_MACROS_SECTION____________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with drivers functions prototypes
 */
#define _________________________________________INTERFACE_PROTOTYPES_SECTION_______________________________________________________________

extern bool             _oC_Drivers_IsDriverRegistered          ( const oC_Driver_t * DriverRegistration );
extern bool             _oC_Drivers_IsDriverTurnedOn            ( const oC_Driver_t * DriverRegistration );
extern oC_ErrorCode_t   _oC_Drivers_ConfigureDriver             ( const oC_Driver_t * DriverRegistration , const void * Config );
extern oC_ErrorCode_t   _oC_Drivers_UnconfigureDriver           ( const oC_Driver_t * DriverRegistration , const void * Config );
extern oC_ErrorCode_t   _oC_Drivers_TurnDriver                  ( const oC_Driver_t * DriverRegistration , bool TurnOn );
extern void             oC_Drivers_TurnOnDriversFromLevel       ( oC_Boot_Level_t Level );
extern void             oC_Drivers_LoadDefaultConfigurations    ( oC_Boot_Level_t Level );

/* END OF SECTION */
#undef  _________________________________________INTERFACE_PROTOTYPES_SECTION_______________________________________________________________

/** ****************************************************************************************************************************************
 * The section global variables definitions
 */
#define _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

#define CONFIGURE( DRIVER_NAME , CONFIGURATION )            extern const oC_DRIVER_CONFIGURATION_TYPE_NAME(DRIVER_NAME) CONFIGURATION;
CFG_LIST_DEFAULT_DRIVERS_CONFIGURATIONS(CONFIGURE)
#undef CONFIGURE

/* END OF SECTION */
#undef  _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________


#endif /* OC_DRIVERS_H_ */
