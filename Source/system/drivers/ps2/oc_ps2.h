/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.h Ver 1.0.0
 *
 *    @file       oc_ps2.h
 *
 *    @brief      File with interface functions for the PS2 driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-09 - 12:54:39)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_PS2_H
#define _OC_PS2_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_gen_macros.h>
#include <oc_errors.h>
#include <stdint.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Configuration of the PS2
 */
//==========================================================================================================================================
typedef struct
{

} oC_PS2_Config_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern void           oC_PS2_TurnOnDriver                 ( void );
extern void           oC_PS2_TurnOffDriver                ( void );
extern oC_ErrorCode_t oC_PS2_Configure                    ( const oC_PS2_Config_t * Config );
extern oC_ErrorCode_t oC_PS2_Unconfigure                  ( const oC_PS2_Config_t * Config );
extern int 	          oC_PS2_SendViaStream				  ( const oC_PS2_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern int 	          oC_PS2_ReceiveViaStream             ( const oC_PS2_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* _OC_PS2_H */
