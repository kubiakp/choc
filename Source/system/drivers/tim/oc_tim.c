/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.c Ver 1.0.0
 *
 *    @file       oc_tim.h
 *
 *    @brief      File with interface of the TIM driver
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-25 - 19:00:21)
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_tim.h>
#include <oc_tim_lld.h>
#include <oc_os.h>

/** ****************************************************************************************************************************************
 * The section local constant variables
 */
#define _________________________________________LOCAL_CONSTANT_SECTION_____________________________________________________________________

#define VERSION         oC_DRIVER_VERSION(1,0,0,0)

/* END OF SECTION */
#undef  _________________________________________LOCAL_CONSTANT_SECTION_____________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with local types
 */
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________



/* END OF SECTION */
#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________



/** ****************************************************************************************************************************************
 * The section with local macros
 */
#define _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

//==========================================================================================================================================
/**
 * The macro creates for loop for each channel in the uC.
 *
 * @param channel       Name of the channel variable
 */
//==========================================================================================================================================
#define foreach_channel( channel )      for(oC_TIM_Channel_t channel = 0 ; channel < oC_TIM_Channel_NumberOfChannels ; channel++)

/* END OF SECTION */
#undef  _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with local variables (static)
 */
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * The structure with variables for storing the informations about TIM driver.
 */
//==========================================================================================================================================
struct
{
    bool                    IsTurnedOn;                                             /**< Flag if driver is turned on */
    bool                    ChannelLockFlags    [oC_TIM_Channel_NumberOfChannels];  /**< Flags if the channel is locked */
    bool                    ChannelConfigured   [oC_TIM_Channel_NumberOfChannels];  /**< Flags if the channel is configured correctly */
    oC_TIM_Callback_t       MatchCallbacks      [oC_TIM_Channel_NumberOfChannels];  /**< Pointer to functions to call when match interrupt occurs */
    void *                  MatchContexts       [oC_TIM_Channel_NumberOfChannels];  /**< Array with contexts for the match callbacks */
    oC_TIM_Callback_t       TimeoutCallbacks    [oC_TIM_Channel_NumberOfChannels];  /**< Pointer to functions to call when timeout interrupt occurs */
    void *                  TimeoutContexts     [oC_TIM_Channel_NumberOfChannels];  /**< Array with contexts for the timeout callbacks */
    const oC_TIM_Config_t * Configurations      [oC_TIM_Channel_NumberOfChannels];  /**< Array of configurations for each timer channel */
} TIM;

/* END OF SECTION */
#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with prototypes of local functions
 */
#define _________________________________________LOCAL_FUNCTION_PROTOTYPES_SECTION__________________________________________________________

static oC_ErrorCode_t       CheckConfiguration              ( const oC_TIM_Config_t * Configuration );
static oC_ErrorCode_t       FindFreeChannelForConfiguration ( const oC_TIM_Config_t * Configuration , oC_TIM_Channel_t * outChannel );
static oC_ErrorCode_t       LockChannel                     ( oC_TIM_Channel_t Channel );
static oC_ErrorCode_t       UnlockChannel                   ( oC_TIM_Channel_t Channel );
static inline bool          IsChannelLocked                 ( oC_TIM_Channel_t Channel );
static inline void          SetChannelConfigured            ( oC_TIM_Channel_t Channel , const oC_TIM_Config_t * Configuration );
static inline void          SetChannelUnconfigured          ( oC_TIM_Channel_t Channel );
static inline void          SetMatchCallback                ( oC_TIM_Channel_t Channel , oC_TIM_Callback_t Callback  , void * Context );
static inline void          SetTimeoutCallback              ( oC_TIM_Channel_t Channel , oC_TIM_Callback_t Callback  , void * Context );
static oC_ErrorCode_t       ConfigureMatchCallback          ( oC_TIM_Channel_t Channel , oC_TIM_Callback_t Callback  , void * Context );
static oC_ErrorCode_t       ConfigureTimeoutCallback        ( oC_TIM_Channel_t Channel , oC_TIM_Callback_t Callback  , void * Context );
static inline bool          IsChannelConfigured             ( oC_TIM_Channel_t Channel );

/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTION_PROTOTYPES_SECTION__________________________________________________________

/** ****************************************************************************************************************************************
 * The section with interface functions sources
 *
 * @addtogroup TIM
 * @{
 */
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


//==========================================================================================================================================
/**
 *  The function for turning on the driver. It initializes module to work, prepares variables, and allocate memory needed this module to work.
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    if ( TIM.IsTurnedOn == false )
    {
        oC_TIM_LLD_TurnOnDriver( );

        foreach_channel(channel)
        {
            oC_TIM_LLD_RestoreDefaultStateOnChannel( channel );
            UnlockChannel(channel);
            SetChannelUnconfigured( channel );
            SetMatchCallback(   channel , NULL , NULL );
            SetTimeoutCallback( channel , NULL , NULL );
        }

        TIM.IsTurnedOn      = true;

        errorCode = oC_ErrorCode_None;
    }
    else
    {
        errorCode = oC_ErrorCode_DriverNotTurnedOff;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 *  The function for turning off the driver. It removes all configurations, and free all allocated by module memory.
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( TIM.IsTurnedOn )
    {
        oC_TIM_LLD_TurnOffDriver( );

        TIM.IsTurnedOn  = false;
        errorCode = oC_ErrorCode_None;
    }
    else
    {
        errorCode = oC_ErrorCode_DriverNotTurnedOn;
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * The function checks, if the driver was initialized before.
 *
 * @return true if the driver is turned on.
 */
//==========================================================================================================================================
bool
oC_TIM_IsTurnedOn( void )
{
   return TIM.IsTurnedOn;
}

//==========================================================================================================================================
/**
 * Configures driver to work with the given configuration. It should be turned on before calling this function.
 *
 * @param Configuration        Reference to the configuration structure
 *
 * @return Code of error if failure.
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_Configure(
                  IN    const oC_TIM_Config_t * Configuration
                  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_TIM_Channel_t channel;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ? oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , CheckConfiguration( Configuration )        )
         && oC_AssignErrorCode( &errorCode , FindFreeChannelForConfiguration( Configuration , &channel ) )
         && oC_AssignErrorCode( &errorCode , LockChannel( channel )   )
            )
    {
        if (
                oC_AssignErrorCode( &errorCode , oC_TIM_LLD_Configure( Configuration , channel ) )
             && oC_AssignErrorCode( &errorCode , ConfigureMatchCallback(    channel , Configuration->MatchCallback   , Configuration->MatchContext   ) )
             && oC_AssignErrorCode( &errorCode , ConfigureTimeoutCallback(  channel , Configuration->TimeoutCallback , Configuration->TimeoutContext ) )
                )
        {
            SetChannelConfigured( channel , Configuration );
            errorCode   = oC_ErrorCode_None;
        }
        else
        {
            oC_TIM_LLD_RestoreDefaultStateOnChannel( channel );
            UnlockChannel(          channel );
            SetChannelUnconfigured( channel );
            SetMatchCallback(       channel , NULL , NULL );
            SetTimeoutCallback(     channel , NULL , NULL );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * This function is for restore default state on the selected driver configuration. In the TIM driver case, this unlock configured channel,
 * and stops timer.
 *
 * @param Configuration        Reference to the configuration structure to remove from list
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_Unconfigure(
                    IN  const oC_TIM_Config_t * Configuration
                    )
{
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    oC_TIM_Channel_t    channel   = oC_TIM_Channel_NumberOfChannels;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ? oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , CheckConfiguration( Configuration ) )
         && oC_AssignErrorCode( &errorCode , oC_TIM_ReadChannelOfConfiguration( Configuration , &channel ) )
         && oC_AssignErrorCode( &errorCode , oC_TIM_LLD_Unconfigure( Configuration , channel) )
         && oC_AssignErrorCode( &errorCode , UnlockChannel( channel ) )
            )
    {
        SetMatchCallback(   channel , NULL , NULL );
        SetTimeoutCallback( channel , NULL , NULL );
        errorCode    = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function to send data via the driver as a stream
 *
 * @param Config        Reference to the config structure
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int
oC_TIM_SendViaStream(
                      IN    const oC_TIM_Config_t * Config ,
                      IN    char *                  Data ,
                      IN    uint32_t                Length ,
                      IN    uint32_t                Timeout_ms)
{
    int sent_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return sent_bytes;
}

//==========================================================================================================================================
/**
 * The function to receive data via the driver as a stream
 *
 * @param Context       Reference to the context of the driver
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int
oC_TIM_ReceiveViaStream(
                         IN     const oC_TIM_Config_t * Config ,
                         OUT    char * Data ,
                         IN     uint32_t Length ,
                         IN     uint32_t Timeout_ms
                         )
{
    int received_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return received_bytes;
}

//==========================================================================================================================================
/**
 * The function reads channel of configuration.
 *
 * @param Configuration
 * @param outChannel
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_ReadChannelOfConfiguration(
                                   IN   const   oC_TIM_Config_t *       Configuration ,
                                   OUT          oC_TIM_Channel_t *      outChannel
                                   )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , oC_OS_IsAddressCorrect( Configuration ) ? oC_ErrorCode_None : oC_ErrorCode_WrongAddress )
         && oC_AssignErrorCode( &errorCode , oC_OS_IsRAMAddress( outChannel ) ?        oC_ErrorCode_None : oC_ErrorCode_OutputAddressNotInRAM )
            )
    {
        if ( Configuration->Channel < oC_TIM_Channel_NumberOfChannels )
        {
            *outChannel     = Configuration->Channel;
            errorCode       = oC_ErrorCode_None;
        }
        else if ( Configuration->Channel == oC_TIM_Channel_FirstAvailable )
        {
            errorCode = oC_ErrorCode_TIMChannelNotConfigured;

            foreach_channel( channel )
            {
                if ( TIM.Configurations[channel] == Configuration )
                {
                    *outChannel = channel;
                    errorCode   = oC_ErrorCode_None;
                    break;
                }
            }
        }
        else
        {
            errorCode  = oC_ErrorCode_WrongChannel;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function starts counting on selected channel of timer.
 *
 * @note Should be configured first!
 *
 * @param Channel       Channel of timer to starts counting
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_StartTimer(
                   IN   oC_TIM_Channel_t Channel
                   )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , oC_TIM_LLD_StartTimer( Channel ) )
            )
    {
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function stops counting on selected channel of timer. See also #oC_TIM_StartTimer function
 *
 * @note Should be configured first!
 *
 * @param Channel       Channel of timer to stops counting
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_StopTimer(
                  IN    oC_TIM_Channel_t Channel
                  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ? oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , oC_TIM_LLD_StopTimer( Channel ) )
            )
    {
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Checks if timer on the given channel is running.
 *
 * @param Channel   Configured channel number
 *
 * @return true if timer is running
 */
//==========================================================================================================================================
bool oC_TIM_IsTimerRunning( oC_TIM_Channel_t Channel )
{
    bool timerRunning = false;

    if (
            Channel < oC_TIM_Channel_NumberOfChannels
         && TIM.ChannelConfigured[Channel]
        )
    {
        timerRunning    = oC_TIM_LLD_IsTimerRunning( Channel );
    }

    return timerRunning;
}

//==========================================================================================================================================
/**
 * Mark the channel as used to protect it from configuring for other modules (for example for PWM)
 *
 * See also #oC_TIM_UnlockChannel
 *
 * @param Channel       Channel to lock
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_LockChannel(
                    IN  oC_TIM_Channel_t Channel
                    )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ? oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , LockChannel(Channel) )
            )
    {
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Mark the channel as not used to enable for configuring for other modules (for example for PWM)
 *
 * See also #oC_TIM_LockChannel
 *
 * @param Channel       Channel to release
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_UnlockChannel(
                      IN    oC_TIM_Channel_t Channel
                      )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ? oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , UnlockChannel(Channel) )
            )
    {
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Checks if the channel is locked. The locked channel cannot be configured.
 *
 * @param Channel       Channel to check if is locked
 *
 * @return true if locked
 */
//==========================================================================================================================================
bool
oC_TIM_IsChannelLocked(
                        IN  oC_TIM_Channel_t Channel
                        )
{
    bool channelLocked = true;

    if (
            Channel < oC_TIM_Channel_NumberOfChannels
         && TIM.IsTurnedOn
            )
    {
        channelLocked   = TIM.ChannelLockFlags[Channel];
    }

    return channelLocked;
}

//==========================================================================================================================================
/**
 * Returns current count value on selected timer
 *
 * @param Channel       Channel to read current count
 *
 * @return count on channel or 0 if error
 */
//==========================================================================================================================================
uint64_t
oC_TIM_GetCurrentCount(
                        IN  oC_TIM_Channel_t Channel
                        )
{
    uint64_t count = 0;

    if (
            Channel < oC_TIM_Channel_NumberOfChannels
         && TIM.IsTurnedOn
         && TIM.ChannelConfigured[Channel]
            )
    {
        count = oC_TIM_LLD_GetCurrentCount( Channel );
    }

    return count;
}

//==========================================================================================================================================
/**
 * Reads current count value on selected timer.
 *
 * @param Channel       Channel to read current count
 * @param outValue      [out]   Reference to variable where current count should be set
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_ReadCurrentCount(
                         IN     oC_TIM_Channel_t Channel ,
                         OUT    uint64_t * outValue
                         )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , oC_OS_IsRAMAddress( outValue ) ?            oC_ErrorCode_None : oC_ErrorCode_WrongAddress )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_ReadCurrentCount( Channel , outValue );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Sets count value for timer on selected channel.
 *
 * @param Channel       Channel to set count
 * @param Value         Count value to set
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_SetCurrentCount(
                        IN      oC_TIM_Channel_t Channel ,
                        IN      uint64_t Value
                        )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_SetCurrentCount( Channel , Value );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Changes maximum value for timer
 *
 * @param Channel       Channel to reconfigure
 * @param Value         Maximum value to set
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_SetMaxValue(
                    IN  oC_TIM_Channel_t Channel ,
                    IN  uint64_t Value
                    )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_SetMaxValue( Channel , Value );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Reads the maximum value, for which the channel is already counting.
 *
 * @param Channel       Channel to read
 * @param outValue      Destination for the maximum value
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_ReadMaxValue(
                     IN     oC_TIM_Channel_t Channel ,
                     OUT    uint64_t * outValue
                     )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , oC_OS_IsRAMAddress( outValue ) ?            oC_ErrorCode_None : oC_ErrorCode_WrongAddress )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_ReadMaxValue( Channel , outValue );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Returns maximum value configured on the channel.
 *
 * @note Channel must be configured first
 *
 * @param Channel   Channel to read
 *
 * @return maximum value or 0 if error
 */
//==========================================================================================================================================
uint64_t
oC_TIM_GetMaxValue(
                    IN  oC_TIM_Channel_t Channel
                    )
{
    uint64_t maxValue = 0;

    if (
            Channel < oC_TIM_Channel_NumberOfChannels
         && TIM.ChannelConfigured[Channel]
                                  )
    {
        maxValue = oC_TIM_LLD_GetMaxValue( Channel );
    }

    return maxValue;
}

//==========================================================================================================================================
/**
 * The function sets the match value on configured channel. When the value is matched, then it can be used to generate an interrupt.
 *
 * @param Channel       Number of configured channel
 * @param Value         Value to set
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_SetMatchValue(
                      IN    oC_TIM_Channel_t Channel ,
                      IN    uint64_t Value
                      )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_SetMatchValue( Channel , Value );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function reads the match value on configured channel. When the value is matched, then it can be used to generate an interrupt.
 *
 * @param Channel       Number of configured channel
 * @param outValue      Destination for the match value
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_ReadMatchValue(
                       IN   oC_TIM_Channel_t Channel ,
                       OUT  uint64_t *       outValue
                       )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , oC_OS_IsRAMAddress( outValue ) ?            oC_ErrorCode_None : oC_ErrorCode_WrongAddress )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_ReadMatchValue( Channel , outValue );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function returns match value on the selected channel.
 *
 * @param Channel       Number of configured channel
 *
 * @return 0 if error
 */
//==========================================================================================================================================
uint64_t
oC_TIM_GetMatchValue(
                      IN    oC_TIM_Channel_t Channel
                      )
{
    uint64_t matchValue = 0;

    if (
            Channel < oC_TIM_Channel_NumberOfChannels
         && TIM.ChannelConfigured[Channel]
                                  )
    {
        matchValue = oC_TIM_LLD_GetMatchValue( Channel );
    }

    return matchValue;
}

//==========================================================================================================================================
/**
 *
 * @param Channel
 * @param Value
 * @return
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_SetValue(
                 IN oC_TIM_Channel_t    Channel ,
                 IN uint64_t            Value
                 )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 *
 * @param Channel
 * @param outValue
 * @return
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_ReadValue(
                  IN    oC_TIM_Channel_t    Channel ,
                  OUT   uint64_t *          outValue
                  )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Reads value of the channel, if is configured
 *
 * @param Channel
 *
 * @return
 */
//==========================================================================================================================================
uint64_t oC_TIM_GetValue(
                          oC_TIM_Channel_t Channel
                          )
{
    uint64_t value = 0;

    if (
            TIM.IsTurnedOn
         && IsChannelConfigured(Channel)
            )
    {
        value = oC_TIM_LLD_GetValue( Channel );
    }

    return value;
}

//==========================================================================================================================================
/**
 * The match interrupt occurs, when current timer value is equal to the match value. This function helps to turn on this interrupt.
 *
 * @param Channel       Number of configured channel
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_TurnOnMatchInterrupt(
                             IN     oC_TIM_Channel_t Channel
                             )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_TurnOnMatchInterrupt( Channel );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The match interrupt occurs, when current timer value is equal to the match value. This function helps to turn off this interrupt.
 *
 * @param Channel       Number of configured channel
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_TurnOffMatchInterrupt(
                              IN    oC_TIM_Channel_t Channel
                              )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_TurnOffMatchInterrupt( Channel );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The timeout interrupt occurs, when current timer value is equal to the maximum value. This function helps to turn on this interrupt.
 *
 * @param Channel       Number of configured channel
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_TurnOnTimeoutInterrupt(
                               IN   oC_TIM_Channel_t Channel
                               )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_TurnOnTimeoutInterrupt( Channel );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The timeout interrupt occurs, when current timer value is equal to the maximum value. This function helps to turn off this interrupt.
 *
 * @param Channel       Number of configured channel
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t
oC_TIM_TurnOffTimeoutInterrupt(
                                IN  oC_TIM_Channel_t Channel
                                )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , TIM.IsTurnedOn ?                            oC_ErrorCode_None : oC_ErrorCode_DriverNotTurnedOn )
         && oC_AssignErrorCode( &errorCode , Channel < oC_TIM_Channel_NumberOfChannels ? oC_ErrorCode_None : oC_ErrorCode_WrongChannel )
         && oC_AssignErrorCode( &errorCode , TIM.ChannelConfigured[Channel] ?            oC_ErrorCode_None : oC_ErrorCode_TIMChannelNotConfigured )
            )
    {
        errorCode = oC_TIM_LLD_TurnOffTimeoutInterrupt( Channel );
    }

    return errorCode;
}

/** @} */
/* END OF SECTION */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with local functions definitions.
 */
#define _________________________________________LOCAL_FUNCTIONS_DEFINITIONS_SECTION________________________________________________________

//==========================================================================================================================================
/**
 * Checks if the configuration is correct
 *
 * @param Configuration     Reference to the configuration structure which should be checked
 *
 * @return code of error if wrong, or oC_ErrorCode_None if OK
 */
//==========================================================================================================================================
static oC_ErrorCode_t
CheckConfiguration(
                    IN  const oC_TIM_Config_t * Configuration
                    )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , oC_OS_IsAddressCorrect( Configuration ) ? oC_ErrorCode_None : oC_ErrorCode_WrongAddress )
            )
    {
        if (
                Configuration->Channel < oC_TIM_Channel_NumberOfChannels
             || Configuration->Channel == oC_TIM_Channel_FirstAvailable
             )
        {
            errorCode   = oC_ErrorCode_None;
        }
        else
        {
            errorCode   = oC_ErrorCode_WrongChannel;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Finds free channel for configuration or return set channel if it is already set in the configuration
 *
 * @param Configuration     Configuration pointer to check
 * @param outChannel        [out] Destination for the channel variable
 *
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t
FindFreeChannelForConfiguration(
                                 IN     const   oC_TIM_Config_t *   Configuration ,
                                 OUT            oC_TIM_Channel_t *  outChannel
                                 )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Configuration->Channel < oC_TIM_Channel_NumberOfChannels )
    {
        *outChannel = Configuration->Channel;
        errorCode   = oC_ErrorCode_None;
    }
    else if ( Configuration->Channel == oC_TIM_Channel_FirstAvailable )
    {
        errorCode   = oC_ErrorCode_TIMNoneOfChannelsAvailable;

        foreach_channel( channel )
        {
            if (
                        IsChannelLocked(channel) == false
                    &&  oC_TIM_LLD_IsConfigurationPossible( channel , Configuration )
                )
            {
                *outChannel = channel;
                errorCode   = oC_ErrorCode_None;
                break;
            }
        }
    }
    else
    {
        errorCode   = oC_ErrorCode_WrongChannel;
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * Locks the channel if it is already unlocked. The locked channel cannot be configured, unless it will be unlocked.
 *
 * @param Channel       Channel to lock
 *
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t
LockChannel(
             IN     oC_TIM_Channel_t Channel
             )
{
    oC_ErrorCode_t errorCode;

    if ( TIM.ChannelLockFlags[Channel] == false )
    {
        TIM.ChannelLockFlags[Channel]   = true;
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_TIMLockingLockedChannel;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Unlocks the channel if already locked. The unlocked channel can be configured. It checks if channel was locked before, and if it wasn't,
 * then the function returns code of error.
 *
 * @param Channel       Channel to unlock
 *
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t
UnlockChannel(
               IN   oC_TIM_Channel_t Channel
               )
{
    oC_ErrorCode_t errorCode;

    if ( TIM.ChannelLockFlags[Channel] == true )
    {
        TIM.ChannelLockFlags[Channel]   = false;
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_TIMUnlockingUnlockedChannel;
    }

    return errorCode;

}

//==========================================================================================================================================
/**
 * Checks if the channel is locked
 *
 * @param Channel   Channel to check
 *
 * @return true if locked
 */
//==========================================================================================================================================
static inline bool
IsChannelLocked(
                 IN oC_TIM_Channel_t Channel
                 )
{
    return TIM.ChannelLockFlags[Channel];
}

//==========================================================================================================================================
/**
 * The function sets a channel as configured
 *
 * @param Channel       Number of channel (it must be lower than #oC_TIM_Channel_NumberOfChannels)
 */
//==========================================================================================================================================
static inline void
SetChannelConfigured(
                      IN          oC_TIM_Channel_t  Channel ,
                      IN    const oC_TIM_Config_t * Configuration
                      )
{
    TIM.ChannelConfigured[Channel]  = true;
    TIM.Configurations[Channel]     = Configuration;
}

//==========================================================================================================================================
/**
 * The functions sets a channel as unconfigured
 *
 * @param Channel       Number of channel (it must be lower than #oC_TIM_Channel_NumberOfChannels
 */
//==========================================================================================================================================
static inline void
SetChannelUnconfigured(
                        IN  oC_TIM_Channel_t Channel
                        )
{
    TIM.ChannelConfigured[Channel]  = false;
    TIM.Configurations[Channel]     = NULL;
}

//==========================================================================================================================================
/**
 * The function sets match interrupt without checking if interrupt address is correct
 *
 * @param Channel           Channel to set interrupt function
 * @param Interrupt         Pointer to the interrupt handler function
 * @param Context           User-context for callback
 */
//==========================================================================================================================================
static inline void
SetMatchCallback(
                   IN   oC_TIM_Channel_t    Channel ,
                   IN   oC_TIM_Callback_t   Callback ,
                   IN   void *              Context
                   )
{
    TIM.MatchCallbacks[Channel]    = Callback;
    TIM.MatchContexts[Channel]     = Context;
}

//==========================================================================================================================================
/**
 *  The function sets timeout interrupt without checking if interrupt address is correct
 *
 * @param Channel           Channel to set interrupt function
 * @param Interrupt         Pointer to the interrupt handler function
 * @param Context           User-context for callback
 */
//==========================================================================================================================================
static inline void
SetTimeoutCallback(
                     IN     oC_TIM_Channel_t    Channel ,
                     IN     oC_TIM_Callback_t   Callback ,
                     IN     void *              Context
                     )
{
    TIM.TimeoutCallbacks[Channel]  = Callback;
    TIM.TimeoutContexts[Channel]   = Context;
}

//==========================================================================================================================================
/**
 * Configures callback for match interrupt on the given channel.
 *
 * @param Channel       Number of channel
 * @param Callback      Pointer to the function which should be called, when the interrupt occurs
 * @param Context       User-context for the callback
 *
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t
ConfigureMatchCallback(
                        IN  oC_TIM_Channel_t    Channel ,
                        IN  oC_TIM_Callback_t   Callback ,
                        IN  void *              Context
                        )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Callback == NULL
            ||
                (
                        oC_AssignErrorCode( &errorCode , oC_OS_IsROMAddress( Callback ) ?    oC_ErrorCode_None : oC_ErrorCode_TIMMatchCallbackNotCorrect )
                     && oC_AssignErrorCode( &errorCode , oC_TIM_LLD_TurnOnMatchInterrupt( Channel ) )
                    )
            )
    {

        SetMatchCallback( Channel , Callback , Context );
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Configures callback for timeout interrupt on the given channel.
 *
 * @param Channel       Number of channel
 * @param Callback      Pointer to the function which should be called, when the interrupt occurs
 * @param Context       User-context for the callback
 *
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t
ConfigureTimeoutCallback(
                          IN    oC_TIM_Channel_t    Channel ,
                          IN    oC_TIM_Callback_t   Callback ,
                          IN    void *              Context
                          )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Callback == NULL
                ||
                (
                        oC_AssignErrorCode( &errorCode , oC_OS_IsROMAddress( Callback ) ?    oC_ErrorCode_None : oC_ErrorCode_TIMMatchCallbackNotCorrect )
                     && oC_AssignErrorCode( &errorCode , oC_TIM_LLD_TurnOnTimeoutInterrupt( Channel ) )
                     )
            )
    {
        SetTimeoutCallback( Channel , Callback , Context );
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Checks if the channel is configured
 *
 * @param Channel       number of the channel
 *
 * @return true if configured
 */
//==========================================================================================================================================
static inline bool
IsChannelConfigured(
                     IN oC_TIM_Channel_t Channel
                     )
{
    return TIM.ChannelConfigured[Channel];
}

/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTIONS_DEFINITIONS_SECTION________________________________________________________

/** ****************************************************************************************************************************************
 * The section contains definition of interrupt handlers
 */
#define _________________________________________INTERRUPT_HANDLERS_SECTION_________________________________________________________________

//==========================================================================================================================================
/**
 * The handler of the interrupt, that occurs, when the timer value is equal to the match value
 *
 * @param Channel       Channel of the interrupt
 *
 * @return flag if task was woken during this interrupt
 */
//==========================================================================================================================================
bool oC_TIM_INTHandler_MatchInterrupt( oC_TIM_Channel_t Channel )
{
    bool taskWoken = false;

    if ( oC_OS_IsROMAddress(TIM.MatchCallbacks[Channel]) )
    {
        taskWoken = TIM.MatchCallbacks[Channel]( TIM.MatchContexts[Channel] );
    }

    return taskWoken;
}

//==========================================================================================================================================
/**
 * The handler of the interrupt, that occurs, when the timer value is equal to the 0 value
 *
 * @param Channel       Channel of the interrupt
 *
 * @return flag if task was woken during this interrupt
 */
//==========================================================================================================================================
bool oC_TIM_INTHandler_TimeoutInterrupt( oC_TIM_Channel_t Channel )
{
    bool taskWoken = false;

    if ( oC_OS_IsROMAddress(TIM.TimeoutCallbacks[Channel]) )
    {
        taskWoken = TIM.TimeoutCallbacks[Channel]( TIM.TimeoutContexts[Channel] );
    }

    return taskWoken;
}

/* END OF SECTION */
#undef  _________________________________________INTERRUPT_HANDLERS_SECTION_________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with driver definition
 */
#define _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________

static const oC_Driver_Interface_t Interface = {
                                                .RequiredBootLevel  = oC_Boot_Level_DontRequireAnything ,
                                                .TurnOnFunction     = oC_TIM_TurnOnDriver ,
                                                .TurnOffFunction    = oC_TIM_TurnOffDriver ,
                                                .IsTurnedOnFunction = oC_TIM_IsTurnedOn ,
                                                .ConfigureFunction  = oC_TIM_Configure ,
                                                .UnconfigureFunction= oC_TIM_Unconfigure
};

oC_DRIVER_DEFINE(TIM , "tim" , VERSION , Interface );

/* END OF SECTION */
#undef  _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________
