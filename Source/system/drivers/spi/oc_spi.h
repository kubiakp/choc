/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_spi.h
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 1 gru 2014 17:19:12) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_DRIVERS_SPI_OC_SPI_H_
#define SYSTEM_DRIVERS_SPI_OC_SPI_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_spi_defs.h>
#include <oc_gen_macros.h>
#include <oc_gpio.h>
#include <oc_driver.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Macro for adding channel to the enum list of channels
 */
//==========================================================================================================================================
#define oC_SPI_ADD_CHANNEL_TO_ENUM( CHANNEL_NAME )                  oC_CAT_2( oC_SPI_Channel_ , CHANNEL_NAME ) ,

//==========================================================================================================================================
/**
 * Macro for adding pin to pins definitions
 */
//==========================================================================================================================================
#define oC_SPI_ADD_PIN_TO_PINS( CHANNEL_NAME , SIGNAL_NAME , PIN_NAME , ... )   extern const oC_GPIO_PinLink_t * oC_CAT_6(oC_GPIO_PinLink_, CHANNEL_NAME , _ , SIGNAL_NAME , _ , PIN_NAME );

/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/


//==========================================================================================================================================
/**
 * List of the channels available in the micro-controller.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_ChannelsList(oC_SPI_ADD_CHANNEL_TO_ENUM)//!< oC_SPI_ChannelsList
    oC_SPI_Channel_NumberOfChannels
} oC_SPI_Channel_t;

//==========================================================================================================================================
/**
 * Order of bits in the transmission
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_BitOrder_MSBFirst ,                      /**< Most significant bit first */
    oC_SPI_BitOrder_LSBFirst                        /**< Least significant bit first */
} oC_SPI_BitOrder_t;

//==========================================================================================================================================
/**
 * The type represents state of the transmission
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_TransmissionState_Initialized ,          /**< Transmission is initialized (not in progress) */
    oC_SPI_TransmissionState_InProgress ,           /**< Transmission is in progress */
    oC_SPI_TransmissionState_Finished ,             /**< Transmit data finished */
    oC_SPI_TransmissionState_Timeout ,              /**< Time for transmission expired */
    oC_SPI_TransmissionState_Error ,                /**< Error while transmission */
} oC_SPI_TransmissionState_t;

//==========================================================================================================================================
/**
 * The type represents number of bits send in the frame
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_FrameWidth_NotConfigured ,
    oC_SPI_FrameWidth_1Bit ,
    oC_SPI_FrameWidth_2Bits ,
    oC_SPI_FrameWidth_3Bits ,
    oC_SPI_FrameWidth_4Bits ,
    oC_SPI_FrameWidth_5Bits ,
    oC_SPI_FrameWidth_6Bits ,
    oC_SPI_FrameWidth_7Bits ,
    oC_SPI_FrameWidth_8Bits ,
    oC_SPI_FrameWidth_9Bits ,
    oC_SPI_FrameWidth_10Bits ,
    oC_SPI_FrameWidth_11Bits ,
    oC_SPI_FrameWidth_12Bits ,
    oC_SPI_FrameWidth_13Bits ,
    oC_SPI_FrameWidth_14Bits ,
    oC_SPI_FrameWidth_15Bits ,
    oC_SPI_FrameWidth_16Bits ,
    oC_SPI_FrameWidth_32Bits
} oC_SPI_FrameWidth_t;

//==========================================================================================================================================
/**
 * The type represents phase of signal
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_Phase_FirstEdge ,                        /**< Data is captured on the first clock edge of transition */
    oC_SPI_Phase_SecondEdge                         /**< Data is captured on the second clock edge of transition */
} oC_SPI_Phase_t;

//==========================================================================================================================================
/**
 * Polarity of the signal
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_Polarity_HighWhenActive ,                /**< Signal is active high */
    oC_SPI_Polarity_LowWhenActive                   /**< Signal is active low */
} oC_SPI_Polarity_t;

//==========================================================================================================================================
/*
 *  redeclared types
 */
//==========================================================================================================================================
typedef uint32_t oC_SPI_Data_t;                     /**< Type for storing data of the transmission */
typedef size_t oC_SPI_Size_t;                       /**< Size of the buffers */

//==========================================================================================================================================
/*
 * Structures
 */
//==========================================================================================================================================


//==========================================================================================================================================
/**
 * Connections of the pins
 */
//==========================================================================================================================================
typedef struct
{
    const oC_GPIO_PinLink_t *   Clk;
    const oC_GPIO_PinLink_t *   Tx;
    const oC_GPIO_PinLink_t *   Rx;
    const oC_GPIO_PinLink_t *   CS;
    oC_GPIO_Protection_t        Protection;
} oC_SPI_Pins_t;

//==========================================================================================================================================
/**
 * Configuration of the SPI
 */
//==========================================================================================================================================
typedef struct
{
    oC_SPI_Channel_t            Channel;
    oC_SPI_Pins_t               Pins;
    oC_SPI_BitOrder_t           BitOrder;
    oC_SPI_FrameWidth_t         FrameWidth;
    oC_SPI_Phase_t              ClockPhase;
    oC_SPI_Polarity_t           ClockPolarity;
    oC_SPI_Polarity_t           CSPolarity;
    oC_SPI_Data_t               DummyMessage;
    oC_Frequency_t              Frequency;
} oC_SPI_Config_t;

//==========================================================================================================================================
/**
 * Context of the SPI driver
 */
//==========================================================================================================================================
typedef struct
{
    void *                      Self;
    const oC_SPI_Config_t *     Config;             /**< Configuration of the SPI */
    oC_SPI_Data_t*              TxBuffer;
    oC_SPI_Data_t*              RxBuffer;
    oC_SPI_Size_t               Length;
} oC_SPI_Context_t;

typedef oC_SPI_Config_t oC_SPI_StreamConfig_t;
/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/

oC_SPI_PinsList(oC_SPI_ADD_PIN_TO_PINS)

/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern bool oC_SPI_Configure( const oC_SPI_Config_t * Config );
extern int  oC_SPI_SendViaStream( void * Context , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern int  oC_SPI_ReceiveViaStream( void * Context , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern bool oC_SPI_ConfigureAsStream( oC_SPI_StreamConfig_t const * const Config , void * Context );
extern bool oC_SPI_WaitForChannelReady( oC_SPI_Channel_t Channel , uint32_t Timeout_ms );
extern bool oC_SPI_Transceive( oC_SPI_Context_t * Context , uint32_t Timeout_ms);
extern bool oC_SPI_IsChannelConfigured( oC_SPI_Channel_t Channel );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function checks if the channel is correct.
 *
 * @param Channel       Channel to check
 *
 * @return true if correct
 */
//==========================================================================================================================================
static inline bool oC_SPI_IsChannelCorrect( oC_SPI_Channel_t Channel )
{
    return Channel < oC_SPI_Channel_NumberOfChannels;
}

//==========================================================================================================================================
/**
 * The function checks if context of the module is correctly initialized.
 *
 * @param Context       Reference to check
 *
 * @return true if correct
 */
//==========================================================================================================================================
static inline bool oC_SPI_IsContextCorrect( oC_SPI_Context_t * Context )
{
    return Context && Context->Self == Context;
}

//==========================================================================================================================================
/**
 * Prepares context for transmisssion
 *
 * @param Context               Reference to a context structure
 * @param Config                Reference to a configuration structure
 * @param TxBuffer              Reference to a Tx buffer
 * @param RxBuffer              Reference to a Rx buffer
 * @param Length                Length of the buffers
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool oC_SPI_PrepareContext( oC_SPI_Context_t * Context , const oC_SPI_Config_t * Config , oC_SPI_Data_t * TxBuffer , oC_SPI_Data_t * RxBuffer , oC_SPI_Size_t Length )
{
    bool result = false;

    if ( Context && Config && TxBuffer && RxBuffer && Length > 0 )
    {
        Context->Self       = Context;
        Context->Config     = Config;
        Context->Length     = Length;
        Context->TxBuffer   = TxBuffer;
        Context->RxBuffer   = RxBuffer;

        result = true;
    }

    return result;
}

oC_DRIVER_DECLARE(SPI);

#endif /* SYSTEM_DRIVERS_SPI_OC_SPI_H_ */
