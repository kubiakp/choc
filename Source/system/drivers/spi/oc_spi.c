/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_spi.c
 *
 *    @brief      The file with driver functionality
 *
 *    @author     Patryk Kubiak - (Created on: 1 gru 2014 17:20:57) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_spi.h>
#include <oc_spi_lld.h>
#include <oc_drivers_cfg.h>
#include <oc_tranh.h>
#include <oc_assert.h>

/*==========================================================================================================================================
//
//     LLD PROTOTYPES
//
//========================================================================================================================================*/

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/

#define VERSION oC_DRIVER_VERSION(2,0,0,0)

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

typedef struct
{
    oC_SPI_Channel_t        Channel;
} _ChannelContext_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define _ADD_CHANNEL_TO_CHANNEL_CONTEXT_LIST( CHANNEL_NAME )            { oC_CAT_2( oC_SPI_Channel_ , CHANNEL_NAME ) } ,

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static        bool              _PrepareChannel          ( oC_SPI_Channel_t Channel );
static        void              _ReleaseChannel          ( oC_SPI_Channel_t Channel );

/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Container for the driver
 */
//==========================================================================================================================================
static struct
{
    oC_TRANH_t *            TransmissionHandler[ oC_SPI_Channel_NumberOfChannels ];
} SPI;

//==========================================================================================================================================
/**
 * Configuration of the driver for transmission handler
 */
//==========================================================================================================================================
static const oC_TRANH_Config_t TransmissionConfig = {
                                              .Send   = (oC_TRANH_Send_t)   oC_SPI_LLD_Send ,
                                              .Receive= (oC_TRANH_Receive_t)oC_SPI_LLD_Receive ,
};

//==========================================================================================================================================
/**
 * contexts for transmission handler
 */
//==========================================================================================================================================
static const _ChannelContext_t ChannelContextes[]    = {
                                               oC_SPI_ChannelsList( _ADD_CHANNEL_TO_CHANNEL_CONTEXT_LIST )
                                               { 0 }
};

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 *  Function called before system starts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_TurnOnDriver( void )
{
    oC_SPI_LLD_TurnOnDriver();

    for(int channel = 0 ; channel < oC_SPI_Channel_NumberOfChannels ; channel++)
    {
        SPI.TransmissionHandler[channel]   = NULL;
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 *  Function called before system restarts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_TurnOffDriver( void )
{
    oC_SPI_LLD_TurnOffDriver();

    for(int channel = 0 ; channel < oC_SPI_Channel_NumberOfChannels ; channel++)
    {
        if ( SPI.TransmissionHandler[channel] )
        {
            oC_TRANH_Delete( &SPI.TransmissionHandler[channel] );
        }
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * The function for configuring the driver.
 *
 * @param Config        Reference to the configuration structure
 *
 * @return true if configuration success, false if error
 */
//==========================================================================================================================================
bool oC_SPI_Configure( oC_SPI_Config_t const * const Config )
{
    bool result = false;

    if (
            oC_SPI_LLD_Configure( Config )
        &&  oC_SPI_LLD_ConfigurePins( Config )
        &&  _PrepareChannel( Config->Channel )
       )
    {
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function configures driver to work as a stream
 *
 * @param Config        Reference to the configuration structure
 * @param Context       Reference to context of the driver
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_SPI_ConfigureAsStream( oC_SPI_StreamConfig_t const * const Config , void * Context )
{
    return oC_SPI_Configure( Config ) && oC_SPI_PrepareContext(Context , Config , NULL , NULL , 0 );
}

//==========================================================================================================================================
/**
 * The function to send data via the driver as a stream
 *
 * @param Context       Reference to the context of the driver
 * @param Data          Reference to the buffer to receive
 * @param Length        Size of the buffer to receive (in bytes)
 * @param Timeout_ms    Maximum time in ms for request
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
int oC_SPI_SendViaStream( void * Context , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int sent_bytes = 0;

    if ( oC_SPI_IsContextCorrect( Context ) )
    {
        oC_SPI_Context_t * context = Context;
        oC_SPI_Data_t * old_tx = context->TxBuffer;
        oC_SPI_Data_t * old_rx = context->RxBuffer;
        oC_SPI_Data_t * tx = malloc( sizeof(oC_SPI_Data_t) * Length );
        oC_SPI_Data_t * rx = malloc( sizeof(oC_SPI_Data_t) * Length );

        if( tx && rx)
        {

            for(oC_SPI_Size_t i = 0 ; i < Length ; i++ )
            {
                tx[i]   = Data[i];
            }

            context->TxBuffer       = tx;
            context->RxBuffer       = rx;
            context->Length         = Length;

            if ( oC_SPI_Transceive(context , Timeout_ms) )
            {
                sent_bytes  = Length;

                for(oC_SPI_Size_t i = 0 ; i < Length ; i++ )
                {
                    Data[i]     = rx[i];
                }
            }
        }

        context->RxBuffer   = old_rx;
        context->TxBuffer   = old_tx;

        if ( tx ) free(tx);
        if ( rx ) free(rx);
    }

    return sent_bytes;
}

//==========================================================================================================================================
/**
 * The function to receive data via the driver as a stream
 *
 * @param Context       Reference to the context of the driver
 * @param Data          Reference to the buffer to send
 * @param Length        Size of the buffer to send (in bytes)
 * @param Timeout_ms    Maximum time in ms for request
 *
 * @return number of received bytes
 */
//==========================================================================================================================================
int oC_SPI_ReceiveViaStream( void * Context , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int received_bytes = 0;

    if ( oC_SPI_IsContextCorrect( Context ) )
    {
        oC_SPI_Context_t * context = Context;
        oC_SPI_Data_t * old_tx = context->TxBuffer;
        oC_SPI_Data_t * old_rx = context->RxBuffer;
        oC_SPI_Data_t * tx = malloc( sizeof(oC_SPI_Data_t) * Length );
        oC_SPI_Data_t * rx = malloc( sizeof(oC_SPI_Data_t) * Length );

        if( tx && rx)
        {

            for(oC_SPI_Size_t i = 0 ; i < Length ; i++ )
            {
                tx[i]   = context->Config->DummyMessage;
            }

            context->TxBuffer       = tx;
            context->RxBuffer       = rx;
            context->Length         = Length;

            if ( oC_SPI_Transceive(context , Timeout_ms) )
            {
                received_bytes  = Length;

                for(oC_SPI_Size_t i = 0 ; i < Length ; i++ )
                {
                    Data[i]     = rx[i];
                }
            }
        }

        context->RxBuffer   = old_rx;
        context->TxBuffer   = old_tx;

        if ( tx ) free(tx);
        if ( rx ) free(rx);
    }

    return received_bytes;
}

//==========================================================================================================================================
/**
 * Waits for channel ready
 *
 * @param Channel       Channel to wait
 * @param Timeout_ms    Maximum time in ms to wait for the channel
 *
 * @return false if channel is not configured or timeout
 */
//==========================================================================================================================================
bool oC_SPI_WaitForChannelReady( oC_SPI_Channel_t Channel , uint32_t Timeout_ms )
{
    bool result = false;

    if ( oC_SPI_IsChannelConfigured( Channel ) )
    {
        result = oC_TRANH_WaitForSendFinished( SPI.TransmissionHandler[Channel] , oC_CONVERT_TIME_MS_TO_TICKS(Timeout_ms));
    }

    return result;
}

//==========================================================================================================================================
/**
 * Send and receive data via the driver. It is needed to call #oC_SPI_PrepareContext function before.
 *
 * @param Context       Reference to the context of the driver
 * @param Timeout_ms    Maximum time in ms for request
 *
 * @return true if success, false if timeout
 */
//==========================================================================================================================================
bool oC_SPI_Transceive( oC_SPI_Context_t * Context , uint32_t Timeout_ms)
{
    bool result = false;

    if( oC_SPI_IsContextCorrect( Context ) )
    {
        oC_TickType_t timeout = oC_CONVERT_TIME_MS_TO_TICKS(Timeout_ms);


        if (    oC_TRANH_LockSend( SPI.TransmissionHandler[Context->Config->Channel] , timeout )
             && oC_TRANH_LockReceive( SPI.TransmissionHandler[Context->Config->Channel] , timeout)
                )
        {

            if (  oC_SPI_LLD_RestoreConfig( Context->Config )
               || oC_SPI_LLD_Configure(Context->Config) )
            {
                oC_SPI_LLD_SetCSActive( Context->Config );

                result =    oC_TRANH_Receive(SPI.TransmissionHandler[Context->Config->Channel]  , (uint8_t*)Context->RxBuffer  , Context->Length * sizeof(oC_SPI_Data_t) )
                         && oC_TRANH_Send( SPI.TransmissionHandler[Context->Config->Channel]    , (uint8_t*)Context->TxBuffer  , Context->Length * sizeof(oC_SPI_Data_t) )
                         && oC_TRANH_WaitForReceiveFinished( SPI.TransmissionHandler[Context->Config->Channel] , timeout);

                oC_SPI_LLD_SetCSNotActive( Context->Config );
            }
            else
            {
                oC_TRANH_UnlockSend(SPI.TransmissionHandler[Context->Config->Channel]);
                oC_TRANH_UnlockReceive(SPI.TransmissionHandler[Context->Config->Channel]);
            }
        }

    }

    return result;
}

//==========================================================================================================================================
/**
 * Checks if channel was configured
 *
 * @param Channel       Channel to check
 *
 * @return true if channel was already configured
 */
//==========================================================================================================================================
bool oC_SPI_IsChannelConfigured( oC_SPI_Channel_t Channel )
{
    oC_ASSERT( oC_SPI_IsChannelCorrect(Channel) );  // Channel is not correct!

    return SPI.TransmissionHandler[Channel]  != NULL;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Prepares channel to work if it is not configured before
 *
 * @param Channel       Channel to prepare
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _PrepareChannel( oC_SPI_Channel_t Channel )
{
    bool result = false;

    if ( oC_SPI_IsChannelConfigured(Channel) )
    {
        result = true;
    }
    else
    {
        SPI.TransmissionHandler[Channel]  = oC_TRANH_New( &TransmissionConfig  );

        if( oC_TRANH_IsContextCorrect( SPI.TransmissionHandler[Channel] ) )
        {
            oC_TRANH_SetParameter( SPI.TransmissionHandler[Channel] , &ChannelContextes[Channel] );
            result = true;
        }
    }

    if ( !result )
    {
        _ReleaseChannel(Channel);
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function releases channel (deinitializes it)
 *
 * @param Channel       Channel to release
 */
//==========================================================================================================================================
static void _ReleaseChannel( oC_SPI_Channel_t Channel )
{
    if ( SPI.TransmissionHandler[Channel]  )
    {
        oC_TRANH_Delete( &SPI.TransmissionHandler[Channel] );
    }
}

/*==========================================================================================================================================
//
//     INTERRUPTS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Function called when transmission is finished
 *
 * @param Channel       Channel of the finished transmission
 *
 * @return higherPriorityTaskWoken true if task should be yield
 */
//==========================================================================================================================================
bool oC_SPI_INT_TransmissionFinished( oC_SPI_Channel_t Channel )
{
    bool taskWoken = false;

    oC_TRANH_HandleRx( SPI.TransmissionHandler[Channel]  , &taskWoken );
    oC_TRANH_HandleTx( SPI.TransmissionHandler[Channel]  , &taskWoken );

    return taskWoken;
}


/** ****************************************************************************************************************************************
 * The section with driver definition
 */
#define _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________

static const oC_Driver_Interface_t Interface = {
                                                .RequiredBootLevel          = oC_Boot_Level_DontRequireAnything  ,
                                                .TurnOnFunction             = oC_SPI_TurnOnDriver ,
                                                .TurnOffFunction            = oC_SPI_TurnOffDriver ,
                                                .ConfigureFunction          = oC_SPI_Configure ,
                                                .SendViaStreamFunction      = oC_SPI_SendViaStream ,
                                                .ReceiveViaStreamFunction   = oC_SPI_ReceiveViaStream
};

oC_DRIVER_DEFINE(SPI , "spi" , VERSION , Interface);

/* END OF SECTION */
#undef  _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________
