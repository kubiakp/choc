/** ****************************************************************************************************************************************
 *
 * @file       oc_driver.h
 *
 * @brief      Contains definitions for creating driver.
 *
 * @author     Patryk Kubiak - (Created on: 26 mar 2015 19:54:24) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @page Drivers Drivers
 *
 * @section intro Introduction
 *
 * @par
 * The driver is software component, that helps handle some device connected to the uC. It is true, but it is not all functionality of it,
 * because it also helps using a micro-controller peripherials. Drivers can use each others, thanks to that the configuration is limited to
 * set only really necessary parameters. For example, if you are using SPI driver, you mustn't use GPIO driver to configure the pins -
 * you just set the pins reference and SPI driver takes care of the rest. Moreover configuration of the drivers is independent of the selected uC.
 * You do not need to know what kind of uC is currently in use, and how to initialize a peripherials in it. If you will choose some functions,
 * that are not handled in selected uC, the configuration function will return you code of error.
 *
 * @par
 * Drivers has defined their own space, which is named **Drivers Space**. It is hardware independent space, so all operations that are
 * micro-controller specific are moved to the **Portable Space**. Drivers can use definitions and functions from that space, but only if
 * the function is not hardware specific, so it must be defined in each micro-controller (also if it is not handled - then it should be defined
 * as empty call). The driver space should provide an interface for the core space to handle the peripheral or device, and contain each operations
 * that are common in each uC.
 *
 * @par
 * Drivers are managed by the **Drivers Manager** that is defined in the oc_drivers.h file. Each driver is stored in a separate directory
 * placed in the system/drivers/ directory, that is named as the lowercase version of the driver (for example system/drivers/gpio folder contains
 * files for GPIO driver). Drivers can be used from the **Core Space** and from other drivers to provide the simplest way to use a handled
 * device. The main purpose of drivers is to maximum simplify devices handling without loosing its functionality (if it is possible).
 *
 * @par
 * Each driver must be turned on before usage. There are few ways to provide it. The first and the best is to add the driver to default turned
 * drivers list in the oc_drivers_cfg.h file. It will be performed during booting of the system as soon, as it is possible. The second way
 * is turning it manually by using turning on function in a **Core Space** program. Remember that when you are choosing this way,
 * you cannot use this driver until the program will perform this action - that's why it is not recommended way. The last possibility is
 * to register driver in the oc_drivers_cfg.h file and then it will be turned on automatically, when some other driver will need to use it.
 *
 * @section interface Interface
 *
 * @par
 * Each driver should provide at least functions for configure, unconfigure, and check if driver is turned on consistent with types #oC_Driver_ConfigureFunction_t,
 * #oC_Driver_UnconfigureFunction_t and #oC_Driver_IsTurnedOnFunction_t. 'At least' means, that if the driver do not require any actions before configuration, it must not
 * contain function to turn on it, but it must inform other drivers, that it is already turned on - so the IsTurnedOnFunction will be required.
 * Full description of the basic driver interface you can find in the #oC_Driver_Interface_t structure definition.
 *
 * @par
 * Each driver should be able to configure it by basic interface configuration function. This function must be declared in a main driver header
 * (for example in the oc_gpio.h) file with consistent of #oC_Driver_ConfigureFunction_t type, where the Configuration argument should be
 * given in type **oC_[DRIVER_NAME]_Config_t** (where DRIVER_NAME is short name of the driver, for example GPIO). The structure should contains
 * all definitions that are needed to configure the driver, and it should be hardware independent. If some of functionality is not handled on
 * the target machine, then the portable layer should inform about it using error codes.
 *
 * @warning
 * Drivers cannot use any print operations - if it is needed to inform about some problems, it should return an error code. See #oC_ErrorCode_t
 * description for more informations.
 *
 * @par
 * Despite of basic interface functions the driver can also contain additional definitions, that helps to handle a device properly. These
 * functions will be able to use in the **Core Space** layer. Note, that each function interface must be defined in the **main driver header**
 * file with extern keyword.
 *
 * @par Example:
 * ~~~~~~~~~~~~~{.c}
 * extern bool oC_GPIO_SetData( oC_GPIO_PinLink_t Pin , oC_GPIO_Pins_t Pins );
 * ~~~~~~~~~~~~~
 *
 * @section createDriver Create own driver
 *
 * @par
 * If you want to create your own driver, you must follow the rules. The basic template for the driver you can create using the @subpage drivergen
 * tool which is placed in the tools directory. If you dont want to use the tool, then you can fill files contents by yourself.
 *
 * @note Chapter below you can skip, if you use the #drivergen.
 *
 * @par
 * The first of all you should create following files:
 *
 *  - **oc_[driver_name].h** - *Main driver header with all definitions needed to use this driver. It must contains all interface types, and
 *  function prototypes that are needed to use the driver. This file should include the oc_driver.h header.*
 *  - **oc_[driver_name].c** - *Main file with sources of the interface functions.*
 *
 * @par
 * The main header of the driver must contains declaration of the driver by using macro #oC_DRIVER_DECLARE. Here you have an example:
 *
 * ~~~~~~~~~~~~~{.c}
 * oC_DRIVER_DECLARE( GPIO );
 * ~~~~~~~~~~~~~
 *
 * @par
 * The interface source file (**oc_[driver_name].c**) must contains definition of the driver interface, file name, and version. To do it, you
 * must to create constant definition of the #oC_Driver_Interface_t type (see the type description for information how to fill it), and then
 * use a macro #oC_DRIVER_DEFINE to relate the interface structure with the driver. Optionally you can add define of the driver version using
 * #oC_DRIVER_VERSION macro. You can find example below:
 *
 * ~~~~~~~~~~~~~{.c}
 * static const oC_Driver_Interface_t Interface = {
 *      .ConfigureFunction      = oC_GPIO_Configure ,
 *      .UnconfigureFunction    = oC_GPIO_Unconfigure ,
 *      .IsTurnedOnFunction     = oC_GPIO_IsTurnedOn ,
 *      .TurnOnFunction         = oC_GPIO_TurnOn ,
 *      .TurnOffFunction        = oC_GPIO_TurnOff ,
 *      .RequiredBootLevel      = oC_Boot_Level_DontRequireAnything
 * };
 *
 * #define VERSION oC_DRIVER_VERSION(0,0,0,1)
 *
 * oC_DRIVER_DEFINE( GPIO , "gpio-dev" , VERSION , Interface );
 * ~~~~~~~~~~~~~
 *
 * @section list Drivers List
 *
 *  - @subpage ONEWIRE
 *  - @subpage A4982
 *  - @subpage ACOM
 *  - @subpage ADC
 *  - @subpage BLUET
 *  - @subpage CAN
 *  - @subpage DAC
 *  - @subpage DS18B20
 *  - @subpage ENC
 *  - @subpage ENET
 *  - @subpage EROM
 *  - @subpage GPIO
 *  - @subpage GPRS
 *  - @subpage GSM
 *  - @subpage HD44780
 *  - @subpage I2C
 *  - @subpage LED
 *  - @subpage MKC
 *  - @subpage PCM
 *  - @subpage PS2
 *  - @subpage PWM
 *  - @subpage RFID
 *  - @subpage RTC
 *  - @subpage SDCC
 *  - @subpage SPI
 *  - @subpage SYS
 *  - @subpage TFT
 *  - @subpage TIM
 *  - @subpage TSC
 *  - @subpage UART
 *  - @subpage USB
 *  - @subpage WDG
 *  - @subpage WIFI
 *  - @subpage XBEE
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_DRIVERS_OC_DRIVER_H_
#define SYSTEM_DRIVERS_OC_DRIVER_H_

#include <oc_compiler.h>
#include <oc_boot.h>
#include <oc_errors.h>
#include <stdint.h>
#include <oc_gen_macros.h>

/** ****************************************************************************************************************************************
 * The section with interface types for handling modules
 */
#define _________________________________________INTERFACE_TYPES_SECTION____________________________________________________________________


//==========================================================================================================================================
/**
 * @brief turning on driver function
 *
 * The type for the function, that turns on a driver to initiate variables before calling any other functions. The driver should
 * prevents by itself for returning on driver when it is still turned on. If this function require some additional functionality, you should
 * set **RequiredBootLevel** in the #oC_Driver_Interface_t structure to proper value.
 *
 * @return code of error
 */
//==========================================================================================================================================
typedef oC_ErrorCode_t (*oC_Driver_TurnOnFunction_t)            (void);
//==========================================================================================================================================
/**
 * @brief turning off driver function
 *
 * The type for storing pointer to the function to release all memory allocated by the driver. The driver should prevents by itself for turning
 * off driver that is already turned off.
 *
 * @return code of error
 */
//==========================================================================================================================================
typedef oC_ErrorCode_t (*oC_Driver_TurnOffFunction_t)           (void);
//==========================================================================================================================================
/**
 * @brief configuration driver function
 *
 * The type for storing pointer to the function that configure a driver. It must be defined for each driver and should be prevented by itself
 * from using this function without turning on driver.
 *
 * @param Configuration     Reference to the configuration structure. In driver it should be defined with type **oC_[DRIVER_NAME]_Config_t**
 *                          type. It should contains all settings that are required to set to use a device.
 *
 * @return code of error
 */
//==========================================================================================================================================
typedef oC_ErrorCode_t (*oC_Driver_ConfigureFunction_t)         (const void * Configuration);
//==========================================================================================================================================
/**
 * @brief removes configuration from the driver
 *
 * The type for storing pointer to the function that helps to remove configuration from the driver. It should release all resources that was
 * allocated by the configure function.
 *
 * @param Configuration     Reference to the configuration structure. In driver it should be defined with type **oC_[DRIVER_NAME]_Config_t**
 *                          type. It should contains all settings that are required to set to use a device.
 *
 * @return code of error
 */
//==========================================================================================================================================
typedef oC_ErrorCode_t (*oC_Driver_UnconfigureFunction_t)       (const void * Configuration);
//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * The type for storing pointer to the function that helps to check if the driver is turned on. Each driver must has definition of this function.
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
typedef bool           (*oC_Driver_IsTurnedOnFunction_t)        (void);
//==========================================================================================================================================
/**
 * @brief sends data to the driver
 *
 * The type for storing pointer to the function that sends data using this driver.
 *
 * @param Configuration     Reference to the configuration structure. In driver it should be defined with type **oC_[DRIVER_NAME]_Config_t**
 *                          type. It should contains all settings that are required to set to use a device.
 *
 * @param Data              Data to send
 * @param Length            Length of the data buffer
 * @param Timeout_ms        Maximum time for the operation in ms
 *
 *
 * @return number of sent bytes if success, or -1 if error
 */
//==========================================================================================================================================
typedef int            (*oC_Driver_SendViaStreamFunction_t)     (const void * Configuration , const char * Data , uint32_t Length , uint32_t Timeout_ms);
//==========================================================================================================================================
/**
 * @brief receives data to the driver
 *
 * The type for storing pointer to the function that receives data using this driver.
 *
 * @param Configuration     Reference to the configuration structure. In driver it should be defined with type **oC_[DRIVER_NAME]_Config_t**
 *                          type. It should contains all settings that are required to set to use a device.
 *
 * @param Data              Data to receive
 * @param Length            Length of the data buffer
 * @param Timeout_ms        Maximum time for the operation in ms
 *
 *
 * @return number of received bytes if success, or -1 if error
 */
//==========================================================================================================================================
typedef int            (*oC_Driver_ReceiveViaStreamFunction_t)  (const void * Configuration ,       char * Data , uint32_t Length , uint32_t Timeout_ms);

//==========================================================================================================================================
//==========================================================================================================================================
//typedef oC_ErrorCode_t (*oC_Driver_IoctlFunction_t)             (const void * Config , oC_Ioctl_Command_t Command);

//==========================================================================================================================================
/**
 * @brief Structure with an interface for a driver
 *
 * The structure contains pointers to basic interface driver functions. If some function is not handled, then it should be set as #NULL.
 *
 * @note #RequiredBootLevel must be always filled.
 */
//==========================================================================================================================================
typedef struct
{
    /**
     * @brief level of boot that is required to turn on driver
     *
     * The field defines a boot level, that is required to turn on this driver. It provide, that it will not be turned on, before this level
     * will be not achieved. See #oC_Boot_Level_t for more details.
     */
    oC_Boot_Level_t                         RequiredBootLevel;
    /**
     * @brief function to turn on driver
     *
     * The field contains pointer to the function that turns on the driver. If it is not required to implement this function, you can
     * set it to #NULL.
     */
    oC_Driver_TurnOnFunction_t              TurnOnFunction;
    /**
     * @brief function to turn off driver
     * The field contains pointer to the function that turns off the driver. This function should free all memory, that was allocated by it,
     * and release all stored informations and resources. If it is not required to implement this function, you can
     * set it to #NULL.
     */
    oC_Driver_TurnOffFunction_t             TurnOffFunction;
    /**
     * @brief function to configure driver
     *
     * The field contains pointer to the function that must be always defined. It is main configuration function that will be called to configure the driver.
     */
    oC_Driver_ConfigureFunction_t           ConfigureFunction;
    /**
     * @brief function to remove configuration
     *
     * The field contains pointer to the function to remove configuration from the driver and restore default states instead of it. It must be always defined.
     */
    oC_Driver_UnconfigureFunction_t         UnconfigureFunction;
    /**
     * @brief function to check if driver is turned on
     *
     * The field contains pointer to the function that helps to check if the driver is turned on. If the driver must not has a turn on function,
     * that this function should just always return true, but it must be defined.
     */
    oC_Driver_IsTurnedOnFunction_t          IsTurnedOnFunction;
    /**
     * @brief function to send data to the driver
     *
     * The function is for sending data to the driver as to a stream. If it is not used, then it should be set to #NULL.
     */
    oC_Driver_SendViaStreamFunction_t       SendViaStreamFunction;
    /**
     * @brief function to receive data from the driver
     *
     * The function is for receiving data to the driver as to a stream. If it is not used, then it should be set to #NULL.
     */
    oC_Driver_ReceiveViaStreamFunction_t    ReceiveViaStreamFunction;
} oC_Driver_Interface_t;

//==========================================================================================================================================
/**
 * @brief contains informations about driver
 *
 * The structure must be created as (for most of it as constant) for each driver that is defined in the system. In most of cases it is done
 * by using #oC_DRIVER_DEFINE macro in the main source file.
 */
//==========================================================================================================================================
typedef struct
{
    /**
     * @brief Interface structure for the driver
     *
     * The reference to the structure that contains interface functions pointers for the basic functionality of the driver.
     */
    const oC_Driver_Interface_t *   If;
    /**
     * @brief Name of the driver file
     *
     * The system creates file for each driver that is installed. This is the name of this file. You will be able to find it in the
     * system directory at path: /driv/[DeviceName]
     */
    char *                          DeviceName;
    /**
     * @brief Version of the driver
     *
     * The version of the driver. It should be set using #oC_DRIVER_VERSION definition.
     */
    uint32_t                        Version;
} oC_Driver_t;

/* END OF SECTION */
#undef  _________________________________________INTERFACE_TYPES_SECTION____________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with macros that helps to define driver interface
 */
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns interface structure reference for the driver
 *
 * The function returns reference to the structure with interface of a driver. Note, that it will return only that driver, that are included
 * already. If you want to get interface of any driver using this macro, you should include oc_drivers.h header.
 *
 * @param DRIVER_NAME   name of the driver to get interface
 */
//==========================================================================================================================================
#define oC_Driver_Interface_(DRIVER_NAME)           oC_DRIVER_REGISTRATION_NAME(DRIVER_NAME).If

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief creates value for storing driver version
 *
 * The macro is for creating a version of the driver.
 *
 * @param   MINOR       Changing of this value means, that the interface of the driver was changed totally.
 * @param   MAJOR       This value is changed, when some of interface functions (maximum 50%) was changed
 * @param   BUILD       Value changed with some important changes, but without changing the interface
 * @param   PATCH       Little changes, fixing bugs, etc...
 */
//==========================================================================================================================================
#define oC_DRIVER_VERSION( MINOR , MAJOR , BUILD , PATCH )      ( ((uint32_t)(MINOR)) << 24 ) | ( ((uint32_t)(MAJOR)) << 16 ) | ((uint32_t)(BUILD)<< 8) | (((uint32_t)(PATCH))<<0)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief helps to define a driver
 *
 * The macro that should be used to define a driver in the main driver source file.
 *
 * @param NAME          Name of the driver, for example: GPIO
 * @param DEVICE_NAME   Name of the file, that will be created for the driver
 * @param VERSION       Version of the driver (see #oC_DRIVER_VERSION for more information)
 * @param INTERFACE     Structure with an interface for the driver. See #oC_Driver_Interface_t type for more info.
 */
//==========================================================================================================================================
#define oC_DRIVER_DEFINE( NAME , DEVICE_NAME , VERSION , INTERFACE )    \
        const oC_Driver_t oC_DRIVER_REGISTRATION_NAME(NAME) = { \
                                                           .If = &INTERFACE , \
                                                           .DeviceName = DEVICE_NAME , \
                                                           .Version = VERSION };

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief declare prototype of the driver structure
 *
 *  The macro that should be used to declare a driver in the main driver header file.
 *
 */
//==========================================================================================================================================
#define oC_DRIVER_DECLARE( NAME )   \
        extern const oC_Driver_t oC_DRIVER_REGISTRATION_NAME(NAME)

//==========================================================================================================================================
/**
 * @brief creates registration variable name for the driver
 *
 * Returns registration of the driver structure name.
 */
//==========================================================================================================================================
#define oC_DRIVER_REGISTRATION_NAME( NAME )     oC_ONE_WORD_FROM_2(_Registration , NAME)

//==========================================================================================================================================
/**
 * @brief creates name of the configuration type for driver
 *
 * Creates registration structure type name.
 */
//==========================================================================================================================================
#define oC_DRIVER_CONFIGURATION_TYPE_NAME( NAME )   oC_ONE_WORD_FROM_3( oC_ , NAME , _Config_t  )

/* END OF SECTION */
#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


#endif /* SYSTEM_DRIVERS_OC_DRIVER_H_ */
