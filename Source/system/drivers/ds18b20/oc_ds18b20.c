/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.c Ver 1.0.0
 *
 *    @file       oc_ds18b20.h
 *
 *    @brief      File with interface of the DS18B20 driver
 *
 *    @author     Kamil Drobienko - (Created on: 2014-12-09 - 13:02:36)
 *
 *    @note       Copyright (C) 2015 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_ds18b20.h>
#include <oc_time.h>
// Drivers
#include <oc_drivers.h>             // access to all drivers interfaces

// LLD
#include <oc_gpio_lld.h>            // access to lld interface layer

// Libraries
#include <oc_dlist.h>               // dynamic list for the interrupts and signals
#include <oc_synch.h>               // signals for the interrupts

// Others
#include <oc_os.h>
#include <oc_assert.h>
#include <oc_driver.h>
/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/
static inline bool   _ConvertTempScale   ( const oC_DS18B20_Config_t * Config, float * Temp );
static inline bool   _ChangeResolution   ( const oC_DS18B20_Config_t * Config );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/
//==========================================================================================================================================
/**
 *  Function called before system starts
 */
//==========================================================================================================================================
void oC_DS18B20_TurnOnDriver( void )
{
}

//==========================================================================================================================================
/**
 *  Function called before system restarts
 */
//==========================================================================================================================================
void oC_DS18B20_TurnOffDriver( void )
{
}

//==========================================================================================================================================
/**
 * The function for configuring the driver.
 *
 * @param Config        Reference to the configuration structure
 *
 * @return Code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DS18B20_Configure( const oC_DS18B20_Config_t * Config )
{
    oC_ErrorCode_t Result = oC_ErrorCode_None;


    if (
              oC_ErrorCode_None != oC_1WIRE_Configure( (Config->OneWireConfig)   )
        )
    {
        oC_IMPLEMENT_FAILURE();
    }

    return Result;
}

//==========================================================================================================================================
/**
 * Removes configuration from configured list. The function is called before software system restart.
 *
 * @param Config        Reference to the configuration structure to remove from list
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DS18B20_Unconfigure( const oC_DS18B20_Config_t * Config )
{
    oC_UNUSED_ARG( Config );

    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * The function to send data via the driver as a stream
 *
 * @param Config        Reference to the config structure
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_DS18B20_SendViaStream( const oC_DS18B20_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int sent_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return sent_bytes;
}

//==========================================================================================================================================
/**
 * The function to receive data via the driver as a stream
 *
 * @param Context       Reference to the context of the driver
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_DS18B20_ReceiveViaStream( const oC_DS18B20_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int received_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return received_bytes;
}

bool oC_DS18B20_ReadTemp ( const oC_DS18B20_Config_t * Config  )
{
    bool result = false;
    bool Presence = 0;
    uint8_t Temp1 = 0;
    uint8_t Temp2 = 0;
    float Temp = 0;
    Presence = oC_1WIRE_RESET( (Config->OneWireConfig) );

    if(Presence == 1)
    {
        oC_1WIRE_SendByte( ( Config->OneWireConfig ) , oC_1WIRE_SKIP );
        oC_1WIRE_SendByte( ( Config->OneWireConfig ) , oC_DS18B20_Order_ConvertT);
        oC_Time_DelayFor_us(750);

        Presence = oC_1WIRE_RESET( ( Config->OneWireConfig ) );
        oC_1WIRE_SendByte( ( Config->OneWireConfig ), oC_1WIRE_SKIP );
        oC_1WIRE_SendByte( ( Config->OneWireConfig ), oC_DS18B20_Order_ReadScratchPad );

        oC_1WIRE_ReceiveByte((Config->OneWireConfig), &Temp1);
        oC_1WIRE_ReceiveByte((Config->OneWireConfig), &Temp2);
        Presence = oC_1WIRE_RESET((Config->OneWireConfig));

        Temp=((float)Temp1+((float)Temp2*256))/16;

        _ConvertTempScale(Config, &Temp);

        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}


static inline bool  _ConvertTempScale   ( const oC_DS18B20_Config_t * Config , float * Temp )
{
    bool result = true;

    switch(Config->Scale)
    {
        default:
            break;
        case oC_DS18B20_TemperatureScale_Default:
            break;
        case oC_DS18B20_TemperatureScale_Celsius:
            break;
        case oC_DS18B20_TemperatureScale_Fahrenheit:
            *Temp = ( 32 + ( 9 * ( (*Temp)/5 ) ) );
            break;
        case oC_DS18B20_TemperatureScale_Kelvin:
            *Temp = *Temp + 273.15;
            break;
    }

    return result;
}

static inline bool  _ChangeResolution   ( const oC_DS18B20_Config_t * Config )
{
    bool result = true;

    switch( Config->Resolution )
    {
        default:
            break;
        case oC_DS18B20_Resolution_Default:
            break;
        case oC_DS18B20_Resolution_9Bit:
            break;
        case oC_DS18B20_Resolution_10Bit:
            break;
        case oC_DS18B20_Resolution_11Bit:
            break;
        case oC_DS18B20_Resolution_12Bit:
            break;
    }

    return result;
}
/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/
