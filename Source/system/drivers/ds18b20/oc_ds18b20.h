/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.h Ver 1.0.0
 *
 *    @file       oc_ds18b20.h
 *
 *    @brief      File with interface functions for the DS18B20 driver
 *
 *    @author     Kamil Drobienko - (Created on: 2015-04-26 - 13:02:36)
 *
 *    @note       Copyright (C) 2015 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_DS18B20_H
#define _OC_DS18B20_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_gen_macros.h>
#include <oc_errors.h>
#include <oc_1wire.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/
uint8_t oC_DS18B20_SctatchPad[8];

/**
 * Type represents commands for the DS18B20 digital thermometer
 */
typedef enum
{
    oC_DS18B20_Order_ConvertT           = 0x44,
    oC_DS18B20_Order_ReadScratchPad     = 0xBE,
    oC_DS18B20_Order_WriteScratchPad    = 0x4E,
    oC_DS18B20_Order_CopyScratchPad     = 0x48,
    oC_DS18B20_Order_RecallE2           = 0xB8,
    oC_DS18B20_Order_ReadPowerSupply    = 0xB4
}oC_DS18B20_Order_t;

/**
 * Type represents resolution of the DS18B20 digital thermometer
 */
typedef enum
{
    oC_DS18B20_Resolution_Default,
    oC_DS18B20_Resolution_9Bit,
    oC_DS18B20_Resolution_10Bit,
    oC_DS18B20_Resolution_11Bit,
    oC_DS18B20_Resolution_12Bit
}oC_DS18B20_Resolution_t;

/**
 * Type represents scale of measure temperature
 */
typedef enum
{
    oC_DS18B20_TemperatureScale_Default,
    oC_DS18B20_TemperatureScale_Celsius,
    oC_DS18B20_TemperatureScale_Fahrenheit,
    oC_DS18B20_TemperatureScale_Kelvin
}oC_DS18B20_TemperatureScale_t;

//==========================================================================================================================================
/**
 * Configuration of the DS18B20
 */
//==========================================================================================================================================
typedef struct
{
    oC_1WIRE_Config_t  const       *OneWireConfig;          /**< 1WIRE configuration for the DS18B20 digital thermometer */
    oC_DS18B20_Resolution_t         Resolution;             /**< resolution of the DS18B20 digital thermometer */
    oC_DS18B20_TemperatureScale_t   Scale;                  /**< scale of measure temperature */
} oC_DS18B20_Config_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern void           oC_DS18B20_TurnOnDriver                 ( void );
extern void           oC_DS18B20_TurnOffDriver                ( void );
extern oC_ErrorCode_t oC_DS18B20_Configure                    ( const oC_DS18B20_Config_t * Config );
extern oC_ErrorCode_t oC_DS18B20_Unconfigure                  ( const oC_DS18B20_Config_t * Config );
extern int 	          oC_DS18B20_SendViaStream				  ( const oC_DS18B20_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern int 	          oC_DS18B20_ReceiveViaStream             ( const oC_DS18B20_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern bool           oC_DS18B20_ReadTemp                     ( const oC_DS18B20_Config_t * Config );                                         //in future Temp should be float variable (float doesn't displat in PuTTY console now)
extern bool           oC_DS18B20_ChangeResolution             ( const oC_DS18B20_Config_t * Config );
extern bool           oC_DS18B20_ReadScratchPad               ( const oC_DS18B20_Config_t * Config );
extern bool           oC_DS18B20_WriteScratchPad              ( const oC_DS18B20_Config_t * Config );
extern bool           oC_DS18B20_CopyScratchPad               ( const oC_DS18B20_Config_t * Config );
extern bool           oC_DS18B20_RecallE2                     ( const oC_DS18B20_Config_t * Config );
extern bool           oC_DS18B20_ReadPowerSupply              ( const oC_DS18B20_Config_t * Config );


/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* _OC_DS18B20_H */
