/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_drivers.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 6 pa� 2014 17:06:10)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_drivers.h>
#include <oc_drivers_cfg.h>
#include <oc_default_drivers_cfg.h>
#include <oc_array.h>
#include <oc_address.h>

#ifndef CFG_LIST_INCLUDED_DRIVERS
#   error CFG_LIST_INCLUDED_DRIVERS is not defined in oc_drivers_cfg.h
#endif

/** ****************************************************************************************************************************************
 * The section with local types
 */
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

/**
 * Type stores informations about driver default configurations
 */
typedef struct
{
    const oC_Driver_t *     Driver;
    const void *            Configuration;
    const char *            Name;
    bool                    Configured;
} DefaultConfiguration_t;

/* END OF SECTION */
#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with global variables
 */
#define _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

//------------------------------------------------------------------------------------------------------------------------------------------
/**
 * The array with references to the driver registration structures. The array is created according to the system configuration.
 */
//------------------------------------------------------------------------------------------------------------------------------------------
static const oC_Driver_t * const oC_DriversRegistrations[] = {
#define REGISTER(DRIVER_NAME)       &oC_DRIVER_REGISTRATION_NAME(DRIVER_NAME) ,
                                                 CFG_LIST_INCLUDED_DRIVERS( REGISTER , REGISTER )
#undef REGISTER
};

//------------------------------------------------------------------------------------------------------------------------------------------
/**
 * List of driver default configurations
 */
//------------------------------------------------------------------------------------------------------------------------------------------
static DefaultConfiguration_t DefaultConfigurations[] = {
#define CONFIGURE(DRIVER_NAME,CONFIGURATION)    \
    { \
        .Driver         = &oC_DRIVER_REGISTRATION_NAME(DRIVER_NAME) , \
        .Configuration  = &CONFIGURATION , \
        .Name           = #CONFIGURATION , \
        .Configured     = false} ,
                                                         CFG_LIST_DEFAULT_DRIVERS_CONFIGURATIONS(CONFIGURE)
#undef CONFIGURE
};

/* END OF SECTION */
#undef  _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

/** ****************************************************************************************************************************************
 * The section interface functions
 */
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * Checks if the driver is registered in the system.
 *
 * @param DriverRegistration        Reference to the structure with registration of the driver
 *
 * @return true if driver is registered
 */
//==========================================================================================================================================
bool _oC_Drivers_IsDriverRegistered( const oC_Driver_t * DriverRegistration )
{
    bool registered = false;

    if ( oC_Address_IsCorrect( DriverRegistration ) )
    {
        oC_ARRAY_FOREACH_IN_ARRAY(oC_DriversRegistrations , index)
        {
            if ( oC_DriversRegistrations[index] == DriverRegistration )
            {
                registered  = true;
                break;
            }
        }
    }

    return registered;
}

//==========================================================================================================================================
/**
 * Checks if the driver is turned on.
 *
 * @param DriverRegistration        Reference to the driver registration function
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool _oC_Drivers_IsDriverTurnedOn( const oC_Driver_t * DriverRegistration )
{
    bool turnedOn = false;

    if (
            oC_Address_IsCorrect( DriverRegistration )
         && oC_Address_IsCorrect( DriverRegistration->If )
         && oC_Address_IsCorrect( DriverRegistration->If->IsTurnedOnFunction )
         )
    {
        turnedOn = DriverRegistration->If->IsTurnedOnFunction;
    }

    return turnedOn;
}

//==========================================================================================================================================
/**
 * Configures a driver.
 *
 * @param DriverRegistration            Reference to the registration of driver
 * @param Config                        Configuration structure for driver
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t _oC_Drivers_ConfigureDriver( const oC_Driver_t * DriverRegistration , const void * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration)                             , oC_ErrorCode_WrongParameters )
         && oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration->If)                         , oC_ErrorCode_WrongParameters )
         && oC_AssignErrorCodeIfFalse( &errorCode , DriverRegistration->If->RequiredBootLevel >= oC_Boot_CurrentLevel()  , oC_ErrorCode_ModuleNotStartedYet)
         && oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration->If->ConfigureFunction)      , oC_ErrorCode_FunctionNotExists )
            )
    {
        errorCode   = DriverRegistration->If->ConfigureFunction(Config);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Unconfigures a driver.
 *
 * @param DriverRegistration            Reference to the driver registration
 * @param Config                        Configuration structure for driver
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t _oC_Drivers_UnconfigureDriver( const oC_Driver_t * DriverRegistration , const void * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration)                            , oC_ErrorCode_WrongParameters )
         && oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration->If)                        , oC_ErrorCode_WrongParameters )
         && oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration->If->UnconfigureFunction)   , oC_ErrorCode_FunctionNotExists)
            )
    {
        errorCode   = DriverRegistration->If->UnconfigureFunction(Config);
    }

    return errorCode;

}

//==========================================================================================================================================
/**
 * The function turns on/off driver
 *
 * @param DriverRegistration    Registration structure
 * @param TurnOn                Flag if the driver should turned on, or off
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t _oC_Drivers_TurnDriver( const oC_Driver_t * DriverRegistration , bool TurnOn )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration)                            , oC_ErrorCode_WrongParameters )
         && oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration->If)                        , oC_ErrorCode_WrongParameters )
            )
    {
        if ( TurnOn && oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration->If->TurnOnFunction) , oC_ErrorCode_FunctionNotExists ))
        {
            errorCode   = DriverRegistration->If->TurnOnFunction();
        }
        else if ( !TurnOn && oC_AssignErrorCodeIfFalse( &errorCode , oC_Address_IsCorrect(DriverRegistration->If->TurnOffFunction) , oC_ErrorCode_FunctionNotExists ) )
        {
            errorCode   = DriverRegistration->If->TurnOffFunction();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function turns on all registered drivers from the given level
 *
 * @param Level     Level of drivers to turn on
 */
//==========================================================================================================================================
void oC_Drivers_TurnOnDriversFromLevel( oC_Boot_Level_t Level )
{
#define REGISTER(DRIVER_NAME)
#define TURN_ON(DRIVER_NAME)   if ( oC_DRIVER_REGISTRATION_NAME(DRIVER_NAME).If->RequiredBootLevel == Level )  oC_Drivers_TurnOnDriver(DRIVER_NAME);
    CFG_LIST_INCLUDED_DRIVERS(REGISTER , TURN_ON);
#undef TURN_ON
#undef REGISTER
}

//==========================================================================================================================================
/**
 * Loads all default configurations from the given level
 *
 * @param Level         the level to load configurations
 */
//==========================================================================================================================================
void oC_Drivers_LoadDefaultConfigurations( oC_Boot_Level_t Level )
{
    printf("Loading default configurations on boot level %d\n" , Level);

    oC_ARRAY_FOREACH_IN_ARRAY( DefaultConfigurations , i )
    {
        if ( !DefaultConfigurations[i].Configured && DefaultConfigurations[i].Driver->If->RequiredBootLevel >= Level )
        {
            printf("Configure %s with %s: " , DefaultConfigurations[i].Driver->DeviceName , DefaultConfigurations[i].Name);

            if ( oC_Address_IsCorrect(DefaultConfigurations[i].Driver->If->ConfigureFunction) )
            {
                oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

                if (
                        oC_AssignErrorCodeIfFalse( &errorCode , _oC_Drivers_IsDriverRegistered( DefaultConfigurations[i].Driver ) , oC_ErrorCode_DriverNotRegistered )
                     && oC_AssignErrorCode( &errorCode  , DefaultConfigurations[i].Driver->If->ConfigureFunction(DefaultConfigurations[i].Configuration))
                        )
                {
                    DefaultConfigurations[i].Configured = true;
                    errorCode                           = oC_ErrorCode_None;
                }

                oC_PrintError( errorCode );
                printf("\n");
            }
            else
            {
                printf("failed. Configure function for this driver is not set!\n");
            }
        }
    }
}


/* END OF SECTION */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
