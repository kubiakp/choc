/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.h Ver 1.0.0
 *
 *    @file       oc_7seg.h
 *
 *    @brief      File with interface functions for the 7SEG driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-09 - 13:02:43)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_7SEG_H
#define _OC_7SEG_H

#include <oc_errors.h>
#include <oc_driver.h>

/** ****************************************************************************************************************************************
 * The section with local types
 */
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * Configuration of the 7SEG
 */
//==========================================================================================================================================
typedef struct
{

} oC_7SEG_Config_t;

/* END OF SECTION */
#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with interface prototypes
 */
#define _________________________________________INTERFACE_PROTOTYPES_SECTION_______________________________________________________________

extern oC_ErrorCode_t oC_7SEG_Configure                    ( const oC_7SEG_Config_t * Config );
extern oC_ErrorCode_t oC_7SEG_Unconfigure                  ( const oC_7SEG_Config_t * Config );

/* END OF SECTION */
#undef  _________________________________________INTERFACE_PROTOTYPES_SECTION_______________________________________________________________

/** ****************************************************************************************************************************************
 * The section with global variables
 */
#define _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

oC_DRIVER_DECLARE( 7SEG );

/* END OF SECTION */
#undef  _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________


#endif /* _OC_7SEG_H */
