/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.c Ver 1.0.0
 *
 *    @file       oc_7seg.h
 *
 *    @brief      File with interface of the 7SEG driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-09 - 13:02:43)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_7seg.h>

/** ****************************************************************************************************************************************
 * The section local constant definitions
 */
#define _________________________________________LOCAL_CONSTS_SECTION_______________________________________________________________________

//==========================================================================================================================================
/**
 * Version of the 7seg driver.
 */
//==========================================================================================================================================
#define VERSION             oC_DRIVER_VERSION( 0 , 0 , 0 , 0 )

//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
static const oC_Driver_Interface_t _DriverInterface = {
                                                       .RequiredBootLevel       = oC_Boot_Level_DontRequireAnything ,
                                                       .TurnOnFunction          = NULL ,
                                                       .TurnOffFunction         = NULL ,
                                                       .ConfigureFunction       = oC_7SEG_Configure ,
                                                       .UnconfigureFunction     = oC_7SEG_Unconfigure ,
                                                       .IsTurnedOnFunction      = NULL ,
                                                       .SendViaStreamFunction   = NULL ,
                                                       .ReceiveViaStreamFunction= NULL
};

/*
 * Definition of driver interface
 */
oC_DRIVER_DEFINE( 7SEG , "7seg" , VERSION , _DriverInterface );

/* END OF SECTION */
#undef  _________________________________________LOCAL_CONSTS_SECTION_______________________________________________________________________

/** ****************************************************************************************************************************************
 * The section local function prototypes
 */
#define _________________________________________LOCAL_FUNCTIONS_PROTOTYPES_SECTION_________________________________________________________



/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTIONS_PROTOTYPES_SECTION_________________________________________________________

/** ****************************************************************************************************************************************
 * The section with interface functions
 */
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * The function for configuring the driver.
 *
 * @param Config        Reference to the configuration structure
 *
 * @return Code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_7SEG_Configure( const oC_7SEG_Config_t * Config )
{
    oC_UNUSED_ARG( Config );

    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Removes configuration from configured list. The function is called before software system restart.
 *
 * @param Config        Reference to the configuration structure to remove from list
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_7SEG_Unconfigure( const oC_7SEG_Config_t * Config )
{
    oC_UNUSED_ARG( Config );

    return oC_ErrorCode_NotImplemented;
}


/* END OF SECTION */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with local functions
 */
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________



/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
