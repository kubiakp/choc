/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.c Ver 1.0.0
 *
 *    @file       oc_can.h
 *
 *    @brief      File with interface of the CAN driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-09 - 12:53:57)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_can.h>
#include <oc_can_lld.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 *  Function called before system starts
 */
//==========================================================================================================================================
void oC_CAN_TurnOnDriver( void )
{
}

//==========================================================================================================================================
/**
 *  Function called before system restarts
 */
//==========================================================================================================================================
void oC_CAN_TurnOffDriver( void )
{
}

//==========================================================================================================================================
/**
 * The function for configuring the driver.
 *
 * @param Config        Reference to the configuration structure
 *
 * @return Code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CAN_Configure( const oC_CAN_Config_t * Config )
{
    oC_UNUSED_ARG( Config );

    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Removes configuration from configured list. The function is called before software system restart.
 *
 * @param Config        Reference to the configuration structure to remove from list
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CAN_Unconfigure( const oC_CAN_Config_t * Config )
{
    oC_UNUSED_ARG( Config );

    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * The function to send data via the driver as a stream
 *
 * @param Config        Reference to the config structure
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_CAN_SendViaStream( const oC_CAN_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int sent_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return sent_bytes;
}

//==========================================================================================================================================
/**
 * The function to receive data via the driver as a stream
 *
 * @param Context       Reference to the context of the driver
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_CAN_ReceiveViaStream( const oC_CAN_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int received_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return received_bytes;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/
