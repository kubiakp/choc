/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.h Ver 1.0.0
 *
 *    @file       oc_can.h
 *
 *    @brief      File with interface functions for the CAN driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-09 - 12:53:57)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_CAN_H
#define _OC_CAN_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_can_defs.h>
#include <oc_gen_macros.h>
#include <oc_errors.h>
#include <oc_gpio.h>

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/
#define ADD_CAN(_X_)        oC_CAT_2(oC_CAN_CAN_, _X_)

#define TRANSFER_CONTENT ( _ADD_ )                          \
                                _ADD_( IDMASK   , 0     ),  \
                                _ADD_( DIR      , 0     ),  \
                                _ADD_( MXTD     , 0     ),  \

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/


//==========================================================================================================================================
/**
 * Possible states of CAN.
 */
//==========================================================================================================================================
typedef enum
{
    oC_CAN_State_BusOff,
    oC_CAN_State_Mute,
    oC_CAN_State_Idle
}oC_CAN_State;;

//==========================================================================================================================================
/**
 * List of available CANs. Now there are only internal, but external will be added.
 */
//==========================================================================================================================================
typedef enum
{
    CAN_CAN_LIST( ADD_CAN )
    /* External CANs will be added in the future */
    oC_CAN_CAN_NumberOfInternalCans,
    oC_CAN_CAN_NumberOfCANs = oC_CAN_CAN_NumberOfInternalCans -1
}oC_CAN_CAN_t;


//==========================================================================================================================================
/**
 * Length of the message identifier. 11 or 29 (for CAN 2.0B) bits possible.
 */
//==========================================================================================================================================
typedef enum
{
    oC_CAN_IDLength_11Bit,
    oC_CAN_IDLength_29Bit
}oC_CAN_IDLength_t;

typedef oC_uint32_t oC_CAN_ID_t;           /**< Message identifier (11/29 bit length) */

typedef oC_uint16_t oC_CAN_BitRate_t;      /**< Bit rate in Kbps */

//==========================================================================================================================================
/**
 * Specifies message type.
 */
//==========================================================================================================================================
typedef enum
{
    oC_CAN_MsgType_Default,                 /**< Message send on user request */
    oC_CAN_MsgType_Cyclic,                  /**< Message is send periodically */
}oC_CAN_MsgType_t;


//==========================================================================================================================================
/**
 * Allow user to define callback
 */
//==========================================================================================================================================
typedef enum
{
    oC_CAN_IntSupport_Disabled,
    oC_CAN_IntSupport_Enabled
}oC_CAN_IntSupport_t;


//==========================================================================================================================================
/**
 * Automatic retransmission enabled/disabled if message error detected.
 */
//==========================================================================================================================================
typedef enum
{
    oC_CAN_AutoRetry_Enabled,
    oC_CAN_AutoRetry_Disabled,
    oC_CAN_AutoRetry_Default = oC_CAN_AutoRetry_Disabled
}oC_CAN_AutoRetry_t;

//==========================================================================================================================================
/**
 * Defines which functions will be used.
 */
//==========================================================================================================================================
typedef enum
{
    oC_CAN_Functions_Rx,                             /**< Receiving enabled  */
    oC_CAN_Functions_Tx,                             /**< Sending enabled    */
    oC_CAN_Functions_RxTx,                           /**< Receiving and sending enabled */
    oC_CAN_Functions_Default = oC_CAN_Functions_RxTx
}oC_CAN_Functions_t;

//==========================================================================================================================================
/**
 * CAN transmission structure.
 */
//==========================================================================================================================================
typedef struct
{
    oC_CAN_ID_t         Id;
    oC_CAN_MsgType_t    MsgType;

    oC_uint8_t          *Data;
    oC_uint8_t          DataLength;

    oC_CAN_AutoRetry_t  AutoRetry;              /**< Automatic retransmission if error detected */

}oC_CAN_Transmission_t;


//==========================================================================================================================================
/**
 * Configuration of the CAN
 */
//==========================================================================================================================================
typedef struct
{
    oC_CAN_CAN_t        CAN;

    oC_GPIO_PinLink_t   RxPin;
    oC_GPIO_PinLink_t   TxPin;

    oC_CAN_IDLength_t   IDLength;
    oC_CAN_ID_t         ID;

    oC_CAN_BitRate_t    BitRate;            /**< Bit rate in Kbps */

    oC_CAN_Functions_t  Functions;          /**< Rx, Tx or Rx + Tx */

    void                *RxCallback;        /**< Message receive interrupt callback, NULL if not needed */
    void                *TxCallback;        /**< Message receive interrupt callback, NULL if not needed */

} oC_CAN_Config_t;



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern void           oC_CAN_TurnOnDriver                 ( void );
extern void           oC_CAN_TurnOffDriver                ( void );
extern oC_ErrorCode_t oC_CAN_Configure                    ( const oC_CAN_Config_t * Config );
extern oC_ErrorCode_t oC_CAN_Unconfigure                  ( const oC_CAN_Config_t * Config );
extern int            oC_CAN_SendViaStream                ( const oC_CAN_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern int            oC_CAN_ReceiveViaStream             ( const oC_CAN_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* _OC_CAN_H */
