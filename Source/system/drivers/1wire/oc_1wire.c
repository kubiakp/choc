/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.c Ver 1.0.0
 *
 *    @file       oc_1wire.h
 *
 *    @brief      File with interface of the 1WIRE driver
 *
 *    @author     Kamil Drobienko - (Created on: 2015-03-15 - 20:50:23)
 *
 *    @note       Copyright (C) 2015 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_1wire.h>
#include <oc_time.h>
// Drivers
#include <oc_drivers.h>             // access to all drivers interfaces

// LLD
#include <oc_gpio_lld.h>            // access to lld interface layer

// Libraries
#include <oc_dlist.h>               // dynamic list for the interrupts and signals
#include <oc_synch.h>                 // signals for the interrupts

// Others
#include <oc_os.h>
#include <oc_assert.h>
#include <oc_driver.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/
#define VERSION oC_DRIVER_VERSION(1,0,0,0)

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/
uint8_t ROMAddr[8];
unsigned int Data = 0;
/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 *  Function called before system starts
 */
//==========================================================================================================================================
void oC_1WIRE_TurnOnDriver( void )
{
}

//==========================================================================================================================================
/**
 *  Function called before system restarts
 */
//==========================================================================================================================================
void oC_1WIRE_TurnOffDriver( void )
{
}

//==========================================================================================================================================
/**
 * The function for configuring the driver.
 *
 * @param Config             Reference to the configuration structure
 *
 * @return Code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_1WIRE_Configure( const oC_1WIRE_Config_t * Config )
{

    oC_ErrorCode_t result = oC_ErrorCode_ImplementError;

    oC_GPIO_Config_t  oC_1Wire_GPIOConfiguration;

    oC_GPIO_SetDefaultValuesForConfig(&oC_1Wire_GPIOConfiguration);

    oC_1Wire_GPIOConfiguration.PinLink = Config->PinLink;


    if (
            ( oC_ErrorCode_None != oC_GPIO_Configure( &oC_1Wire_GPIOConfiguration ) )
    )
    {
        oC_IMPLEMENT_FAILURE();
    }
    else
    {
        result = oC_ErrorCode_None;
    }

    switch( Config->Mode )
    {
        default:
            break;

        case oC_1WIRE_Mode_Single:
            if( oC_1WIRE_RESET( Config ) )
            {
                result = oC_ErrorCode_None;
            }
            else
            {
                result = oC_ErrorCode_ImplementError;
            }
            break;

        case oC_1WIRE_Mode_Multiple:
            if( oC_1WIRE_SearchSlaveID( &Config ) )
            {
                result = oC_ErrorCode_None;
            }
            else
            {
                result = oC_ErrorCode_ImplementError;
            }
            break;

    }


    return  oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * Removes configuration from configured list. The function is called before software system restart.
 *
 * @param Config        Reference to the configuration structure to remove from list
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_1WIRE_Unconfigure( const oC_1WIRE_Config_t * Config )
{
    oC_UNUSED_ARG( Config );

    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * The function to send data via the driver as a stream
 *
 * @param Config        Reference to the config structure
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_1WIRE_SendViaStream( const oC_1WIRE_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int sent_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return sent_bytes;
}

//==========================================================================================================================================
/**
 * The function to receive data via the driver as a stream
 *
 * @param Context       Reference to the context of the driver
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_1WIRE_ReceiveViaStream( const oC_1WIRE_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int received_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return received_bytes;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/
//==========================================================================================================================================
/**
 * RESET PULSE, PRESENCE PULSE
 *
 * @param 		Config          Reference to the configuration structure
 *
 * @return 		Presence 		Presence slave device on the bus
 *
 **/
//==========================================================================================================================================
bool oC_1WIRE_RESET(const oC_1WIRE_Config_t * Config)
{
    bool Presence = false;

    oC_Time_DelayFor_us(10);
    oC_GPIO_SetData(Config->PinLink, 0x00);
    oC_Time_DelayFor_us(480);
    oC_GPIO_SetMode(Config->PinLink, oC_GPIO_Mode_Input);

    // After 65us slave device should set Presence Impulse -> logic '0'
    oC_Time_DelayFor_us(90);
    Presence = oC_GPIO_GetData(Config->PinLink);
    oC_Time_DelayFor_us(480);
    oC_GPIO_SetMode(Config->PinLink, oC_GPIO_Mode_Output);

    if(Presence == 0)
        return 1;
    else
        return 0;
}
//==========================================================================================================================================
/**
 * Cyclic Redundancy Check Generator
 *
 * @param 		Config          Reference to the configuration structure
 * @param       ROMAddress[8]   Array with 64 bit ROM address
 *
 * @return 		Result			Status of the correct or incorrect transmission
 *
 **/
//==========================================================================================================================================
bool oC_1WIRE_CRCGenerator ( const oC_1WIRE_Config_t * Config , uint8_t ROMAddress[8] )
{
    bool Result;
    uint8_t ROMByte = 0x00;
    uint8_t CRC = 0x00;

    for ( int ByteIdx = 0 ; ByteIdx < 7 ; ByteIdx++ )
    {
        ROMByte = ROMAddress[ByteIdx];

        for( int bitIdx = 0 ; bitIdx < 8 ; bitIdx++ )
        {
            if( ( ROMByte ^ CRC ) & 0x01 )
            {
                CRC >>= 1;
                CRC ^= 0x8C;
            }
            else
            {
                CRC >>= 1;
            }
            ROMByte >>= 1;
        }
    }

    if( ( CRC - ROMAddress[7] ) == 0 )
    {
        Result = true;
    }
    else
    {
        Result = false;
    }

    return Result;
}
//==========================================================================================================================================
/**
 * Write 1 or 0 signal
 *
 * @param       Config          Reference to the configuration structure
 * @param       Signal          Value of write signal ('1' or '0')
 *
 * @return                      Status
 *
 **/
//==========================================================================================================================================
bool oC_1WIRE_WriteBit(const oC_1WIRE_Config_t * Config, bool Signal)
{
    bool result = true;
    oC_Time_DelayFor_us(10);

    if(Signal==1)
    {
        oC_GPIO_SetData(Config->PinLink, 0x00);                   // force low state
        oC_Time_DelayFor_us(6);                                         // max 15us
        oC_GPIO_SetMode(Config->PinLink, oC_GPIO_Mode_Input);     // release bus
        oC_Time_DelayFor_us(64);                                        // Tx'1'
        oC_GPIO_SetMode(Config->PinLink, oC_GPIO_Mode_Output);
    }
    else if(Signal==0)
    {
        oC_GPIO_SetData(Config->PinLink, 0x00);
        oC_Time_DelayFor_us(70);                                       // 60us < Tx'0' < 120us
        oC_GPIO_SetMode(Config->PinLink, oC_GPIO_Mode_Input);
        oC_Time_DelayFor_us(10);
        oC_GPIO_SetMode(Config->PinLink, oC_GPIO_Mode_Output);
    }
    else
    {
        result = false;
    }

    return result;
}
//==========================================================================================================================================
/**
 * Read signal
 *
 * @param Config          Reference to the configuration structure
 *
 * @return                value of the signal from slave device
 *
 **/
//==========================================================================================================================================
bool oC_1WIRE_ReadBit(const oC_1WIRE_Config_t * Config)
{
    bool signal = 0;
    //oC_Time_DelayFor_us(1);
    oC_GPIO_SetData(Config->PinLink, 0x00);                       // force low state
    oC_Time_DelayFor_us(6);                                             // min 1us
    oC_GPIO_SetMode(Config->PinLink, oC_GPIO_Mode_Input);         // realase bus
    //oC_Time_DelayFor_us(9);                                             // all procedure may take up max 15us

    if(oC_GPIO_GetData(Config->PinLink) == 0)
    {
        signal = 0;
    }
    else
    {
        signal = 1;
    }
    oC_Time_DelayFor_us(55);

    oC_GPIO_SetMode(Config->PinLink, oC_GPIO_Mode_Output);
    oC_Time_DelayFor_us(10); 											//Recovery  bus

    return signal;
}
//==========================================================================================================================================
/**
 * Send Byte
 *
 * @param Config          Reference to the configuration structure
 *
 * @param Value           Value of write data byte
 *
 **/
//=========================================================================================================================================
bool  oC_1WIRE_SendByte ( const oC_1WIRE_Config_t * Config , char Message )
{
    bool result = 0;
    unsigned int i = 0;
    uint8_t Data = 1;

    for( i=0 ; i<8 ; i++)
    {
        if( ( Data<<i & Message ) )
            oC_1WIRE_WriteBit(Config, 1);
        else
            oC_1WIRE_WriteBit(Config, 0);
    }

    return result;
}
//==========================================================================================================================================
/**
 * Receive Byte
 *
 * @param Config          Reference to the configuration structure
 *
 * @param outMessage      Pointer to value for read data
 *
 * @return                The success of the action
 *
 **/
//=========================================================================================================================================
bool oC_1WIRE_ReceiveByte ( const oC_1WIRE_Config_t * Config , uint8_t * outMessage )
{
    bool result = 0;
    unsigned int i = 0;
    *outMessage=0x00;

    for( i=0 ; i<8 ; i++ )
    {
        if(oC_1WIRE_ReadBit(Config))
        {
            (*outMessage) = (*outMessage)  | (0x01<<i);
        }
    }

    return result;
}
//==========================================================================================================================================
/**
 * Read ROM address from slave device
 *
 * @param Config            Reference to the configuration structure
 * @param ROMAddress[8]     Array for 64 bit ROM address
 *
 * @return                  Status
 *
 **/
//=========================================================================================================================================
bool oC_1WIRE_ReadROM( const oC_1WIRE_Config_t * Config, uint8_t ROMAddress[8] )
{
    bool result = true;
    unsigned int i=0;

    if( oC_1WIRE_RESET(Config) == 1)
    {
        oC_1WIRE_SendByte(Config, oC_1WIRE_READ);

        for(i=0;i<8;i++)
        {
            oC_1WIRE_ReceiveByte(Config, &ROMAddress[i]);
        }
    }
    else
    {
        result = false;
    }

    return result;
}
//==========================================================================================================================================
/**
 * Search slave devices
 *
 * @param Config          Reference to the configuration structure
 *
 * @return
 *
 **/
//=========================================================================================================================================
bool oC_1WIRE_SearchSlaveID( const oC_1WIRE_Config_t * Config )
{
    //unsigned int Data = 0;
    bool Result = true;
    unsigned int i=0;
    unsigned int j=0;
    uint64_t BitIdx = 1;
    if( oC_1WIRE_RESET(Config) == 1)
         {
             oC_1WIRE_SendByte(Config, oC_1WIRE_SEARCH);
         }
    for(int bitIdx = 0 ; bitIdx < 64 ; bitIdx++ )
    {
        Data = 0;
        Data = oC_1WIRE_ReadBit(Config);
        Data = Data + (oC_1WIRE_ReadBit(Config) << 1);

        switch(Data)
        {
            case 0:                                                 /**<select bit to save*/

                break;

            case 1:                                                 /**<write '0' as next bit of actual ROM address*/
                ROMAddr[j] = ROMAddr[j] | (0x01<<i);
                i++;
                oC_1WIRE_WriteBit(Config,1);
                break;

            case 2:                                                 /**<write '1' as next bit of actual ROM address*/
                i++;
                oC_1WIRE_WriteBit(Config,0);
                  break;

        }
        if( ! ( i % 8 ) )
        {
            j++;
            i=0;
        }

    }



    return Result;
}


/** ****************************************************************************************************************************************
 * The section with driver definition
 */
#define _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________

static const oC_Driver_Interface_t Interface = {
                                                .RequiredBootLevel              = oC_Boot_Level_DontRequireAnything ,
                                                .TurnOnFunction                 = oC_1WIRE_TurnOnDriver ,
                                                .TurnOffFunction                = oC_1WIRE_TurnOffDriver ,
                                                .IsTurnedOnFunction             = NULL ,
                                                .ConfigureFunction              = oC_1WIRE_Configure ,
                                                .UnconfigureFunction            = oC_1WIRE_Unconfigure ,
                                                .SendViaStreamFunction          = NULL ,
                                                .ReceiveViaStreamFunction       = NULL
};

oC_DRIVER_DEFINE(1WIRE , "1wire" , VERSION , Interface);

/* END OF SECTION */
#undef  _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________
