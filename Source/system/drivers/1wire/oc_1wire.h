/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.h Ver 1.0.0
 *
 *    @file       oc_1wire.h
 *
 *    @brief      File with interface functions for the 1WIRE driver
 *
 *    @author     Kamil Drobienko - (Created on: 2015-03-09 - 12:54:29)
 *
 *    @note       Copyright (C) 2015 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_1WIRE_H
#define _OC_1WIRE_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_gen_macros.h>
#include <oc_errors.h>
#include <oc_gpio.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/
typedef uint8_t oC_1WIRE_ROMAddress[8];

//==========================================================================================================================================

/**
 * Type represents the number of slaves on the line
 */
typedef enum
{
    oC_1WIRE_Mode_Default,
    oC_1WIRE_Mode_Single,
    oC_1WIRE_Mode_Multiple
}oC_1WIRE_Mode_t;

/**
 * Type represents speed of the transmission
 */
typedef enum
{
    oC_1WIRE_Speed_Default,
    oC_1WIRE_Speed_Normal,
    oC_1WIRE_Speed_Overdrive
} oC_1WIRE_Speed_t;

/**
 * ROM Commands
 */
typedef enum
{
    oC_1WIRE_SEARCH            = 0xF0,              /**< Search slave devices */
    oC_1WIRE_READ              = 0x33,              /**< Read ROM code from slave */
    oC_1WIRE_MATCH             = 0x55,              /**< Address specific devices */
    oC_1WIRE_SKIP              = 0xCC,              /**< Skip addressing, default ROM configuration */
    oC_1WIRE_ALARM_SEARCH      = 0xEC,              /**< Search slave devices with alarm flag */
    oC_1WIRE_OVERDRIVE_SKIP    = 0x3C,              /**< Overdrive version of SKIP ROM */
    oC_1WIRE_OVERDRIVE_MATCH   = 0x69               /**< Overdrive version of MATCH ROM */
}oC_1WIRE_ROM_t;


/**
 * Configuration of the 1WIRE
 */
typedef struct
{
    oC_GPIO_PinLink_t   const   *PinLink;                           /**< pin to configure */
    oC_1WIRE_Mode_t              Mode;
    oC_1WIRE_Speed_t             Speed;
} oC_1WIRE_Config_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern void           oC_1WIRE_TurnOnDriver                 ( void );
extern void           oC_1WIRE_TurnOffDriver                ( void );
extern oC_ErrorCode_t oC_1WIRE_Configure                    ( const oC_1WIRE_Config_t * Config );
extern oC_ErrorCode_t oC_1WIRE_Unconfigure                  ( const oC_1WIRE_Config_t * Config );
extern int 	          oC_1WIRE_SendViaStream				( const oC_1WIRE_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern int 	          oC_1WIRE_ReceiveViaStream             ( const oC_1WIRE_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms);
extern bool           oC_1WIRE_RESET                        ( const oC_1WIRE_Config_t * Config );
extern bool           oC_1WIRE_CRCGenerator                 ( const oC_1WIRE_Config_t * Config , uint8_t ROMAddress[8] );
extern bool           oC_1WIRE_WriteBit                     ( const oC_1WIRE_Config_t * Config , bool Signal );
extern bool           oC_1WIRE_ReadBit                      ( const oC_1WIRE_Config_t * Config );
extern bool           oC_1WIRE_SendByte                     ( const oC_1WIRE_Config_t * Config , char Message );
extern bool           oC_1WIRE_ReceiveByte                  ( const oC_1WIRE_Config_t * Config , uint8_t * outMessage );
extern int            oC_1WIRE_SendArray                    ( const oC_1WIRE_Config_t * Config , uint8_t Array , int ArraySize );
extern int            oC_1WIRE_ReceiveArray                 ( const oC_1WIRE_Config_t * Config , uint8_t Array , int ArraySize );
extern bool           oC_1WIRE_ReadROM                      ( const oC_1WIRE_Config_t * Config , uint8_t ROMAddress[8] );
extern bool           oC_1WIRE_SearchSlaveID                ( const oC_1WIRE_Config_t * Config );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* _OC_1WIRE_H */
