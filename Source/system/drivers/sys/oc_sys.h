/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.h Ver 1.0.0
 *
 *    @file       oc_sys.h
 *
 *    @brief      File with interface functions for the SYS driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-21 - 22:31:21)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_SYS_H
#define _OC_SYS_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_sys_defs.h>
#include <oc_gen_macros.h>
#include <oc_gen_types.h>
#include <oc_errors.h>
#include <stdlib.h>
#include <oc_driver.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Type represents source of the oscillator.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SYS_OscillatorSource_Internal ,              /**< System will work on the internal oscillator */
    oC_SYS_OscillatorSource_External                /**< System will work on the external oscillator */
} oC_SYS_OscillatorSource_t;

//==========================================================================================================================================
/**
 * Configuration of the SYS
 */
//==========================================================================================================================================
typedef struct
{
    oC_Frequency_t                      WantedFrequency;        /**< The frequency to achieve */
    oC_Frequency_t                      OscillatorFrequency;    /**< The frequency of the external oscillator. If you do not use external oscillator, just leave this field empty (0) */
    oC_SYS_OscillatorSource_t           OscillatorSource;       /**< Source of the oscillator for the system. In case of the external oscillator, it is needed to fill #OscillatorFrequency */
} oC_SYS_Config_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/


extern oC_ErrorCode_t       oC_SYS_Configure                    ( const oC_SYS_Config_t * Config );
extern oC_ErrorCode_t       oC_SYS_Unconfigure                  ( const oC_SYS_Config_t * Config );
extern size_t               oC_SYS_GetUsedFlashSize             ( void );
extern size_t               oC_SYS_GetFlashSize                 ( void );
extern size_t               oC_SYS_GetRAMSize                   ( void );
extern size_t               oC_SYS_GetDataSize                  ( void );
extern size_t               oC_SYS_GetStackSize                 ( void );
extern size_t               oC_SYS_GetHeapSize                  ( void );
extern oC_Frequency_t       oC_SYS_GetSysClock                  ( void );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function add simple delay for about count * 4 cycles. (it is depending from compiler)
 *
 * @param   count   how many iteration should be delayed
 */
//==========================================================================================================================================
static inline void oC_SYS_Delay( volatile uint32_t count )
{
    while(count--);
}

oC_DRIVER_DECLARE(SYS);

#endif /* _OC_SYS_H */
