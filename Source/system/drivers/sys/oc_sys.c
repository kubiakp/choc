/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver.c Ver 1.0.0
 *
 *    @file       oc_sys.h
 *
 *    @brief      File with interface of the SYS driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-21 - 22:31:21)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_sys.h>
#include <oc_instr_defs.h>
#include <oc_sys_lld.h>
#include <oc_driver.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/

#define VERSION                 oC_DRIVER_VERSION(1,0,0,0)

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

extern const char __rom_size;
extern const char __ram_size;
extern const char __text_size;
extern const char __data_size;
extern const char __stack_size;
extern const char __heap_size;

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 *  Function called before system starts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_TurnOnDriver( void )
{
    oC_ENABLE_INTERRUPTS();
    oC_SYS_LLD_TurnOnDriver( );
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 *  Function called before system restarts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_TurnOffDriver( void )
{
    oC_SYS_LLD_TurnOffDriver( );
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * The function for configuring the driver.
 *
 * @param Config        Reference to the configuration structure
 *
 * @return Code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_Configure( const oC_SYS_Config_t * Config )
{
    oC_ErrorCode_t errorCode;

    //
    //  Configuration
    //
    if (
            ( (Config->OscillatorSource == oC_SYS_OscillatorSource_Internal)
                    && ( true == oC_SYS_LLD_ConfigureInternalOscillator( Config->WantedFrequency ) ))
                    ||
                    ( (Config->OscillatorSource == oC_SYS_OscillatorSource_External)
                            && ( true == oC_SYS_LLD_ConfigureExternalOscillator( Config->WantedFrequency  , Config->OscillatorFrequency) ))
    )
    {
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        if (    Config->OscillatorSource != oC_SYS_OscillatorSource_Internal
                &&  Config->OscillatorSource != oC_SYS_OscillatorSource_External)
        {
            errorCode   = oC_ErrorCode_SYSUnknownOscillatorSource;
        }
        else
        {
            errorCode   = oC_ErrorCode_SYSConfigurationError;
        }

    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * Removes configuration from configured list. The function is called before software system restart.
 *
 * @param Config        Reference to the configuration structure to remove from list
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_Unconfigure( const oC_SYS_Config_t * Config )
{
    oC_UNUSED_ARG( Config );

    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * The function to send data via the driver as a stream
 *
 * @param Config        Reference to the config structure
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_SYS_SendViaStream( const oC_SYS_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int sent_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return sent_bytes;
}

//==========================================================================================================================================
/**
 * The function to receive data via the driver as a stream
 *
 * @param Context       Reference to the context of the driver
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_SYS_ReceiveViaStream( const oC_SYS_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int received_bytes = 0;

    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
    oC_UNUSED_ARG( Timeout_ms );

    return received_bytes;
}

//==========================================================================================================================================
/**
 * The function return size of the used flash
 *
 * @return size_t - size of the used memory
 */
//==========================================================================================================================================
size_t oC_SYS_GetUsedFlashSize( void )
{
    size_t retVal = (size_t) &__text_size;

    return retVal;
}

//==========================================================================================================================================
/**
 * The function return size of the ROM
 *
 * @return size_t - size of the ROM memory
 */
//==========================================================================================================================================
size_t oC_SYS_GetFlashSize( void )
{
    size_t retVal = (size_t) &__rom_size;

    return retVal;
}

//==========================================================================================================================================
/**
 * The function return size of the RAM
 *
 * @return size_t - size of the RAM memory
 */
//==========================================================================================================================================
size_t oC_SYS_GetRAMSize( void )
{
    size_t retVal = (size_t) &__ram_size;

    return retVal;
}

//==========================================================================================================================================
/**
 * The function return size of DATA section
 *
 * @return size_t - size of the DATA section memory
 */
//==========================================================================================================================================
size_t oC_SYS_GetDataSize( void )
{
    size_t retVal = (size_t) &__data_size;

    return retVal;
}

//==========================================================================================================================================
/**
 * The function return size of the stack
 *
 * @return size_t - size of the stack
 */
//==========================================================================================================================================
size_t oC_SYS_GetStackSize( void )
{
    size_t retVal = (size_t) &__stack_size;

    return retVal;
}

//==========================================================================================================================================
/**
 * The function return size of the heap
 *
 * @return size_t - size of the heap
 */
//==========================================================================================================================================
size_t oC_SYS_GetHeapSize( void )
{
    size_t retVal = (size_t) &__heap_size;

    return retVal;
}
/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/

/** ****************************************************************************************************************************************
 * The section driver definition
 */
#define _________________________________________DRIVER_DEFINE_SECTION______________________________________________________________________

static const oC_Driver_Interface_t Interface = {
                                                .RequiredBootLevel          = oC_Boot_Level_DontRequireAnything ,
                                                .TurnOnFunction             = oC_SYS_TurnOnDriver ,
                                                .TurnOffFunction            = oC_SYS_TurnOffDriver ,
                                                .ConfigureFunction          = oC_SYS_Configure ,
                                                .UnconfigureFunction        = oC_SYS_Unconfigure
};

oC_DRIVER_DEFINE(SYS , "sys" , VERSION , Interface)

/* END OF SECTION */
#undef  _________________________________________DRIVER_DEFINE_SECTION______________________________________________________________________
