/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver_types.h Ver 1.0.0
 *
 *    @file       oc_gpio.h
 *
 *    @brief      File with interface types for the GPIO driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-11-13 - 10:58:46)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_GPIO_TYPES_H
#define _OC_GPIO_TYPES_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// GeneraL
#include <oc_gpio_lld_types.h>               // access to types depended of the hardware

// Drivers

// Libraries
#include <oc_synch.h>                          // signals for configuration

// Others

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

/**
 * The type is for protection for configure special pins, like JTAG and NMI pins
 */
typedef enum
{
    oC_GPIO_Protection_DontUnlockProtectedPins ,    /**< Do not configure special pins */
    oC_GPIO_Protection_UnlockProtectedPins          /**< Configure special pins */
} oC_GPIO_Protection_t;

/**
 * The type represents speed of the GPIO pins
 */
typedef enum
{
    oC_GPIO_Speed_Default ,      /**< Default value for oC_GPIO_Speed_t */
    oC_GPIO_Speed_Minimum ,             /**< Pin works with minimum speed */
    oC_GPIO_Speed_Medium ,              /**< Pin works with medium speed */
    oC_GPIO_Speed_Maximum ,             /**< Pin works with maximum speed */
    oC_GPIO_Speed_NumberOfElements  /**< Number of elements in oC_GPIO_Speed_t enum */
} oC_GPIO_Speed_t;

/**
 * @brief
 *           Type represents maximum current for GPIO pin
 */
typedef enum
{
    oC_GPIO_Current_Default ,        /**< Pin works with default current (not changed) */
    oC_GPIO_Current_Minimum ,        /**< Pin works with minimum current */
    oC_GPIO_Current_Medium ,         /**< Pin works with medium current */
    oC_GPIO_Current_Maximum          /**< Pin works with maximum current */
} oC_GPIO_Current_t;

/**
 * @brief
 *          Type represents mode of works for gpio pin.
 */
typedef enum
{
    oC_GPIO_Mode_Default ,       /**< Pin works in default mode (not changed) */
    oC_GPIO_Mode_Input ,         /**< Pin works as input */
    oC_GPIO_Mode_Output ,        /**< Pin works as output */
    oC_GPIO_Mode_Alternate       /**< Pin works in alternative mode */
} oC_GPIO_Mode_t;

/**
 * @brief
 *          Type represents gpio pull mode
 */
typedef enum
{
    oC_GPIO_Pull_Default ,       /**< Pin works with default pull mode (not changed) */
    oC_GPIO_Pull_Up ,            /**< Pull-up */
    oC_GPIO_Pull_Down            /**< Pull-down */
} oC_GPIO_Pull_t;

/**
 * @brief
 *          Type represents GPIO output circuit mode ( Open Drain / PushPull )
 */
typedef enum
{
    oC_GPIO_OutputCircuit_Default ,
    oC_GPIO_OutputCircuit_OpenDrain ,
    oC_GPIO_OutputCircuit_PushPull
} oC_GPIO_OutputCircuit_t;

/**
 * @brief
 *          Type represents GPIO interrupt trigger source
 */
typedef enum
{
    oC_GPIO_IntTrigger_Default ,     /**< Pin works in default interrupt mode (not changed) */
    oC_GPIO_IntTrigger_Off ,         /**< Interrupts on this pin are turned off */
    oC_GPIO_IntTrigger_RisingEdge ,  /**< Interrupt on rising edge */
    oC_GPIO_IntTrigger_FallingEdge , /**< Interrupt on falling edge */
    oC_GPIO_IntTrigger_BothEdges,    /**< Interrupt on both edges */
    oC_GPIO_IntTrigger_HighLevel ,   /**< Interrupt on high level */
    oC_GPIO_IntTrigger_LowLevel ,    /**< Interrupt on low level */
    oC_GPIO_IntTrigger_BothLevels    /**< Interrupt on both levels */
} oC_GPIO_IntTrigger_t;

/**
 *  The type represents link to the GPIO Pin
 */
typedef struct
{
    oC_GPIO_Port_t                      Port;   /**< The port of the pin */
    oC_GPIO_Pin_t                       Pin;    /**< The pin number */
} oC_GPIO_PinLink_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     REQUIRED DRIVER TYPES
//
//========================================================================================================================================*/

/**
 *	The type represents configuration of the driver. It is given as a parameter to the driver configure function
 */
typedef struct
{
    oC_GPIO_PinLink_t *                 PinLink;            /**< Port and pin(s) to configure (To select more than 1 pin, just use 'OR' operations ) */
    oC_GPIO_Mode_t                      Mode;               /**< Mode ( Input / Output / Alternate ) for configured pins */
    oC_GPIO_Pull_t                      Pull;               /**< Pull ( Up / Down ) */
    oC_GPIO_IntTrigger_t                Trigger;            /**< Interrupt trigger event */
    oC_GPIO_Speed_t                     Speed;              /**< Speed for pins (Minimum / Medium / Maximum ) */
    oC_GPIO_Current_t                   Current;            /**< Output current */
    oC_GPIO_OutputCircuit_t             OutputCircuit;      /**< OpenDrain / PushPull */
    oC_GPIO_Protection_t                Protection;         /**< Protection for special pins like JTAG and NMI */
} oC_GPIO_Config_t;

/**
 * The type represents context of the module. It is needed specially if module send / receive a data from a direct device
 */
typedef struct
{
    const oC_GPIO_Config_t * const      Config;            /**< Configuration of the driver (if needed) */
} oC_GPIO_Context_t ;

/**
 * The type represents configuration of the driver as a stream. It is given as a parameter to the stream driver configuration function. 
 */
typedef struct
{
    const oC_GPIO_Config_t * const      Config;			    /**< Configuration of the driver */
} oC_GPIO_StreamConfig_t;

/**
 * The type represents slot for the interrupt
 *
 * @param Config    Reference to the configuration
 */
typedef void (*oC_GPIO_TriggerSlot_t)( const oC_GPIO_PinLink_t * const PinLink );

#endif /* _OC_GPIO_TYPES_H */
