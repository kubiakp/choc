/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver_interface.h Ver 1.0.0
 *
 *    @file       oc_gpio.h
 *
 *    @brief      File with interface functions for the GPIO driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-11-13 - 10:58:45)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_GPIO_H
#define _OC_GPIO_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// General
#include <oc_gpio_types.h>                      // types from the driver
#include <oc_gpio_defs.h>                       // access to GPIO definitions
#include <oc_gpio_lld_quick.h>
#include <oc_errors.h>
#include <oc_driver.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern oC_ErrorCode_t   oC_GPIO_Configure                   ( const oC_GPIO_Config_t * Config );
extern oC_ErrorCode_t   oC_GPIO_Unconfigure                 ( const oC_GPIO_Config_t * Config );
extern void             oC_GPIO_SetDefaultValuesForConfig   ( oC_GPIO_Config_t * Config );
extern bool             oC_GPIO_WaitForTrigger              ( const oC_GPIO_PinLink_t * const PinLink , oC_TickType_t Timeout );
extern bool             oC_GPIO_ConnectToTrigger            ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_TriggerSlot_t Slot , const void * Parameter );
extern bool             oC_GPIO_IsPinUsed                   ( const oC_GPIO_PinLink_t * const PinLink );
extern bool             oC_GPIO_IsPinCorrect                ( const oC_GPIO_PinLink_t * const PinLink );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

oC_DRIVER_DECLARE(GPIO);

#endif /* _OC_GPIO_H */
