/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on driver_interface.c Ver 1.0.0
 *
 *    @file       oc_gpio.h
 *
 *    @brief      File with interface of the GPIO driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-11-13 - 10:58:45)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// Drivers
#include <oc_drivers.h>             // access to all drivers interfaces

// LLD
#include <oc_gpio_lld.h>            // access to lld interface layer

// Libraries
#include <oc_dlist.h>               // dynamic list for the interrupts and signals
#include <oc_synch.h>                 // signals for the interrupts

// Others
#include <oc_os.h>
#include <oc_assert.h>
#include <oc_driver.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/

#define VERSION oC_DRIVER_VERSION(1,0,0,0)

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

/**
 * The structure for handling the interrupt
 */
typedef struct
{
    oC_Signal_t *               Signal;         ///< Signal of the interrupt
    const oC_GPIO_PinLink_t *   PinLink;        ///< Pin link of the interrupt
} _InterruptContainer_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

/**
 * The container for the driver
 */
static struct
{
    oC_DLIST_t*         InterruptsList;             ///< List of the configured interrupts (each element is _InterruptContainer_t)
    oC_DLIST_t*         UsedList;                   ///< List of the configured pins ( each element is oC_GPIO_PinLink_t )
} GPIO;

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static inline bool       _AddInterruptToList            ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_IntTrigger_t Trigger );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 *  Function called before system starts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_TurnOnDriver( void )
{
    oC_GPIO_LLDDriverInit();

    GPIO.InterruptsList     = oC_DLIST_New();
    GPIO.UsedList           = oC_DLIST_New();

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 *  Function called before system restarts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_TurnOffDriver( void )
{
    oC_GPIO_LLDDriverDeinit( );

    oC_DLIST_Delete( GPIO.InterruptsList );
    oC_DLIST_Delete( GPIO.UsedList );

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * The function for configuring the driver.
 *
 * @param Config        Reference to the configuration structure
 *
 * @return Code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_Configure( const oC_GPIO_Config_t * Config )
{

    oC_ErrorCode_t result;

    //
    //	Checking parameters
    //
    oC_ASSERT( Config != oC_NULL );

    oC_GPIO_InitializeConfig( Config );

    //
    // CHECKING CONFIGURATION CONDITIONS
    //
    if(false   == oC_GPIO_IsPinCorrect               ( Config->PinLink) )
    {
        result = oC_ErrorCode_GPIOPinIncorrect;
    }
#if false == oC_GPIO_CFG_ALLOW_TO_RECONFIGURE
    else if  ( true == oC_GPIO_IsPinUsed ( Config->PinLink ))
    {
        result = oC_ErrorCode_GPIOPinIsUsed;
    }
#endif
    else if(false  == oC_GPIO_SetMode               ( Config->PinLink , Config->Mode            ))
    {
        result = oC_ErrorCode_GPIOModeConfigureError;
    }
    else if(false  == oC_GPIO_SetOutputCircuit      ( Config->PinLink , Config->OutputCircuit   ))
    {
         result = oC_ErrorCode_GPIOOutputCircuitConfigureError;
    }
    else if(false  == oC_GPIO_SetPull               ( Config->PinLink , Config->Pull            ))
    {
         result = oC_ErrorCode_GPIOPullConfigureError;
    }
    else if(false  == oC_GPIO_SetCurrent            ( Config->PinLink , Config->Current         ))
    {
        result = oC_ErrorCode_GPIOCurrentConfigureError;
    }
    else if(false  == oC_GPIO_SetSpeed              ( Config->PinLink , Config->Speed           ))
    {
        result = oC_ErrorCode_GPIOSpeedConfigureError;
    }
    else if(false  == oC_GPIO_SetInterrupt          ( Config->PinLink , Config->Trigger         ))
    {
        result = oC_ErrorCode_GPIOInterruptConfigureError;
    }
    else if(false  == _AddInterruptToList          ( Config->PinLink , Config->Trigger         ))
    {
        result = oC_ErrorCode_GPIOADDInterruptToListError;
    }
    else
    {
        //
        //	AFTER SUCCES CONFIGURATION CONDITIONS
        //
        oC_GPIO_FinishConfig( Config );
        oC_GPIO_AddPinToUsed( Config->PinLink );

        result = oC_ErrorCode_None;
    }

    if(result != oC_ErrorCode_None)
    {
        oC_GPIO_FinishFailedConfig( Config );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Removes configuration from configured list. The function is called before software system restart.
 *
 * @param Config        Reference to the configuration structure to remove from list
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_Unconfigure( const oC_GPIO_Config_t * Config )
{
    oC_UNUSED_ARG( Config );
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * The function to send data via the driver as a stream
 *
 * @param Config        Reference to the config structure
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_GPIO_SendViaStream( const oC_GPIO_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int sent_bytes = 0;
	
	oC_UNUSED_ARG( Config );
	oC_UNUSED_ARG( Data );
	oC_UNUSED_ARG( Length );
	oC_UNUSED_ARG( Timeout_ms );

    return sent_bytes;
}

//==========================================================================================================================================
/**
 * The function to receive data via the driver as a stream
 *
 * @param Config        Reference to the config structure
 * @param Data
 * @param Length
 * @param Timeout_ms
 *
 * @return
 */
//==========================================================================================================================================
int oC_GPIO_ReceiveViaStream( const oC_GPIO_Config_t * Config , char * Data , uint32_t Length , uint32_t Timeout_ms)
{
    int received_bytes = 0;
	
	// Checking parameters
    oC_UNUSED_ARG( Config );
    oC_UNUSED_ARG( Data );
    oC_UNUSED_ARG( Length );
	oC_UNUSED_ARG( Timeout_ms );
	oC_ASSERT( Data    != NULL );

    return received_bytes;
}

//==========================================================================================================================================
/**
 * Sets default values in the configuration structure
 *
 * @param Config        Reference to configuration
 */
//==========================================================================================================================================
void oC_GPIO_SetDefaultValuesForConfig( oC_GPIO_Config_t * Config )
{
    if ( Config )
    {
        Config->Current         = oC_GPIO_Current_Default;
        Config->Mode            = oC_GPIO_Mode_Default;
        Config->OutputCircuit   = oC_GPIO_OutputCircuit_Default;
        Config->Protection      = oC_GPIO_Protection_DontUnlockProtectedPins;
        Config->Pull            = oC_GPIO_Pull_Default;
        Config->Speed           = oC_GPIO_Speed_Default;
        Config->Trigger         = oC_GPIO_IntTrigger_Default;
        Config->PinLink         = oC_GPIO_PinLink_NOT_USED;
    }
}

//==========================================================================================================================================
/**
 * Checks if the pin(or pins) is/are used (was configured). If at least one of the given pins is used, then function returns true.
 *
 * @param PinLink           reference to the pin to check
 *
 * @return true if used
 */
//==========================================================================================================================================
bool oC_GPIO_IsPinUsed( const oC_GPIO_PinLink_t * const PinLink )
{
    bool result = false;

    oC_ASSERT( PinLink != NULL ); // Pin is not given

    foreach( oC_GPIO_PinLink_t* , pinLink , GPIO.UsedList )
    {
        if ( pinLink->Port == PinLink->Port && ((pinLink->Pin & PinLink->Pin) != 0) )
        {
            result = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Checks if the pin is correct
 *
 * @param PinLink           reference to the pin link structure
 *
 * @return true if the pin is correct
 */
//==========================================================================================================================================
bool oC_GPIO_IsPinCorrect( const oC_GPIO_PinLink_t * const PinLink )
{
    return      ( NULL != PinLink ) &&
                ( PinLink->Port < oC_GPIO_Port_NumberOfPorts ) &&
                ( PinLink->Pin > 0  ) &&
                ( PinLink->Pin <= oC_GPIO_Pin_All )
                ;
}

//==========================================================================================================================================
/**
 * Waiting for interrupt on the pin.
 *
 * @param PinLink           reference to the pin link structure
 * @param Timeout           Maximum time for the event
 *
 * @return false if interrupt not occur in the Timeout time.
 */
//==========================================================================================================================================
bool oC_GPIO_WaitForTrigger( const oC_GPIO_PinLink_t * const PinLink , oC_TickType_t Timeout )
{
    bool result = false;

    oC_DLIST_foreach( _InterruptContainer_t , interrupt , GPIO.InterruptsList )
    {
        if ( (PinLink->Port == interrupt.PinLink->Port) && ((PinLink->Pin & interrupt.PinLink->Pin) != 0 ) )
        {
            result = oC_Signal_WaitForSignal( interrupt.Signal , Timeout);
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Waiting for interrupt on the pin.
 *
 * @param PinLink           reference to the pin link structure
 * @param Slot              Slot to call when interrupt occur
 * @param Parameter         Parameter which will be given to the slot, when signal occurs
 *
 * @return false if pin link not configured
 */
//==========================================================================================================================================
bool oC_GPIO_ConnectToTrigger( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_TriggerSlot_t Slot , const void * Parameter )
{
    bool result = false;

    oC_DLIST_foreach( _InterruptContainer_t , interrupt , GPIO.InterruptsList )
    {
        if ( (PinLink->Port == interrupt.PinLink->Port) && ((PinLink->Pin & interrupt.PinLink->Pin) != 0 ) )
        {
            oC_Signal_Connect( interrupt.Signal , Slot , Parameter );
            result  = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Adds pin to used list
 *
 * @param PinLink               Reference to the pin
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_AddPinToUsed( const oC_GPIO_PinLink_t * const PinLink )
{
    bool result = false;

    if ( PinLink != NULL )
    {
        oC_DLIST_EmplacePushBack(GPIO.UsedList , PinLink , sizeof(oC_GPIO_PinLink_t));

        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Removes pin from used list
 *
 * @param PinLink               Reference to the pin
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_RemovePinFromUsed( const oC_GPIO_PinLink_t * const PinLink )
{
    bool result = false;

    if ( PinLink != NULL )
    {
        result = oC_DLIST_RemoveValue( GPIO.UsedList , PinLink , sizeof(oC_GPIO_PinLink_t*));
    }

    return result;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Add interrupt to list
 *
 * @param PinLink               Reference to the pin
 * @param Trigger
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool _AddInterruptToList( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_IntTrigger_t Trigger )
{
    bool result = false;

    if ( !( Trigger == oC_GPIO_IntTrigger_Off || (Trigger == oC_GPIO_IntTrigger_Default)) )
    {
        _InterruptContainer_t container;
        container.Signal                    = oC_Signal_New( PinLink );
        container.PinLink                   = PinLink;

        if ( container.Signal != NULL )
        {
            result = oC_DLIST_EmplacePushBack( GPIO.InterruptsList , &container , sizeof(_InterruptContainer_t) );
        }

        if ( !result )
        {
            oC_Signal_Delete( container.Signal );
        }
    }
    else
    {
        result  = true;
    }

    return result;
}

/*==========================================================================================================================================
//
//     INTERRUPTS HADNLE
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Handler for the pins interrupt.
 *
 * @param Port          Port of the interrupt
 * @param Pins          Pins which trigger the interrupt
 *
 * @return true if task yield
 */
//==========================================================================================================================================
bool oC_GPIO_InterruptCallbacksHandler( oC_GPIO_Port_t Port , oC_GPIO_Pin_t Pins )
{
    bool higherPriorityTaskWoken = false;

    foreach( _InterruptContainer_t , container , GPIO.InterruptsList )
    {
        if ( (container.PinLink->Pin & Pins) && Port == container.PinLink->Port)
        {
            oC_Signal_EmitSignal( container.Signal , &higherPriorityTaskWoken );
            break;
        }
    }

    return higherPriorityTaskWoken;
}

/** ****************************************************************************************************************************************
 * The section with driver definition
 */
#define _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________

//==========================================================================================================================================
/**
 * The structure with driver interface
 */
//==========================================================================================================================================
static const oC_Driver_Interface_t Interface = {
                                             .RequiredBootLevel         = oC_Boot_Level_RequireMalloc ,
                                             .TurnOnFunction            = oC_GPIO_TurnOnDriver ,
                                             .TurnOffFunction           = oC_GPIO_TurnOffDriver ,
                                             .ConfigureFunction         = oC_GPIO_Configure ,
                                             .UnconfigureFunction       = oC_GPIO_Unconfigure ,
                                             .IsTurnedOnFunction        = NULL ,
                                             .SendViaStreamFunction     = NULL ,
                                             .ReceiveViaStreamFunction  = NULL
};

oC_DRIVER_DEFINE(GPIO , "gpio" , VERSION , Interface );

/* END OF SECTION */
#undef  _________________________________________DRIVER_DEFINITION_SECTION__________________________________________________________________

