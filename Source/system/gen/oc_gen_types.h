/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_core_types.h
 *
 *    @brief      The file contains types for the core of the system.
 *
 *    @author     Patryk Kubiak - (Created on: 7 pa� 2014 14:41:58)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef OC_CORE_TYPES_H_
#define OC_CORE_TYPES_H_

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INCLUDES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Core
#include <oc_general_cfg.h>
#include <oc_programs_cfg.h>
#include <oc_gen_macros.h>
#include <stdint.h>
#include <FreeRTOSConfig.h>
#include <FreeRTOS.h>
#include <task.h>

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    CONSTS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define oC_TickType_Maximum     portMAX_DELAY

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    TYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if oC_USE_RTOS

/**
 * Type for storing count of the ticks.
 */
typedef TickType_t oC_TickType_t;

/**
 * The type represents priorities of the tasks
 */
typedef enum
{
    oC_TaskPriority_IdleTask            = tskIDLE_PRIORITY ,            /**< The priority of the idle task (lowest) */
    oC_TaskPriority_1 ,
    oC_TaskPriority_2 ,
    oC_TaskPriority_3 ,
    oC_TaskPriority_4 ,
    oC_TaskPriority_5 ,
    oC_TaskPriority_6 ,
    oC_TaskPriority_7 ,
    oC_TaskPriority_8 ,
    oC_TaskPriority_9 ,
    oC_TaskPriority_10 ,
    oC_TaskPriority_11 ,
    oC_TaskPriority_12 ,
    oC_TaskPriority_13 ,
    oC_TaskPriority_14 ,
    oC_TaskPriority_15 ,
    oC_TaskPriority_16 ,
    oC_TaskPriority_17 ,
    oC_TaskPriority_18 ,
    oC_TaskPriority_19 ,
    oC_TaskPriority_20 ,
    oC_TaskPriority_21 ,
    oC_TaskPriority_22 ,
    oC_TaskPriority_23 ,
    oC_TaskPriority_24 ,
    oC_TaskPriority_25 ,
    oC_TaskPriority_26 ,
    oC_TaskPriority_27 ,
    oC_TaskPriority_28 ,
    oC_TaskPriority_29 ,
    oC_TaskPriority_30 ,
    oC_TaskPriority_31 ,
    oC_TaskPriority_MaxPriority         = configMAX_PRIORITIES          /**< The maximum priority (highest) */
} oC_TaskPriority_t;

/**
 * Size of the task stack
 */
typedef enum
{
    oC_StackSize_Minimum                = configMINIMAL_STACK_SIZE  ,   /**< The minimum size of the stack */
} oC_StackSize_t;

/**
 * The status of the program
 */
typedef enum
{
    oC_ProgramStatus_Ready ,            /**< A program is ready to the run */
    oC_ProgramStatus_Running ,          /**< A program is already running */
    oC_ProgramStatus_Finished           /**< A program has been finished */
} oC_ProgramStatus_t;

/**
 * The type represents id list of the programs contained in the system
 */
typedef enum
{
    oC_SYSTEM_PROGRAMS_LIST( oC_PROGRAM_ADD_PROGRAM_TO_ENUM_LIST , oC_PROGRAM_DONT_USE )
    oC_USER_PROGRAMS_LIST( oC_PROGRAM_ADD_PROGRAM_TO_ENUM_LIST , oC_PROGRAM_DONT_USE )
    oC_ProgramID_NumberOfPrograms
} oC_ProgramID_t;

#else
typedef bool oC_Semaphore_t;
typedef bool oC_Mutex_t;
#endif

/**
 * The type represents function that is main for the programs.
 * @param argc  Counter of the arguments given to the program
 * @param argv  List of the program arguments
 * @return result of the program execution (0 if success, otherwise code of the error from the error.h)
 */
typedef int (*oC_ProgramFunction_t)( int argc, char** argv );

/**
 * Type to store frequency
 */
typedef uint64_t oC_Frequency_t;

#if oC_USE_RTOS

typedef TaskFunction_t oC_TaskFunction_t;   /**< The type represents function of the tasks */
typedef TaskHandle_t oC_TaskHandle_t;       /**< The type represents handle of the task */

#endif

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MACROS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Macro for set own frequency in MHz
 */
#define oC_Frequency_MHz(_MHz_)              ( ( _MHz_ ) * 1000000UL )
/**
 * Macro for set own frequency in kHz
 */
#define oC_Frequency_kHz(_kHz_)              ( ( _kHz_ ) * 1000UL )
/**
 * Macro for set own frequency in Hz
 */
#define oC_Frequency_Hz(_Hz_)                ( ( _Hz_ ) * 1UL )

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    EXTERN VARIABLES DEFINITIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    FUNCTION PROTOTYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INLINE FUNCTIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#endif /* OC_CORE_TYPES_H_ */
