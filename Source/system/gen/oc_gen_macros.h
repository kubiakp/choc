/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_os_macros.h
 *
 *    @brief      The file with definitions of general macros.
 *
 *    @author     Patryk Kubiak - (Created on: 3 pa� 2014 17:31:44)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef OC_GEN_MACROS_H_
#define OC_GEN_MACROS_H_

#include <stdio.h>
#include <oc_debug.h>

/** ****************************************************************************************************************************************
 * The section contains macros that helps in concatenating parameters to receive one macro (or type) name.
 *
 * @example "Concatenating parameters"
 *      int LoveSugar = 0;
 *
 *      oC_ONE_WORD_FROM_2( Love , Sugar ) = 11;
 *
 *      printf("%d\n" , LoveSugar );
 */
#define _________________________________________CONCATENATE_SECTION________________________________________________________________________

/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
#define _oC_ONE_WORD_FROM_2( A , B )                          A##B
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
#define _oC_ONE_WORD_FROM_3( A , B , C)                       A##B##C
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
#define _oC_ONE_WORD_FROM_4( A , B , C , D)                   A##B##C##D
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
#define _oC_ONE_WORD_FROM_5( A , B , C , D , E)               A##B##C##D##E
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
#define _oC_ONE_WORD_FROM_6( A , B , C , D , E , F)           A##B##C##D##E##F
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
#define _oC_ONE_WORD_FROM_7( A , B , C , D , E , F , G)       A##B##C##D##E##F##G
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
#define _oC_ONE_WORD_FROM_8( A , B , C , D , E , F , G , H)   A##B##C##D##E##F##G##H

/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_ONE_WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
#define oC_ONE_WORD_FROM_2( A , B )                           _oC_ONE_WORD_FROM_2( A , B )
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_ONE_WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
#define oC_ONE_WORD_FROM_3( A , B , C)                        _oC_ONE_WORD_FROM_3( A , B , C)
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_ONE_WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
#define oC_ONE_WORD_FROM_4( A , B , C , D)                    _oC_ONE_WORD_FROM_4( A , B , C , D)
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_ONE_WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
#define oC_ONE_WORD_FROM_5( A , B , C , D , E)                _oC_ONE_WORD_FROM_5( A , B , C , D , E)
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_ONE_WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
#define oC_ONE_WORD_FROM_6( A , B , C , D , E, F)             _oC_ONE_WORD_FROM_6( A , B , C , D , E , F)
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_ONE_WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
#define oC_ONE_WORD_FROM_7( A , B , C , D , E, F , G)         _oC_ONE_WORD_FROM_7( A , B , C , D , E , F , G)
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_ONE_WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
#define oC_ONE_WORD_FROM_8( A , B , C , D , E, F , G , H)     _oC_ONE_WORD_FROM_8( A , B , C , D , E , F , G , H)

/* END OF SECTION */
#undef  _________________________________________CONCATENATE_SECTION________________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with definitions which helps in types definitions
 */
#define _________________________________________TYPES_DEFINITION_HELPERS_SECTION___________________________________________________________

/**
 * The macro returns enum value name in the ChocoOS standard.
 *
 * @param MODULE_NAME           Name of a module to get a enum value
 * @param TYPE_NAME             Part of the enum type name (without prefix, and _t suffix)
 * @param VALUE_NAME            Name of a value
 */
#define oC_TYPE_HELPER_GET_CHOCOS_ENUM_VALUE_NAME( MODULE_NAME , TYPE_NAME , VALUE_NAME )   oC_ONE_WORD_FROM_6( oC_ , MODULE_NAME , _ , TYPE_NAME , _ , VALUE_NAME )
/**
 *
 * @param MODULE_NAME           Shortcut name of a module
 * @param TYPE_NAME             Real name of the type in the module
 */
#define oC_TYPE_HELPER_GET_CHOCOS_ENUM_TYPE_NAME( MODULE_NAME , TYPE_NAME )         oC_ONE_WORD_FROM_5( oC_ , MODULE_NAME , _ , TYPE_NAME  , _t )

/* END OF SECTION */
#undef  _________________________________________TYPES_DEFINITION_HELPERS_SECTION___________________________________________________________

/** ****************************************************************************************************************************************
 * The section contains macros, that helps in driver development.
 */
#define _________________________________________DRIVER_HELPERS_MACROS_SECTION______________________________________________________________

/* END OF SECTION */
#undef  _________________________________________DRIVER_HELPERS_MACROS_SECTION______________________________________________________________


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MACRO HELPERS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define IN
#define OUT

/**
 * The macros for concatenate macro names
 */
#define oC__CAT_2( A , B )                          A##B
#define oC__CAT_3( A , B , C)                       A##B##C
#define oC__CAT_4( A , B , C , D)                   A##B##C##D
#define oC__CAT_5( A , B , C , D , E)               A##B##C##D##E
#define oC__CAT_6( A , B , C , D , E , F)           A##B##C##D##E##F
#define oC__CAT_7( A , B , C , D , E , F , G)       A##B##C##D##E##F##G
#define oC__CAT_8( A , B , C , D , E , F , G , H)   A##B##C##D##E##F##G##H

#define oC_CAT_2( A , B )                       oC__CAT_2( A , B )
#define oC_CAT_3( A , B , C)                    oC__CAT_3( A , B , C)
#define oC_CAT_4( A , B , C , D)                oC__CAT_4( A , B , C , D)
#define oC_CAT_5( A , B , C , D , E)            oC__CAT_5( A , B , C , D , E)
#define oC_CAT_6( A , B , C , D , E, F)         oC__CAT_6( A , B , C , D , E , F)
#define oC_CAT_7( A , B , C , D , E, F , G)     oC__CAT_7( A , B , C , D , E , F , G)
#define oC_CAT_8( A , B , C , D , E, F , G , H) oC__CAT_8( A , B , C , D , E , F , G , H)

//
//  BITS
//
#define oC_BIT_N( _N_ )                                 ( 1 << (_N_) )
#define oC_BIT_0                                        oC_BIT_N( 0 )
#define oC_BIT_1                                        oC_BIT_N( 1 )
#define oC_BIT_2                                        oC_BIT_N( 2 )
#define oC_BIT_3                                        oC_BIT_N( 3 )
#define oC_BIT_4                                        oC_BIT_N( 4 )
#define oC_BIT_5                                        oC_BIT_N( 5 )
#define oC_BIT_6                                        oC_BIT_N( 6 )
#define oC_BIT_7                                        oC_BIT_N( 7 )
#define oC_BIT_8                                        oC_BIT_N( 8 )
#define oC_BIT_9                                        oC_BIT_N( 9 )
#define oC_BIT_10                                       oC_BIT_N( 10 )
#define oC_BIT_11                                       oC_BIT_N( 11 )
#define oC_BIT_12                                       oC_BIT_N( 12 )
#define oC_BIT_13                                       oC_BIT_N( 13 )
#define oC_BIT_14                                       oC_BIT_N( 14 )
#define oC_BIT_15                                       oC_BIT_N( 15 )
#define oC_BIT_16                                       oC_BIT_N( 16 )
#define oC_BIT_17                                       oC_BIT_N( 17 )
#define oC_BIT_18                                       oC_BIT_N( 18 )
#define oC_BIT_19                                       oC_BIT_N( 19 )
#define oC_BIT_20                                       oC_BIT_N( 20 )
#define oC_BIT_21                                       oC_BIT_N( 21 )
#define oC_BIT_22                                       oC_BIT_N( 22 )
#define oC_BIT_23                                       oC_BIT_N( 23 )
#define oC_BIT_24                                       oC_BIT_N( 24 )
#define oC_BIT_25                                       oC_BIT_N( 25 )
#define oC_BIT_26                                       oC_BIT_N( 26 )
#define oC_BIT_27                                       oC_BIT_N( 27 )
#define oC_BIT_28                                       oC_BIT_N( 28 )
#define oC_BIT_29                                       oC_BIT_N( 29 )
#define oC_BIT_30                                       oC_BIT_N( 30 )
#define oC_BIT_31                                       oC_BIT_N( 31 )

//
//  BITS MASK
//
#define oC_BITS_MASK_1                                  oC_BIT_0
#define oC_BITS_MASK_2                                  oC_BITS_MASK_1  | oC_BIT_1
#define oC_BITS_MASK_3                                  oC_BITS_MASK_2  | oC_BIT_2
#define oC_BITS_MASK_4                                  oC_BITS_MASK_3  | oC_BIT_3
#define oC_BITS_MASK_5                                  oC_BITS_MASK_4  | oC_BIT_4
#define oC_BITS_MASK_6                                  oC_BITS_MASK_5  | oC_BIT_5
#define oC_BITS_MASK_7                                  oC_BITS_MASK_6  | oC_BIT_6
#define oC_BITS_MASK_8                                  oC_BITS_MASK_7  | oC_BIT_7
#define oC_BITS_MASK_9                                  oC_BITS_MASK_8  | oC_BIT_8
#define oC_BITS_MASK_10                                 oC_BITS_MASK_9  | oC_BIT_9
#define oC_BITS_MASK_11                                 oC_BITS_MASK_10 | oC_BIT_10
#define oC_BITS_MASK_12                                 oC_BITS_MASK_11 | oC_BIT_11
#define oC_BITS_MASK_13                                 oC_BITS_MASK_12 | oC_BIT_12
#define oC_BITS_MASK_14                                 oC_BITS_MASK_13 | oC_BIT_13
#define oC_BITS_MASK_15                                 oC_BITS_MASK_14 | oC_BIT_14
#define oC_BITS_MASK_16                                 oC_BITS_MASK_15 | oC_BIT_15
#define oC_BITS_MASK_17                                 oC_BITS_MASK_16 | oC_BIT_16
#define oC_BITS_MASK_18                                 oC_BITS_MASK_17 | oC_BIT_17
#define oC_BITS_MASK_19                                 oC_BITS_MASK_18 | oC_BIT_18
#define oC_BITS_MASK_20                                 oC_BITS_MASK_19 | oC_BIT_19
#define oC_BITS_MASK_21                                 oC_BITS_MASK_20 | oC_BIT_20
#define oC_BITS_MASK_22                                 oC_BITS_MASK_21 | oC_BIT_21
#define oC_BITS_MASK_23                                 oC_BITS_MASK_22 | oC_BIT_22
#define oC_BITS_MASK_24                                 oC_BITS_MASK_23 | oC_BIT_23
#define oC_BITS_MASK_25                                 oC_BITS_MASK_24 | oC_BIT_24
#define oC_BITS_MASK_26                                 oC_BITS_MASK_25 | oC_BIT_25
#define oC_BITS_MASK_27                                 oC_BITS_MASK_26 | oC_BIT_26
#define oC_BITS_MASK_28                                 oC_BITS_MASK_27 | oC_BIT_27
#define oC_BITS_MASK_29                                 oC_BITS_MASK_28 | oC_BIT_28
#define oC_BITS_MASK_30                                 oC_BITS_MASK_29 | oC_BIT_29
#define oC_BITS_MASK_31                                 oC_BITS_MASK_30 | oC_BIT_30
#define oC_BITS_MASK_32                                 oC_BITS_MASK_31 | oC_BIT_31

#define oC_BITS_0_TO_0                                  ( ( oC_BITS_MASK_1 ) << 0  )
#define oC_BITS_1_TO_1                                  ( ( oC_BITS_MASK_1 ) << 1  )
#define oC_BITS_2_TO_2                                  ( ( oC_BITS_MASK_1 ) << 2  )
#define oC_BITS_3_TO_3                                  ( ( oC_BITS_MASK_1 ) << 3  )
#define oC_BITS_4_TO_4                                  ( ( oC_BITS_MASK_1 ) << 4  )
#define oC_BITS_5_TO_5                                  ( ( oC_BITS_MASK_1 ) << 5  )
#define oC_BITS_6_TO_6                                  ( ( oC_BITS_MASK_1 ) << 6  )
#define oC_BITS_7_TO_7                                  ( ( oC_BITS_MASK_1 ) << 7  )
#define oC_BITS_8_TO_8                                  ( ( oC_BITS_MASK_1 ) << 8  )
#define oC_BITS_9_TO_9                                  ( ( oC_BITS_MASK_1 ) << 9  )
#define oC_BITS_10_TO_10                                ( ( oC_BITS_MASK_1 ) << 10 )
#define oC_BITS_11_TO_11                                ( ( oC_BITS_MASK_1 ) << 11 )
#define oC_BITS_12_TO_12                                ( ( oC_BITS_MASK_1 ) << 12 )
#define oC_BITS_13_TO_13                                ( ( oC_BITS_MASK_1 ) << 13 )
#define oC_BITS_14_TO_14                                ( ( oC_BITS_MASK_1 ) << 14 )
#define oC_BITS_15_TO_15                                ( ( oC_BITS_MASK_1 ) << 15 )
#define oC_BITS_16_TO_16                                ( ( oC_BITS_MASK_1 ) << 16 )
#define oC_BITS_17_TO_17                                ( ( oC_BITS_MASK_1 ) << 17 )
#define oC_BITS_18_TO_18                                ( ( oC_BITS_MASK_1 ) << 18 )
#define oC_BITS_19_TO_19                                ( ( oC_BITS_MASK_1 ) << 19 )
#define oC_BITS_20_TO_20                                ( ( oC_BITS_MASK_1 ) << 20 )
#define oC_BITS_21_TO_21                                ( ( oC_BITS_MASK_1 ) << 21 )
#define oC_BITS_22_TO_22                                ( ( oC_BITS_MASK_1 ) << 22 )
#define oC_BITS_23_TO_23                                ( ( oC_BITS_MASK_1 ) << 23 )
#define oC_BITS_24_TO_24                                ( ( oC_BITS_MASK_1 ) << 24 )
#define oC_BITS_25_TO_25                                ( ( oC_BITS_MASK_1 ) << 25 )
#define oC_BITS_26_TO_26                                ( ( oC_BITS_MASK_1 ) << 26 )
#define oC_BITS_27_TO_27                                ( ( oC_BITS_MASK_1 ) << 27 )
#define oC_BITS_28_TO_28                                ( ( oC_BITS_MASK_1 ) << 28 )
#define oC_BITS_29_TO_29                                ( ( oC_BITS_MASK_1 ) << 29 )
#define oC_BITS_30_TO_30                                ( ( oC_BITS_MASK_1 ) << 30 )
#define oC_BITS_31_TO_31                                ( ( oC_BITS_MASK_1 ) << 31 )

#define  oC_BITS_0_TO_1                                 ( ( oC_BITS_MASK_2 ) << 0  )
#define  oC_BITS_1_TO_2                                 ( ( oC_BITS_MASK_2 ) << 1  )
#define  oC_BITS_2_TO_3                                 ( ( oC_BITS_MASK_2 ) << 2  )
#define  oC_BITS_3_TO_4                                 ( ( oC_BITS_MASK_2 ) << 3  )
#define  oC_BITS_4_TO_5                                 ( ( oC_BITS_MASK_2 ) << 4  )
#define  oC_BITS_5_TO_6                                 ( ( oC_BITS_MASK_2 ) << 5  )
#define  oC_BITS_6_TO_7                                 ( ( oC_BITS_MASK_2 ) << 6  )
#define  oC_BITS_7_TO_8                                 ( ( oC_BITS_MASK_2 ) << 7  )
#define  oC_BITS_8_TO_9                                 ( ( oC_BITS_MASK_2 ) << 8  )
#define oC_BITS_9_TO_10                                 ( ( oC_BITS_MASK_2 ) << 9  )
#define oC_BITS_10_TO_11                                ( ( oC_BITS_MASK_2 ) << 10 )
#define oC_BITS_11_TO_12                                ( ( oC_BITS_MASK_2 ) << 11 )
#define oC_BITS_12_TO_13                                ( ( oC_BITS_MASK_2 ) << 12 )
#define oC_BITS_13_TO_14                                ( ( oC_BITS_MASK_2 ) << 13 )
#define oC_BITS_14_TO_15                                ( ( oC_BITS_MASK_2 ) << 14 )
#define oC_BITS_15_TO_16                                ( ( oC_BITS_MASK_2 ) << 15 )
#define oC_BITS_16_TO_17                                ( ( oC_BITS_MASK_2 ) << 16 )
#define oC_BITS_17_TO_18                                ( ( oC_BITS_MASK_2 ) << 17 )
#define oC_BITS_18_TO_19                                ( ( oC_BITS_MASK_2 ) << 18 )
#define oC_BITS_19_TO_20                                ( ( oC_BITS_MASK_2 ) << 19 )
#define oC_BITS_20_TO_21                                ( ( oC_BITS_MASK_2 ) << 20 )
#define oC_BITS_21_TO_22                                ( ( oC_BITS_MASK_2 ) << 21 )
#define oC_BITS_22_TO_23                                ( ( oC_BITS_MASK_2 ) << 22 )
#define oC_BITS_23_TO_24                                ( ( oC_BITS_MASK_2 ) << 23 )
#define oC_BITS_24_TO_25                                ( ( oC_BITS_MASK_2 ) << 24 )
#define oC_BITS_25_TO_26                                ( ( oC_BITS_MASK_2 ) << 25 )
#define oC_BITS_26_TO_27                                ( ( oC_BITS_MASK_2 ) << 26 )
#define oC_BITS_27_TO_28                                ( ( oC_BITS_MASK_2 ) << 27 )
#define oC_BITS_28_TO_29                                ( ( oC_BITS_MASK_2 ) << 28 )
#define oC_BITS_29_TO_30                                ( ( oC_BITS_MASK_2 ) << 29 )
#define oC_BITS_30_TO_31                                ( ( oC_BITS_MASK_2 ) << 30 )

#define  oC_BITS_0_TO_2                                 ( ( oC_BITS_MASK_3 ) << 0  )
#define  oC_BITS_1_TO_3                                 ( ( oC_BITS_MASK_3 ) << 1  )
#define  oC_BITS_2_TO_4                                 ( ( oC_BITS_MASK_3 ) << 2  )
#define  oC_BITS_3_TO_5                                 ( ( oC_BITS_MASK_3 ) << 3  )
#define  oC_BITS_4_TO_6                                 ( ( oC_BITS_MASK_3 ) << 4  )
#define  oC_BITS_5_TO_7                                 ( ( oC_BITS_MASK_3 ) << 5  )
#define  oC_BITS_6_TO_8                                 ( ( oC_BITS_MASK_3 ) << 6  )
#define  oC_BITS_7_TO_9                                 ( ( oC_BITS_MASK_3 ) << 7  )
#define  oC_BITS_8_TO_10                                ( ( oC_BITS_MASK_3 ) << 8  )
#define  oC_BITS_9_TO_11                                ( ( oC_BITS_MASK_3 ) << 9  )
#define oC_BITS_10_TO_12                                ( ( oC_BITS_MASK_3 ) << 10 )
#define oC_BITS_11_TO_13                                ( ( oC_BITS_MASK_3 ) << 11 )
#define oC_BITS_12_TO_14                                ( ( oC_BITS_MASK_3 ) << 12 )
#define oC_BITS_13_TO_15                                ( ( oC_BITS_MASK_3 ) << 13 )
#define oC_BITS_14_TO_16                                ( ( oC_BITS_MASK_3 ) << 14 )
#define oC_BITS_15_TO_17                                ( ( oC_BITS_MASK_3 ) << 15 )
#define oC_BITS_16_TO_18                                ( ( oC_BITS_MASK_3 ) << 16 )
#define oC_BITS_17_TO_19                                ( ( oC_BITS_MASK_3 ) << 17 )
#define oC_BITS_18_TO_20                                ( ( oC_BITS_MASK_3 ) << 18 )
#define oC_BITS_19_TO_21                                ( ( oC_BITS_MASK_3 ) << 19 )
#define oC_BITS_20_TO_22                                ( ( oC_BITS_MASK_3 ) << 20 )
#define oC_BITS_21_TO_23                                ( ( oC_BITS_MASK_3 ) << 21 )
#define oC_BITS_22_TO_24                                ( ( oC_BITS_MASK_3 ) << 22 )
#define oC_BITS_23_TO_25                                ( ( oC_BITS_MASK_3 ) << 23 )
#define oC_BITS_24_TO_26                                ( ( oC_BITS_MASK_3 ) << 24 )
#define oC_BITS_25_TO_27                                ( ( oC_BITS_MASK_3 ) << 25 )
#define oC_BITS_26_TO_28                                ( ( oC_BITS_MASK_3 ) << 26 )
#define oC_BITS_27_TO_29                                ( ( oC_BITS_MASK_3 ) << 27 )
#define oC_BITS_28_TO_30                                ( ( oC_BITS_MASK_3 ) << 28 )
#define oC_BITS_29_TO_31                                ( ( oC_BITS_MASK_3 ) << 29 )

#define  oC_BITS_0_TO_3                                 ( ( oC_BITS_MASK_4 ) << 0  )
#define  oC_BITS_1_TO_4                                 ( ( oC_BITS_MASK_4 ) << 1  )
#define  oC_BITS_2_TO_5                                 ( ( oC_BITS_MASK_4 ) << 2  )
#define  oC_BITS_3_TO_6                                 ( ( oC_BITS_MASK_4 ) << 3  )
#define  oC_BITS_4_TO_7                                 ( ( oC_BITS_MASK_4 ) << 4  )
#define  oC_BITS_5_TO_8                                 ( ( oC_BITS_MASK_4 ) << 5  )
#define  oC_BITS_6_TO_9                                 ( ( oC_BITS_MASK_4 ) << 6  )
#define  oC_BITS_7_TO_10                                ( ( oC_BITS_MASK_4 ) << 7  )
#define  oC_BITS_8_TO_11                                ( ( oC_BITS_MASK_4 ) << 8  )
#define  oC_BITS_9_TO_12                                ( ( oC_BITS_MASK_4 ) << 9  )
#define oC_BITS_10_TO_13                                ( ( oC_BITS_MASK_4 ) << 10 )
#define oC_BITS_11_TO_14                                ( ( oC_BITS_MASK_4 ) << 11 )
#define oC_BITS_12_TO_15                                ( ( oC_BITS_MASK_4 ) << 12 )
#define oC_BITS_13_TO_16                                ( ( oC_BITS_MASK_4 ) << 13 )
#define oC_BITS_14_TO_17                                ( ( oC_BITS_MASK_4 ) << 14 )
#define oC_BITS_15_TO_18                                ( ( oC_BITS_MASK_4 ) << 15 )
#define oC_BITS_16_TO_19                                ( ( oC_BITS_MASK_4 ) << 16 )
#define oC_BITS_17_TO_20                                ( ( oC_BITS_MASK_4 ) << 17 )
#define oC_BITS_18_TO_21                                ( ( oC_BITS_MASK_4 ) << 18 )
#define oC_BITS_19_TO_22                                ( ( oC_BITS_MASK_4 ) << 19 )
#define oC_BITS_20_TO_23                                ( ( oC_BITS_MASK_4 ) << 20 )
#define oC_BITS_21_TO_24                                ( ( oC_BITS_MASK_4 ) << 21 )
#define oC_BITS_22_TO_25                                ( ( oC_BITS_MASK_4 ) << 22 )
#define oC_BITS_23_TO_26                                ( ( oC_BITS_MASK_4 ) << 23 )
#define oC_BITS_24_TO_27                                ( ( oC_BITS_MASK_4 ) << 24 )
#define oC_BITS_25_TO_28                                ( ( oC_BITS_MASK_4 ) << 25 )
#define oC_BITS_26_TO_29                                ( ( oC_BITS_MASK_4 ) << 26 )
#define oC_BITS_27_TO_30                                ( ( oC_BITS_MASK_4 ) << 27 )
#define oC_BITS_28_TO_31                                ( ( oC_BITS_MASK_4 ) << 28 )

#define  oC_BITS_0_TO_4                                 ( ( oC_BITS_MASK_5 ) << 0  )
#define  oC_BITS_1_TO_5                                 ( ( oC_BITS_MASK_5 ) << 1  )
#define  oC_BITS_2_TO_6                                 ( ( oC_BITS_MASK_5 ) << 2  )
#define  oC_BITS_3_TO_7                                 ( ( oC_BITS_MASK_5 ) << 3  )
#define  oC_BITS_4_TO_8                                 ( ( oC_BITS_MASK_5 ) << 4  )
#define  oC_BITS_5_TO_9                                 ( ( oC_BITS_MASK_5 ) << 5  )
#define  oC_BITS_6_TO_10                                ( ( oC_BITS_MASK_5 ) << 6  )
#define  oC_BITS_7_TO_11                                ( ( oC_BITS_MASK_5 ) << 7  )
#define  oC_BITS_8_TO_12                                ( ( oC_BITS_MASK_5 ) << 8  )
#define  oC_BITS_9_TO_13                                ( ( oC_BITS_MASK_5 ) << 9  )
#define oC_BITS_10_TO_14                                ( ( oC_BITS_MASK_5 ) << 10 )
#define oC_BITS_11_TO_15                                ( ( oC_BITS_MASK_5 ) << 11 )
#define oC_BITS_12_TO_16                                ( ( oC_BITS_MASK_5 ) << 12 )
#define oC_BITS_13_TO_17                                ( ( oC_BITS_MASK_5 ) << 13 )
#define oC_BITS_14_TO_18                                ( ( oC_BITS_MASK_5 ) << 14 )
#define oC_BITS_15_TO_19                                ( ( oC_BITS_MASK_5 ) << 15 )
#define oC_BITS_16_TO_20                                ( ( oC_BITS_MASK_5 ) << 16 )
#define oC_BITS_17_TO_21                                ( ( oC_BITS_MASK_5 ) << 17 )
#define oC_BITS_18_TO_22                                ( ( oC_BITS_MASK_5 ) << 18 )
#define oC_BITS_19_TO_23                                ( ( oC_BITS_MASK_5 ) << 19 )
#define oC_BITS_20_TO_24                                ( ( oC_BITS_MASK_5 ) << 20 )
#define oC_BITS_21_TO_25                                ( ( oC_BITS_MASK_5 ) << 21 )
#define oC_BITS_22_TO_26                                ( ( oC_BITS_MASK_5 ) << 22 )
#define oC_BITS_23_TO_27                                ( ( oC_BITS_MASK_5 ) << 23 )
#define oC_BITS_24_TO_28                                ( ( oC_BITS_MASK_5 ) << 24 )
#define oC_BITS_25_TO_29                                ( ( oC_BITS_MASK_5 ) << 25 )
#define oC_BITS_26_TO_30                                ( ( oC_BITS_MASK_5 ) << 26 )
#define oC_BITS_27_TO_31                                ( ( oC_BITS_MASK_5 ) << 27 )

#define  oC_BITS_0_TO_5                                 ( ( oC_BITS_MASK_6 ) << 0  )
#define  oC_BITS_1_TO_6                                 ( ( oC_BITS_MASK_6 ) << 1  )
#define  oC_BITS_2_TO_7                                 ( ( oC_BITS_MASK_6 ) << 2  )
#define  oC_BITS_3_TO_8                                 ( ( oC_BITS_MASK_6 ) << 3  )
#define  oC_BITS_4_TO_9                                 ( ( oC_BITS_MASK_6 ) << 4  )
#define  oC_BITS_5_TO_10                                ( ( oC_BITS_MASK_6 ) << 5  )
#define  oC_BITS_6_TO_11                                ( ( oC_BITS_MASK_6 ) << 6  )
#define  oC_BITS_7_TO_12                                ( ( oC_BITS_MASK_6 ) << 7  )
#define  oC_BITS_8_TO_13                                ( ( oC_BITS_MASK_6 ) << 8  )
#define  oC_BITS_9_TO_14                                ( ( oC_BITS_MASK_6 ) << 9  )
#define oC_BITS_10_TO_15                                ( ( oC_BITS_MASK_6 ) << 10 )
#define oC_BITS_11_TO_16                                ( ( oC_BITS_MASK_6 ) << 11 )
#define oC_BITS_12_TO_17                                ( ( oC_BITS_MASK_6 ) << 12 )
#define oC_BITS_13_TO_18                                ( ( oC_BITS_MASK_6 ) << 13 )
#define oC_BITS_14_TO_19                                ( ( oC_BITS_MASK_6 ) << 14 )
#define oC_BITS_15_TO_20                                ( ( oC_BITS_MASK_6 ) << 15 )
#define oC_BITS_16_TO_21                                ( ( oC_BITS_MASK_6 ) << 16 )
#define oC_BITS_17_TO_22                                ( ( oC_BITS_MASK_6 ) << 17 )
#define oC_BITS_18_TO_23                                ( ( oC_BITS_MASK_6 ) << 18 )
#define oC_BITS_19_TO_24                                ( ( oC_BITS_MASK_6 ) << 19 )
#define oC_BITS_20_TO_25                                ( ( oC_BITS_MASK_6 ) << 20 )
#define oC_BITS_21_TO_26                                ( ( oC_BITS_MASK_6 ) << 21 )
#define oC_BITS_22_TO_27                                ( ( oC_BITS_MASK_6 ) << 22 )
#define oC_BITS_23_TO_28                                ( ( oC_BITS_MASK_6 ) << 23 )
#define oC_BITS_24_TO_29                                ( ( oC_BITS_MASK_6 ) << 24 )
#define oC_BITS_25_TO_30                                ( ( oC_BITS_MASK_6 ) << 25 )
#define oC_BITS_26_TO_31                                ( ( oC_BITS_MASK_6 ) << 26 )

#define  oC_BITS_0_TO_6                                 ( ( oC_BITS_MASK_7 ) << 0  )
#define  oC_BITS_1_TO_7                                 ( ( oC_BITS_MASK_7 ) << 1  )
#define  oC_BITS_2_TO_8                                 ( ( oC_BITS_MASK_7 ) << 2  )
#define  oC_BITS_3_TO_9                                 ( ( oC_BITS_MASK_7 ) << 3  )
#define  oC_BITS_4_TO_10                                ( ( oC_BITS_MASK_7 ) << 4  )
#define  oC_BITS_5_TO_11                                ( ( oC_BITS_MASK_7 ) << 5  )
#define  oC_BITS_6_TO_12                                ( ( oC_BITS_MASK_7 ) << 6  )
#define  oC_BITS_7_TO_13                                ( ( oC_BITS_MASK_7 ) << 7  )
#define  oC_BITS_8_TO_14                                ( ( oC_BITS_MASK_7 ) << 8  )
#define  oC_BITS_9_TO_15                                ( ( oC_BITS_MASK_7 ) << 9  )
#define oC_BITS_10_TO_16                                ( ( oC_BITS_MASK_7 ) << 10 )
#define oC_BITS_11_TO_17                                ( ( oC_BITS_MASK_7 ) << 11 )
#define oC_BITS_12_TO_18                                ( ( oC_BITS_MASK_7 ) << 12 )
#define oC_BITS_13_TO_19                                ( ( oC_BITS_MASK_7 ) << 13 )
#define oC_BITS_14_TO_20                                ( ( oC_BITS_MASK_7 ) << 14 )
#define oC_BITS_15_TO_21                                ( ( oC_BITS_MASK_7 ) << 15 )
#define oC_BITS_16_TO_22                                ( ( oC_BITS_MASK_7 ) << 16 )
#define oC_BITS_17_TO_23                                ( ( oC_BITS_MASK_7 ) << 17 )
#define oC_BITS_18_TO_24                                ( ( oC_BITS_MASK_7 ) << 18 )
#define oC_BITS_19_TO_25                                ( ( oC_BITS_MASK_7 ) << 19 )
#define oC_BITS_20_TO_26                                ( ( oC_BITS_MASK_7 ) << 20 )
#define oC_BITS_21_TO_27                                ( ( oC_BITS_MASK_7 ) << 21 )
#define oC_BITS_22_TO_28                                ( ( oC_BITS_MASK_7 ) << 22 )
#define oC_BITS_23_TO_29                                ( ( oC_BITS_MASK_7 ) << 23 )
#define oC_BITS_24_TO_30                                ( ( oC_BITS_MASK_7 ) << 24 )
#define oC_BITS_25_TO_31                                ( ( oC_BITS_MASK_7 ) << 25 )

#define  oC_BITS_0_TO_7                                 ( ( oC_BITS_MASK_8 ) << 0  )
#define  oC_BITS_1_TO_8                                 ( ( oC_BITS_MASK_8 ) << 1  )
#define  oC_BITS_2_TO_9                                 ( ( oC_BITS_MASK_8 ) << 2  )
#define  oC_BITS_3_TO_10                                ( ( oC_BITS_MASK_8 ) << 3  )
#define  oC_BITS_4_TO_11                                ( ( oC_BITS_MASK_8 ) << 4  )
#define  oC_BITS_5_TO_12                                ( ( oC_BITS_MASK_8 ) << 5  )
#define  oC_BITS_6_TO_13                                ( ( oC_BITS_MASK_8 ) << 6  )
#define  oC_BITS_7_TO_14                                ( ( oC_BITS_MASK_8 ) << 7  )
#define  oC_BITS_8_TO_15                                ( ( oC_BITS_MASK_8 ) << 8  )
#define  oC_BITS_9_TO_16                                ( ( oC_BITS_MASK_8 ) << 9  )
#define oC_BITS_10_TO_17                                ( ( oC_BITS_MASK_8 ) << 10 )
#define oC_BITS_11_TO_18                                ( ( oC_BITS_MASK_8 ) << 11 )
#define oC_BITS_12_TO_19                                ( ( oC_BITS_MASK_8 ) << 12 )
#define oC_BITS_13_TO_20                                ( ( oC_BITS_MASK_8 ) << 13 )
#define oC_BITS_14_TO_21                                ( ( oC_BITS_MASK_8 ) << 14 )
#define oC_BITS_15_TO_22                                ( ( oC_BITS_MASK_8 ) << 15 )
#define oC_BITS_16_TO_23                                ( ( oC_BITS_MASK_8 ) << 16 )
#define oC_BITS_17_TO_24                                ( ( oC_BITS_MASK_8 ) << 17 )
#define oC_BITS_18_TO_25                                ( ( oC_BITS_MASK_8 ) << 18 )
#define oC_BITS_19_TO_26                                ( ( oC_BITS_MASK_8 ) << 19 )
#define oC_BITS_20_TO_27                                ( ( oC_BITS_MASK_8 ) << 20 )
#define oC_BITS_21_TO_28                                ( ( oC_BITS_MASK_8 ) << 21 )
#define oC_BITS_22_TO_29                                ( ( oC_BITS_MASK_8 ) << 22 )
#define oC_BITS_23_TO_30                                ( ( oC_BITS_MASK_8 ) << 23 )
#define oC_BITS_24_TO_31                                ( ( oC_BITS_MASK_8 ) << 24 )

#define  oC_BITS_0_TO_8                                 ( ( oC_BITS_MASK_9 ) << 0  )
#define  oC_BITS_1_TO_9                                 ( ( oC_BITS_MASK_9 ) << 1  )
#define  oC_BITS_2_TO_10                                ( ( oC_BITS_MASK_9 ) << 2  )
#define  oC_BITS_3_TO_11                                ( ( oC_BITS_MASK_9 ) << 3  )
#define  oC_BITS_4_TO_12                                ( ( oC_BITS_MASK_9 ) << 4  )
#define  oC_BITS_5_TO_13                                ( ( oC_BITS_MASK_9 ) << 5  )
#define  oC_BITS_6_TO_14                                ( ( oC_BITS_MASK_9 ) << 6  )
#define  oC_BITS_7_TO_15                                ( ( oC_BITS_MASK_9 ) << 7  )
#define  oC_BITS_8_TO_16                                ( ( oC_BITS_MASK_9 ) << 8  )
#define  oC_BITS_9_TO_17                                ( ( oC_BITS_MASK_9 ) << 9  )
#define oC_BITS_10_TO_18                                ( ( oC_BITS_MASK_9 ) << 10 )
#define oC_BITS_11_TO_19                                ( ( oC_BITS_MASK_9 ) << 11 )
#define oC_BITS_12_TO_20                                ( ( oC_BITS_MASK_9 ) << 12 )
#define oC_BITS_13_TO_21                                ( ( oC_BITS_MASK_9 ) << 13 )
#define oC_BITS_14_TO_22                                ( ( oC_BITS_MASK_9 ) << 14 )
#define oC_BITS_15_TO_23                                ( ( oC_BITS_MASK_9 ) << 15 )
#define oC_BITS_16_TO_24                                ( ( oC_BITS_MASK_9 ) << 16 )
#define oC_BITS_17_TO_25                                ( ( oC_BITS_MASK_9 ) << 17 )
#define oC_BITS_18_TO_26                                ( ( oC_BITS_MASK_9 ) << 18 )
#define oC_BITS_19_TO_27                                ( ( oC_BITS_MASK_9 ) << 19 )
#define oC_BITS_20_TO_28                                ( ( oC_BITS_MASK_9 ) << 20 )
#define oC_BITS_21_TO_29                                ( ( oC_BITS_MASK_9 ) << 21 )
#define oC_BITS_22_TO_30                                ( ( oC_BITS_MASK_9 ) << 22 )
#define oC_BITS_23_TO_31                                ( ( oC_BITS_MASK_9 ) << 23 )

#define  oC_BITS_0_TO_9                                 ( ( oC_BITS_MASK_10 ) << 0  )
#define  oC_BITS_1_TO_10                                ( ( oC_BITS_MASK_10 ) << 1  )
#define  oC_BITS_2_TO_11                                ( ( oC_BITS_MASK_10 ) << 2  )
#define  oC_BITS_3_TO_12                                ( ( oC_BITS_MASK_10 ) << 3  )
#define  oC_BITS_4_TO_13                                ( ( oC_BITS_MASK_10 ) << 4  )
#define  oC_BITS_5_TO_14                                ( ( oC_BITS_MASK_10 ) << 5  )
#define  oC_BITS_6_TO_15                                ( ( oC_BITS_MASK_10 ) << 6  )
#define  oC_BITS_7_TO_16                                ( ( oC_BITS_MASK_10 ) << 7  )
#define  oC_BITS_8_TO_17                                ( ( oC_BITS_MASK_10 ) << 8  )
#define  oC_BITS_9_TO_18                                ( ( oC_BITS_MASK_10 ) << 9  )
#define  oC_BITS_10_TO_19                               ( ( oC_BITS_MASK_10 ) << 10 )
#define  oC_BITS_11_TO_20                               ( ( oC_BITS_MASK_10 ) << 11 )
#define  oC_BITS_12_TO_21                               ( ( oC_BITS_MASK_10 ) << 12 )
#define  oC_BITS_13_TO_22                               ( ( oC_BITS_MASK_10 ) << 13 )
#define  oC_BITS_14_TO_23                               ( ( oC_BITS_MASK_10 ) << 14 )
#define  oC_BITS_15_TO_24                               ( ( oC_BITS_MASK_10 ) << 15 )
#define  oC_BITS_16_TO_25                               ( ( oC_BITS_MASK_10 ) << 16 )
#define  oC_BITS_17_TO_26                               ( ( oC_BITS_MASK_10 ) << 17 )
#define  oC_BITS_18_TO_27                               ( ( oC_BITS_MASK_10 ) << 18 )
#define  oC_BITS_19_TO_28                               ( ( oC_BITS_MASK_10 ) << 19 )
#define  oC_BITS_20_TO_29                               ( ( oC_BITS_MASK_10 ) << 20 )
#define  oC_BITS_21_TO_30                               ( ( oC_BITS_MASK_10 ) << 21 )
#define  oC_BITS_22_TO_31                               ( ( oC_BITS_MASK_10 ) << 22 )

#define  oC_BITS_0_TO_10                                ( ( oC_BITS_MASK_11 ) << 0  )
#define  oC_BITS_1_TO_11                                ( ( oC_BITS_MASK_11 ) << 1  )
#define  oC_BITS_2_TO_12                                ( ( oC_BITS_MASK_11 ) << 2  )
#define  oC_BITS_3_TO_13                                ( ( oC_BITS_MASK_11 ) << 3  )
#define  oC_BITS_4_TO_14                                ( ( oC_BITS_MASK_11 ) << 4  )
#define  oC_BITS_5_TO_15                                ( ( oC_BITS_MASK_11 ) << 5  )
#define  oC_BITS_6_TO_16                                ( ( oC_BITS_MASK_11 ) << 6  )
#define  oC_BITS_7_TO_17                                ( ( oC_BITS_MASK_11 ) << 7  )
#define  oC_BITS_8_TO_18                                ( ( oC_BITS_MASK_11 ) << 8  )
#define  oC_BITS_9_TO_19                                ( ( oC_BITS_MASK_11 ) << 9  )
#define  oC_BITS_10_TO_20                               ( ( oC_BITS_MASK_11 ) << 10 )
#define  oC_BITS_11_TO_21                               ( ( oC_BITS_MASK_11 ) << 11 )
#define  oC_BITS_12_TO_22                               ( ( oC_BITS_MASK_11 ) << 12 )
#define  oC_BITS_13_TO_23                               ( ( oC_BITS_MASK_11 ) << 13 )
#define  oC_BITS_14_TO_24                               ( ( oC_BITS_MASK_11 ) << 14 )
#define  oC_BITS_15_TO_25                               ( ( oC_BITS_MASK_11 ) << 15 )
#define  oC_BITS_16_TO_26                               ( ( oC_BITS_MASK_11 ) << 16 )
#define  oC_BITS_17_TO_27                               ( ( oC_BITS_MASK_11 ) << 17 )
#define  oC_BITS_18_TO_28                               ( ( oC_BITS_MASK_11 ) << 18 )
#define  oC_BITS_19_TO_29                               ( ( oC_BITS_MASK_11 ) << 19 )
#define  oC_BITS_20_TO_30                               ( ( oC_BITS_MASK_11 ) << 20 )
#define  oC_BITS_21_TO_31                               ( ( oC_BITS_MASK_11 ) << 21 )

#define  oC_BITS_0_TO_11                                ( ( oC_BITS_MASK_12 ) << 0  )
#define  oC_BITS_1_TO_12                                ( ( oC_BITS_MASK_12 ) << 1  )
#define  oC_BITS_2_TO_13                                ( ( oC_BITS_MASK_12 ) << 2  )
#define  oC_BITS_3_TO_14                                ( ( oC_BITS_MASK_12 ) << 3  )
#define  oC_BITS_4_TO_15                                ( ( oC_BITS_MASK_12 ) << 4  )
#define  oC_BITS_5_TO_16                                ( ( oC_BITS_MASK_12 ) << 5  )
#define  oC_BITS_6_TO_17                                ( ( oC_BITS_MASK_12 ) << 6  )
#define  oC_BITS_7_TO_18                                ( ( oC_BITS_MASK_12 ) << 7  )
#define  oC_BITS_8_TO_19                                ( ( oC_BITS_MASK_12 ) << 8  )
#define  oC_BITS_9_TO_20                                ( ( oC_BITS_MASK_12 ) << 9  )
#define  oC_BITS_10_TO_21                               ( ( oC_BITS_MASK_12 ) << 10 )
#define  oC_BITS_11_TO_22                               ( ( oC_BITS_MASK_12 ) << 11 )
#define  oC_BITS_12_TO_23                               ( ( oC_BITS_MASK_12 ) << 12 )
#define  oC_BITS_13_TO_24                               ( ( oC_BITS_MASK_12 ) << 13 )
#define  oC_BITS_14_TO_25                               ( ( oC_BITS_MASK_12 ) << 14 )
#define  oC_BITS_15_TO_26                               ( ( oC_BITS_MASK_12 ) << 15 )
#define  oC_BITS_16_TO_27                               ( ( oC_BITS_MASK_12 ) << 16 )
#define  oC_BITS_17_TO_28                               ( ( oC_BITS_MASK_12 ) << 17 )
#define  oC_BITS_18_TO_29                               ( ( oC_BITS_MASK_12 ) << 18 )
#define  oC_BITS_19_TO_30                               ( ( oC_BITS_MASK_12 ) << 19 )
#define  oC_BITS_20_TO_31                               ( ( oC_BITS_MASK_12 ) << 20 )

#define  oC_BITS_0_TO_12                                ( ( oC_BITS_MASK_13 ) << 0  )
#define  oC_BITS_1_TO_13                                ( ( oC_BITS_MASK_13 ) << 1  )
#define  oC_BITS_2_TO_14                                ( ( oC_BITS_MASK_13 ) << 2  )
#define  oC_BITS_3_TO_15                                ( ( oC_BITS_MASK_13 ) << 3  )
#define  oC_BITS_4_TO_16                                ( ( oC_BITS_MASK_13 ) << 4  )
#define  oC_BITS_5_TO_17                                ( ( oC_BITS_MASK_13 ) << 5  )
#define  oC_BITS_6_TO_18                                ( ( oC_BITS_MASK_13 ) << 6  )
#define  oC_BITS_7_TO_19                                ( ( oC_BITS_MASK_13 ) << 7  )
#define  oC_BITS_8_TO_20                                ( ( oC_BITS_MASK_13 ) << 8  )
#define  oC_BITS_9_TO_21                                ( ( oC_BITS_MASK_13 ) << 9  )
#define  oC_BITS_10_TO_22                               ( ( oC_BITS_MASK_13 ) << 10 )
#define  oC_BITS_11_TO_23                               ( ( oC_BITS_MASK_13 ) << 11 )
#define  oC_BITS_12_TO_24                               ( ( oC_BITS_MASK_13 ) << 12 )
#define  oC_BITS_13_TO_25                               ( ( oC_BITS_MASK_13 ) << 13 )
#define  oC_BITS_14_TO_26                               ( ( oC_BITS_MASK_13 ) << 14 )
#define  oC_BITS_15_TO_27                               ( ( oC_BITS_MASK_13 ) << 15 )
#define  oC_BITS_16_TO_28                               ( ( oC_BITS_MASK_13 ) << 16 )
#define  oC_BITS_17_TO_29                               ( ( oC_BITS_MASK_13 ) << 17 )
#define  oC_BITS_18_TO_30                               ( ( oC_BITS_MASK_13 ) << 18 )
#define  oC_BITS_19_TO_31                               ( ( oC_BITS_MASK_13 ) << 19 )

#define  oC_BITS_0_TO_13                                ( ( oC_BITS_MASK_14 ) << 0  )
#define  oC_BITS_1_TO_14                                ( ( oC_BITS_MASK_14 ) << 1  )
#define  oC_BITS_2_TO_15                                ( ( oC_BITS_MASK_14 ) << 2  )
#define  oC_BITS_3_TO_16                                ( ( oC_BITS_MASK_14 ) << 3  )
#define  oC_BITS_4_TO_17                                ( ( oC_BITS_MASK_14 ) << 4  )
#define  oC_BITS_5_TO_18                                ( ( oC_BITS_MASK_14 ) << 5  )
#define  oC_BITS_6_TO_19                                ( ( oC_BITS_MASK_14 ) << 6  )
#define  oC_BITS_7_TO_20                                ( ( oC_BITS_MASK_14 ) << 7  )
#define  oC_BITS_8_TO_21                                ( ( oC_BITS_MASK_14 ) << 8  )
#define  oC_BITS_9_TO_22                                ( ( oC_BITS_MASK_14 ) << 9  )
#define  oC_BITS_10_TO_23                               ( ( oC_BITS_MASK_14 ) << 10 )
#define  oC_BITS_11_TO_24                               ( ( oC_BITS_MASK_14 ) << 11 )
#define  oC_BITS_12_TO_25                               ( ( oC_BITS_MASK_14 ) << 12 )
#define  oC_BITS_13_TO_26                               ( ( oC_BITS_MASK_14 ) << 13 )
#define  oC_BITS_14_TO_27                               ( ( oC_BITS_MASK_14 ) << 14 )
#define  oC_BITS_15_TO_28                               ( ( oC_BITS_MASK_14 ) << 15 )
#define  oC_BITS_16_TO_29                               ( ( oC_BITS_MASK_14 ) << 16 )
#define  oC_BITS_17_TO_30                               ( ( oC_BITS_MASK_14 ) << 17 )
#define  oC_BITS_18_TO_31                               ( ( oC_BITS_MASK_14 ) << 18 )

#define  oC_BITS_0_TO_14                                ( ( oC_BITS_MASK_15 ) << 0  )
#define  oC_BITS_1_TO_15                                ( ( oC_BITS_MASK_15 ) << 1  )
#define  oC_BITS_2_TO_16                                ( ( oC_BITS_MASK_15 ) << 2  )
#define  oC_BITS_3_TO_17                                ( ( oC_BITS_MASK_15 ) << 3  )
#define  oC_BITS_4_TO_18                                ( ( oC_BITS_MASK_15 ) << 4  )
#define  oC_BITS_5_TO_19                                ( ( oC_BITS_MASK_15 ) << 5  )
#define  oC_BITS_6_TO_20                                ( ( oC_BITS_MASK_15 ) << 6  )
#define  oC_BITS_7_TO_21                                ( ( oC_BITS_MASK_15 ) << 7  )
#define  oC_BITS_8_TO_22                                ( ( oC_BITS_MASK_15 ) << 8  )
#define  oC_BITS_9_TO_23                                ( ( oC_BITS_MASK_15 ) << 9  )
#define  oC_BITS_10_TO_24                               ( ( oC_BITS_MASK_15 ) << 10 )
#define  oC_BITS_11_TO_25                               ( ( oC_BITS_MASK_15 ) << 11 )
#define  oC_BITS_12_TO_26                               ( ( oC_BITS_MASK_15 ) << 12 )
#define  oC_BITS_13_TO_27                               ( ( oC_BITS_MASK_15 ) << 13 )
#define  oC_BITS_14_TO_28                               ( ( oC_BITS_MASK_15 ) << 14 )
#define  oC_BITS_15_TO_29                               ( ( oC_BITS_MASK_15 ) << 15 )
#define  oC_BITS_16_TO_30                               ( ( oC_BITS_MASK_15 ) << 16 )
#define  oC_BITS_17_TO_31                               ( ( oC_BITS_MASK_15 ) << 17 )

#define  oC_BITS_0_TO_15                                ( ( oC_BITS_MASK_16 ) << 0  )
#define  oC_BITS_1_TO_16                                ( ( oC_BITS_MASK_16 ) << 1  )
#define  oC_BITS_2_TO_17                                ( ( oC_BITS_MASK_16 ) << 2  )
#define  oC_BITS_3_TO_18                                ( ( oC_BITS_MASK_16 ) << 3  )
#define  oC_BITS_4_TO_19                                ( ( oC_BITS_MASK_16 ) << 4  )
#define  oC_BITS_5_TO_20                                ( ( oC_BITS_MASK_16 ) << 5  )
#define  oC_BITS_6_TO_21                                ( ( oC_BITS_MASK_16 ) << 6  )
#define  oC_BITS_7_TO_22                                ( ( oC_BITS_MASK_16 ) << 7  )
#define  oC_BITS_8_TO_23                                ( ( oC_BITS_MASK_16 ) << 8  )
#define  oC_BITS_9_TO_24                                ( ( oC_BITS_MASK_16 ) << 9  )
#define oC_BITS_10_TO_25                                ( ( oC_BITS_MASK_16 ) << 10 )
#define oC_BITS_11_TO_26                                ( ( oC_BITS_MASK_16 ) << 11 )
#define oC_BITS_12_TO_27                                ( ( oC_BITS_MASK_16 ) << 12 )
#define oC_BITS_13_TO_28                                ( ( oC_BITS_MASK_16 ) << 13 )
#define oC_BITS_14_TO_29                                ( ( oC_BITS_MASK_16 ) << 14 )
#define oC_BITS_15_TO_30                                ( ( oC_BITS_MASK_16 ) << 15 )
#define oC_BITS_16_TO_31                                ( ( oC_BITS_MASK_16 ) << 16 )

#define  oC_BITS_0_TO_16                                ( ( oC_BITS_MASK_17 ) << 0  )
#define  oC_BITS_1_TO_17                                ( ( oC_BITS_MASK_17 ) << 1  )
#define  oC_BITS_2_TO_18                                ( ( oC_BITS_MASK_17 ) << 2  )
#define  oC_BITS_3_TO_19                                ( ( oC_BITS_MASK_17 ) << 3  )
#define  oC_BITS_4_TO_20                                ( ( oC_BITS_MASK_17 ) << 4  )
#define  oC_BITS_5_TO_21                                ( ( oC_BITS_MASK_17 ) << 5  )
#define  oC_BITS_6_TO_22                                ( ( oC_BITS_MASK_17 ) << 6  )
#define  oC_BITS_7_TO_23                                ( ( oC_BITS_MASK_17 ) << 7  )
#define  oC_BITS_8_TO_24                                ( ( oC_BITS_MASK_17 ) << 8  )
#define  oC_BITS_9_TO_25                                ( ( oC_BITS_MASK_17 ) << 9  )
#define oC_BITS_10_TO_26                                ( ( oC_BITS_MASK_17 ) << 10 )
#define oC_BITS_11_TO_27                                ( ( oC_BITS_MASK_17 ) << 11 )
#define oC_BITS_12_TO_28                                ( ( oC_BITS_MASK_17 ) << 12 )
#define oC_BITS_13_TO_29                                ( ( oC_BITS_MASK_17 ) << 13 )
#define oC_BITS_14_TO_30                                ( ( oC_BITS_MASK_17 ) << 14 )
#define oC_BITS_15_TO_31                                ( ( oC_BITS_MASK_17 ) << 15 )

#define  oC_BITS_0_TO_17                                ( ( oC_BITS_MASK_18 ) << 0  )
#define  oC_BITS_1_TO_18                                ( ( oC_BITS_MASK_18 ) << 1  )
#define  oC_BITS_2_TO_19                                ( ( oC_BITS_MASK_18 ) << 2  )
#define  oC_BITS_3_TO_20                                ( ( oC_BITS_MASK_18 ) << 3  )
#define  oC_BITS_4_TO_21                                ( ( oC_BITS_MASK_18 ) << 4  )
#define  oC_BITS_5_TO_22                                ( ( oC_BITS_MASK_18 ) << 5  )
#define  oC_BITS_6_TO_23                                ( ( oC_BITS_MASK_18 ) << 6  )
#define  oC_BITS_7_TO_24                                ( ( oC_BITS_MASK_18 ) << 7  )
#define  oC_BITS_8_TO_25                                ( ( oC_BITS_MASK_18 ) << 8  )
#define  oC_BITS_9_TO_26                                ( ( oC_BITS_MASK_18 ) << 9  )
#define  oC_BITS_10_TO_27                               ( ( oC_BITS_MASK_18 ) << 10 )
#define  oC_BITS_11_TO_28                               ( ( oC_BITS_MASK_18 ) << 11 )
#define  oC_BITS_12_TO_29                               ( ( oC_BITS_MASK_18 ) << 12 )
#define  oC_BITS_13_TO_30                               ( ( oC_BITS_MASK_18 ) << 13 )
#define  oC_BITS_14_TO_31                               ( ( oC_BITS_MASK_18 ) << 14 )

#define  oC_BITS_0_TO_18                                ( ( oC_BITS_MASK_19 ) << 0  )
#define  oC_BITS_1_TO_19                                ( ( oC_BITS_MASK_19 ) << 1  )
#define  oC_BITS_2_TO_20                                ( ( oC_BITS_MASK_19 ) << 2  )
#define  oC_BITS_3_TO_21                                ( ( oC_BITS_MASK_19 ) << 3  )
#define  oC_BITS_4_TO_22                                ( ( oC_BITS_MASK_19 ) << 4  )
#define  oC_BITS_5_TO_23                                ( ( oC_BITS_MASK_19 ) << 5  )
#define  oC_BITS_6_TO_24                                ( ( oC_BITS_MASK_19 ) << 6  )
#define  oC_BITS_7_TO_25                                ( ( oC_BITS_MASK_19 ) << 7  )
#define  oC_BITS_8_TO_26                                ( ( oC_BITS_MASK_19 ) << 8  )
#define  oC_BITS_9_TO_27                                ( ( oC_BITS_MASK_19 ) << 9  )
#define  oC_BITS_10_TO_28                               ( ( oC_BITS_MASK_19 ) << 10 )
#define  oC_BITS_11_TO_29                               ( ( oC_BITS_MASK_19 ) << 11 )
#define  oC_BITS_12_TO_30                               ( ( oC_BITS_MASK_19 ) << 12 )
#define  oC_BITS_13_TO_31                               ( ( oC_BITS_MASK_19 ) << 13 )

#define  oC_BITS_0_TO_19                                ( ( oC_BITS_MASK_20 ) << 0  )
#define  oC_BITS_1_TO_20                                ( ( oC_BITS_MASK_20 ) << 1  )
#define  oC_BITS_2_TO_21                                ( ( oC_BITS_MASK_20 ) << 2  )
#define  oC_BITS_3_TO_22                                ( ( oC_BITS_MASK_20 ) << 3  )
#define  oC_BITS_4_TO_23                                ( ( oC_BITS_MASK_20 ) << 4  )
#define  oC_BITS_5_TO_24                                ( ( oC_BITS_MASK_20 ) << 5  )
#define  oC_BITS_6_TO_25                                ( ( oC_BITS_MASK_20 ) << 6  )
#define  oC_BITS_7_TO_26                                ( ( oC_BITS_MASK_20 ) << 7  )
#define  oC_BITS_8_TO_27                                ( ( oC_BITS_MASK_20 ) << 8  )
#define  oC_BITS_9_TO_28                                ( ( oC_BITS_MASK_20 ) << 9  )
#define  oC_BITS_10_TO_29                               ( ( oC_BITS_MASK_20 ) << 10 )
#define  oC_BITS_11_TO_30                               ( ( oC_BITS_MASK_20 ) << 11 )
#define  oC_BITS_12_TO_31                               ( ( oC_BITS_MASK_20 ) << 12 )

#define  oC_BITS_0_TO_20                                ( ( oC_BITS_MASK_21 ) << 0  )
#define  oC_BITS_1_TO_21                                ( ( oC_BITS_MASK_21 ) << 1  )
#define  oC_BITS_2_TO_22                                ( ( oC_BITS_MASK_21 ) << 2  )
#define  oC_BITS_3_TO_23                                ( ( oC_BITS_MASK_21 ) << 3  )
#define  oC_BITS_4_TO_24                                ( ( oC_BITS_MASK_21 ) << 4  )
#define  oC_BITS_5_TO_25                                ( ( oC_BITS_MASK_21 ) << 5  )
#define  oC_BITS_6_TO_26                                ( ( oC_BITS_MASK_21 ) << 6  )
#define  oC_BITS_7_TO_27                                ( ( oC_BITS_MASK_21 ) << 7  )
#define  oC_BITS_8_TO_28                                ( ( oC_BITS_MASK_21 ) << 8  )
#define  oC_BITS_9_TO_29                                ( ( oC_BITS_MASK_21 ) << 9  )
#define oC_BITS_10_TO_30                                ( ( oC_BITS_MASK_21 ) << 10 )
#define oC_BITS_11_TO_31                                ( ( oC_BITS_MASK_21 ) << 11 )

#define  oC_BITS_0_TO_21                                ( ( oC_BITS_MASK_22 ) << 0  )
#define  oC_BITS_1_TO_22                                ( ( oC_BITS_MASK_22 ) << 1  )
#define  oC_BITS_2_TO_23                                ( ( oC_BITS_MASK_22 ) << 2  )
#define  oC_BITS_3_TO_24                                ( ( oC_BITS_MASK_22 ) << 3  )
#define  oC_BITS_4_TO_25                                ( ( oC_BITS_MASK_22 ) << 4  )
#define  oC_BITS_5_TO_26                                ( ( oC_BITS_MASK_22 ) << 5  )
#define  oC_BITS_6_TO_27                                ( ( oC_BITS_MASK_22 ) << 6  )
#define  oC_BITS_7_TO_28                                ( ( oC_BITS_MASK_22 ) << 7  )
#define  oC_BITS_8_TO_29                                ( ( oC_BITS_MASK_22 ) << 8  )
#define  oC_BITS_9_TO_30                                ( ( oC_BITS_MASK_22 ) << 9  )
#define oC_BITS_10_TO_31                                ( ( oC_BITS_MASK_22 ) << 10 )

#define  oC_BITS_0_TO_22                                ( ( oC_BITS_MASK_23 ) << 0  )
#define  oC_BITS_1_TO_23                                ( ( oC_BITS_MASK_23 ) << 1  )
#define  oC_BITS_2_TO_24                                ( ( oC_BITS_MASK_23 ) << 2  )
#define  oC_BITS_3_TO_25                                ( ( oC_BITS_MASK_23 ) << 3  )
#define  oC_BITS_4_TO_26                                ( ( oC_BITS_MASK_23 ) << 4  )
#define  oC_BITS_5_TO_27                                ( ( oC_BITS_MASK_23 ) << 5  )
#define  oC_BITS_6_TO_28                                ( ( oC_BITS_MASK_23 ) << 6  )
#define  oC_BITS_7_TO_29                                ( ( oC_BITS_MASK_23 ) << 7  )
#define  oC_BITS_8_TO_30                                ( ( oC_BITS_MASK_23 ) << 8  )
#define  oC_BITS_9_TO_31                                ( ( oC_BITS_MASK_23 ) << 9  )

#define  oC_BITS_0_TO_23                                ( ( oC_BITS_MASK_24 ) << 0  )
#define  oC_BITS_1_TO_24                                ( ( oC_BITS_MASK_24 ) << 1  )
#define  oC_BITS_2_TO_25                                ( ( oC_BITS_MASK_24 ) << 2  )
#define  oC_BITS_3_TO_26                                ( ( oC_BITS_MASK_24 ) << 3  )
#define  oC_BITS_4_TO_27                                ( ( oC_BITS_MASK_24 ) << 4  )
#define  oC_BITS_5_TO_28                                ( ( oC_BITS_MASK_24 ) << 5  )
#define  oC_BITS_6_TO_29                                ( ( oC_BITS_MASK_24 ) << 6  )
#define  oC_BITS_7_TO_30                                ( ( oC_BITS_MASK_24 ) << 7  )
#define  oC_BITS_8_TO_31                                ( ( oC_BITS_MASK_24 ) << 8  )

#define  oC_BITS_0_TO_24                                ( ( oC_BITS_MASK_25 ) << 0  )
#define  oC_BITS_1_TO_25                                ( ( oC_BITS_MASK_25 ) << 1  )
#define  oC_BITS_2_TO_26                                ( ( oC_BITS_MASK_25 ) << 2  )
#define  oC_BITS_3_TO_27                                ( ( oC_BITS_MASK_25 ) << 3  )
#define  oC_BITS_4_TO_28                                ( ( oC_BITS_MASK_25 ) << 4  )
#define  oC_BITS_5_TO_29                                ( ( oC_BITS_MASK_25 ) << 5  )
#define  oC_BITS_6_TO_30                                ( ( oC_BITS_MASK_25 ) << 6  )
#define  oC_BITS_7_TO_31                                ( ( oC_BITS_MASK_25 ) << 7  )

#define  oC_BITS_0_TO_25                                ( ( oC_BITS_MASK_26 ) << 0  )
#define  oC_BITS_1_TO_26                                ( ( oC_BITS_MASK_26 ) << 1  )
#define  oC_BITS_2_TO_27                                ( ( oC_BITS_MASK_26 ) << 2  )
#define  oC_BITS_3_TO_28                                ( ( oC_BITS_MASK_26 ) << 3  )
#define  oC_BITS_4_TO_29                                ( ( oC_BITS_MASK_26 ) << 4  )
#define  oC_BITS_5_TO_30                                ( ( oC_BITS_MASK_26 ) << 5  )
#define  oC_BITS_6_TO_31                                ( ( oC_BITS_MASK_26 ) << 6  )

#define  oC_BITS_0_TO_26                                ( ( oC_BITS_MASK_27 ) << 0  )
#define  oC_BITS_1_TO_27                                ( ( oC_BITS_MASK_27 ) << 1  )
#define  oC_BITS_2_TO_28                                ( ( oC_BITS_MASK_27 ) << 2  )
#define  oC_BITS_3_TO_29                                ( ( oC_BITS_MASK_27 ) << 3  )
#define  oC_BITS_4_TO_30                                ( ( oC_BITS_MASK_27 ) << 4  )
#define  oC_BITS_5_TO_31                                ( ( oC_BITS_MASK_27 ) << 5  )

#define  oC_BITS_0_TO_27                                ( ( oC_BITS_MASK_28 ) << 0  )
#define  oC_BITS_1_TO_28                                ( ( oC_BITS_MASK_28 ) << 1  )
#define  oC_BITS_2_TO_29                                ( ( oC_BITS_MASK_28 ) << 2  )
#define  oC_BITS_3_TO_30                                ( ( oC_BITS_MASK_28 ) << 3  )
#define  oC_BITS_4_TO_31                                ( ( oC_BITS_MASK_28 ) << 4  )

#define  oC_BITS_0_TO_28                                ( ( oC_BITS_MASK_29 ) << 0  )
#define  oC_BITS_1_TO_29                                ( ( oC_BITS_MASK_29 ) << 1  )
#define  oC_BITS_2_TO_30                                ( ( oC_BITS_MASK_29 ) << 2  )
#define  oC_BITS_3_TO_31                                ( ( oC_BITS_MASK_29 ) << 3  )

#define  oC_BITS_0_TO_29                                ( ( oC_BITS_MASK_30 ) << 0  )
#define  oC_BITS_1_TO_30                                ( ( oC_BITS_MASK_30 ) << 1  )
#define  oC_BITS_2_TO_31                                ( ( oC_BITS_MASK_30 ) << 2  )

#define  oC_BITS_0_TO_30                                ( ( oC_BITS_MASK_31 ) << 0  )
#define  oC_BITS_1_TO_31                                ( ( oC_BITS_MASK_31 ) << 1  )

#define  oC_BITS_0_TO_31                                ( ( oC_BITS_MASK_32 ) << 0  )

#define oC_BITS_MASK_N(_N_)                             oC_CUT(oC_BITS_MASK_ , _N_)
#define oC_BITS( FROM , TO )                            (\
                                                            (\
                                                                    ( (((uint64_t)1)<<(TO)) - 1 ) \
                                                                    | (((uint64_t)1)<<(TO)) \
                                                            ) \
                                                            ^((((uint64_t)1)<<(FROM)) - 1) \
                                                        )

//
//  BASIC MACROS
//
#define oC_SET_BIT( _VAR_ , _BIT_ )                     _VAR_ |=  ( _BIT_ )
#define oC_CLR_BIT( _VAR_ , _BIT_ )                     _VAR_ &= ~( _BIT_ )
#define oC_WRITE_WITH_MASK( _VAR_ , _MASK_ , _VALUE_ )  oC_CLR_BIT( _VAR_ , (_MASK_) );\
                                                        oC_SET_BIT( _VAR_ , (_MASK_) & (_VALUE_) )
#define oC_IS_BIT_SET( _VAR_ , _BIT_ )                  ( ((_VAR_) & (1U<<(_BIT_))) == (1U<<(_BIT_)) )

//
//  VALUES IN REGISTERS
//
#define oC_VALUE_ON_BITS( _BIT_NR_ , _VALUE_ )          ( (_VALUE_) << (_BIT_NR_) )

#define oC_WRITE_REG_WITH_MASK( _REG_ , _MASK_ , _VALUE_ )                                      \
                                oC_WRITE_WITH_MASK( (_REG_) , (_MASK_) , (oC_Register_t)(_VALUE_) )
//
//  REGISTER MAP CASTING
//
#define oC_CREATE_REGISTER_MAP_CAST(_RMAP_TYPE_,_BASE_) ((_RMAP_TYPE_ *)(_BASE_) )

/**
 * The macro for create a weak definition of the function. If the function exists, then call the function, otherwise call a default_function
 * @param DEFAULT_FUNCTION_NAME     Name of the function which should be called when the source of the function not exists
 */
//#define oC_WEAK_DEFAULT( DEFAULT_FUNCTION )                     __attribute__ ((weak, alias(#DEFAULT_FUNCTION)))

/**
 * The macro return minimum value from a 2 parameters
 */
#define oC_MIN( A , B )         ( ( (A) > (B) ) ? (B) : (A) )

/**
 * The macro return maximum value from a 2 parameters
 */
#define oC_MAX( A , B )         ( ( (A) > (B) ) ? (A) : (B) )

/**
 * Returns size of the static array
 */
#define oC_SIZE_OF_ARRAY( ARRAY )   ( sizeof(ARRAY) / sizeof(ARRAY[0]) )

/**
 * Macro for unused function parameters. It is created for remove compile warning
 */
#define oC_UNUSED_ARG( ARG )        (void)ARG

#define oC_UINT8_MAX    0xFFU
#define oC_UINT16_MAX   0xFFFFU
#define oC_UINT32_MAX   0xFFFFFFFFU
#define oC_UINT64_MAX   0xFFFFFFFFFFFFFFFFUL

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    DEFINITION CURRENT CONFIGURATION
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Instruction defs
#define oC_ASM               __asm

// NULL pointer
#define oC_NULL              ( (void*)0 )

#ifndef NULL
#   define NULL oC_NULL
#endif

// ABS value
#define oC_ABS( _A_ , _B_ )                             ( (_A_) > (_B_) ? ( (_A_) - (_B_) ) : ( (_B_) - (_A_) ) )

//
//  TIME MACROS
//
#define oC_CONVERT_TICKS_TO_TIME_MS( _TICKS_ )                      ( (_TICKS_) * (portTICK_PERIOD_MS) )
#define oC_CONVERT_TIME_MS_TO_TICKS( _TIME_ )                       ( (_TIME_) / (portTICK_PERIOD_MS) )

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    PROGRAM MACROS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 /**
  * The macro for creating value name of the enum with program id list
  */
#define oC_PROGRAM_CREATE_ENUM_VALUE( PROGRAM_NAME )                        oC_CAT_2(oC_ProgramID_ , PROGRAM_NAME)
#define oC_PROGRAM_CREATE_PROGRAM_PROTOTYPE( PROGRAM_MAIN  , ... )          extern int PROGRAM_MAIN ( int Argv , char ** Argc )

 /**
  * The macro for adding program to program id type enum list
  */
#define oC_PROGRAM_ADD_PROGRAM_TO_ENUM_LIST( PROGRAM_MAIN , NAME , STACK_SIZE )         oC_PROGRAM_CREATE_ENUM_VALUE( PROGRAM_MAIN ) ,
#define oC_PROGRAM_ADD_PROGRAM_TO_PROTOTYPE_LIST( PROGRAM_MAIN , NAME , STACK_SIZE )    oC_PROGRAM_CREATE_PROGRAM_PROTOTYPE( PROGRAM_MAIN );

/**
 * The macro for not used programs
 */
#define oC_PROGRAM_DONT_USE( PROGRAM_NAME , STACK_SIZE )

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INLINE FUNCTIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#endif /* OC_CORE_MACROS_H_ */
