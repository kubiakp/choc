/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          choc.h
 *
 *    @brief         Main file of the ChOC system. It provides interface to all modules in the system.
 *
 *    @author        Patryk Kubiak
 *
 *    @note          Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                   This program is free software; you can redistribute it and/or modify
 *                   it under the terms of the GNU General Public License as published by
 *                   the Free Software Foundation; either version 2 of the License, or
 *                   (at your option) any later version.
 *
 *                   This program is distributed in the hope that it will be useful,
 *                   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                   GNU General Public License for more details.
 *
 *                   You should have received a copy of the GNU General Public License
 *                   along with this program; if not, write to the Free Software
 *                   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @mainpage Choco OS - Chocolate Operating System
 *
 * The <b>Choco</b> is a real time operating system that is designed for embedded architectures. The main assumption of the system is
 * to speed up development of embedded projects, and independent it of selected micro-controller. It consist of many features, that can be
 * included or not to the project, thanks to that it can be applying both in small like in big projects. Each module in the system is
 * implemented with special rules, that make it easy to learn it and makes work simple and nice. The system contains many features, that
 * helps engineers to quickly find potential bugs, track memory usage, measure CPU load of processes and to run programs in an isolated environment
 * which provide that the crash one of programs will not crash the others. Moreover the system is able to self repairing many problems
 * by using embedded tools, and a shell can be used for remotely solve the others. There are included many of drivers, that controls
 * processor peripherals such as UART, SPI, Timers, PWM, ADC, and also some kind of populate external chips. This makes possible, to
 * write programs without special knowledge about selected processor.
 *
 * @par Features
 *
 * - Preemptive multitasking with dynamically memory space allocation
 * - Expended system of tasks synchronization
 * - Tools helping to track memory and CPU usage
 * - Errors code systems which helps to print results of operations
 * - Time and date control tools
 * - Handle of terminals
 * - The 'chell' - shell like sh
 * - Hardware-independent drivers to control most of peripherals and many chips
 * - And many more!
 *
 * @par Supported Architectures
 *
 * - lm4fh5qr
 *
 * @par Supported Chips
 *
 * - hd44780
 *
 * @page Trainings Trainings
 *
 * @section Install 1. Installation Guide
 * _________________________________________________________________________________________________________________________________________
 * Installation guide
 *
 * # Introduction #
 * To work with the **Choco OS**, the first thing you should do, is decide if you want to develop a system or write your own applications on it. This chapter shows how to configure a environment in both possibilities.
 * # Getting sources #
 * <i>First of all, you should download the sources of the Choco OS. You can do it either by download a latest version release (from [this](https://bitbucket.org/kubiakp/choc/downloads) link) or by cloning git repository</i>
 * ## Download Latest Release ##
 * This option is for developers, who just want to use the Choco OS in theirs application. The latest release you can get by clicking [here](https://bitbucket.org/kubiakp/choc/get/V.0.3.0.zip)*
 * ## Clone Repository ##
 * If you want to develop the Choco OS, you should clone the git repository to your hard drive. To do it, first think, that you should do, is getting a git client. You can find it [here](http://git-scm.com/downloads).
 * Now, when you have a git client, you can clone the Choco OS repository. To do it, run **Git Bash** app, and type a command below:
 * @code
   git clone git@bitbucket.org:kubiakp/choc.git
 * @endcode
 *
 * @note If you did not use a bitbucket before, you should generate a **ssh key** to link your account with your PC before you clone sources. [Here](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git) you can find out how to do it.
 *
 * @subsection Compilation Compilation
 * When you have sources, you should can compile a project. To do it, just run **Source/build.bat** script in command line. If it will finish successfully, you will see
 * ![Compilation successfully screen.PNG](https://bitbucket.org/repo/ro4RAb/images/3363919024-Compilation%20successfully%20screen.PNG)
 *
 * @subsection Flash Flash a micro-controller
 * @subsubsection Install-programmer Install programmer drivers
 * Drivers are stored in **Tools/drivers** directory. Note, that these drivers are unsigned, so if you are using Windows 8, you will need to perform [this guide](http://www.makeuseof.com/tag/how-can-i-install-hardware-with-unsigned-drivers-in-windows-8/)
 *
 * @subsubsection Connect Connect to the programmer
 *
 * To with the debugger, you should run the **runoocd.bat** script from the **Source** directory.
 *
 * @subsubsection Flashing Flashing
 *
 * At least you can flash a micro controller, using **flash.bat** script file. Rember, that before you will flash, you must compile a sources with **build.bat** script!!*
 *
 * @section MyFirstApp 2. My First Application
 * _________________________________________________________________________________________________________________________________________
 *
 * @subsection Introduction Introduction
 * This chapter describes how to write your first program in the **Choco OS** system.
 *
 * First of all, you should meet the structure of directories in the project. Screen below shows view of the main directory.
 *
 * ![main directory view.PNG](https://bitbucket.org/repo/ro4RAb/images/908228221-main%20directory%20view.PNG)
 *
 * It contains:
 *
 * - **Docs** *- documentations which helps develop a system*
 * - **Resources** *- additional resources files such as screens, links, file templates, etc*
 * - **Source** *- main directory with sources*
 * - **Tests** *- unit tests for oC modules*
 * - **Tools** <i>- additional tools, and all needed programs that makes working with the oC is easier</i>
 *
 * ![source directory view.PNG](https://bitbucket.org/repo/ro4RAb/images/3886862549-source%20directory%20view.PNG)
 *
 * Sources are available in the **Source** directory, so the first thing we should do, is enter to it. Insinde, we can see few files, and directories, but most important for now are 2 of them:
 *
 * - **application**
 * - **system**
 *
 * The **system** directory contains all the **Choco OS** system sources. The **application** is reserved for the user space. It contains user configuration files, user programs, libraries, etc. We want to create the **hello world** program, which is not a part of the system, so we should use the application layer. After you enter it, you will see directories:
 *
 * ![application dir.PNG](https://bitbucket.org/repo/ro4RAb/images/230605320-application%20dir.PNG)
 *
 * - **config** <i>- configuration files</i>
 * - **libraries** <i>- user libraries</i>
 * - **programs** <i>- user programs</i>
 *
 * The list above represents probably all directories, that will be necessary for you. The **config** contains files with configuration of the system, **libraries** list of libraries, that user need to add, and the **programs** contains all programs created for **Choco OS** by user.
 *
 * @subsection Create-Source-File Creating application source file
 *
 * The program is a part of the 'Choco' system, which performs some tasks. It can be everything from turning on a diode to control spy ships. The system can be consist of many programs, which can be executed at the same time.
 *
 * Programs in the system are compiled with the rest of the sources. To add your own program, just create a source file in the **programs** directory. Here you have an example:
 *
 * ~~~~~~~~~~~~~~~{.c}
 * //
 * // Optional for stdio operations
 * //
 * #include <stdio.h>
 * //
 * // For choco os headers. If you do not need to use special libraries
 * // from the system, you must not add this include
 * //
 * #include <choco.h>
 *
 * //
 * // The main function of the program.
 * //
 * // @param Argc    Number of arguments
 * // @param Argv    Array of program arguments
 * //
 * // @return status code
 * //
 * int ExampleProgramName( int Argc , char ** Argv )
 * {
 *     return 0;
 * }
 *
 * ~~~~~~~~~~~~~~~
 *
 * In a code above you can see the simplest program in the system, named **ExampleProgramName**. It consist of the main function, which will be called, when the system, or user will execute the program. It is important, to define this function exactly in the same type. It should return **int** and has 2 parameters in types **int** and **char ****
 *
 * Either the **stdio.h** and **choc.h** headers are included only if necessary.
 *
 * @subsection Registration Registration the program in the system
 *
 * After you create a source file, you must register it in the system. So far as it is automatically added to the makefile, you must register its main function manually. To do it, find the **oc_programs_cfg.h** file in the config directory, and then find the oC_USER_PROGRAMS_LIST definition, where you should add following line at the end of the list
 *
 * ~~~~~~~~~~~~~{.c}
 *
    USE( ExampleProgramName , "MyApp" , 512)\
 * ~~~~~~~~~~~~~
 *
 * The first parameter is the name of the main program function. The second one is the name in the file system. Last parameter determines how many stack words is needed to handle it. If you dont know, you can leave 512 value. If there will be any problems (hard faults), then just type here grower value.
 *
 * In the result the programs list should looks something like this:
 *
 * ~~~~~~~~~~~~~~~{.c}
 *
 * #define oC_USER_PROGRAMS_LIST( USE , DONT_USE )\
 *                      USE( HelloWorld , "hello-world" , 512)\
 *                      USE( ExampleProgramName , "MyApp", 512) \
 *
 * ~~~~~~~~~~~~~~~
 *
 * # Execute a program #
 *
 * Now, when your program is registered, you must call it in the system. The easier way to do it, is set it as the program which will be executed after system starts. To do it, you just must set the **oC_DEFAULT_PROGRAM** in the **config/oc_programs_cfg.h** file to your program name, as in the example below:
 *
 *
 * ~~~~~~~~~~~~~{.c}
 *
 * #define oC_DEFAULT_PROGRAM             ExampleProgramName
 *
 * ~~~~~~~~~~~~~
 *
 *
 * @section DriversUsage 3. Drivers Usage
 * _________________________________________________________________________________________________________________________________________
 *
 * @subsection Introduction Introduction
 *
 * This training describes how to use drivers in the system. After it you will be able to:
 * - understand the essence of drivers
 * - create a configuration structure for the driver
 * - configure driver
 * - check if configuration was successfully
 *
 * @subsection About What is a driver
 *
 * As you probably guess, the driver is software component, that helps handle some device connected to the uC. It is true, but it is not all functionality of it, because it also helps using a micro-controller peripherials. Drivers can use each others, thanks to that the configuration is limited to set only really necessary parameters. For example, if you are using SPI driver, you mustn't use GPIO driver to configure the pins - you just set the pins reference and SPI driver takes care of the rest. Moreover configuration of the drivers is independent of the selected uC. You do not need to know what kind of uC is currently in use, and how to initialize a peripherials in it. If you will choose some functions, that are not handled in selected uC, the configuration function will return you code of error.
 *
 * @subsection Naming Naming convention
 *
 * The **Choco OS** is split into modules. Each module has its own name prefix, for example the GPIO driver functions begins from oC_GPIO_[FunctionName]. Additionaly types in the system ends by **_t** suffix, and the values that can be assigned to the type (for example enumerators values) begins by type name. Some example:
 *
 * ~~~~~~~~~~~~~{.c}
 * typedef enum
 * {
 *   oC_GPIO_Speed_Minimum ,
 *   oC_GPIO_Speed_Maximum
 * } oC_GPIO_Speed_t;
 * ~~~~~~~~~~~~~
 *
 * @subsection DriverConfig Driver Configuration
 *
 * To configure any driver, the first thing you should do, is create a configuration structure. Each driver has its own configure definition, with different fields in the structure, but the usage always looks the same. When you have created the structure, you should call the **oC_[DRIVER_NAME]_Configure** function with a reference to the configuration structure. Note, that in most of cases you cannot lost this configuration structure after configuration, so it is recommended to set it as constant values (stored in flash memory). Here you have some example, which will configure a gpio pin to work in output mode:
 *
 * ~~~~~~~~~~~~~{.c}
 *
 * int ConfigureLed(int Argc , char ** Argv )
 * {
 *   oC_ErrorCode_t errorCode = oC_ErrorCode_None;
 *   const oC_GPIO_Config_t led = {
 *       .PinLink = oC_GPIO_PinLink_PA3 ,
 *       .Mode    = oC_GPIO_Mode_Output
 *   };
 *
 *   errorCode = oC_GPIO_Configure( &led );
 *
 *   if ( oC_PrintResult( errorCode , "led configuration") )
 *   {
 *      // This will be executed when operation will be success
 *   }
 *
 *   return 0;
 * }
 * ~~~~~~~~~~~~~
 *
 * @warning
 * Note, that if you are using the **const** keyword, you must not fill all configuration fields, because in this case, all other fields will be set to default (0) values. In other cases (when the config is stored in the RAM memory), you should fill all fields
 *
 * @subsection Interface Driver interface
 *
 * Each driver in the system must have defined some basic functions and types, below is the list which represents most important of them.
 *
 * ~~~~~~~~~~~~~{.c}
 *
 * //
 * //  Each driver must has a configuration structure, like this
 * //
 * typedef struct
 * {
 *    // Config fields here
 * } oC_[DRIVER_NAME]_Config_t;
 *
 * // Function to configure driver
 * extern oC_ErrorCode_t oC_[DRIVER_NAME]_Configure( const oC_[DRIVER_NAME]_Config_t * const Config );
 *
 * // Function to unconfigure driver (it restore default settings, and remove config reference from the list)
 * extern oC_ErrorCode_t oC_[DRIVER_NAME]_Unconfigure( const oC_[DRIVER_NAME]_Config_t * const Config );
 *
 * ~~~~~~~~~~~~~
 *
 * Of course each occurrence of **[DRIVER_NAME]** you should replace by real driver name, for example **GPIO**.
 * The **oC_[DRIVER_NAME]_Unconfigure** function is for removing configuration from the list. It restores default values on the selected peripherial. Both of these functions (Configure and Unconfigure) returns an error code. Thanks to that, you can check if the operation finishes successfully (for example using function **oC_ErrorOccur()** which returns a true, if there was an error ), and get to know, what went wrong (for example by using function **oC_PrintError()**)
 *
 * @subsection DefaultCfg Default drivers configurations
 *
 * There is one very comfortable way to configure drivers before start of the system. Thanks to this option, you must not call the **Configure** function, and you will be able to use the configuration structures in each program, that you will write with sureness, that driver is correctly initialized. To create some default configuration, the first thing you should do is open the **oc_default_drivers_cfg.c** source file, and create a configuration structure. Note, that it **must** be declared as constant. You can name it as you want, there is no special rules about it, but it is recommended to add the **[DRIVER_NAME]** prefix to it. Example below shows configuration of the **SYS** driver to work with external oscillator with final frequency of 80MHz.
 *
 * ~~~~~~~~~~~~~{.c}
 * const oC_SYS_Config_t SYS_Config    = {
 *                                       .WantedFrequency     = oC_Frequency_MHz(80),
 *                                       .OscillatorFrequency = oC_Frequency_MHz(16),
 *                                       .OscillatorSource    = oC_SYS_OscillatorSource_External
 * };
 * ~~~~~~~~~~~~~
 *
 * As you see, the configuration was named **SYS_Config**. It is important to remember this name, because now we must use it for adding the configuration to the default load list. It is placed in the **oc_default_drivers_cfg.h** file in the **DEFAULT_DRIVERS_CONFIGURATION_LIST** definition. To add our configuration to list, just use **CONFIGURE** macro, and add new line at the end, as in example below:
 *
 * ~~~~~~~~~~~~~{.c}
 * #define DEFAULT_DRIVERS_CONFIGURATION_LIST( CONFIGURE , DONT_CONFIGURE )                                 \
 *     CONFIGURE( SYS , SYS_Config )   \
 *
 * ~~~~~~~~~~~~~
 *
 * The first parameter of the **CONFIGURE** macro ( *SYS* ) is the name of the driver, which should be used for configuration operation, and the second parameter is the name of the configuration structure, which we have created before.
 *
 * Now, if all ours configurations are filled correctly, we will be able to use **SYS_Config** structure in each program.
 *
 * @subsection Example Example blinking LED program
 *
 * This example shows an application, that will configure a LED and blinks it. We will not use the **defualt drivers configurations* mechanism, because we want to configure it only when the program will be run. Out program is just named *BlinkingLED*, and it contains code below:
 *
 * ~~~~~~~~~~~~~{.c}
 *
 * int BlinkingLED( int Argc , char ** Argv )
 * {
 *   const oC_GPIO_Config_t LED = {
 *                                                .PinLink = oC_GPIO_PinLink_PA3 ,
 *                                                .Mode    = oC_GPIO_Mode_Output
 *   };
 *
 *   if ( oC_GPIO_Configure( &LED ) == oC_ErrorCode_None )
 *   {
 *       while(1)
 *       {
 *           oC_GPIO_SetData( LED.PinLink , oC_GPIO_Pin_All );  // This will set high state on all configured pins
 *           oC_Time_SleepFor_msec( 500 ) ;
 *           oC_GPIO_SetData( LED.PinLink , oC_GPIO_Pin_None );  // This will set low state on all configured pins
 *           oC_Time_SleepFor_msec( 500 ) ;
 *        }
 *   }
 *   else
 *   {
 *      oC_IMPLEMENT_FAILURE(); // Configuration failed
 *   }
 *
 *   return 0;
 * }
 * ~~~~~~~~~~~~~
 *
 * @section PuTTYConfiguration 4. PuTTY Configuration
 * @subsection Introduction
 * This chapter describes how to configure PuTTY terminal for Choco OS system
 *
 * @subsection GetRes Getting Resources
 *
 * Of course, you need to download [PuTTY](http://www.putty.org/) and the converter UART<->USB will be required.
 *
 * @subsection InstalDriv Installing drivers for the converter
 *
 * Install the drivers for the converter. After successful installation check COM port which is running converter.
 * To do this go sequentially (in Windows 7): Start->Control Panel->Manager Devices, then search your converter.
 *
 * @subsection Read Read hardware specification and connect pins to board
 *
 * In oc_default_drivers_cfg.c read configuration of UART driver(especially .BitRate and .Pins)
 * After this, connect Tx pin of USB converter to Rx pin of UART microcontroller.
 * And similiary Rx pin of USB converter to Tx pin of UART microcontroller.
 *
 * @subsection PuttyCfg PuTTY Configuration
 *
 * Run PuTTY, choose Serial Connection type. In Connection\Serial section enter number of COM port and UART specification.
 * ![PuTTY.jpg](https://bitbucket.org/repo/ro4RAb/images/1441148983-PuTTY.jpg)Configure Putty
 * Then in session section you can give a name to the configuration for the future.
 *
 * After all these steps, you can open session in PuTTY.
 *
 *
 ******************************************************************************************************************************************/
#ifndef CHOC_H_
#define CHOC_H_

#include <oc_os.h>
#include <oc_synch.h>
#include <oc_time.h>
#include <oc_drivers.h>

/** ****************************************************************************************************************************************
 * The section with architecture
 */
#define _________________________________________ARCHITECTURE_SECTION_______________________________________________________________________

/**
 * @page Architecture Architecture
 *
 * @section Introduction Introduction
 *  ________________________________________________________________________________________________________________________________________
 *
 * The **Choco OS** system is divided on spaces that defines access to the system resources. Modules in the same space can communicate each
 * other, but it cannot use resources from higher layers. There are 2 exceptions of this rule: helpers and libraries space, that can be
 * accessed from each layer. Detailed description of each space is placed in a later section of this page. The picture below shows arrangement
 * of basic system spaces.
 *
 * ![main architecture.jpg](https://bitbucket.org/repo/ro4RAb/images/76918815-main%20architecture.jpg)
 *
 * @section UserSpace User Space
 *  ________________________________________________________________________________________________________________________________________
 *
 * The User Space is the highest level in the system. It consist of user libraries and programs, that can access only to 3 spaces:
 *
 *  - Core Space
 *  - Libraries Space
 *  - Helpers Space
 *
 * The Program allocated in this space can't use lower layers (drivers for example). This is the space, that is most safe in the system what
 * is the most significant advantage and reason why to use this layer. The main weakness of it is that in many cases it require much more
 * time to handle some operations such as write and read data from drivers, but the user can simply move their programs to the system space
 * by changing the oc_programs_cfg.h configuration file.
 *
 * @subsection Libraries Libraries
 *
 * The user can add an own library that will be shared between programs (it will be visible also in system space programs).
 *
 * @subsection UserPrograms Programs
 *
 * Program that are placed in the user space can use core space modules, system and user libraries, and standard libraries. Access to the
 * drivers in this layer can be assigned using the IOCTL and file system interfaces.
 *
 * @section CoreSpace Core Space
 *  ________________________________________________________________________________________________________________________________________
 *
 * The Core Space consists of modules that handles system resources such as CPU time, memory and file system. It also contains programs
 * that helps to use them properly for example from the system shell. This layer is the main border between user and system resources. It has
 * access to following spaces:
 *
 *  - Full Core Space (access to itself)
 *  - Drivers Space (both drivers manager and drivers)
 *  - Libraries Space
 *  - Helpers Space
 *  - Part of the Portable Space ( machine definitions )
 *
 * @subsection Programs
 *
 * The Core Space programs are admitted to use both full the core space and the drivers space what is the main advantage of keeping programs in
 * this layer. These programs are also little faster than programs in the user space, but it is less secure to keep programs here, so it is recommended
 * to do it only when it is really necessary.
 *
 * @subsection FileSystem File System
 *
 * The file system allow to manage files. Note that each driver can be handled using special files. More informations about it you will find
 * in drivers and files sections.
 *
 * @subsection OS OS
 *
 * The OS (Operating System) consist of threads, processes, and synchronization modules, that helps to manage system resources. More informations
 * you can find on the OS page.
 *
 * @subsection MemoryManager Memory Manager
 *
 * The memory manager module is responsible for all operations that handles memory, such as allocate, free, and calculating statistics about
 * allocated size, available size, etc.
 *
 * @section Helpers Helpers Space
 *  ________________________________________________________________________________________________________________________________________
 *
 * The Helpers Space is special space, that can access to the same layers as the Core Space, but it can be used in every spaces, also in the
 * portable part. To get more informations look at the Helpers page.
 *
 * @section LibrariesSpace Libraries Space
 *  ________________________________________________________________________________________________________________________________________
 *
 * The system libraries space has access only to itself, but it can be used in any module at any layer. To get to know more about it, look at
 * the libraries page.
 *
 * @section DriversSpace Drivers Space
 *  ________________________________________________________________________________________________________________________________________
 *
 * The drivers in the system are used for handle physical parts such as machine peripherals and chips that are connected to the micro-controller.
 * To know more see the drivers page. The space has access to:
 *
 *  - Portable Space
 *  - Helpers Space
 *  - Libraries Space
 * ________________________________________________________________________________________________________________________________________
 *
 * @section PortableSpace Portable Space
 *
 * The Portable Space consists of all definitions and functions that are depended of the micro controller, such as register map, interrupts
 * management, peripherals configurations, etc. It is allowed to use:
 *
 *  - Libraries Space
 *  - Helpers Space
 */

/* END OF SECTION */
#undef  _________________________________________ARCHITECTURE_SECTION_______________________________________________________________________



#endif /* CHOC_H_ */
