/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_vfs.h
 *
 *    @brief      The file with interface for the virtual file system module
 *
 *    @author     Patryk Kubiak - (Created on: 10 pa� 2014 09:48:09)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_VFS_OC_VFS_H_
#define SYSTEM_VFS_OC_VFS_H_

/*==========================================================================================================================================
//
//    INCLUDES
//
//========================================================================================================================================*/
// CFG
#include <oc_vfs_cfg.h>

// Libraries
#include <oc_string.h>

// VFS
#include <oc_vfs_types.h>

// Others

/*==========================================================================================================================================
//
//    CONSTS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    MACROS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    FUNCTION PROTOTYPES
//
//========================================================================================================================================*/
extern void             oC_VFS_Initialize                                   ( void );
extern void             oC_VFS_Deinitialize                                 ( void );
extern bool             oC_VFS_Mount                                        ( const oC_STRING_t MountPoint , const oC_VFS_FileSystemInterface_t * const FileSystemInterface );
extern bool             oC_VFS_Unmount                                      ( const oC_STRING_t MountPoint );
extern bool             oC_VFS_MakeDirectory                                ( const oC_STRING_t Path );
extern bool             oC_VFS_OpenDirectory                                ( const oC_STRING_t Path , oC_VFS_Dir_t * DirReference );
extern bool             oC_VFS_OpenFile                                     ( oC_VFS_File_t * FilePointer , const oC_STRING_t Path , oC_VFS_Mode_t Mode );
extern bool             oC_VFS_CloseFile                                    ( oC_VFS_File_t * FilePointer );
extern bool             oC_VFS_ReadFile                                     ( oC_VFS_File_t * FilePointer , void * BufferDestination , oC_Size_t Size , oC_Size_t * BytesRead );
extern bool             oC_VFS_WriteFile                                    ( oC_VFS_File_t * FilePointer , const void * Buffer , oC_Size_t Size , oC_Size_t * BytesWritten );
extern bool             oC_VFS_ChangeFilePosition                           ( oC_VFS_File_t * FilePointer , oC_MemoryInt_t Offset );
extern bool             oC_VFS_Truncate                                     ( oC_VFS_File_t * FilePointer );
extern bool             oC_VFS_FlushCachedData                              ( oC_VFS_File_t * FilePointer );
extern bool             oC_VFS_Forward                                      ( oC_VFS_File_t * FilePointer , void * StreamContext , int BytesToForward , int * BytesForwarded );
extern bool             oC_VFS_GetStringFromFile                            ( oC_VFS_File_t * FilePointer , oC_STRING_t * BufferDestination , int BufferLength );
extern bool             oC_VFS_PutString                                    ( oC_VFS_File_t * FilePointer , const oC_STRING_t String );
extern bool             oC_VFS_GetCurrentFilePosition                       ( oC_VFS_File_t * FilePointer , oC_MemoryInt_t * PosDestination );
extern bool             oC_VFS_CheckIfIsEndOfFile                           ( oC_VFS_File_t * FilePointer , bool * EndOfFileDestination );
extern bool             oC_VFS_GetFileSize                                  ( oC_VFS_File_t * FilePointer , oC_Size_t * SizeDestination );
extern bool             oC_VFS_CheckIfError                                 ( oC_VFS_File_t * FilePointer , bool * ErrorDestination );

extern bool             oC_VFS_OpenFileByDescriptor                         ( int * FileDescriptor , const oC_STRING_t Path , oC_VFS_Mode_t Mode );
extern bool             oC_VFS_CloseFileByDescriptor                        ( int   FileDescriptor );
extern bool             oC_VFS_ReadFileByDescriptor                         ( int   FileDescriptor , void * BufferDestination , oC_Size_t Size , oC_Size_t * BytesRead );
extern bool             oC_VFS_WriteFileByDescriptor                        ( int   FileDescriptor , const void * Buffer , oC_Size_t Size , oC_Size_t * BytesWritten );
extern bool             oC_VFS_ChangeFilePositionByDescriptor               ( int   FileDescriptor , oC_MemoryInt_t Offset );
extern bool             oC_VFS_TruncateByDescriptor                         ( int   FileDescriptor );
extern bool             oC_VFS_FlushCachedDataByDescriptor                  ( int   FileDescriptor );
extern bool             oC_VFS_ForwardByDescriptor                          ( int   FileDescriptor , void * StreamContext , int BytesToForward , int * BytesForwarded );
extern bool             oC_VFS_GetStringFromFileByDescriptor                ( int   FileDescriptor , oC_STRING_t * BufferDestination , int BufferLength );
extern bool             oC_VFS_PutCharByDescriptor                          ( int   FileDescriptor , char C );
extern bool             oC_VFS_PutStringByDescriptor                        ( int   FileDescriptor , const oC_STRING_t String );
extern bool             oC_VFS_GetCurrentFilePositionByDescriptor           ( int   FileDescriptor , oC_MemoryInt_t * PosDestination );
extern bool             oC_VFS_CheckIfIsEndOfFileByDescriptor               ( int   FileDescriptor , bool * EndOfFileDestination );
extern bool             oC_VFS_GetFileSizeByDescriptor                      ( int   FileDescriptor , oC_Size_t * SizeDestination );
extern bool             oC_VFS_CheckIfErrorByDescriptor                     ( int   FileDescriptor , bool * ErrorDestination );

extern bool             oC_VFS_GetFileInfo                                  ( const oC_STRING_t Path , oC_VFS_FileInfo_t * FileInfoDestination );
extern bool             oC_VFS_Remove                                       ( const oC_STRING_t Path );
extern bool             oC_VFS_ChangeAttributes                             ( const oC_STRING_t Path , oC_VFS_Attribute_t Attributes , oC_VFS_Attribute_t AttributesMask );
extern bool             oC_VFS_SetTimeStamp                                 ( const oC_STRING_t Path , oC_MemoryInt_t TimeStamp );
extern bool             oC_VFS_Rename                                       ( const oC_STRING_t OldPath , const oC_STRING_t NewPath );
extern bool             oC_VFS_GetFree                                      ( const oC_STRING_t Path , oC_Size_t * FreeSpace );

/*==========================================================================================================================================
//
//    INLINE FUNCTIONS
//
//========================================================================================================================================*/


#endif /* SYSTEM_VFS_OC_VFS_H_ */
