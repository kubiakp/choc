/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_vfs_types.h
 *
 *    @brief      File with types for the VFS
 *
 *    @author     Patryk Kubiak - (Created on: 29 paź 2014 18:46:38)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_VFS_OC_VFS_TYPES_H_
#define SYSTEM_VFS_OC_VFS_TYPES_H_

/*==========================================================================================================================================
//
//    INCLUDES
//
//========================================================================================================================================*/
// ChOC Definitions
#include <oc_gen_macros.h>

#include <oc_string.h>
#include <oc_memory_types.h>

// STD LIBRARIES
#include <stdbool.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/stat.h>

/*==========================================================================================================================================
//
//    CONSTS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    TYPES
//
//========================================================================================================================================*/
/**
 * Mode flags that specifies the type of access and open method for the file. It is specified by a combination of following flags.
 */
typedef enum
{
    oC_VFS_Mode_Read ,               //!< Specifies read access to the object. Data can be read from the file. Combine with FA_WRITE for read-write access.
    oC_VFS_Mode_Write ,              //!< Specifies write access to the object. Data can be written to the file. Combine with FA_READ for read-write access.
    oC_VFS_Mode_OpenExisting ,       //!< Opens the file. The function fails if the file is not existing. (Default)
    oC_VFS_Mode_OpenAlways ,         //!< Opens the file if it is existing. If not, a new file is created.
    oC_VFS_Mode_CreateNew ,          //!< Creates a new file. The function fails with FR_EXIST if the file is existing.
    oC_VFS_Mode_CreateNewAlways      //!< Creates a new file. If the file is existing, it will be truncated and overwritten.
} oC_VFS_Mode_t;

typedef void * oC_VFS_Dir_t;        //!< Directory object structure (DIR)

/**
 * Structure with the file data for operations on the files
 */
typedef struct
{
    void*               FSContext;  //!< A pointer for the file system
    oC_STRING_t         Path;       //!< Path of the file
} oC_VFS_File_t;

/**
 * The structure with the file status
 */
typedef struct
{
    oC_Size_t           fsize;          /**< File size */
    oC_MemoryInt_t      fdate;          /**< Last modified date */
    oC_MemoryInt_t      ftime;          /**< Last modified time */
    uint8_t             fattrib;        /**< Attribute */
    oC_STRING_t         fname;          /**< Short file name (8.3 format) */
    oC_STRING_t         lfname;         /**< Pointer to the LFN buffer */
    oC_MemoryInt_t      lfsize;         /**< Size of LFN buffer in bytes */
} oC_VFS_FileInfo_t;

/**
 * The enum represents attribute flags to be set in one or more combination of the following flags.
 */
typedef enum
{
    oC_VFS_Attribute_ReadOnly = oC_BIT_0,   //!< Read Only
    oC_VFS_Attribute_Archive  = oC_BIT_1,   //!< Archive
    oC_VFS_Attribute_System   = oC_BIT_2,   //!< System
    oC_VFS_Attribute_Hidden   = oC_BIT_3    //!< Hidden
} oC_VFS_Attribute_t;

/**
 * The type with file system interface definitions.
 */
typedef struct
{
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Register a work area
     *
     * @param ContextDestination    [out]   Destination to a context of the file system if needed.
     * @param Path                  [in]    Path to mount file system
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*mount                 )( void ** ContextDestination , const oC_STRING_t Path );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Open/Create a file
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [out]   Pointer to a file structure
     * @param Path                  [in]    Path to the file to open
     * @param Mode                  [in]    Mode of the open file
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*fopen                 )( void *  Context , void **  FilePointer , const oC_STRING_t Path , oC_VFS_Mode_t Mode );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Close an open file
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*fclose                )( void *  Context , void *  FilePointer );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Read file
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param BufferDestination     [out]   Destination for the read data
     * @param Size                  [in]    Size of the BufferDestination
     * @param BytesRead             [out]   Bytes read from a file
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*fread                 )( void *  Context , void *  FilePointer , void * BufferDestination , oC_Size_t Size , oC_Size_t * BytesRead );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Write file
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param Buffer                [in]    Buffer to write into the file
     * @param Size                  [in]    Size of the buffer to write
     * @param BytesWritten          [out]   Number of bytes written into the file
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*fwrite                )( void *  Context , void *  FilePointer , const void * Buffer , oC_Size_t Size , oC_Size_t * BytesWritten );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Move read/write pointer, Expand file size
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param Offset                [in]    Byte offset from top of the file.
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*lseek                 )( void *  Context , void *  FilePointer , oC_MemoryInt_t Offset );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Truncate file size
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*truncate              )( void *  Context , void *  FilePointer );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Flush cached data
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*sync                  )( void *  Context , void *  FilePointer );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Forward file data to the stream
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param StreamContext         [in]    Context of the stream to forward file
     * @param BytesToForward        [in]    Number of bytes to forward from the file
     * @param BytesForwarded        [out]   Number of forwarded bytes from the file
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*forward               )( void *  Context , void *  FilePointer , void * StreamContext , int BytesToForward , int * BytesForwarded );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Read a string
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param BufferDestination     [out]   Reference to the buffer read data
     * @param BufferLength          [in]    Length of the BufferDestination
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*gets                  )( void *  Context , void *  FilePointer , oC_STRING_t * BufferDestination , int BufferLength );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Write a string
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param String                [in]    String to put into the file
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*puts                  )( void *  Context , void *  FilePointer , const oC_STRING_t String );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Get current read/write pointer
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param PosDestination        [out]   Current read/write pointer of the file.
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*tell                  )( void *  Context , void *  FilePointer , oC_MemoryInt_t * PosDestination );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Test for end-of-file on a file
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param EOFDestination        [out]   Non-zero value if the read/write pointer has reached end of the file; otherwise it returns a zero.
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*eof                   )( void *  Context , void *  FilePointer , bool * EOFDestination );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * The size function gets the size of a file.
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param SizeDestination       [out]   Returns the size of the file in unit of byte.
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*size                  )( void *  Context , void *  FilePointer , oC_Size_t * SizeDestination );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * The f_error tests for an error on a file.
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FilePointer           [in]    Pointer to the opened file
     * @param ErrorDestination      [out]   Non-zero value if a hard error has occurred; otherwise it returns a zero.
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*error                 )( void *  Context , void *  FilePointer , bool * ErrorDestination );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * The stat function checks the existence of a file or sub-directory.
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param Path                  [in]    Path to a file / sub-directory to check
     * @param FileInfoDestination   [out]   Pointer to the blank FileInfo structure to store the information of the object. Set null pointer if it is not needed.
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*stat                  )( void *  Context , const oC_STRING_t Path , oC_VFS_FileInfo_t * FileInfoDestination );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Unregister file system
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*unmount               )( void *  Context );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Open a directory
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param Path                  [in]    Path to a directory to open
     * @param DirDestination        [out]   Pointer to the blank directory object to create a new one.
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*opendir               )( void *  Context , const oC_STRING_t Path , oC_VFS_Dir_t * DirDestination );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * The function creates a new directory.
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param Path                  [in]    Path to a directory to create
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*mkdir                 )( void *  Context , const oC_STRING_t Path );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Remove a file or sub-directory
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param Path                  [in]    Path to a file or directory to remove
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*unlink                )( void *  Context , const oC_STRING_t Path );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * The chmod function changes the attribute of a file or sub-directory.
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param Path                  [in]    Path to a file or sub-directory to change the attributes
     * @param Attributes            [in]    Attribute flags to be set in one or more combination of the following flags. The specified flags are set and others are cleard.
     * @param AttributesMask        [in]    Attribute mask that specifies which attribute is changed. The specified attributes are set or cleard and others are left unchanged.
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*chmod                 )( void *  Context , const oC_STRING_t Path , oC_VFS_Attribute_t Attributes , oC_VFS_Attribute_t AttributesMask );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * The utime function changes the time-stamp of a file or sub-directory.
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param Path                  [in]    Path to a file to set time-stamp
     * @param TimeStamp             [in]    Time-stamp to set
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*utime                 )( void *  Context , const oC_STRING_t Path , oC_MemoryInt_t TimeStamp );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Rename/Move a file or sub-directory
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param OldPath               [in]    Old path to a file / sub-directory
     * @param NewPath               [in]    New path to a file / sub-directory
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*rename                )( void *  Context , const oC_STRING_t OldPath , const oC_STRING_t NewPath );
    //--------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Get free space on the volume
     *
     * @param Context               [in]    Pointer to the context structure, which was created during mounting. If not used it can be null
     * @param FreeSpace             [out]   Free size in the file system
     *
     * @return  0 if success
     */
    //--------------------------------------------------------------------------------------------------------------------------------------
    int         (*getfree               )( void *  Context , oC_Size_t * FreeSpace );
} oC_VFS_FileSystemInterface_t;

/*==========================================================================================================================================
//
//    MACROS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    FUNCTION PROTOTYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    INLINE FUNCTIONS
//
//========================================================================================================================================*/


#endif /* SYSTEM_VFS_OC_VFS_TYPES_H_ */
