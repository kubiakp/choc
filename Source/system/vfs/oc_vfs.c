/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_vfs.c
 *
 *    @brief      The file with virtual file system sources.
 *
 *    @author     Patryk Kubiak - (Created on: 13 pa� 2014 15:47:22)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//    INCLUDES
//
//========================================================================================================================================*/

#include <oc_gen_macros.h>
#include <oc_vfs.h>
#include <oC_Debug.h>
#include <oc_dlist.h>

/*==========================================================================================================================================
//
//    LOCAL CONSTS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    LOCAL TYPES
//
//========================================================================================================================================*/
/**
 * The structure with information about mounted file system.
 */
typedef struct
{
    oC_STRING_t                                 MountPoint;         //!< Path to the mounted point of the file system
    int                                         MountPointLen;      //!< Length of the MountPoint string
    const oC_VFS_FileSystemInterface_t *        Interface;          //!< Interface for handling the file system
    void *                                      Context;            //!< Context of the file system
} _FileSystem_t;

/*==========================================================================================================================================
//
//    MACROS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//    LOCAL VARIABLES
//
//========================================================================================================================================*/

/**
 * The container for storing variables of the VFS module.
 */
struct
{
    oC_DLIST_t*                     MountedFileSystems;             /**< List of the mounted file systems. Each element of the struct is in type #_FileSystem_t */
} _VFS_Container;

/*==========================================================================================================================================
//
//    LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static        bool                  _FindFileSystem                 ( const oC_STRING_t MountPoint , _FileSystem_t ** FileSystem , oC_STRING_t * SubPath );
static        bool                  _IsCorrectMountPointPath        ( const oC_STRING_t MountPoint );
static        bool                  _Mount                          ( const oC_STRING_t MountPoint , const oC_VFS_FileSystemInterface_t * const FileSystemInterface );
static        bool                  _Unmount                        ( const oC_STRING_t MountPoint );
static        bool                  _CheckFileSystemInterface       ( const oC_VFS_FileSystemInterface_t * const FileSystemInterface );
static        bool                  _MakeDirectory                  ( const oC_STRING_t Path );
static        bool                  _OpenDirectory                  ( const oC_STRING_t Path , oC_VFS_Dir_t * DirDestination );
static        bool                  _OpenFile                       ( oC_VFS_File_t * FilePointer , const oC_STRING_t Path , oC_VFS_Mode_t Mode );
static        bool                  _CloseFile                      ( oC_VFS_File_t * FilePointer );
static        bool                  _ReadFile                       ( oC_VFS_File_t * FilePointer , void * BufferDestination , oC_Size_t Size , oC_Size_t * BytesRead );
static        bool                  _WriteFile                      ( oC_VFS_File_t * FilePointer , const void * Buffer , oC_Size_t Size , oC_Size_t * BytesWritten );
static        bool                  _ChangeFilePosition             ( oC_VFS_File_t * FilePointer , oC_MemoryInt_t Offset );
static        bool                  _Truncate                       ( oC_VFS_File_t * FilePointer );
static        bool                  _FlushCachedData                ( oC_VFS_File_t * FilePointer );
static        bool                  _Forward                        ( oC_VFS_File_t * FilePointer , void * StreamContext , int BytesToForward , int * BytesForwarded );
static        bool                  _GetStringFromFile              ( oC_VFS_File_t * FilePointer , oC_STRING_t * BufferDestination , int BufferLength );
static        bool                  _PutString                      ( oC_VFS_File_t * FilePointer , const oC_STRING_t String );
static        bool                  _GetCurrentFilePosition         ( oC_VFS_File_t * FilePointer , oC_MemoryInt_t * PosDestination );
static        bool                  _CheckIfIsEndOfFile             ( oC_VFS_File_t * FilePointer , bool * EndOfFileDestination );
static        bool                  _GetFileSize                    ( oC_VFS_File_t * FilePointer , oC_Size_t * SizeDestination );
static        bool                  _CheckIfError                   ( oC_VFS_File_t * FilePointer , bool * ErrorDestination );
static        bool                  _GetFileInfo                    ( const oC_STRING_t Path , oC_VFS_FileInfo_t * FileInfoDestination );
static        bool                  _Remove                         ( const oC_STRING_t Path );
static        bool                  _ChangeAttributes               ( const oC_STRING_t Path , oC_VFS_Attribute_t Attributes , oC_VFS_Attribute_t AttributesMask );
static        bool                  _SetTimeStamp                   ( const oC_STRING_t Path , oC_MemoryInt_t TimeStamp );
static        bool                  _Rename                         ( const oC_STRING_t OldPath , const oC_STRING_t NewPath );
static        bool                  _GetFree                        ( const oC_STRING_t Path , oC_Size_t * FreeSpace );


/*==========================================================================================================================================
//
//    INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function initializes VFS module to work.
 */
//==========================================================================================================================================
void             oC_VFS_Initialize                                   ( void )
{
    _VFS_Container.MountedFileSystems   = oC_DLIST_New();
}

//==========================================================================================================================================
/**
 * The function releases VFS module when it is no needed anymore.
 */
//==========================================================================================================================================
void             oC_VFS_Deinitialize                                 ( void )
{
    oC_DLIST_Delete( _VFS_Container.MountedFileSystems );
}

//==========================================================================================================================================
/**
 * Mounts the file system in the MountPoint path.
 *
 * @param MountPoint                    Path where file system shall be mounted
 * @param FileSystemInterface           The reference to struct with interface of the file system.
 *
 * @return true if success, false if error
 */
//==========================================================================================================================================
bool oC_VFS_Mount( const oC_STRING_t MountPoint , const oC_VFS_FileSystemInterface_t * const FileSystemInterface )
{
    bool result = false;

    if ( FileSystemInterface == NULL )
    {
        oC_Debug( "FileSystemInterface cannot be NULL pointer!\n" );
    }
    else if ( !_IsCorrectMountPointPath( MountPoint ) )
    {
        oC_Debug( "The mount point '%s' is not correct!\n" , MountPoint );
    }
    else
    {
        result = _Mount( MountPoint , FileSystemInterface );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Remove mounted at the MountPoint path file system.
 *
 * @param MountPoint                    Path to the mounted file system
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VFS_Unmount( const oC_STRING_t MountPoint )
{
    bool result = false;

    if ( !_IsCorrectMountPointPath( MountPoint ) )
    {
        oC_Debug( "The mount point '%s' is not correct!\n" , MountPoint );
    }
    else
    {
        result  = _Unmount( MountPoint );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Makes new directory in the Path.
 *
 * @param Path                      Path to create a directory
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VFS_MakeDirectory( const oC_STRING_t Path )
{
    return _MakeDirectory( Path );
}

//==========================================================================================================================================
/**
 * Opens directory to read a content
 *
 * @param Path                      Path to directory to open
 * @param DirDestination            Destination for the open dir
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VFS_OpenDirectory( const oC_STRING_t Path , oC_VFS_Dir_t * DirDestination )
{
    bool result = false;

    if ( DirDestination != NULL )
    {
        result = _OpenDirectory( Path , DirDestination );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Function opens a file and create new pointer to it. It is VFS version of the 'fopen' function
 *
 * @param FilePointer               Pointer to the file structure
 * @param Path                      Path to the file to open
 * @param Mode                      Mode of the open file
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VFS_OpenFile( oC_VFS_File_t * FilePointer , const oC_STRING_t Path , oC_VFS_Mode_t Mode )
{
    bool result = false;

    if ( FilePointer != NULL )
    {
        result  = _OpenFile( FilePointer , Path , Mode );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function closes an opened file. It is VFS version of the 'fclose' function.
 *
 * @param FilePointer               [in]    Pointer to the file structure
 *
 * @return true if success
 */
//==========================================================================================================================================
bool             oC_VFS_CloseFile                                    ( oC_VFS_File_t * FilePointer )
{
    bool result = false;

    if ( FilePointer != NULL )
    {
        result = _CloseFile( FilePointer );
    }

    return result;
}



//==========================================================================================================================================
/**
 * The function for reading data from the file. It is VFS version ot the 'fread' function.
 *
 * @param FilePointer               [in]    Pointer to the file context
 * @param BufferDestination         [out]   Destination for a data
 * @param Size                      [in]    Size of the buffer
 * @param BytesRead                 [out]   Destination for the bytes read from the file. It can be NULL if not used.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VFS_ReadFile( oC_VFS_File_t * FilePointer , void * BufferDestination , oC_Size_t Size , oC_Size_t * BytesRead )
{
    bool result = false;

    if (
            (FilePointer != NULL)
         && (BufferDestination != NULL)
         && (Size > 0)
            )
    {
        result  = _ReadFile( FilePointer , BufferDestination , Size , BytesRead );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param Buffer
 * @param Size
 * @param BytesWritten
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_WriteFile              ( oC_VFS_File_t * FilePointer , const void * Buffer , oC_Size_t Size , oC_Size_t * BytesWritten )
{
    bool result = false;

    if (
            ( FilePointer != NULL )
         && ( Buffer      != NULL )
         && ( Size > 0 )
            )
    {
        result  = _WriteFile( FilePointer , Buffer , Size , BytesWritten );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param Offset
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_ChangeFilePosition     ( oC_VFS_File_t * FilePointer , oC_MemoryInt_t Offset )
{
    bool result = false;

    if ( FilePointer != NULL )
    {
        result  = _ChangeFilePosition( FilePointer , Offset );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_Truncate               ( oC_VFS_File_t * FilePointer )
{
    bool result = false;

    if ( FilePointer != NULL )
    {
        result = _Truncate( FilePointer );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_FlushCachedData        ( oC_VFS_File_t * FilePointer )
{
    bool result = false;

    if ( FilePointer != NULL )
    {
        result = _FlushCachedData( FilePointer );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param StreamContext
 * @param BytesToForward
 * @param BytesForwarded
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_Forward                ( oC_VFS_File_t * FilePointer , void * StreamContext , int BytesToForward , int * BytesForwarded )
{
    bool result = false;

    if (
            ( FilePointer != NULL )
         && ( StreamContext != NULL )
            )
    {
        result = _Forward( FilePointer , StreamContext , BytesToForward , BytesForwarded );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param BufferDestination
 * @param BufferLength
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_GetStringFromFile      ( oC_VFS_File_t * FilePointer , oC_STRING_t * BufferDestination , int BufferLength )
{
    bool result = false;

    if (
            ( FilePointer != NULL )
         && ( BufferDestination != NULL )
         && ( BufferLength > 0 )
            )
    {
        result = _GetStringFromFile( FilePointer , BufferDestination , BufferLength );
    }

    return result;
}


//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param String
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_PutString              ( oC_VFS_File_t * FilePointer , const oC_STRING_t String )
{
    bool result = false;

    if ( FilePointer != NULL && !oC_STRING_IsEmpty(String) )
    {
        result  = _PutString( FilePointer , String );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param PosDestination
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_GetCurrentFilePosition ( oC_VFS_File_t * FilePointer , oC_MemoryInt_t * PosDestination )
{
    bool result = false;

    if ( FilePointer != NULL && PosDestination != NULL )
    {
        result  = _GetCurrentFilePosition( FilePointer , PosDestination );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param EndOfFileDestination
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_CheckIfIsEndOfFile     ( oC_VFS_File_t * FilePointer , bool * EndOfFileDestination )
{
    bool result = false;

    if ( FilePointer != NULL && EndOfFileDestination != NULL )
    {
        result = _CheckIfIsEndOfFile( FilePointer , EndOfFileDestination );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param SizeDestination
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_GetFileSize            ( oC_VFS_File_t * FilePointer , oC_Size_t * SizeDestination )
{
    bool result = false;

    if ( FilePointer != NULL && SizeDestination != NULL )
    {
        result = _GetFileSize( FilePointer , SizeDestination );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param ErrorDestination
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_CheckIfError           ( oC_VFS_File_t * FilePointer , bool * ErrorDestination )
{
    bool result = false;

    if ( FilePointer != NULL && ErrorDestination != NULL )
    {
        result = _CheckIfError( FilePointer , ErrorDestination );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 * @param FileInfoDestination
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_GetFileInfo     ( const oC_STRING_t Path , oC_VFS_FileInfo_t * FileInfoDestination )
{
    bool result = false;

    if ( (FileInfoDestination != NULL) && ( !oC_STRING_IsEmpty( Path ) ) )
    {
        result = _GetFileInfo( Path , FileInfoDestination );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_Remove          ( const oC_STRING_t Path )
{
    bool result = false;

    if ( !oC_STRING_IsEmpty( Path ) )
    {
        result = _Remove( Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 * @param Attributes
 * @param AttributesMask
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_ChangeAttributes( const oC_STRING_t Path , oC_VFS_Attribute_t Attributes , oC_VFS_Attribute_t AttributesMask )
{
    bool result = false;

    if ( !oC_STRING_IsEmpty( Path ) )
    {
        result = _ChangeAttributes( Path , Attributes , AttributesMask );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 * @param TimeStamp
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_SetTimeStamp    ( const oC_STRING_t Path , oC_MemoryInt_t TimeStamp )
{
    bool result = false;

    if ( !oC_STRING_IsEmpty( Path ) )
    {
        result = _SetTimeStamp( Path , TimeStamp );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param OldPath
 * @param NewPath
 *
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_Rename          ( const oC_STRING_t OldPath , const oC_STRING_t NewPath )
{
    bool result = false;

    if ( !oC_STRING_IsEmpty(OldPath) && !oC_STRING_IsEmpty(NewPath) )
    {
        result = _Rename( OldPath , NewPath );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 * @param FreeSpace
 * @return
 */
//==========================================================================================================================================
bool oC_VFS_GetFree         ( const oC_STRING_t Path , oC_Size_t * FreeSpace )
{
    bool result = false;

    if ( !oC_STRING_IsEmpty(Path) && FreeSpace != NULL )
    {
        result = _GetFree( Path , FreeSpace );
    }

    return result;
}


/*==========================================================================================================================================
//
//    LOCAL FUNCTION 
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Checks if the mount point path is correct
 *
 * @param MountPoint            Path to check
 *
 * @return true if correct
 */
//==========================================================================================================================================
static bool _IsCorrectMountPointPath( const oC_STRING_t MountPoint )
{
    return !oC_STRING_IsEmpty( MountPoint );
}

//==========================================================================================================================================
/**
 * Search file system mounted at the MountPoint and return it by
 *
 * @param MountPoint            Path of the file system mount
 * @param FileSystem            Reference for return the FileSystem by the function
 * @param SubPath               Destination for the prepared subpath in the file system
 *
 * @return true if found
 */
//==========================================================================================================================================
static bool _FindFileSystem( const oC_STRING_t MountPoint , _FileSystem_t ** FileSystem , oC_STRING_t * SubPath )
{
    bool result = false;

    foreach( _FileSystem_t* , fileSystem , _VFS_Container.MountedFileSystems )
    {
        if ( oC_STRING_CompareN( fileSystem->MountPoint , MountPoint , fileSystem->MountPointLen , oC_STRING_CaseSensitivity_Sensitive ) )
        {
            *FileSystem = fileSystem;
            if (
                    oC_STRING_Set( SubPath , MountPoint )
                 && oC_STRING_Truncate( SubPath , fileSystem->MountPointLen )
                 )
            {
                result = true;
            }
            break;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function mounts the file system in the selected MountPoint.
 *
 * @warning It not checks parameters!
 *
 * @param MountPoint                    Path where file system shall be mounted
 * @param FileSystemInterface           The reference to struct with interface of the file system.
 *
 * @return true if success, false if error
 */
//==========================================================================================================================================
static bool _Mount( const oC_STRING_t MountPoint ,  const oC_VFS_FileSystemInterface_t * FileSystemInterface )
{
    bool result = false;

    _FileSystem_t fileSystem;

    fileSystem.Interface    = FileSystemInterface;
    fileSystem.MountPoint   = oC_STRING_New( MountPoint );
    fileSystem.MountPointLen= oC_STRING_Length( MountPoint );

    if ( _CheckFileSystemInterface( FileSystemInterface ) )
    {
        if ( FileSystemInterface->mount( &fileSystem.Context , MountPoint ) == 0 )
        {
            result = oC_DLIST_EmplacePushBack( _VFS_Container.MountedFileSystems , &fileSystem , sizeof(_FileSystem_t) );
        }
        else
        {
            oC_Debug( "Cannot mount file system in the mount point '%s' - error while initialization!\n" , MountPoint );
        }
    }
    else
    {
        oC_Debug( "Cannot mount file system in the mount point '%s' - interface is not correct!\n" , MountPoint );
    }


    return result;
}

//==========================================================================================================================================
/**
 * The function unmount file system from the MountPoint path.
 *
 * @param MountPoint                    Path to the file system
 *
 * @return true if success
 */
//==========================================================================================================================================
static        bool                  _Unmount                        ( const oC_STRING_t MountPoint )
{
    bool result = false;
    _FileSystem_t * fileSystem;

    if ( _FindFileSystem( MountPoint , &fileSystem , NULL ) )
    {
        oC_DLIST_Counter_t index = oC_DLIST_GetPosOfValue( _VFS_Container.MountedFileSystems , fileSystem );

        if ( index >= 0 )
        {
            if ( !fileSystem->Interface->unmount( fileSystem->Context ) )
            {
                oC_Debug("Error while deinitializing file system '%s'" , MountPoint);
            }

            result = oC_DLIST_RemoveAtPos( _VFS_Container.MountedFileSystems , index );
        }
        else
        {
            oC_Debug( "File system found, but cannot read the position on the list of it\n" );
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , MountPoint);
    }

    return result;
}

//==========================================================================================================================================
/**
 * Checks if the file system interface is correct
 *
 * @param FileSystemInterface           Reference to file system interface to check
 *
 * @return true if the interface is correct
 */
//==========================================================================================================================================
static bool _CheckFileSystemInterface( const oC_VFS_FileSystemInterface_t * const FileSystemInterface )
{
    return
            (FileSystemInterface->chmod     != NULL)
         && (FileSystemInterface->eof       != NULL)
         && (FileSystemInterface->error     != NULL)
         && (FileSystemInterface->fclose    != NULL)
         && (FileSystemInterface->fopen     != NULL)
         && (FileSystemInterface->forward   != NULL)
         && (FileSystemInterface->fread     != NULL)
         && (FileSystemInterface->fwrite    != NULL)
         && (FileSystemInterface->getfree   != NULL)
         && (FileSystemInterface->gets      != NULL)
         && (FileSystemInterface->lseek     != NULL)
         && (FileSystemInterface->mkdir     != NULL)
         && (FileSystemInterface->mount     != NULL)
         && (FileSystemInterface->opendir   != NULL)
         && (FileSystemInterface->puts      != NULL)
         && (FileSystemInterface->rename    != NULL)
         && (FileSystemInterface->size      != NULL)
         && (FileSystemInterface->stat      != NULL)
         && (FileSystemInterface->sync      != NULL)
         && (FileSystemInterface->tell      != NULL)
         && (FileSystemInterface->truncate  != NULL)
         && (FileSystemInterface->unlink    != NULL)
         && (FileSystemInterface->unmount   != NULL)
         && (FileSystemInterface->utime     != NULL);

}

//==========================================================================================================================================
/**
 * The function creates new directory. It not checks parameters
 *
 * @param Path          The full path to create a dir
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _MakeDirectory( const oC_STRING_t Path )
{
    bool result = false;
    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->mkdir( fileSystem->Context , subPath ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , Path);
    }

    return result;
}

//==========================================================================================================================================
/**
 * Opens a directory without checking the parameters
 *
 * @param Path              The full path to the directory to create
 * @param DirDestination    Destination for a open dir
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _OpenDirectory( const oC_STRING_t Path , oC_VFS_Dir_t * DirDestination )
{
    bool result = false;
    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->opendir( fileSystem->Context , subPath , DirDestination ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , Path);
    }

    return result;
}

//==========================================================================================================================================
/**
 * Function opens a file
 *
 * @warning it not checks parameters
 *
 * @param FilePointer           [out]   Reference to the file
 * @param Path                  [in]    Path to the file to open
 * @param Mode                  [in]    Mode of operation
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _OpenFile( oC_VFS_File_t * FilePointer , const oC_STRING_t Path , oC_VFS_Mode_t Mode )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->fopen( fileSystem->Context , &FilePointer->FSContext , subPath , Mode ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , Path);
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function closing the previously open file.
 *
 * @warning It not checks parameters
 *
 * @param FilePointer           Reference to the file context
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _CloseFile( oC_VFS_File_t * FilePointer )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->fclose( fileSystem->Context , FilePointer->FSContext ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function reads data from the file.
 *
 * @warning it not checks the parameters.
 *
 * @param FilePointer               Reference to the file context
 * @param BufferDestination         [out]   Reference to the buffer for data
 * @param Size                      [in]    Size of the buffer
 * @param BytesRead                 [out]   Number of bytes read from the file. It can be NULL if not used
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _ReadFile( oC_VFS_File_t * FilePointer , void * BufferDestination , oC_Size_t Size , oC_Size_t * BytesRead )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->fread( fileSystem->Context , FilePointer->FSContext , BufferDestination , Size , BytesRead) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}



//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param Buffer
 * @param Size
 * @param BytesWritten
 *
 * @return
 */
//==========================================================================================================================================
static bool _WriteFile             ( oC_VFS_File_t * FilePointer , const void * Buffer , oC_Size_t Size , oC_Size_t * BytesWritten )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->fwrite( fileSystem->Context , FilePointer->FSContext , Buffer , Size , BytesWritten) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param Offset
 *
 * @return
 */
//==========================================================================================================================================
static bool _ChangeFilePosition    ( oC_VFS_File_t * FilePointer , oC_MemoryInt_t Offset )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->lseek( fileSystem->Context , FilePointer->FSContext , Offset ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 *
 * @return
 */
//==========================================================================================================================================
static bool _Truncate              ( oC_VFS_File_t * FilePointer )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->truncate( fileSystem->Context , FilePointer->FSContext ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 *
 * @return
 */
//==========================================================================================================================================
static bool _FlushCachedData       ( oC_VFS_File_t * FilePointer )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->sync( fileSystem->Context , FilePointer->FSContext ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param StreamContext
 * @param BytesToForward
 * @param BytesForwarded
 *
 * @return
 */
//==========================================================================================================================================
static bool _Forward               ( oC_VFS_File_t * FilePointer , void * StreamContext , int BytesToForward , int * BytesForwarded )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->forward( fileSystem->Context , FilePointer->FSContext , StreamContext , BytesToForward , BytesForwarded ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param BufferDestination
 * @param BufferLength
 *
 * @return
 */
//==========================================================================================================================================
static bool _GetStringFromFile     ( oC_VFS_File_t * FilePointer , oC_STRING_t * BufferDestination , int BufferLength )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->gets( fileSystem->Context , FilePointer->FSContext , BufferDestination , BufferLength ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}


//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param String
 *
 * @return
 */
//==========================================================================================================================================
static bool _PutString             ( oC_VFS_File_t * FilePointer , const oC_STRING_t String )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->puts( fileSystem->Context , FilePointer->FSContext , String ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param PosDestination
 *
 * @return
 */
//==========================================================================================================================================
static bool _GetCurrentFilePosition( oC_VFS_File_t * FilePointer , oC_MemoryInt_t * PosDestination )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->tell( fileSystem->Context , FilePointer->FSContext , PosDestination ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param EndOfFileDestination
 *
 * @return
 */
//==========================================================================================================================================
static bool _CheckIfIsEndOfFile    ( oC_VFS_File_t * FilePointer , bool * EndOfFileDestination )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->eof( fileSystem->Context , FilePointer->FSContext , EndOfFileDestination ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param SizeDestination
 *
 * @return
 */
//==========================================================================================================================================
static bool _GetFileSize( oC_VFS_File_t * FilePointer , oC_Size_t * SizeDestination )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->size( fileSystem->Context , FilePointer->FSContext , SizeDestination ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param FilePointer
 * @param ErrorDestination
 *
 * @return
 */
//==========================================================================================================================================
static bool _CheckIfError( oC_VFS_File_t * FilePointer , bool * ErrorDestination )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( FilePointer->Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->error( fileSystem->Context , FilePointer->FSContext , ErrorDestination ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , FilePointer->Path );
    }

    return result;
}


//==========================================================================================================================================
/**
 *
 * @param Path
 * @param FileInfoDestination
 *
 * @return
 */
//==========================================================================================================================================
static bool _GetFileInfo     ( const oC_STRING_t Path , oC_VFS_FileInfo_t * FileInfoDestination )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->stat( fileSystem->Context , Path , FileInfoDestination ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 *
 * @return
 */
//==========================================================================================================================================
static bool _Remove          ( const oC_STRING_t Path )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->unlink( fileSystem->Context , Path ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 * @param Attributes
 * @param AttributesMask
 *
 * @return
 */
//==========================================================================================================================================
static bool _ChangeAttributes( const oC_STRING_t Path , oC_VFS_Attribute_t Attributes , oC_VFS_Attribute_t AttributesMask )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->chmod( fileSystem->Context , Path , Attributes , AttributesMask ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 * @param TimeStamp
 *
 * @return
 */
//==========================================================================================================================================
static bool _SetTimeStamp    ( const oC_STRING_t Path , oC_MemoryInt_t TimeStamp )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->utime( fileSystem->Context , Path , TimeStamp ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , Path );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param OldPath
 * @param NewPath
 *
 * @return
 */
//==========================================================================================================================================
static bool _Rename          ( const oC_STRING_t OldPath , const oC_STRING_t NewPath )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( OldPath , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->rename( fileSystem->Context , OldPath , NewPath ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , OldPath );
    }

    return result;
}

//==========================================================================================================================================
/**
 *
 * @param Path
 * @param FreeSpace
 *
 * @return
 */
//==========================================================================================================================================
static bool _GetFree         ( const oC_STRING_t Path , oC_Size_t * FreeSpace )
{
    bool result = false;

    _FileSystem_t * fileSystem;
    oC_STRING_t subPath;

    if ( _FindFileSystem( Path , &fileSystem , &subPath) )
    {
        if ( fileSystem->Interface->getfree( fileSystem->Context , FreeSpace ) == 0 )
        {
            result = true;
        }
    }
    else
    {
        oC_Debug("No file system mounted at '%s' path\n" , Path );
    }

    return result;
}
