#ifndef oC_DLIST_H
#define oC_DLIST_H

//--------------------------------------------------------------------------------------------------
//
// INCLUDES
//
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

//--------------------------------------------------------------------------------------------------
//
// USER INCLUDES
//
#include <stdlib.h>
#include <oc_gen_macros.h>
#include <oc_assert.h>

//--------------------------------------------------------------------------------------------------
//
// CONFIG
//

/**
 *  The library checks in the runtime all parameters, and when some of it is not correctly given, then it
 *  will stop in the "while 1" loop.
 *
 *  Values:
 *      true / false
 */
#define oC_DLIST_ASSERTS_ON                        true

/**
 *  The function for the assertions
 *
 * @param expr Expression to check
 */
#define oC_DLIST_ASSERT( expr )                    oC_ASSERT( (expr) )

/**
 *  The function for the dynamic memory allocation. It should return NULL pointer if allocation fails
 *
 *  @param size size of the block to allocate
 *
 *  @return pointer to the allocated block
 */
#define oC_DLIST_MALLOC( size )                    malloc( size )
/**
 *  The function to free memory
 *
 *  @param ptr Pointer to the memory to free
 */
#define oC_DLIST_FREE( ptr )                       free( ptr )

//--------------------------------------------------------------------------------------------------
//
// TYPES
//

/**
 * @brief
 * The type for store number of elements in the list
 */
typedef int_fast64_t oC_DLIST_Counter_t;

/**
 * @brief
 * The type represents one element of the list
 */
typedef struct
{
    void*           This;               /**< The reference to the this structure. */
    void*           Value;              /**< Value of the element */
    size_t          Size;               /**< Size in bytes for the element */
    void*           Previous;           /**< The reference to the previous element (NULL if not exists) */
    void*           Next;               /**< The reference to the next element (NULL if not exists) */
} oC_DLIST_Element_t;

/**
 * @brief
 * The type for manage dynamic list.
 */
typedef struct
{
    void*               This;           /**< The reference to this structure. */
    oC_DLIST_Element_t*    FirstElement;   /**< The reference to first element on the list */
    oC_DLIST_Element_t*    LastElement;    /**< The reference to last element on the list */
    oC_DLIST_Counter_t     Counter;        /**< The counter of the elements on the list */
} oC_DLIST_t;

//--------------------------------------------------------------------------------------------------
//
// MACROS
//
#ifndef foreach
#   define foreach( type , variable , container )    oC_DLIST_foreach( type , variable , container )
#else
#   warning "foreach is already defined. You should use oC_DLINK_foreach macro instead of it"
#endif

#ifndef foreach_backward
#   define foreach_backward( type , variable , container )    oC_DLIST_foreach_backward( type , variable , container )
#else
#   warning "foreach_backward is already defined. You should use oC_DLINK_foreach_backward macro instead of it"
#endif

#define oC_DLIST_foreach( type , variable , container )                                                \
                                            for(bool __execute = true ;__execute && (container->Counter > 0) && (oC_DLIST_IsListCorrect(container));__execute = false) \
                                            for(oC_DLIST_Element_t * _element = container->FirstElement; __execute ; __execute = false)\
                                            for(type variable ; (_element != NULL) && (memcpy(&variable , _element->Value , sizeof(type) ) == &variable) ; _element = _element->Next , __execute = false )

#define oC_DLIST_foreach_backward( type , variable , container )                                       \
                                            for(bool __execute = true ;__execute && (container->Counter > 0) && (oC_DLIST_IsListCorrect(container));__execute = false) \
                                            for(oC_DLIST_Element_t * _element = container->LastElement; __execute ; __execute = false)\
                                            for(type variable ; (_element != NULL) && (memcpy(&variable , _element->Value , sizeof(type) ) == &variable) ; _element = _element->Previous , __execute = false )

#define oC_DLIST_for_range( type , variable , container , from , to )                                  \
                                            for(bool __execute = true ;__execute && (from <= to) && (container->Counter > 0) && (oC_DLIST_IsListCorrect(container));__execute = false) \
                                            for(oC_DLIST_Element_t * _last_element = oC_DLIST_GetElement(container , to + 1) ; __execute ; __execute = false)\
                                            for(oC_DLIST_Element_t * _element = oC_DLIST_GetElement(container , from); __execute ; __execute = false)\
                                            for(type variable ; (_element != NULL) && (_element != _last_element) && (memcpy(&variable , _element->Value , sizeof(type) ) == &variable) ; _element = _element->Next , __execute = false )

#define oC_DLIST_for_element                _element

//--------------------------------------------------------------------------------------------------
//
// QUICK INTERFACES
//

//==========================================================================================================================================
/**
 *  The macro for the quick pushing back element to the end of the list
 *
 * @param list      reference to the list
 * @param type      type of the variable to push
 * @param value     value of the variable to push
 *
 */
//==========================================================================================================================================
#define oC_DLIST_QuickPushBack( list , type , value ) { type _val = value; oC_DLIST_EmplacePushBack(list , &_val , sizeof(type)); }

//==========================================================================================================================================
/**
 *  The macro for the quick pushing front element to the end of the list
 *
 * @param list      reference to the list
 * @param type      type of the variable to push
 * @param value     value of the variable to push
 *
 */
//==========================================================================================================================================
#define oC_DLIST_QuickPushFront( list , type , value ) { type _val = value; oC_DLIST_EmplacePushFront(list , &_val , sizeof(type)); }

//==========================================================================================================================================
/**
 *  The macro for the quick pushing front element to the end of the list
 *
 * @param list      reference to the list
 * @param type      type of the data to push
 * @param data      data to push
 * @param pos       position to put data
 */
//==========================================================================================================================================
#define oC_DLIST_QuickPushAt( list , type , data , pos) { type _val = data; oC_DLIST_EmplacePushAt(list , &_val , sizeof(type) , pos); }

//==========================================================================================================================================
/**
 *  The macro for the quick read value from the selected position on the list
 *
 * @param list              reference to the list
 * @param type              type of the element on the list
 * @param pos               position on the list
 *
 * @return value of the element
 */
//==========================================================================================================================================
#define oC_DLIST_QuickValue( list , pos , type )                       ((type *) oC_DLIST_Value( list , pos ))[0]

//==========================================================================================================================================
/**
 *  The macro for the quick set new value on the selected position on the list
 *
 * @param list              reference to the list
 * @param type              type of the element on the list
 * @param data              new data to save
 * @param pos               position of the element to store data
 */
//==========================================================================================================================================
#define oC_DLIST_QuickSetValue( list , type , data , pos )             { type _var = data; oC_DLIST_EmplaceSetValue(list , pos , &_var , sizeof(type) ); }

//==========================================================================================================================================
/**
 *  The macro for the quick take value from the back of the list.
 *
 * @warning It takes value from the list, but it not deletes it
 *
 * @param list      reference to the list
 * @param type      type of the element on the list
 *
 * @return value of the element
 */
//==========================================================================================================================================
#define oC_DLIST_QuickTakeFromBack( list , type )                     ((type *) oC_DLIST_TakeFromBack( list ))[0]

//==========================================================================================================================================
/**
 *  The macro for the quick take value from the front of the list.
 *
 * @warning It takes value from the list, but it not deletes it
 *
 * @param list      reference to the list
 * @param type      type of the element on the list
 *
 * @return value of the element
 */
//==========================================================================================================================================
#define oC_DLIST_QuickTakeFromFront( list , type )                     ((type *) oC_DLIST_TakeFromFront( list ))[0]

//==========================================================================================================================================
/**
 *  The macro for the quick take value from position.
 *
 * @warning It takes value from the list, but it not deletes it
 *
 * @param list      reference to the list
 * @param pos       position on the list
 * @param type      type of the element on the list
 *
 * @return value of the element
 */
//==========================================================================================================================================
#define oC_DLIST_QuickTakeFromPos( list , pos , type )                 ((type *) oC_DLIST_TakeFromPos( list , pos ))[0]

//--------------------------------------------------------------------------------------------------
//
// INTERFACES
//

extern oC_DLIST_t *             oC_DLIST_New                        ( void );
extern void                     oC_DLIST_Delete                     ( oC_DLIST_t * List );
extern void                     oC_DLIST_DeleteElement              ( oC_DLIST_Element_t * Element , bool WithValue );
extern bool                     oC_DLIST_IsListCorrect              ( oC_DLIST_t * List );
extern bool                     oC_DLIST_IsEmpty                    ( oC_DLIST_t * List );
extern bool                     oC_DLIST_PushBack                   ( oC_DLIST_t * List , const void * Value , size_t Size );
extern bool                     oC_DLIST_PushFront                  ( oC_DLIST_t * List , const void * Value , size_t Size );
extern bool                     oC_DLIST_PushAt                     ( oC_DLIST_t * List , const void * Value , size_t Size , oC_DLIST_Counter_t Pos);
extern bool                     oC_DLIST_EmplacePushBack            ( oC_DLIST_t * List , const void * Value , size_t Size );
extern bool                     oC_DLIST_EmplacePushFront           ( oC_DLIST_t * List , const void * Value , size_t Size );
extern bool                     oC_DLIST_EmplacePushAt              ( oC_DLIST_t * List , const void * Value , size_t Size , oC_DLIST_Counter_t Pos );
extern oC_DLIST_Element_t *     oC_DLIST_TakeElementFromBack        ( oC_DLIST_t * List );
extern oC_DLIST_Element_t *     oC_DLIST_TakeElementFromFront       ( oC_DLIST_t * List );
extern oC_DLIST_Element_t *     oC_DLIST_TakeElementFromPos         ( oC_DLIST_t * List , oC_DLIST_Counter_t Pos );
extern bool                     oC_DLIST_TakeOffElement             ( oC_DLIST_t * List , oC_DLIST_Element_t * Element );
extern void*                    oC_DLIST_TakeFromFront              ( oC_DLIST_t * List );
extern void*                    oC_DLIST_TakeFromBack               ( oC_DLIST_t * List );
extern void*                    oC_DLIST_TakeFromPos                ( oC_DLIST_t * List  , oC_DLIST_Counter_t Pos);
extern bool                     oC_DLIST_RemoveLast                 ( oC_DLIST_t * List );
extern bool                     oC_DLIST_RemoveFirst                ( oC_DLIST_t * List );
extern bool                     oC_DLIST_RemoveAtPos                ( oC_DLIST_t * List , oC_DLIST_Counter_t Pos );
extern bool                     oC_DLIST_RemoveValue                ( oC_DLIST_t * List , const void * Value , size_t Size);
extern bool                     oC_DLIST_Swap                       ( oC_DLIST_t * List , oC_DLIST_Counter_t i , oC_DLIST_Counter_t j );
extern oC_DLIST_Element_t*      oC_DLIST_GetElement                 ( oC_DLIST_t * List , oC_DLIST_Counter_t Pos );
extern oC_DLIST_Element_t*      oC_DLIST_GetElementOfValue          ( oC_DLIST_t * List , const void * Value );
extern oC_DLIST_Counter_t       oC_DLIST_GetPosOfValue              ( oC_DLIST_t * List , const void * Value );
extern void*                    oC_DLIST_Value                      ( oC_DLIST_t * List , oC_DLIST_Counter_t Pos );
extern bool                     oC_DLIST_SetValue                   ( oC_DLIST_t * List , oC_DLIST_Counter_t Pos , const void * Value );
extern bool                     oC_DLIST_EmplaceSetValue            ( oC_DLIST_t * List , oC_DLIST_Counter_t Pos , const void * Value , size_t Size );
extern int                      oC_DLIST_Count                      ( oC_DLIST_t * List );

#endif // oC_DLIST_H
