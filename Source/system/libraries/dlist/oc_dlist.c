//--------------------------------------------------------------------------------------------------
//
// INCLUDES
//
#include <oc_dlist.h>
#include <stdio.h>
//--------------------------------------------------------------------------------------------------
//
// LOCAL MACROS
//

//--------------------------------------------------------------------------------------------------
//
// LOCAL TYPES
//

//--------------------------------------------------------------------------------------------------
//
// LOCAL FUNCTION PROTOTYPES
//
static oC_DLIST_Element_t* _CreateElement( oC_DLIST_Element_t* previous , const void * value , size_t size , bool alloc);
static bool             _PushBack       ( oC_DLIST_t * list , const void * value , size_t size , bool emplace);
static bool             _PushFront      ( oC_DLIST_t * list , const void * value , size_t size , bool emplace);
static bool             _PushAtPos      ( oC_DLIST_t * list , const void * value , size_t size, oC_DLIST_Counter_t pos , bool emplace);

//--------------------------------------------------------------------------------------------------
//
// INTERFACES
//

//==========================================================================================================================================
/**
 * The function creates new dynamic list
 *
 * @return Reference to the created list, or NULL if list was not created.
 */
//==========================================================================================================================================
oC_DLIST_t * oC_DLIST_New( void )
{
    oC_DLIST_t * list = oC_DLIST_MALLOC(sizeof(oC_DLIST_t));

    if ( list != NULL )
    {
        list->This          = list;
        list->Counter       = 0;
        list->FirstElement  = NULL;
        list->LastElement   = NULL;
    }

    return list;
}

//==========================================================================================================================================
/**
 * The function delete dynamic list. If the list was not created, then it do nth.
 *
 * @param list The reference to the list.
 */
//==========================================================================================================================================
void oC_DLIST_Delete( oC_DLIST_t * List )
{
    if ( oC_DLIST_IsListCorrect(List) )
    {
        while( !oC_DLIST_IsEmpty(List) )
        {
            oC_DLIST_RemoveLast(List);
        }

        List->This  = NULL;

        oC_DLIST_FREE( List );
    }
}

//==========================================================================================================================================
/**
 * The function delete element, but not remove it from the list.
 *
 * @param Element       reference to the element
 * @param WithValue     Remove value flag - flag if value of the element should be remove also
 */
//==========================================================================================================================================
void oC_DLIST_DeleteElement(oC_DLIST_Element_t * Element , bool WithValue)
{
    if ( WithValue )
    {
        oC_DLIST_FREE(Element->Value);
    }

    oC_DLIST_FREE( Element );
}

//==========================================================================================================================================
/**
 * The function checks if list reference is correct
 *
 * @param List  reference to the list
 *
 * @return true if correct
 */
//==========================================================================================================================================
bool oC_DLIST_IsListCorrect( oC_DLIST_t * List )
{
    bool result = false;

    if ( List != NULL )
    {
        if ( List->This == List )
        {
            result = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function checks if the list is empty
 *
 * @param List  reference to the list
 *
 * @return true if empty
 */
//==========================================================================================================================================
bool oC_DLIST_IsEmpty( oC_DLIST_t * List )
{
    bool result = true;

    if ( oC_DLIST_IsListCorrect(List) )
    {
        result = (List->FirstElement == NULL) && (List->LastElement == NULL);
    }
    else
    {
        /* given list is not correct */
        oC_DLIST_ASSERT(true);
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function pushes value to back of the list
 *
 * @param List  reference to the list
 * @param Value value to set
 * @param Size  size of the value to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_PushBack( oC_DLIST_t * List , const void * Value , size_t Size )
{
    return _PushBack(List , Value , Size , false);
}

//==========================================================================================================================================
/**
 * The function pushes value to the front of the list
 *
 * @param List  reference to the list
 * @param Value value to set
 * @param Size  size of the value to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_PushFront( oC_DLIST_t * List , const void * Value , size_t Size )
{
    return _PushFront(List , Value , Size , false);
}

//==========================================================================================================================================
/**
 * The function pushes value to the selected position
 *
 * @param List  reference to the list
 * @param Value value to set
 * @param Size  size of the value to set
 * @param Pos position to push data
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_PushAt( oC_DLIST_t * List , const void * Value , size_t Size , oC_DLIST_Counter_t Pos )
{
    return _PushAtPos (List , Value , Size , Pos , false);
}

//==========================================================================================================================================
/**
 * The function pushes copy of the value to back of the list.
 *
 * @param List  reference to the list
 * @param Value value to set
 * @param Size  size of the value to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_EmplacePushBack( oC_DLIST_t * List , const void * Value , size_t Size )
{
    return _PushBack(List , Value , Size , true);
}

//==========================================================================================================================================
/**
 * The function pushes copy of the value to the front of the list
 *
 * @param List  reference to the list
 * @param Value value to set
 * @param Size  size of the value to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_EmplacePushFront( oC_DLIST_t * List , const void * Value , size_t Size )
{
    return _PushFront(List , Value , Size , true);
}

//==========================================================================================================================================
/**
 * The function pushes copy of the value to the selected position
 *
 * @param List  reference to the list
 * @param Value value to set
 * @param Size  size of the value to set
 * @param Pos   position to push data
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_EmplacePushAt( oC_DLIST_t * List , const void * Value , size_t Size , oC_DLIST_Counter_t Pos)
{
    return _PushAtPos (List , Value , Size , Pos , true);
}

//==========================================================================================================================================
/**
 * The function to tak element from the back of the list.
 *
 * @param List      reference to the list
 *
 * @return  reference to the element
 */
//==========================================================================================================================================
oC_DLIST_Element_t * oC_DLIST_TakeElementFromBack( oC_DLIST_t * List )
{
    oC_DLIST_Element_t * element = NULL;

    if ( (List != NULL) && (List->This == List) )
    {
        element = List->LastElement;

        if ( !oC_DLIST_TakeOffElement(List , element) )
        {
            element = NULL;
        }
    }

    return element;
}

//==========================================================================================================================================
/**
 * The function to tak element from the front of the list.
 *
 * @param List      reference to the list
 *
 * @return  reference to the element
 */
//==========================================================================================================================================
oC_DLIST_Element_t * oC_DLIST_TakeElementFromFront( oC_DLIST_t * List )
{
    oC_DLIST_Element_t * element = NULL;

    if ( (List != NULL) && (List->This == List) )
    {
        element = List->FirstElement;

        if ( !oC_DLIST_TakeOffElement(List , element) )
        {
            element = NULL;
        }
    }

    return element;
}

//==========================================================================================================================================
/**
 * The function to take element from the list from selected position.
 *
 * @param List      reference to the list
 * @param Pos       position on the list
 *
 * @return  reference to the element
 */
//==========================================================================================================================================
oC_DLIST_Element_t * oC_DLIST_TakeElementFromPos( oC_DLIST_t * List , oC_DLIST_Counter_t Pos)
{
    oC_DLIST_Element_t * element = NULL;

    if ( (List != NULL) && (List->This == List) )
    {
        element = oC_DLIST_GetElement (List , Pos);

        if ( !oC_DLIST_TakeOffElement(List , element) )
        {
            element = NULL;
        }
    }

    return element;
}

//==========================================================================================================================================
/**
 * Takes off element from the list
 *
 * @param List              Reference to the list
 * @param Element           Element to take off
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_TakeOffElement( oC_DLIST_t * List , oC_DLIST_Element_t * Element )
{
    bool result = false;

    /* check parameters */
    if ( (List != NULL) && (List->This == List) && (Element != NULL) && (Element->This == Element) && (List->FirstElement != NULL) && (List->LastElement != NULL) )
    {
        oC_DLIST_Element_t * next      = (oC_DLIST_Element_t *)Element->Next;
        oC_DLIST_Element_t * previous  = (oC_DLIST_Element_t *)Element->Previous;

        /* unlink element */
        if ( previous != NULL ) previous->Next          = next;
        if ( next != NULL )     next->Previous          = previous;

        if ( List->FirstElement == Element )    List->FirstElement  = next;
        if ( List->LastElement  == Element )    List->LastElement   = previous;

        Element->Next       = NULL;
        Element->Previous   = NULL;

        List->Counter--;

        result = true;

    }

    return result;
}

//==========================================================================================================================================
/**
 * The function take element from the front of the list.
 *
 * @param list  reference to the list
 * @return      reference to the value
 */
//==========================================================================================================================================
void* oC_DLIST_TakeFromFront( oC_DLIST_t * list )
{
    void * value = NULL;

    oC_DLIST_Element_t * element = oC_DLIST_TakeElementFromFront (list);

    if ( element != NULL )
    {
        value = element->Value;
    }

    oC_DLIST_DeleteElement (element , false);

    return value;
}

//==========================================================================================================================================
/**
 * The function take element from the back of the list.
 *
 * @param list  reference to the list
 *
 * @return      reference to the value
 */
//==========================================================================================================================================
void* oC_DLIST_TakeFromBack( oC_DLIST_t * list )
{
    void * value = NULL;

    oC_DLIST_Element_t * element = oC_DLIST_TakeElementFromBack (list);

    if ( element != NULL )
    {
        value = element->Value;
    }

    oC_DLIST_DeleteElement (element , false);

    return value;
}

//==========================================================================================================================================
/**
 * The function take element from the list from selected position.
 *
 * @param list  reference to the list
 * @param pos   position on the list
 *
 * @return      reference to the value
 */
//==========================================================================================================================================
void* oC_DLIST_TakeFromPos( oC_DLIST_t * list  , oC_DLIST_Counter_t pos)
{
    void * value = NULL;

    oC_DLIST_Element_t * element = oC_DLIST_TakeElementFromPos (list , pos);

    if ( element != NULL )
    {
        value = element->Value;
    }

    oC_DLIST_DeleteElement (element , false);

    return value;
}

//==========================================================================================================================================
/**
 * The function removes last element on the list
 *
 * @param list  reference to the list
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_RemoveLast( oC_DLIST_t * list )
{
    bool result = false;

    oC_DLIST_Element_t * element = oC_DLIST_TakeElementFromBack (list);

    if ( element != NULL )
    {
        oC_DLIST_DeleteElement (element , true);
        result   = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function removes first element on the list
 *
 * @param list  reference to the list
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_RemoveFirst( oC_DLIST_t * list )
{
    bool result = false;

    oC_DLIST_Element_t * element = oC_DLIST_TakeElementFromFront (list);

    if ( element != NULL )
    {
        oC_DLIST_DeleteElement (element , true);
        result   = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function removes element on the list with selected pos
 *
 * @param list  reference to the list
 * @param pos   position on the list
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_RemoveAtPos( oC_DLIST_t * list , oC_DLIST_Counter_t pos )
{
    bool result = false;

    oC_DLIST_Element_t * element = oC_DLIST_TakeElementFromPos (list , pos);

    if ( element != NULL )
    {
        oC_DLIST_DeleteElement (element , true);
        result   = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function removes each element that contains the Value
 *
 * @param List      reference to the list
 * @param Value     value to search in list
 * @param Size      Size of the value
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_RemoveValue( oC_DLIST_t * List , const void * Value , size_t Size)
{
    bool result = false;

    for(oC_DLIST_Element_t * element = List->FirstElement ; element != NULL ; element = element->Next)
    {
        if ( 0 == memcmp( element->Value , Value , (Size < element->Size) ? Size : element->Size ) )
        {

        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function swap elements on the list ( element i will be in place of element j and vice versa)
 *
 * @param list  reference to the list
 * @param i     position of the first element
 * @param j     position of the second element
 *
 * @return      true if success
 */
//==========================================================================================================================================
bool oC_DLIST_Swap( oC_DLIST_t * list , oC_DLIST_Counter_t i , oC_DLIST_Counter_t j )
{
    bool result = false;

    if ( oC_DLIST_IsListCorrect(list) )
    {
        if ( (i < list->Counter) && (j < list->Counter) && (i >= 0) && (j >= 0) )
        {
            oC_DLIST_Element_t * i_e = oC_DLIST_GetElement (list , i);
            oC_DLIST_Element_t * j_e = oC_DLIST_GetElement (list , j);
            oC_DLIST_Element_t * i_p_e = i_e->Previous;
            oC_DLIST_Element_t * i_n_e = i_e->Next;
            oC_DLIST_Element_t * j_p_e = j_e->Previous;
            oC_DLIST_Element_t * j_n_e = j_e->Next;

            if ( (i_e != NULL) && (j_e != NULL) )
            {
                i_e->Previous       = j_p_e;
                i_e->Next           = j_n_e;

                j_e->Previous       = i_p_e;
                j_e->Next           = i_n_e;

                if ( i_p_e != NULL )    i_p_e->Next         = j_e;
                if ( i_n_e != NULL )    i_n_e->Previous     = j_e;
                if ( j_p_e != NULL )    j_p_e->Next         = i_e;
                if ( j_n_e != NULL )    j_n_e->Previous     = i_e;

                if ( list->FirstElement == i_e )            list->FirstElement  = j_e;
                else if ( list->FirstElement == j_e )       list->FirstElement  = i_e;

                if ( list->LastElement == i_e )             list->LastElement  = j_e;
                else if ( list->LastElement == j_e )        list->LastElement  = i_e;

                result = true;
            }
            else
            {
                // The elements i or j not exists (counter can be not correct)
                oC_DLIST_ASSERT(true);
            }
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function returns element at selected position on the list
 *
 * @param list  The reference to the list
 * @param pos   The position on the list
 *
 * @return NULL if error, element reference otherwise
 */
//==========================================================================================================================================
oC_DLIST_Element_t* oC_DLIST_GetElement( oC_DLIST_t * list , oC_DLIST_Counter_t pos )
{
    oC_DLIST_Element_t* result = NULL;

    if ( (list != NULL) && (list->This == list) )
    {
        oC_DLIST_Element_t * current_element = list->FirstElement;
        oC_DLIST_Counter_t index;
        for(index = 0 ;  (index < list->Counter) && (index <= pos) && (current_element != NULL); index++)
        {
            if ( index == pos )
            {
                result = current_element;
                break;
            }

            current_element = current_element->Next;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function returns element contains the selected value
 *
 * @param list  The reference to the list
 * @param pos   The position on the list
 *
 * @return NULL if error, element reference otherwise
 */
//==========================================================================================================================================
oC_DLIST_Element_t* oC_DLIST_GetElementOfValue( oC_DLIST_t * list , const void * value )
{
    oC_DLIST_Element_t* result = NULL;

    if ( (list != NULL) && (list->This == list) )
    {
        oC_DLIST_Element_t * current_element = list->FirstElement;

        while ( (current_element != NULL) && (current_element->Value != value) )
        {
            current_element     = (oC_DLIST_Element_t *)current_element->Next;
        }

        result  = current_element;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function returns position of the value
 *
 * @param list  The reference to the list
 * @param value value to search
 *
 * @return -1 if not found, index otherwise
 */
//==========================================================================================================================================
oC_DLIST_Counter_t oC_DLIST_GetPosOfValue( oC_DLIST_t * list , const void * value )
{
    oC_DLIST_Counter_t* result = -1;

    if ( (list != NULL) && (list->This == list) )
    {
        oC_DLIST_Counter_t index = 0;
        oC_DLIST_Element_t * current_element = list->FirstElement;

        while ( (current_element != NULL) && (current_element->Value != value) )
        {
            current_element     = (oC_DLIST_Element_t *)current_element->Next;
            index++;
        }

        result  = index;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function reads value on the selected position
 *
 * @param list      The reference to the list
 * @param pos       The position to read
 *
 * @return value    reference to value or NULL if not found
 */
//==========================================================================================================================================
void* oC_DLIST_Value( oC_DLIST_t * list , oC_DLIST_Counter_t pos )
{
    oC_DLIST_Element_t* element = oC_DLIST_GetElement (list , pos);
    void * value = NULL;

    if ( element != NULL )
    {
        value = element->Value;
    }

    return value;
}

//==========================================================================================================================================
/**
 * The function sets new value for the element on the list. It deletes old value, and save new.
 *
 * @see oC_DLIST_EmplaceSetValue if you want to store only a copy of the value
 *
 * @param list      reference to the list
 * @param pos       position of the element on the list
 * @param value     new data to store
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_DLIST_SetValue( oC_DLIST_t * list , oC_DLIST_Counter_t pos , const void * value )
{
    bool result = false;

    if ( oC_DLIST_IsListCorrect(list) )
    {
        oC_DLIST_Element_t * element = oC_DLIST_GetElement (list , pos);

        if ( element != NULL)
        {
            oC_DLIST_FREE(element->Value);

            element->Value  = value;

            result  = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The funciton sets new value for the element on the list. It deletes old value and save copy of the new data.
 *
 * @param list      reference to the list
 * @param pos       position of the element on the list
 * @param value     new data to store
 * @param size      size of the new data
 *
 * @return  true if success
 */
//==========================================================================================================================================
bool oC_DLIST_EmplaceSetValue( oC_DLIST_t * list , oC_DLIST_Counter_t pos , const void * value , size_t size )
{
    bool result = false;

    if ( oC_DLIST_IsListCorrect(list) )
    {
        oC_DLIST_Element_t * element = oC_DLIST_GetElement (list , pos);

        if ( element != NULL)
        {
            oC_DLIST_FREE(element->Value);

            element->Value  = oC_DLIST_MALLOC(size);

            if ( element->Value != NULL )
            {
                result          = memcpy( element->Value , value , size ) == element->Value;
            }
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Returns number of elements on the list
 *
 * @param List      Reference to the list
 *
 * @return number of elements, or -1 if error
 */
//==========================================================================================================================================
int oC_DLIST_Count( oC_DLIST_t * List )
{
    int noElements = -1;

    if ( List != NULL && List->This == List )
    {
        noElements = List->Counter;
    }

    return noElements;
}

//--------------------------------------------------------------------------------------------------
//
// LOCAL FUNCTIONS
//

//==========================================================================================================================================
/**
 * The function creates element for the element
 *
 * @param previous  reference to a previous element on the list
 * @param value     value of the element on the list
 * @param size      size of the value
 * @param alloc     flag if should allocate memory for this value
 *
 * @return NULL if error, reference to the element otherwise
 */
//==========================================================================================================================================
static oC_DLIST_Element_t* _CreateElement( oC_DLIST_Element_t* previous , const void * value , size_t size , bool alloc)
{
    oC_DLIST_Element_t* element = oC_DLIST_MALLOC(sizeof(oC_DLIST_Element_t));

    if ( element != NULL )
    {
        element->Next           = NULL;
        element->Previous       = previous;
        element->Size           = size;
        element->This           = element;

        if ( alloc )
        {
            element->Value          = oC_DLIST_MALLOC(size);

            if ( element->Value != NULL )
            {
                memcpy( element->Value , value , size );
            }
            else
            {
                oC_DLIST_FREE(element);
                element                 = NULL;
            }
        }
        else
        {
            element->Value      = value;
        }


    }

    return element;
}

//==========================================================================================================================================
/**
 * The function pushes value to back of the list
 *
 * @param list      reference to the list
 * @param value     value to set
 * @param size      size of the value to set
 * @param emplace   flag if function should allocate memory to object and store only copy
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool             _PushBack       ( oC_DLIST_t * list , const void * value , size_t size , bool emplace)
{
    bool result = false;

    if ( (list != NULL) && (list->This == list) && (value != NULL) && (size > 0) )
    {
        oC_DLIST_Element_t * element  = _CreateElement(list->LastElement , value , size , emplace);

        if ( element != NULL )
        {
            if ( list->FirstElement == NULL )
            {
               list->FirstElement   = element;
            }

            if ( list->LastElement != NULL )
            {
                list->LastElement->Next = element;
            }

            list->LastElement       = element;
            list->Counter++;
            result = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function pushes value to front of the list
 *
 * @param list      reference to the list
 * @param value     value to set
 * @param size      size of the value to set
 * @param emplace   flag if function should allocate memory to object and store only copy
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool             _PushFront      ( oC_DLIST_t * list , const void * value , size_t size , bool emplace)
{
    bool result = false;

    if ( (list != NULL) && (list->This == list) )
    {
        oC_DLIST_Element_t * element  = _CreateElement(NULL , value , size, emplace);

        if ( element != NULL )
        {
            if ( list->FirstElement != NULL )
            {
               list->FirstElement->Previous    = element;
            }

            element->Next           = list->FirstElement;
            list->FirstElement      = element;

            list->Counter++;
            result = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function pushes value to selected position on the list
 *
 * @param list      reference to the list
 * @param value     value to set
 * @param size      size of the value to set
 * @param emplace   flag if function should allocate memory to object and store only copy
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool             _PushAtPos      ( oC_DLIST_t * list , const void * value , size_t size, oC_DLIST_Counter_t pos , bool emplace)
{
    bool result = false;

    if ( (list != NULL) && (list->This == list) )
    {
        if ( pos < list->Counter )
        {
            oC_DLIST_Element_t * old_element   = oC_DLIST_GetElement (list , pos);
            oC_DLIST_Element_t * previous      = NULL;
            oC_DLIST_Element_t * element       = _CreateElement(NULL , value , size, emplace);

            /* Element successfully created */
            if ( element != NULL )
            {
                /* element on this list exists at position */
                if ( old_element != NULL )
                {
                    previous                = old_element->Previous;

                    /* previous element exists */
                    if ( previous != NULL )
                    {
                        previous->Next          = element;
                    }
                    else if ( old_element == list->FirstElement )
                    {
                        list->FirstElement  = element;
                    }
                    else
                    {
                        /* previous element not exists, but old_element is not first element */
                        oC_DLIST_ASSERT(true);
                    }

                    old_element->Previous   = element;
                    element->Next           = old_element;

                    result = true;
                }
                else    /* element not exists at the list */
                {
                    /* position is given correct, but old_element was not correctly readed */
                    oC_DLIST_ASSERT(true);
                }
            }
        }
        else if ( pos == list->Counter ) /* position is at the end of the list */
        {
            result = oC_DLIST_PushBack (list , value , size);
        }
        else /* position is not correct (it is too big) */
        {
            result = false;
        }

    }

    /* increment counter if success */
    if ( result )
    {
        list->Counter++;
    }

    return result;
}
