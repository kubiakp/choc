/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_string.c
 *
 *    @brief      The file sources of the string library
 *
 *    @author     Patryk Kubiak - (Created on: 22 paź 2014 16:35:43)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INCLUDES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Core
#include <oc_string.h>
#include <oc_os.h>
#include <limits.h>
#include <math.h>

// Drivers

// Libraries

// Others
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <oc_gen_macros.h>

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL CONSTS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL TYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MACROS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef oC_IMPLEMENT_FAILURE
#include <stdio.h>
#define oC_IMPLEMENT_FAILURE()  printf("%d: IMPLEMENT FAILURE" , __LINE__)
#endif

#ifndef oC_MIN
#define oC_MIN( A , B )     ( ( (A) > (B) ) ? (B) : (A) )
#endif

#ifndef oC_MAX
#define oC_MAX( A , B )     ( ( (A) > (B) ) ? (A) : (B) )
#endif

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL VARIABLES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL FUNCTION PROTOTYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static inline oC_STRING_t       _New                        ( const oC_STRING_t Str );
static inline oC_STRING_t       _NewWithSize                ( int Size );
static inline void              _Delete                     ( oC_STRING_t * String );
static inline int               _Length                     ( const oC_STRING_t String );
static inline int               _MemorySize                 ( const oC_STRING_t String );
static inline bool              _IsNull                     ( const oC_STRING_t String );
static inline bool              _IsEmpty                    ( const oC_STRING_t String );
static inline bool              _IsIndexCorrect             ( const oC_STRING_t String , int Index );
static inline bool              _IsCaseSensitivityCorrect   ( const oC_STRING_CaseSensitivity_t CaseSensitivity );
static inline bool              _IsWhitespace               ( char c );
static        bool              _Append                     ( oC_STRING_t * String , const oC_STRING_t Str );
static        bool              _Chop                       ( oC_STRING_t * String , const int N );
static        bool              _Clear                      ( oC_STRING_t * String );
static        int               _CountWhitespaces           ( const oC_STRING_t String );
static inline char              _ToUpper                    ( char c );
static inline char              _ToLower                    ( char c );
static inline bool              _CompareCharacters          ( char C1 , char C2 , const oC_STRING_CaseSensitivity_t CaseSensitivity );
static        bool              _Compare                    ( const oC_STRING_t Str1 , const oC_STRING_t Str2 , const oC_STRING_CaseSensitivity_t CaseSensitivity );
static        bool              _CompareNCharacters         ( const oC_STRING_t Str1 , const oC_STRING_t Str2 , int N , const oC_STRING_CaseSensitivity_t CaseSensitivity );
static inline bool              _Contains                   ( const oC_STRING_t String , const oC_STRING_t SubString , const oC_STRING_CaseSensitivity_t CaseSensitivity );
static inline bool              _Fill                       ( oC_STRING_t * String , const char C , int Size );
static        int               _IndexOf                    ( const oC_STRING_t String , const oC_STRING_t SubString , const oC_STRING_CaseSensitivity_t CaseSensitivity );
static        int               _IndexOfWhitespace          ( const oC_STRING_t String , oC_STRING_Dir_t Dir );
static        int               _IndexOfNonWhitespace       ( const oC_STRING_t String , oC_STRING_Dir_t Dir );
static        bool              _Insert                     ( oC_STRING_t * String , int Position , const oC_STRING_t Str );
static        oC_STRING_t       _Left                       ( const oC_STRING_t String , const int N );
static        oC_STRING_t       _LeftJustified              ( const oC_STRING_t String , const int Width , const char C );
static        oC_STRING_t       _Mid                        ( const oC_STRING_t String , int Position , int N );
static        bool              _Prepend                    ( oC_STRING_t * String , const oC_STRING_t Str );
static        bool              _Remove                     ( oC_STRING_t * String , int Position , int N );
static        int               _Replace                    ( oC_STRING_t * String , const oC_STRING_t Before , const oC_STRING_t After , const oC_STRING_CaseSensitivity_t CaseSensitivity);
static        int               _ReplaceChar                ( oC_STRING_t * String , char Before , char After , const oC_STRING_CaseSensitivity_t CaseSensitivity);
static        bool              _Reserve                    ( oC_STRING_t * String , int Size );
static        bool              _Resize                     ( oC_STRING_t * String , int Size , char C );
static        oC_STRING_t       _Right                      ( const oC_STRING_t String , int N );
static        oC_STRING_t       _RightJustified             ( const oC_STRING_t String , int Width , const char C );
static        bool              _Squeeze                    ( oC_STRING_t * String );
static        void              _Swap                       ( oC_STRING_t * String1 , oC_STRING_t * String2 );
static inline void              _StringToUpper              ( oC_STRING_t * String );
static inline void              _StringToLower              ( oC_STRING_t * String );
static        bool              _Trim                       ( oC_STRING_t * String );
static        oC_STRING_t       _Trimmed                    ( const oC_STRING_t String );
static inline bool              _Truncate                   ( oC_STRING_t * String , int Position );
static inline bool              _Set                        ( oC_STRING_t * String , const oC_STRING_t Str );

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INTERFACE FUNCTIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//==========================================================================================================================================
/**
 * The function creates new string, allocate a memory for it and return reference to it
 *
 * @param Str           Default value of the string
 *
 * @return created string or NULL if error
 */
//==========================================================================================================================================
oC_STRING_t         oC_STRING_New           ( const oC_STRING_t Str )
{
    oC_STRING_t string = NULL;

    if ( ! oC_STRING_IsEmpty ( Str ) )
    {
        string = _New( Str );
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function creates a new string with the size of characters
 *
 * @param Size          The count of the characters in a string
 *
 * @return created string or NULL if error
 */
//==========================================================================================================================================
oC_STRING_t         oC_STRING_NewWithSize   ( const int Size )
{
    oC_STRING_t string = NULL;

    if ( Size >= 0 )
    {
        string = _NewWithSize( Size );
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function delete a string by releasing the memory
 *
 * @param String        String to delete (can be empty, or null also)
 *
 * @return true if success, false if String was null
 */
//==========================================================================================================================================
bool                oC_STRING_Delete        ( oC_STRING_t * String )
{
    bool result = false;

    if ( (String != NULL) && !oC_STRING_IsNull( *String ) )
    {
        _Delete( String );
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function appends the string Str onto the end of the String.
 *
 * @param String        A reference to the string
 * @param Str           string to append
 *
 * @return true if success, false if error
 *
 * @example
 *
 * oC_STRING_t x = "free";
 * oC_STRING_t y = "dom";
 *
 * oC_STRING_Append( &x , y );
 * // x = "freedom"
 */
//==========================================================================================================================================
bool                oC_STRING_Append        ( oC_STRING_t * String , const oC_STRING_t Str )
{
    bool result     = false;

    if(
            ( String != NULL     )
         && (!_IsNull( Str )     )
        )
    {
        result = _Append(String , Str);
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function checks and return a char from a string.
 *
 * @param String    String to get a char from
 * @param Index     Index to read a char
 *
 * @return char of the string, or 0 if index is not correct
 */
//==========================================================================================================================================
char                oC_STRING_At            ( const oC_STRING_t String , int Index )
{
    char c = 0;

    if ( !_IsNull( String ) && _IsIndexCorrect(String , Index ) )
    {
        c = String[Index];
    }

    return c;
}

//==========================================================================================================================================
/**
 * The function removes N characters from the end of the string. If N is greater than size, then it removes all string
 *
 * @param String    String to chop
 * @param N         Number of characters to remove
 *
 * @return true if success
 *
 * @example
 *
 *  oC_STRING_t x = "some string";
 *  oC_STRING_Chop( &x , 7 );
 *  // x = "some"
 */
//==========================================================================================================================================
bool                oC_STRING_Chop          ( oC_STRING_t * String , const int N )
{
    bool result = false;

    if (
            (String != NULL)
         && (!_IsNull( * String))
        )
    {
        result = _Chop( String , N );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function clears the string.
 *
 * @param String        The reference to the string to clear
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_Clear         ( oC_STRING_t * String )
{
    bool result = false;

    if ( (String != NULL) && (!_IsNull( * String )) )
    {
        result = _Clear( String );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function compares 2 strings.
 *
 * @param String1           The first string to compare
 * @param String2           The other string to compare
 * @param CaseSensitivity   If it is a difference between lower and upper case letters
 *
 * @return true if the same
 */
//==========================================================================================================================================
bool oC_STRING_Compare( const oC_STRING_t String1 , const oC_STRING_t String2     , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    bool result = false;

    if (
            (!_IsNull( String1 ))
         && (!_IsNull( String2 ))
         && (_IsCaseSensitivityCorrect(CaseSensitivity))
         )
    {
        result = _Compare(String1 , String2 , CaseSensitivity);
    }
    else
    {
        // The given strings are not correct!
        oC_IMPLEMENT_FAILURE();
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function compares first N characters from the strings.
 *
 * @param String1           The first string to compare
 * @param String2           The other string to compare
 * @param N                 Number of characters to compare
 * @param CaseSensitivity   If it is a difference between lower and upper case letters
 *
 * @return true if the same
 */
//==========================================================================================================================================
bool oC_STRING_CompareN( const oC_STRING_t String1 , const oC_STRING_t String2     , int N , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    bool result = false;

    if (
                (!_IsNull( String1 ))
             && (!_IsNull( String2 ))
             && (_IsCaseSensitivityCorrect(CaseSensitivity))
             )
    {
        if ( ( N <= _Length( String1 ) ) && ( N <= _Length( String2 ) ) )
        {
            result = _CompareNCharacters(String1 , String2 , N , CaseSensitivity);
        }
    }
    else
    {
        // The given strings are not correct!
        oC_IMPLEMENT_FAILURE();
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function checks if the String contains the SubString.
 *
 * @param String            The string to check into
 * @param SubString         The sub string to search
 * @param CaseSensitivity   If it is a difference between lower and upper case letters
 *
 * @return true if contains
 */
//==========================================================================================================================================
bool oC_STRING_Contains( const oC_STRING_t String  , const oC_STRING_t SubString   , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    bool result = false;

    if (
            ( !_IsNull( String ) )
         && ( !_IsNull( SubString ) )
         && (_IsCaseSensitivityCorrect(CaseSensitivity))
            )
    {
        result  = _Contains(String , SubString , CaseSensitivity);
    }
    else
    {
        // The given strings are not correct
        oC_IMPLEMENT_FAILURE();
    }

    return result;
}

//==========================================================================================================================================
/**
 * Length of the string
 *
 * @param String           The string to check the length
 *
 * @return Length of the string, -1 if error
 */
//==========================================================================================================================================
int                 oC_STRING_Count         ( const oC_STRING_t String )
{
    int length = -1;

    if ( !oC_STRING_IsNull(String) )
    {
        length  = strlen( String );
    }

    return length;
}

//==========================================================================================================================================
/**
 * The function fill sets every character in a string to C character. If the Size is greater than -1, then it resize a string
 *
 * @param String            The string to fill
 * @param C                 The character to set in a string
 * @param Size              The size of the new string. If -1, then it will be old size.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_Fill          ( oC_STRING_t * String , const char C , const int Size )
{
    bool result = false;

    if (
            ( String != NULL      )
         && ( !_IsNull( *String ) )
         && ( Size >= -1 )
            )
    {
        result = _Fill( String , C , Size );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function creates string from integer
 *
 * @param Size              Minimum size of the string (including '-'/'+' sign, and it will be padding to right) or 0 if auto size
 * @param Value             Value to fill the string
 * @param PlusSign          Flag if plus sign should be added too
 * @param LeftZeroPads      Left-pads the number with zeroes (0) instead of spaces when padding is specified
 *
 * @return string with value
 */
//==========================================================================================================================================
oC_STRING_t oC_STRING_FromInt( int Size , int Value , bool PlusSign , bool LeftZeroPads )
{
    oC_STRING_t string;
    bool        negative    = Value < 0;
    int         realLength  = 1;
    bool        writeZero   = true;
    Value                   = abs( Value );

    if ( PlusSign || negative )   realLength++;
    for(int divider = 10; divider <= Value ;divider *= 10)  realLength++;

    Size            = oC_MAX( Size , realLength );
    string          = oC_STRING_NewWithSize( Size );

    if ( negative )         string[0]   = '-';
    else if ( PlusSign )    string[0]   = '+';

    for(int signIdx = (Size - 1) ; (signIdx >= ( (negative || PlusSign) ? 1 : 0 )) ; signIdx--)
    {
        if ( Value != 0 || writeZero )
            string[signIdx] = '0' + (Value % 10);
        else
            string[signIdx] = ' ';

        Value /= 10;
        writeZero       = LeftZeroPads;
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function creates string from unsigned integer
 *
 * @param Size              Minimum size of the string (including '-'/'+' sign, and it will be padding to right) or 0 if auto size
 * @param Value             Value to fill the string
 * @param PlusSign          Flag if plus sign should be added too
 * @param LeftZeroPads      Left-pads the number with zeroes (0) instead of spaces when padding is specified
 *
 * @return string with value
 */
//==========================================================================================================================================
oC_STRING_t oC_STRING_FromUInt( int Size , unsigned int Value , bool PlusSign , bool LeftZeroPads )
{
    oC_STRING_t string;
    int         realLength  = 1;
    bool        writeZero   = true;

    if ( PlusSign )   realLength++;
    for(unsigned int divider = 10; divider <= Value ;divider *= 10)  realLength++;

    Size            = oC_MAX( Size , realLength );
    string          = oC_STRING_NewWithSize( Size );

    if ( PlusSign )    string[0]   = '+';

    for(int signIdx = (Size - 1) ; (signIdx >= ( (PlusSign) ? 1 : 0 )) ; signIdx--)
    {
        if ( Value != 0 || writeZero )
            string[signIdx] = '0' + (Value % 10);
        else
            string[signIdx] = ' ';

        Value /= 10;
        writeZero       = LeftZeroPads;
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function creates string from unsigned integer in octal format
 *
 * @param Size              Minimum size of the string (including '-'/'+' sign, and it will be padding to right) or 0 if auto size
 * @param Value             Value to fill the string
 * @param OPrefix           If value should be preceded by 0
 * @param LeftZeroPads      Left-pads the number with zeroes (0) instead of spaces when padding is specified
 *
 * @return string with value
 */
//==========================================================================================================================================
oC_STRING_t oC_STRING_FromUIntToOctal(int Size , unsigned int Value , bool OPrefix , bool LeftZeroPads )
{
    oC_STRING_t string;
    int         realLength  = 1;
    bool        writeZero   = true;

    for(unsigned int divider = 8; divider <= Value ;divider *= 8)  realLength++;

    Size            = oC_MAX( Size , realLength );
    string          = oC_STRING_NewWithSize( Size );

    if ( OPrefix )
    {
        string[0]   = '0';
        realLength +=  1;
    }

    for(int signIdx = (Size - 1) ; (signIdx >= ( (OPrefix) ? 1 : 0 )) ; signIdx--)
    {
        if ( Value != 0 || writeZero )
            string[signIdx] = '0' + (Value % 8);
        else
            string[signIdx] = ' ';

        Value /= 8;
        writeZero       = LeftZeroPads;
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function creates string from unsigned integer in hex format
 *
 * @param Size              Minimum size of the string (including '-'/'+' sign, and it will be padding to right) or 0 if auto size
 * @param Value             Value to fill the string
 * @param OxPrefix          If value should be preceded by 0
 * @param LeftZeroPads      Left-pads the number with zeroes (0) instead of spaces when padding is specified
 *
 * @return string with value
 */
//==========================================================================================================================================
oC_STRING_t oC_STRING_FromUIntToHex(int Size , unsigned int Value , bool OxPrefix , bool LeftZeroPads )
{
    oC_STRING_t string;
    int         realLength  = 1;
    bool        writeZero   = true;

    for(unsigned int divider = 16; divider <= Value ;divider *= 16)  realLength++;

    Size            = oC_MAX( Size , realLength );
    string          = oC_STRING_NewWithSize( Size );

    if ( OxPrefix )
    {
        string[0]   = '0';
        string[1]   = 'x';
        realLength +=  2;
    }

    for(int signIdx = (Size - 1) ; (signIdx >= ( (OxPrefix) ? 2 : 0 )) ; signIdx--)
    {
        const char hex[16] = { '0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9' , 'A' , 'B' , 'C' , 'D' , 'E' , 'F' };
        if ( Value != 0 || writeZero )
            string[signIdx] = hex[(Value % 16)];
        else
            string[signIdx] = ' ';

        Value /= 16;
        writeZero       = LeftZeroPads;
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function creates string from pointer in hex format
 *
 * @param Size              Minimum size of the string (including '-'/'+' sign, and it will be padding to right) or 0 if auto size
 * @param Value             Value to fill the string
 * @param OxPrefix          If value should be preceded by 0
 * @param LeftZeroPads      Left-pads the number with zeroes (0) instead of spaces when padding is specified
 *
 * @return string with value
 */
//==========================================================================================================================================
oC_STRING_t oC_STRING_FromPointer( int Size , void * Value , bool OxPrefix , bool LeftZeroPads )
{
    oC_STRING_t string;
    int         realLength  = 1;
    size_t value            = (size_t) Value;
    bool writeZero          = true;

    for(size_t divider = 16; divider <= value ;divider *= 16)  realLength++;

    Size            = oC_MAX( Size , realLength );
    string          = oC_STRING_NewWithSize( Size );

    if ( OxPrefix )
    {
        string[0]   = '0';
        string[1]   = 'x';
        realLength +=  2;
    }

    for(int signIdx = (Size - 1) ; (signIdx >= ( (OxPrefix) ? 2 : 0 )) ; signIdx--)
    {
        if ( value != 0 || writeZero )
            string[signIdx] = '0' + (value % 16);
        else
            string[signIdx] = ' ';

        value       /= 16;
        writeZero    = LeftZeroPads;
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function creates string from unsigned integer in hex format
 *
 * @param Size              Minimum size of the string (including '-'/'+' sign, and it will be padding to right) or 0 if auto size
 * @param Value             Value to fill the string
 * @param Precision         Number of digits after point (xx.dd)
 * @param LeftZeroPads      Left-pads the number with zeroes (0) instead of spaces when padding is specified
 *
 * @return string with value
 */
//==========================================================================================================================================
oC_STRING_t oC_STRING_FromFloat( int Size , double Value , unsigned int Precision , bool LeftZeroPads )
{
    oC_STRING_t string;
    int         realLength  = 1;
    double integral_f;
    int integral;
    bool writeZero           = true;
    double fractionalPart    = modf(Value , &integral_f);

    integral = (int)integral_f;

    realLength      += Precision;

    if ( Precision > 0 ) realLength++; /* for point */

    for(float divider = 10; divider <= Value ;divider *= 10)  realLength++;

    Size            = oC_MAX( Size , realLength );
    string          = oC_STRING_NewWithSize( Size );
    int pointIdx    = Size - 1 - Precision;

    if ( Precision > 0 )
    {
        string[pointIdx]    = '.';

        for(int idx = pointIdx + 1 ; idx < Size ; idx++ )
        {
            fractionalPart *= 10;
            string[idx]     = '0' + ((int)(fractionalPart) % 10);
        }

    }

    for(int idx = pointIdx; idx >= 0 ; idx--)
    {
        if ( integral != 0 || writeZero )
            string[idx] = '0' + ((int)integral % 10);
        else
            string[idx] = ' ';
        integral       /= 10;
        writeZero       = LeftZeroPads;
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function creates signed integer from string
 *
 * @param Str   The stream to conversion
 *
 * @return      signed integer
 */
//==========================================================================================================================================
int oC_STRING_ToInt( const oC_STRING_t Str )
{
    signed int result = 0;
    int sign = 1;
    int EndOfIdx = oC_STRING_Length(Str);
    int Idx=0;

    if( Str[0] == '-' )
    {
        sign = -1;
        Idx = 1;
    }

    while( Idx < EndOfIdx )
    {
        result = result*10 + (int)( Str[Idx]-'0' );

        if( Str[Idx] < '0' || Str[Idx] > '9' )
        {
            Idx = EndOfIdx;
            result = 0;
        }
        Idx++;
    }
    result = result * sign;

    return result;
}
//==========================================================================================================================================
/**
 * The function creates unsigned integer from string
 *
 * @param Str   The stream to conversion
 *
 * @return      unsigned integer
 */
//==========================================================================================================================================
int oC_STRING_ToUInt( const oC_STRING_t Str )
{
    unsigned int result = 0;
    int EndOfIdx = oC_STRING_Length(Str);
    int Idx=0;

    while( Idx < EndOfIdx )
    {
        result = result*10 + (int)( Str[Idx]-'0' );

        if( Str[Idx] < '0' || Str[Idx] > '9' )
        {
            Idx = EndOfIdx;
            result = 0;
        }
        Idx++;
    }

    return result;
}
//==========================================================================================================================================
/**
 * The function creates unsigned integer from octal string
 *
 * @param Str   The stream to conversion
 *
 * @return      unsigned integer
 */
//==========================================================================================================================================
int oC_STRING_ToUOct( const oC_STRING_t Str )
{
    unsigned int result = 0;
    unsigned int pow = 1;
    int EndOfIdx = oC_STRING_Length(Str);
    signed int Idx = ( EndOfIdx - 1 );

    while(Idx >= 0)
    {
        result = result + (int)( (Str[Idx]-'0')*pow );
        pow = pow*8;

        if( Str[Idx] < '0' || Str[Idx] > '7' )
        {
            Idx = -1;
            result = 0;
        }

        Idx--;
    }

    return result;
}
//==========================================================================================================================================
/**
 * The function creates character from string
 *
 * @param Str   The stream to conversion
 *
 * @return      character
 */
//==========================================================================================================================================
char oC_STRING_ToChar( const oC_STRING_t Str )
{
    char result = 0;

    if(oC_STRING_Length(Str) < 2 )
    {
        result = Str[0];
    }
    else
    {
        result = 0;
    }

    return result;
}
//==========================================================================================================================================
/**
 * The function locates substring in a string and return index of the first occur
 *
 * @param String            The string to search into
 * @param SubString         The string to find
 * @param CaseSensitivity   The sensitivity of the compare string operation.
 *
 * @return index if substring found, -1 otherwise
 */
//==========================================================================================================================================
int                 oC_STRING_IndexOf       ( const oC_STRING_t String , const oC_STRING_t SubString , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    int result = -1;

    if (
            ( !_IsNull( String ) )
         && ( !_IsNull( SubString ) )
         && ( _IsCaseSensitivityCorrect( CaseSensitivity ) )
            )
    {
        result = _IndexOf( String , SubString , CaseSensitivity );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function for inserting the Str string to the position in the String. If position is greater than size, then it will put it at the end of
 * the string.
 *
 * @param String            a string for insert into
 * @param Position          a position to put the Str
 * @param Str               a string for insert
 *
 * @return true if success
 *
 * @example
 *
 *     oC_STRING_t x = "The ChOC is operating system";
 *     oC_STRING_t y = "the best ";
 *
 *     oC_STRING_Insert( &x , 12 , y );
 *     // x = "The ChOC is the best operating system"
 */
//==========================================================================================================================================
bool                oC_STRING_Insert        ( oC_STRING_t * String , int Position , const oC_STRING_t Str )
{
    bool result = false;

    if (
            (String != NULL)
         && (!_IsNull( Str ) )
            )
    {
        result  = _Insert( String , Position , Str );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function checks if the given string is empty
 *
 * @param String        Reference to the string
 *
 * @return true if empty
 */
//==========================================================================================================================================
bool                oC_STRING_IsEmpty       ( const oC_STRING_t String )
{
    return _IsEmpty( String );
}

//==========================================================================================================================================
/**
 * The function checks if the given string is null
 *
 * @param String        a string to check
 *
 * @return true if string is NULL
 */
//==========================================================================================================================================
bool                oC_STRING_IsNull        ( const oC_STRING_t String )
{
    return _IsNull( String );
}

//==========================================================================================================================================
/**
 * Checks if the string is correct (if address is correct and there is a '\0' sign at the end of the string
 *
 * @param String
 *
 * @return
 */
//==========================================================================================================================================
bool oC_STRING_IsCorrect( const oC_STRING_t String )
{
    return oC_OS_IsAddressCorrect( String );
}

//==========================================================================================================================================
/**
 * The function returns a substring that contains the n leftmost characters of the string.
 *
 * @param String        String to copy
 * @param N             Count of the characters to copy from the String
 *
 * @return copy of the string, or NULL if error
 */
//==========================================================================================================================================
oC_STRING_t         oC_STRING_Left          ( const oC_STRING_t String , const int N )
{
    oC_STRING_t leftString = NULL;

    if (
            ( !_IsNull( String ) )
         && ( _IsIndexCorrect(String , N) )
            )
    {
        leftString  = _Left( String , N );
    }

    return leftString;
}

//==========================================================================================================================================
/**
 * The function returns a substring that contains the n leftmost characters of the string, and fill others characters as C sign.
 *
 * @param String        String to copy
 * @param Width         a width of the returned string
 * @param C             Character to fill the others
 *
 * @return copy of the string, or NULL if error
 *
 * @example
 *
 * @code
       oC_STRING_t x = "The ChOC OS";
       oC_STRING_t y = oC_STRING_LeftJustified( x , 8 , '.');
       // y = "The ChOC"

       oC_STRING_t a = "The ChOC";
       oC_STRING_t b = oC_STRING_LeftJustified( a , 12 , '.');
       // b = "The ChOC..."
 * @endcode
 */
//==========================================================================================================================================
oC_STRING_t         oC_STRING_LeftJustified ( const oC_STRING_t String , const int Width , const char C )
{
    oC_STRING_t leftString = NULL;

    if (
            ( !_IsNull(String) )
        &&  ( Width > 0 )
            )
    {
        leftString  = _LeftJustified(String , Width , C );
    }

    return leftString;
}

//==========================================================================================================================================
/**
 * The function counts the length of the string
 *
 * @param String        a string to count the length
 *
 * @return length of the string, or -1 if string is not correct
 */
//==========================================================================================================================================
int                 oC_STRING_Length        ( const oC_STRING_t const String )
{
    int length = -1;

    if ( !oC_STRING_IsNull(String) )
    {
        length  = strlen( String );
    }

    return length;
}

//==========================================================================================================================================
/**
 * Returns a string that contains n characters of the String, starting at the specified position index.
 * Returns a null string if the position index exceeds the length of the string.
 * If there are less than n characters available in the string starting at the given position, or if n is -1 (default),
 * the function returns all characters that are available from the specified position.
 *
 * @param String     The reference to the string to cut mid
 * @param Position   Position of the mid to copy.
 * @param N          Size of the returned string. If -1, then it will copy to the end of the string
 *
 * @return reference to the mid string, NULL if error
 *
 * @example
 *
   @code
           oC_STRING_t x = "Nune pineapples";
           oC_STRING_t y = oC_STRING_Mid( x , 5 , 4 ); // y = "pine"
           oC_STRING_t y = oC_STRING_Mid( x , 5 , -1 ); // y = "pineapples"
   @endcode
 */
//==========================================================================================================================================
oC_STRING_t         oC_STRING_Mid           ( const oC_STRING_t String , int Position , const int N )
{
    oC_STRING_t mid = NULL;

    if (
            (! _IsNull( String ))
         && (_IsIndexCorrect( String , Position ))
         && ( N <= _Length( String ) )
            )
    {
        mid = _Mid( String , Position , N );
    }

    return mid;
}

//==========================================================================================================================================
/**
 * The function prepends the string Str onto the front of the String.
 *
 * @param String        A reference to the string
 * @param Str           string to prepend
 *
 * @return true if success, false if error
 *
 * @example
 *
 * oC_STRING_t x = "free";
 * oC_STRING_t y = "dom";
 *
 * oC_STRING_Prepend( &x , y );
 * // x = "domfree"
 */
//==========================================================================================================================================
bool                oC_STRING_Prepend       ( oC_STRING_t * String , const oC_STRING_t Str )
{
    bool result     = false;

    if(
            ( String != NULL     )
         && (!_IsNull( Str )     )
        )
    {
        result = _Prepend(String , Str);
    }

    return result;
}

//==========================================================================================================================================
/**
 *  The function pushes the Str to the end of the String.
 *
 * @param String    The string to push the other into
 * @param Str       The string to push
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_PushBack      ( oC_STRING_t * String , const oC_STRING_t Str )
{
    bool result     = false;

    if(
            ( String != NULL     )
         && (!_IsNull( Str )     )
        )
    {
        result = _Append(String , Str);
    }

    return result;
}

//==========================================================================================================================================
/**
 *  The function pushes the Str to the front of the String.
 *
 * @param String    The string to push the other into
 * @param Str       The string to push
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_PushFront     ( oC_STRING_t * String , const oC_STRING_t Str )
{
    bool result     = false;

    if(
            ( String != NULL     )
         && (!_IsNull( Str )     )
        )
    {
        result = _Prepend(String , Str);
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function removes N characters from the Position.
 *
 * Example:
 * @code
        oC_STRING_t x = "The ChOC system is not the best";

        oC_STRING_Remove( &x , 12 , 4 );
        // x = "The ChOC system is the best";
   @endcode
 *
 * @param String    The reference to a string to remove characters from
 * @param Position  Position of start to remove a characters
 * @param N         Number of characters to remove. -1 to remove to the end of the string
 *
 * @return true if success
 *
 */
//==========================================================================================================================================
bool                oC_STRING_Remove        ( oC_STRING_t * String , int Position , const int N )
{
    bool result = false;

    if (
            ( String != NULL )
         && ( !_IsNull( *String) )
         && ( _IsIndexCorrect( *String , Position) )
            )
    {
        result  = _Remove( String , Position , N );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function replaces all occurrence of the Before string in the String to string After
 *
 * @code
      oC_STRING_t x = "The ChOC is not the best";
      oC_STRING_Replace( &x , "not " , "" , oC_STRING_CaseSensitivity_Insensitive );
   @endcode
 *
 * @warning
 *      This function can be slow in view of many reallocation operations needed. For this reason it is recommended to use #oC_STRING_ReplaceChar
 *      instead of it everywhere, where it is possible.
 *
 * @param String            The string to search into
 * @param Before            The string to find
 * @param After             The string to put instead
 * @param CaseSensitivity   The sensitivity of the compare operation
 *
 * @return Number of replaced occurs of the Before string or -1 if error.
 */
//==========================================================================================================================================
int                oC_STRING_Replace       ( oC_STRING_t * String , const oC_STRING_t Before , const oC_STRING_t After , const oC_STRING_CaseSensitivity_t CaseSensitivity)
{
    int result = -1;

    if (
            ( String != NULL )
         && ( !_IsNull( *String ) )
         && ( !_IsNull( Before ) )
         && ( !_IsNull( After ) )
            )
    {
        result  = _Replace( String , Before , After , CaseSensitivity);
    }

    return result;
}

//==========================================================================================================================================
/**
 * Replaces all occurrence of the Before character to the After. It is much faster than #oC_STRING_Replace
 *
 * @param String            The string to replace a char into
 * @param Before            a character to replace
 * @param After             a character to put instead
 * @param CaseSensitivity   sensitivity for compare
 *
 * @return number of replaced occurrence, or -1 if error
 */
//==========================================================================================================================================
int                 oC_STRING_ReplaceChar   ( oC_STRING_t * String , char Before , char After , const oC_STRING_CaseSensitivity_t CaseSensitivity)
{
    int result = -1;

    if (
            ( String != NULL )
        &&  ( !_IsNull( *String ) )
            )
    {
        result = _ReplaceChar( String , Before , After , CaseSensitivity );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function reserves a memory for the selected Size for the String. If the current string is lower then new size, then
 * characters in the new section will be random.
 *
 * @param String        The reference to the string.
 * @param Size          Size to allocate
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_Reserve       ( oC_STRING_t * String , int Size )
{
    bool result = false;

    if (
            ( String != NULL )
         && ( Size >= 0 )
            )
    {
        result = _Reserve( String , Size );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function resizes a string to a new size
 *
 * @param String        The reference to a string to resize
 * @param Size          New size of the string
 * @param C             The char to fill if the new size is greater than current
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_Resize        ( oC_STRING_t * String , int Size , char C )
{
    bool result = false;

    if (
            ( String != NULL )
         && ( Size >= 0 )
            )
    {
        result = _Resize( String , Size , C );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function returns a substring that contains the n rightmost characters of the string. It not checks if the parameters are correct
 *
 * @param String        String to copy
 * @param N             Count of the characters to copy from the String
 *
 * @return copy of the string, or NULL if error
 */
//==========================================================================================================================================
oC_STRING_t         oC_STRING_Right         ( const oC_STRING_t String , int N )
{
    oC_STRING_t right = NULL;

    if (
            (!_IsEmpty(String))
         && (N > 0)
            )
    {
        right   = _Right( String , N );
    }

    return right;
}

//==========================================================================================================================================
/**
 * The function returns a substring that contains the n rightmost characters of the string, and fill others characters as C sign.
 *
 * @param String        String to copy
 * @param Width         a width of the returned string
 * @param C             Character to fill the others
 *
 * @return copy of the string, or NULL if error
 *
 * @code
       oC_STRING_t x = "The ChOC OS";   // stringLen = 11 , sizeToCopy = 8 , sizeToFill = 8 - 8
       oC_STRING_t y = oC_STRING_RightJustified( x , 8 , '.');
       // y = " ChOC OS"

       oC_STRING_t a = "The ChOC";    // stringLen = 8 , sizeToCopy = 8 , sizeToFill = 12 - 8 = 4
       oC_STRING_t b = oC_STRING_RightJustified( a , 12 , '.');
       // b = "....The ChOC"
 * @endcode
 */
//==========================================================================================================================================
oC_STRING_t         oC_STRING_RightJustified( const oC_STRING_t String , int Width , const char C )
{
    oC_STRING_t right = NULL;

    if (
            ( !_IsNull( String ) )
         && ( Width > 0 )
            )
    {
        right   = _RightJustified( String , Width , C );
    }

    return right;
}

//==========================================================================================================================================
/**
 * The function counts the length of the string.
 *
 * @see #oC_STRING_Count
 * @see #oC_STRING_Length
 *
 * @param String        a string to count the length
 *
 * @return length of the string, or -1 if string is not correct
 */
//==========================================================================================================================================
int                 oC_STRING_Size          ( const oC_STRING_t String )
{
    int length = -1;

    if ( !oC_STRING_IsNull(String) )
    {
        length  = strlen( String );
    }

    return length;
}

//==========================================================================================================================================
/**
 * Releases any memory not required to store the character data.
 *
 * @param String            The reference to a string to squeeze
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_Squeeze       ( oC_STRING_t * String )
{
    bool result = false;

    if (
            ( String != NULL )
         && ( !_IsNull( *String ) )
            )
    {
        result = _Squeeze( String );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Swaps string other with this string. This operation is very fast and never fails (if the references are correct).
 *
 * @param String1           The reference to a string 1 to swap
 * @param String2           The reference to a string 2 to swap
 *
 * @return true if success, false if one of the reference was NULL
 */
//==========================================================================================================================================
bool                oC_STRING_Swap          ( oC_STRING_t * String1 , oC_STRING_t * String2 )
{
    bool result = false;

    if (
            ( String1 != NULL )
         && ( String2 != NULL )
            )
    {
        _Swap( String1 , String2 );
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function converts each character to lower version
 *
 * @param String            The reference to the string to convert
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_ToLower       ( oC_STRING_t * String )
{
    bool result = false;

    if (
            ( String != NULL )
         && ( !_IsEmpty( *String ) )
            )
    {
        _StringToLower( String );
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function converts each character to upper version
 *
 * @param String            The reference to the string to convert
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_ToUpper       ( oC_STRING_t * String )
{
    bool result = false;

    if (
            ( String != NULL )
         && ( !_IsEmpty( *String ) )
            )
    {
        _StringToUpper( String );
        result = true;
    }

    return result;
}


//==========================================================================================================================================
/**
 * The function remove whitespace from the start and the end of the String.
 *
 * @see #oC_STRING_Trimmed to remove it in the copy of the string
 *
 * @param String            The reference to the string to remove whitespace
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_Trim          ( oC_STRING_t * String )
{
    bool result = false;

    if (
            ( String != NULL )
         && ( !_IsNull( *String ) )
            )
    {
        result = _Trim ( String );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function remove whitespace from the start and the end of the String. It operates on a copy of the string
 *
 * @see #oC_STRING_Trimmed to remove it in the copy of the string
 *
 * @param String            The reference to the string to remove whitespace
 *
 * @return Copy of the string with removed whitespace, or NULL if error
 */
//==========================================================================================================================================
oC_STRING_t         oC_STRING_Trimmed       ( const oC_STRING_t String )
{
    oC_STRING_t trimmed = NULL;

    if ( !_IsNull( String ) )
    {
        trimmed     = _Trimmed( String );
    }

    return trimmed;
}

//==========================================================================================================================================
/**
 * Truncates the string at the given Position index. If the specified position index is beyond the end of the string, nothing happens.
 *
 * @param String
 * @param Position
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_Truncate      ( oC_STRING_t * String , int Position )
{
    bool result = false;

    if (
            ( NULL != String )
         && ( !_IsNull( *String ) )
            )
    {
        result = _Truncate( String , Position );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function release old memory, and set new value for the Str string.
 *
 * @param String            The reference to the destination string
 * @param Str               The string to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool                oC_STRING_Set           ( oC_STRING_t * String , const oC_STRING_t Str )
{
    bool result = false;

    if (
            ( String != NULL)
            )
    {

        result = _Set( String , Str );
    }
    return result;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL FUNCTION
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//==========================================================================================================================================
/**
 * The function create a new string without checking a parameters
 *
 * @param Str       Default value for a string
 *
 * @return created string or NULL if error
 */
//==========================================================================================================================================
static inline oC_STRING_t       _New            ( const oC_STRING_t Str )
{
    oC_STRING_t string = _NewWithSize( _MemorySize( Str ) );

    if ( string != NULL )
    {
        strcpy( string , Str );
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function create a new string without checking a parameters
 *
 * @param Size      Size of a new string
 *
 * @return NULL if cannot allocate a memory
 */
//==========================================================================================================================================
static inline oC_STRING_t       _NewWithSize    ( int Size )
{
    oC_STRING_t string = malloc( (Size + 1 ) * sizeof(char) );

    if ( !_IsNull(string) )
    {
        string[Size] = '\0';
    }

    return string;
}

//==========================================================================================================================================
/**
 * The function release the memory needed by a string and set it to NULL
 *
 * @param String    The reference to a string to delete
 *
 */
//==========================================================================================================================================
static inline void              _Delete         ( oC_STRING_t * String )
{
    free( *String );
    *String     = NULL;
}

//==========================================================================================================================================
/**
 * The function count len of the string without checking parameters
 *
 * @param Str       String to count the lenght
 *
 * @return length of the Str
 */
//==========================================================================================================================================
static inline int               _Length         ( const oC_STRING_t String )
{
    return strlen( String );
}

//==========================================================================================================================================
/**
 * The function count memory size needed for the string
 *
 * @param Str       String to count memory size
 *
 * @return size in bytes, needed to store the Str string
 */
//==========================================================================================================================================
static inline int               _MemorySize     ( const oC_STRING_t String )
{
    return _Length( String ) + 1;
}

//==========================================================================================================================================
/**
 * The function checks if the string is null
 *
 * @param String    String to check if null
 *
 * @return true if null
 */
//==========================================================================================================================================
static inline bool              _IsNull         ( const oC_STRING_t String )
{
    return String == NULL;
}

//==========================================================================================================================================
/**
 * The function checks if the string is empty without checking a parameters
 *
 * @param String    String to check if empty
 *
 * @return true if empty
 */
//==========================================================================================================================================
static inline bool              _IsEmpty        ( const oC_STRING_t String )
{
    return _IsNull( String ) || (_Length( String ) == 0 );
}

//==========================================================================================================================================
/**
 * The function checks if the index is correct without checking a parameters
 *
 * @param String    String to check into
 * @param Index     Index to check
 *
 * @return true if index is correct
 */
//==========================================================================================================================================
static inline bool              _IsIndexCorrect ( const oC_STRING_t String , int Index )
{
    return (Index >= 0) && ( Index < _Length(String));
}

//==========================================================================================================================================
/**
 * The function checks if the case sensitivity has correct value
 *
 * @param CaseSensitivity   The value to check
 *
 * @return true if correct
 */
//==========================================================================================================================================
static inline bool              _IsCaseSensitivityCorrect   ( const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    return (CaseSensitivity == oC_STRING_CaseSensitivity_Insensitive) || (CaseSensitivity == oC_STRING_CaseSensitivity_Sensitive);
}

//==========================================================================================================================================
/**
 * The function checks if the char C is whitespace char
 *
 * @param c                 The char to check
 *
 * @return true if the character is whitespace
 */
//==========================================================================================================================================
static inline bool              _IsWhitespace               ( char c )
{
    return isspace((int)c);
}

//==========================================================================================================================================
/**
 * The function append Str string to the end of the String without check the parameters
 *
 * @param String    The reference to the destination string
 * @param Str       The string to append
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool              _Append         ( oC_STRING_t * String , const oC_STRING_t Str )
{
    int stringLen   = (_IsNull(*String)) ? 0 : _Length(* String);

    return _Insert( String , stringLen , Str );
}

//==========================================================================================================================================
/**
 * The function chop the string without checking the String parameter is correct
 *
 * @param String    The string to chop
 * @param N         Count of the elements to chop. If greater than size, then it remove all
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool              _Chop           ( oC_STRING_t * String , const int N )
{
    bool result     = false;
    int stringLen   = _Length( * String );
    int newSize     = oC_MAX( stringLen - N , 0 );

    oC_STRING_t newString = _NewWithSize( newSize );

    if ( !_IsNull( newString ) )
    {
        strncpy( newString , *String , newSize );

        _Delete( String );
        *String = newString;
        result  = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function clears the string without checking the parameters
 *
 * @param String            The reference to a string to clear
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool              _Clear          ( oC_STRING_t * String )
{
    bool result = false;

    _Delete( String );

    oC_STRING_t newString = _NewWithSize( 0 );

    if ( !_IsNull( newString ) )
    {
        *String= newString;
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function count whitespace characters in the string
 *
 * @warning it not checks parameters
 *
 * @param String    String to count
 *
 * @return number of whitespace characters in the string
 */
//==========================================================================================================================================
static        int               _CountWhitespaces           ( const oC_STRING_t String )
{
    int stringLen = _Length( String );
    int whitespaces = 0;

    for( int i = 0 ; i <stringLen ; i++)
    {
        if ( _IsWhitespace( String[i] ) )
        {
            whitespaces++;
        }
    }

    return whitespaces;
}

//==========================================================================================================================================
/**
 * The function converts a character to upper if it is a letter
 *
 * @param c     The character to convert
 *
 * @return uppercase character
 */
//==========================================================================================================================================
static inline char              _ToUpper        ( char c )
{
    return (char)toupper((int)c);
}

//==========================================================================================================================================
/**
 * The function converts a character to lower if it is a letter
 *
 * @param c     The character to convert
 *
 * @return lowercase character
 */
//==========================================================================================================================================
static inline char              _ToLower        ( char c )
{
    return (char)tolower((int)c);
}

//==========================================================================================================================================
/**
 * The function compares characters
 *
 * @param C1                first character to compare
 * @param C2                other character to compare
 * @param CaseSensitivity   if should check sensitivity
 *
 * @return true if the same, false if different
 */
//==========================================================================================================================================
static inline bool              _CompareCharacters      ( char C1 , char C2 , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    if ( CaseSensitivity == oC_STRING_CaseSensitivity_Insensitive )
    {
        return (_ToLower(C1) == _ToLower(C2));
    }
    else
    {
        return C1 == C2;
    }
}

//==========================================================================================================================================
/**
 * The function compares to strings without checking parameters
 *
 * @param Str1              First string to compare
 * @param Str2              Other string to compare
 * @param CaseSensitivity   If should compare lower-case versions
 *
 * @return true if the same
 */
//==========================================================================================================================================
static bool _Compare( const oC_STRING_t Str1 , const oC_STRING_t Str2 , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    bool result = false;

    int str1Len = _Length( Str1 );
    int str2Len = _Length( Str2 );

    if ( str1Len == str2Len )
    {
        result = _CompareNCharacters( Str1 , Str2 , str1Len , CaseSensitivity );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function compares first N signs of 2 strings without checking parameters
 *
 * @param Str1              First string to compare
 * @param Str2              Other string to compare
 * @param N                 Count of characters to compare
 * @param CaseSensitivity   If should compare lower-case versions
 *
 * @return true if the same
 */
//==========================================================================================================================================
static bool _CompareNCharacters( const oC_STRING_t Str1 , const oC_STRING_t Str2 , int N , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    bool result = true;

    if ( (_Length( Str1 ) < N) || (_Length(Str2) < N ) )
    {
        result = false;
    }

    while( result && N-- > 0 )
    {
        result = _CompareCharacters( Str1[N] , Str2[N] , CaseSensitivity );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function checks if the String contains the SubString without checking a parameters. If the SubString is longer than a string, then it return false.
 *
 * @param String            String to search into
 * @param SubString         a string to find
 * @param CaseSensitivity   If should compare lower-case versions
 *
 * @return true if the same
 */
//==========================================================================================================================================
static inline bool _Contains( const oC_STRING_t String , const oC_STRING_t SubString , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    return _IndexOf( String , SubString , CaseSensitivity ) >= 0;
}

//==========================================================================================================================================
/**
 * The function sets every character in the string to C. If the Size is different than -1, then the String will be resized. The function
 * not checks if parameters are correct.
 *
 * @param String            String to fill
 * @param C                 Character to set
 * @param Size              Size of new string (if -1, then it will be not resized)
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool              _Fill                       ( oC_STRING_t * String , const char C , int Size )
{
    bool result = false;

    Size    = (Size == -1) ? _Length( *String ) : Size;

    if ( !_IsNull( *String ) )
    {
        _Delete(String);
    }

    *String     = _NewWithSize( Size );

    if ( !_IsNull( *String ) )
    {
        memset( *String , C , Size );
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function returns index of the substring with selected sensitivity. It not checks the parameters
 *
 * @param String            The string to search into
 * @param SubString         The substring to find
 * @param CaseSensitivity   Sensitivity of the compare operations
 *
 * @return index if found, -1 if the SubString not occurs in the String
 */
//==========================================================================================================================================
static int _IndexOf ( const oC_STRING_t String , const oC_STRING_t SubString , const oC_STRING_CaseSensitivity_t CaseSensitivity )
{
    int result = -1;

    int stringLen   = _Length(String);
    int subStringLen= _Length(SubString);

    if ( stringLen >= subStringLen )
    {
        int iStringMax = stringLen - subStringLen + 1;
        for( int iString = 0; (iString < iStringMax) && (result == -1) ; iString++ )
        {
            bool similar = true;
            for( int iSubString = 0 ; (iSubString < subStringLen) && similar ; iSubString++)
            {
                similar     = _CompareCharacters( String[iString+iSubString] , SubString[iSubString] , CaseSensitivity );
            }

            if ( similar )
            {
                result  = iString;
            }
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function search first occur of whitespace from begin or end of the String
 *
 * @param String            The string to search into
 * @param Dir               Directory of searching whitespace (from begin or from end)
 *
 * @return index of whitespace, or -1 if not found
 */
//==========================================================================================================================================
static        int               _IndexOfWhitespace          ( const oC_STRING_t String , oC_STRING_Dir_t Dir )
{
    int index = -1;
    int stringLen = _Length( String );

    if ( Dir == oC_STRING_Dir_Forward )
    {
        for(int i = 0; i < stringLen ; i++)
        {
            if ( _IsWhitespace( String[i] ) )
            {
                index = i;
                break;
            }
        }
    }
    else
    {
        for(int i = (stringLen - 1); i >= 0 ; i--)
        {
            if ( _IsWhitespace( String[i] ) )
            {
                index = i;
                break;
            }
        }
    }

    return index;
}

//==========================================================================================================================================
/**
 * The function search first occur of first non whitespace from begin or end of the String
 *
 * @param String            The string to search into
 * @param Dir               Directory of searching non whitespace (from begin or from end)
 *
 * @return index of first non whitespace character, or -1 if not found
 */
//==========================================================================================================================================
static        int               _IndexOfNonWhitespace       ( const oC_STRING_t String , oC_STRING_Dir_t Dir )
{
    int index = -1;
    int stringLen = _Length( String );

    if ( Dir == oC_STRING_Dir_Forward )
    {
        for(int i = 0; i < stringLen ; i++)
        {
            if ( !_IsWhitespace( String[i] ) )
            {
                index = i;
                break;
            }
        }
    }
    else
    {
        for(int i = (stringLen - 1); i >= 0 ; i--)
        {
            if ( !_IsWhitespace( String[i] ) )
            {
                index = i;
                break;
            }
        }
    }

    return index;
}

//==========================================================================================================================================
/**
 * The function for inserting the Str string to the position in the String. If position is greater than size, then it will put it at the end of
 * the string. If the String is null, then it will create a new string. The function not checks if parameters are correct
 *
 * @param String            a string for insert into
 * @param Position          a position to put the Str
 * @param Str               a string for insert
 *
 * @return true if success
 *
 * @example
 *
 *     oC_STRING_t x = "The ChOC is operating system";  // stringLen = 28
 *     oC_STRING_t y = "the best "; // strLen = 9
 *
 *     oC_STRING_Insert( &x , 12 , y );
 *     // x = "The ChOC is the best operating system"
 */
//==========================================================================================================================================
static bool _Insert( oC_STRING_t * String , int Position , const oC_STRING_t Str )
{
    bool result = false;

    int stringLen   = ( _IsNull( * String ) ) ? 0 :_Length( * String);
    int strLen      = _Length(Str);
    Position        = oC_MIN( stringLen , Position );

    oC_STRING_t newString = _NewWithSize( stringLen + strLen );

    if ( !_IsNull( newString ) )
    {
        if ( !_IsNull( *String ) )
        {
            strncpy( newString , * String , Position );
        }
        strncpy( &newString[Position] , Str  , strLen);

        if ( !_IsNull( *String ) )
        {
            strncpy( &newString[Position+strLen] , &(*String)[Position] , stringLen-Position );
            _Delete( String );
        }

        *String = newString;

        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function returns a substring that contains the n leftmost characters of the string. It not checks if the parameters are correct
 *
 * @param String        String to copy
 * @param N             Count of the characters to copy from the String
 *
 * @return copy of the string, or NULL if error
 */
//==========================================================================================================================================
static oC_STRING_t _Left( const oC_STRING_t String , const int N )
{
    oC_STRING_t leftString = _NewWithSize(N);

    if ( !_IsNull( leftString ) )
    {
        strncpy( leftString , String , N);
    }

    return leftString;
}

//==========================================================================================================================================
/**
 * The function returns a substring that contains the n leftmost characters of the string, and fill others characters as C sign.
 * It not checks if the parameters are correct
 *
 * @param String        String to copy
 * @param Width         a width of the returned string
 * @param C             Character to fill the others
 *
 * @return copy of the string, or NULL if error
 *
 * @example
 *
 * @code
       oC_STRING_t x = "The ChOC OS";   // stringLen = 11 , sizeToCopy = 8 , sizeToFill = 8 - 8
       oC_STRING_t y = oC_STRING_LeftJustified( x , 8 , '.');
       // y = "The ChOC"

       oC_STRING_t a = "The ChOC";
       oC_STRING_t b = oC_STRING_LeftJustified( a , 12 , '.');
       // b = "The ChOC..."
 * @endcode
 */
//==========================================================================================================================================
static oC_STRING_t _LeftJustified( const oC_STRING_t String , const int Width , const char C )
{
    oC_STRING_t leftString = _NewWithSize(Width);

    if ( !_IsNull( leftString ) )
    {
        int stringLen   = _Length( String );
        int sizeToCopy  = oC_MIN( stringLen , Width );
        strncpy( leftString , String , sizeToCopy);

        int sizeToFill  = Width - sizeToCopy;

        if ( sizeToFill > 0 )
        {
            memset( &leftString[sizeToCopy] , C , sizeToFill );
        }
    }

    return leftString;
}

//==========================================================================================================================================
/**
 * Returns a string that contains n characters of this string, starting at the specified position index.
 *
 * @param String     The reference to the string to cut mid
 * @param Position   Position of the mid to copy
 * @param N          Size of the returned string
 *
 * @return reference to the mid string, NULL if error
 *
 * @warning
 *          The function not checks the parameters.
 *
 * @example
 *
   @code
           oC_STRING_t x = "Nune pineapples";
           oC_STRING_t y = oC_STRING_Mid( x , 5 , 4 ); // y = "pine"
           oC_STRING_t y = oC_STRING_Mid( x , 5 , -1 ); // y = "pineapples"
   @endcode
 */
//==========================================================================================================================================
static oC_STRING_t _Mid( const oC_STRING_t String , int Position , int N )
{
    if ( N == -1 )
    {
        N   = _Length(String) - Position;
    }
    oC_STRING_t mid = _NewWithSize( N );

    if ( !_IsNull( mid ) )
    {
        strncpy( mid , &String[Position] , N );
    }

    return mid;
}

//==========================================================================================================================================
/**
 * The function append Str string to the begin of the String without check the parameters
 *
 * @param String    The reference to the destination string
 * @param Str       The string to prepend
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _Prepend( oC_STRING_t * String , const oC_STRING_t Str )
{
    return _Insert( String , 0 , Str );
}

//==========================================================================================================================================
/**
 * The function removes N characters from the String starts at the Position.
 *
   @code
       oC_STRING_t x = "The ChOC is not the best";
       _Remove( &x , 12 , 4 ); // x = "The ChOC is the best"
   @endcode
 *
 * @warning It not checks the parameters!
 *
 * @param String        The reference to a string to remove characters
 * @param Position      The position to remove a characters
 * @param N             Number of characters to remove. If -1, then it will remove all to the end of the string
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _Remove( oC_STRING_t * String , int Position , int N )
{
    bool result = false;

    int stringLen   = _Length(*String);

    N       = ( N <= -1 || N > stringLen ) ? (stringLen - Position) : N ;

    int newStringLen= stringLen - N;

    oC_STRING_t newString = oC_STRING_NewWithSize( newStringLen );

    if ( !_IsNull( newString ) )
    {
        strncpy( &newString[0]        , &(*String)[0] , Position);
        strncpy( &newString[Position] , &(*String)[Position + N] , newStringLen - Position );

        _Delete( String );
        *String     = newString;

        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function replaces all occurrence of the Before string in the String to string After
 *
 * @warning
 *          Function not checks a parameters
 *
 * @param String            The string to search into
 * @param Before            The string to find
 * @param After             The string to put instead
 * @param CaseSensitivity   The sensitivity of the compare operation
 *
 * @return number of replaced occurs of the Before, -1 if error
 */
//==========================================================================================================================================
static int _Replace( oC_STRING_t * String , const oC_STRING_t Before , const oC_STRING_t After , const oC_STRING_CaseSensitivity_t CaseSensitivity)
{
    int occurs = 0;

    int stringLen   = _Length( *String );
    int beforeLen   = _Length( Before );

    for( int iString = 0 ; iString < stringLen && occurs >= 0 ; iString++ )
    {
        if ( _CompareNCharacters( &(*String)[iString] , Before , beforeLen , CaseSensitivity ) )
        {
            if ( !_Remove ( String , iString , beforeLen ) || !_Insert( String , iString , After ) )
            {
                occurs = -1;
            }
            occurs++;
        }
    }

    return occurs;
}

//==========================================================================================================================================
/**
 *  The function replaces every occurrence of the Before character to the After character.
 *
 * @warning
 *      The function not checks the parameters!
 *
 * @param String            The reference to a string that search into
 * @param Before            The character to find
 * @param After             The character to set instead of Before
 * @param CaseSensitivity   The sensitivity of the compare operations
 *
 * @return number of occurs of the Before character
 */
//==========================================================================================================================================
static int _ReplaceChar( oC_STRING_t * String , char Before , char After , const oC_STRING_CaseSensitivity_t CaseSensitivity)
{
    int occurs = 0;

    int iString = _Length( *String );

    while( --iString >= 0 )
    {
        if ( _CompareCharacters( (*String)[iString] , Before , CaseSensitivity) )
        {
            (*String)[iString]  = After;
            occurs++;
        }
    }

    return occurs;
}

//==========================================================================================================================================
/**
 * The function reserves a memory for the string with Size
 *
 * @warning
 *      It not checks the parameters
 *
 * @param String        The reference to a string
 * @param Size          Size to reserve
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _Reserve( oC_STRING_t * String , int Size )
{
    bool result = false;

    oC_STRING_t newString = _NewWithSize( Size );

    if ( !_IsNull( newString ) )
    {
        if ( !_IsEmpty( *String ) )
        {
            int stringLen = _Length( * String );
            strncpy( newString , *String , oC_MIN( stringLen , Size ));
            _Delete( String );
        }

        *String = newString;
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function resizes a string to a new size, and fill the rest of the memory by a character C
 *
 * @warning The function not checks the parameters!
 *
 * @param String        The reference to a string to resize
 * @param Size          New size of the string
 * @param C             The character to set to new memory
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _Resize( oC_STRING_t * String , int Size , char C )
{
    bool result = false;

    int stringLen = _Length( * String );

    if ( _Reserve( String , Size ) )
    {
        if ( stringLen < Size )
        {
            memset( &(*String)[stringLen] , C , (Size - stringLen) );
        }
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function returns a substring that contains the n rightmost characters of the string. It not checks if the parameters are correct
 *
 * @param String        String to copy
 * @param N             Count of the characters to copy from the String
 *
 * @return copy of the string, or NULL if error
 */
//==========================================================================================================================================
static        oC_STRING_t       _Right                      ( const oC_STRING_t String , int N )
{
    int stringLen = _Length( String );
    N = oC_MIN(N , stringLen);

    oC_STRING_t rightString = _NewWithSize(N);

    if ( !_IsNull( rightString ) )
    {
        strncpy( rightString , &String[ stringLen - N ] , N);
    }

    return rightString;
}

//==========================================================================================================================================
/**
 * The function returns a substring that contains the n rightmost characters of the string, and fill others characters as C sign.
 * It not checks if the parameters are correct
 *
 * @param String        String to copy
 * @param Width         a width of the returned string
 * @param C             Character to fill the others
 *
 * @return copy of the string, or NULL if error
 *
 * @warning
 *      The function not checks the parameters.
 *
 * @example
 *
 * @code
       oC_STRING_t x = "The ChOC OS";   // stringLen = 11 , sizeToCopy = 8 , sizeToFill = 8 - 8
       oC_STRING_t y = oC_STRING_RightJustified( x , 8 , '.');
       // y = " ChOC OS"

       oC_STRING_t a = "The ChOC";    // stringLen = 8 , sizeToCopy = 8 , sizeToFill = 12 - 8 = 4
       oC_STRING_t b = oC_STRING_RightJustified( a , 12 , '.');
       // b = "....The ChOC"
 * @endcode
 */
//==========================================================================================================================================
static        oC_STRING_t       _RightJustified             ( const oC_STRING_t String , int Width , const char C )
{
    oC_STRING_t rightString = _NewWithSize(Width);

    if ( !_IsNull( rightString ) )
    {
        int stringLen   = _Length( String );
        int sizeToCopy  = oC_MIN( stringLen , Width );
        int sizeToFill  = Width - sizeToCopy;

        strncpy( &rightString[sizeToFill] , &String[ stringLen - sizeToCopy ] , sizeToCopy);

        if ( sizeToFill > 0 )
        {
            memset( &rightString[0] , C , sizeToFill );
        }
    }

    return rightString;
}

//==========================================================================================================================================
/**
 * Releases any memory not required to store the character data.
 *
 * @warning
 *      The function not checks if the parameters are correct
 *
 * @param String            The reference to a string to squeeze
 *
 * @return true if success
 */
//==========================================================================================================================================
static        bool              _Squeeze                    ( oC_STRING_t * String )
{
    bool result = false;

    oC_STRING_t  newString = _New( *String );

    if ( !_IsNull( newString ) )
    {
        _Delete( String );
        *String = newString;
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Swaps string other with this string. This operation is very fast and never fails.
 *
 * @param String1           The reference to a string 1 to swap
 * @param String2           The reference to a string 2 to swap
 *
 */
//==========================================================================================================================================
static void _Swap( oC_STRING_t * String1 , oC_STRING_t * String2 )
{
    oC_STRING_t s1 = *String1;
    *String1       = *String2;
    *String2       = s1;
}

//==========================================================================================================================================
/**
 * Converts string to its upper version
 *
 * @warning it not checks parameters
 *
 * @param String string to convert
 */
//==========================================================================================================================================
static inline void              _StringToUpper                    ( oC_STRING_t * String )
{
    int stringLen = _Length( *String );

    for(int i = 0 ; i < stringLen ; i++ )
    {
        (*String)[i] = _ToUpper((*String)[i]);
    }
}

//==========================================================================================================================================
/**
 * Converts string to its lower version
 *
 * @warning it not checks parameters
 *
 * @param String string to convert
 */
//==========================================================================================================================================
static inline void              _StringToLower                    ( oC_STRING_t * String )
{
    int stringLen = _Length( *String );

    for(int i = 0 ; i < stringLen ; i++ )
    {
        (*String)[i] = _ToLower((*String)[i]);
    }
}

//==========================================================================================================================================
/**
 * The function remove whitespace from the start and the end of the String.
 *
 * @warning It not checks parameters
 *
 * @param String            The reference to the string to remove whitespace
 *
 * @return true if success
 */
//==========================================================================================================================================
static        bool              _Trim                       ( oC_STRING_t * String )
{
    bool result = false;

    oC_STRING_t trimmed = _Trimmed( *String );

    if ( !_IsNull( trimmed ) )
    {
        _Delete( String );
        *String  = trimmed;
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function remove whitespace from the start and the end of the String. It operates on a copy of the string
 *
 * @warning It not checks parameters
 *
 * @param String            The reference to the string to remove whitespace
 *
 * @return Copy of the string with removed whitespace
 */
//==========================================================================================================================================
static        oC_STRING_t       _Trimmed                    ( const oC_STRING_t String )
{
    oC_STRING_t trimmed = NULL;

    int firstNonWhitespaceIndex = _IndexOfNonWhitespace( String , oC_STRING_Dir_Forward );
    int lastNonWhitespaceIndex  = _IndexOfNonWhitespace( String , oC_STRING_Dir_Backward );

    if (   (firstNonWhitespaceIndex != -1 )
        && (lastNonWhitespaceIndex  != -1)
        )
    {
        int stringLen           = _Length( String );
        int nStringLen          = stringLen - firstNonWhitespaceIndex - ( stringLen - lastNonWhitespaceIndex - 1);
        trimmed                 = _NewWithSize( nStringLen );

        if ( !_IsNull( trimmed ) )
        {
            strncpy( trimmed , &(String)[firstNonWhitespaceIndex] , nStringLen );
        }
    }

    return trimmed;
}

//==========================================================================================================================================
/**
 * Truncates the string at the given Position index. If the specified position index is beyond the end of the string, nothing happens.
 *
 * @warning It not checks parameters
 *
 * @param String
 * @param Position
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool              _Truncate                   ( oC_STRING_t * String , int Position )
{
    bool result = false;

    int stringLen   = _Length( *String );
    Position = (Position > stringLen ) ? stringLen : Position;

    oC_STRING_t newString = _NewWithSize( Position );

    if ( !_IsNull( newString ) )
    {
        strncpy( newString , &(*String)[0] , Position );
        _Delete(String);
        *String = newString;

        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 *  The function set new value for the string. (And release old allocated memory)
 *
 * @param String            The reference to the string to set
 * @param Str               New value for the string
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool              _Set                        ( oC_STRING_t * String , const oC_STRING_t Str )
{
    bool result = false;

    _Delete( String );
    *String     = _New( Str );

    if ( !_IsNull( *String) )
    {
        result = true;
    }

    return result;
}
