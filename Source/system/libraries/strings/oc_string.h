/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_strings.h
 *
 *    @brief      The header file with interface of the string library.
 *
 *    @author     Patryk Kubiak - (Created on: 13 pa� 2014 15:56:14)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_STRINGS_OC_STRING_H_
#define SYSTEM_LIBRARIES_STRINGS_OC_STRING_H_

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INCLUDES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Core

// Drivers

// Libraries

// Others
#include <stdbool.h>

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    CONSTS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    TYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * The type represents the case sensitive for the string comparing etc
 */
typedef enum
{
    oC_STRING_CaseSensitivity_Insensitive ,     //!< lower-case and upper-case letters are not distinguished
    oC_STRING_CaseSensitivity_Sensitive         //!< lower-case and upper-case letters are distinguished
} oC_STRING_CaseSensitivity_t;

/**
 * The type represents the directory of a string parsing
 */
typedef enum
{
    oC_STRING_Dir_Forward ,                     //!< from begin of a string
    oC_STRING_Dir_Backward                      //!< from end of a string
} oC_STRING_Dir_t;

typedef char * oC_STRING_t;             /**< The type represents string */

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MACROS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    EXTERN VARIABLES DEFINITIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    FUNCTION PROTOTYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern oC_STRING_t         oC_STRING_New           ( const oC_STRING_t Str );
extern oC_STRING_t         oC_STRING_NewWithSize   ( const int Size );
extern bool                oC_STRING_Delete        ( oC_STRING_t * String );
extern bool                oC_STRING_Append        ( oC_STRING_t * String , const oC_STRING_t Str );
extern char                oC_STRING_At            ( const oC_STRING_t String , int Index );
extern bool                oC_STRING_Chop          ( oC_STRING_t * String , const int N );
extern bool                oC_STRING_Clear         ( oC_STRING_t * String );
extern bool                oC_STRING_Compare       ( const oC_STRING_t String1 , const oC_STRING_t String2     , const oC_STRING_CaseSensitivity_t CaseSensitivity );
extern bool                oC_STRING_CompareN      ( const oC_STRING_t String1 , const oC_STRING_t String2     , int N , const oC_STRING_CaseSensitivity_t CaseSensitivity );
extern bool                oC_STRING_Contains      ( const oC_STRING_t String  , const oC_STRING_t SubString   , const oC_STRING_CaseSensitivity_t CaseSensitivity );
extern int                 oC_STRING_Count         ( const oC_STRING_t String );
extern bool                oC_STRING_Fill          ( oC_STRING_t * String , const char C , const int Size );
extern oC_STRING_t         oC_STRING_FromInt       ( int Size , int Value , bool PlusSign , bool LeftZeroPads );
extern oC_STRING_t         oC_STRING_FromUInt      ( int Size , unsigned int Value , bool PlusSign , bool LeftZeroPads );
extern oC_STRING_t         oC_STRING_FromUIntToOctal(int Size , unsigned int Value , bool OPrefix  , bool LeftZeroPads );
extern oC_STRING_t         oC_STRING_FromUIntToHex ( int Size , unsigned int Value , bool OxPrefix , bool LeftZeroPads );
extern oC_STRING_t         oC_STRING_FromPointer   ( int Size , void *       Value , bool OxPrefix , bool LeftZeroPads );
extern oC_STRING_t         oC_STRING_FromFloat     ( int Size , double Value       , unsigned int Precision , bool LeftZeroPads );
extern int                 oC_STRING_ToInt         ( const oC_STRING_t Str );
extern int                 oC_STRING_ToUInt        ( const oC_STRING_t Str );
extern int                 oC_STRING_ToUOct        ( const oC_STRING_t Str );
extern char                oC_STRING_ToChar        ( const oC_STRING_t Str );
extern int                 oC_STRING_IndexOf       ( const oC_STRING_t String , const oC_STRING_t SubString , const oC_STRING_CaseSensitivity_t CaseSensitivity );
extern bool                oC_STRING_Insert        ( oC_STRING_t * String , int Position , const oC_STRING_t Str );
extern bool                oC_STRING_IsEmpty       ( const oC_STRING_t String );
extern bool                oC_STRING_IsNull        ( const oC_STRING_t String );
extern bool                oC_STRING_IsCorrect     ( const oC_STRING_t String );
extern oC_STRING_t         oC_STRING_Left          ( const oC_STRING_t String , const int N );
extern oC_STRING_t         oC_STRING_LeftJustified ( const oC_STRING_t String , const int Width , const char C );
extern int                 oC_STRING_Length        ( const oC_STRING_t const String );
extern oC_STRING_t         oC_STRING_Mid           ( const oC_STRING_t String , int Position , const int N );
extern bool                oC_STRING_Prepend       ( oC_STRING_t * String , const oC_STRING_t Str );
extern bool                oC_STRING_PushBack      ( oC_STRING_t * String , const oC_STRING_t Str );
extern bool                oC_STRING_PushFront     ( oC_STRING_t * String , const oC_STRING_t Str );
extern bool                oC_STRING_Remove        ( oC_STRING_t * String , int Position , const int N );
extern int                 oC_STRING_Replace       ( oC_STRING_t * String , const oC_STRING_t Before , const oC_STRING_t After , const oC_STRING_CaseSensitivity_t CaseSensitivity);
extern int                 oC_STRING_ReplaceChar   ( oC_STRING_t * String , char Before , char After , const oC_STRING_CaseSensitivity_t CaseSensitivity);
extern bool                oC_STRING_Reserve       ( oC_STRING_t * String , int Size );
extern bool                oC_STRING_Resize        ( oC_STRING_t * String , int Size , char C );
extern oC_STRING_t         oC_STRING_Right         ( const oC_STRING_t String , int N );
extern oC_STRING_t         oC_STRING_RightJustified( const oC_STRING_t String , int N , const char C );
extern int                 oC_STRING_Size          ( const oC_STRING_t String );
//extern oC_STRINGLIST_t     oC_STRING_Split         ( const oC_STRING_t String , const oC_STRING_t Sep);
extern bool                oC_STRING_Squeeze       ( oC_STRING_t * String );
extern bool                oC_STRING_Swap          ( oC_STRING_t * String1 , oC_STRING_t * String2 );
extern bool                oC_STRING_ToLower       ( oC_STRING_t * String );
extern bool                oC_STRING_ToUpper       ( oC_STRING_t * String );
extern bool                oC_STRING_Trim          ( oC_STRING_t * String );
extern oC_STRING_t         oC_STRING_Trimmed       ( const oC_STRING_t String );
extern bool                oC_STRING_Truncate      ( oC_STRING_t * String , int Position );
extern bool                oC_STRING_Set           ( oC_STRING_t * String , const oC_STRING_t Str );

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INLINE FUNCTIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#endif /* SYSTEM_LIBRARIES_STRINGS_OC_STRING_H_ */
