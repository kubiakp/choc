/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       queue.h
 *
 *    @brief      The header with the interface for the queue handling library
 *
 *    @author     Patryk Kubiak - (Created on: 20 10 2014 16:28:29)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_QUEUE_OC_QUEUE_H_
#define SYSTEM_LIBRARIES_QUEUE_OC_QUEUE_H_

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INCLUDES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Core

// Drivers

// Libraries

// Others
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>     // For size_t

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    CONSTS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    TYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef uint32_t oC_QUEUE_Index_t;      /**< The type for store the index of the elements */

/**
 * The type for storing context of the queue.
 */
typedef struct
{
    void*               This;               /**< The reference to the itself for verification */
    void*               Elements;           /**< The array with elements for the queue */
    size_t              ElementSize;        /**< Size of the one element */
    oC_QUEUE_Index_t    MaximumLength;      /**< Maximum number of elements in the queue */
    oC_QUEUE_Index_t    ElementsInQueue;    /**< Number of elements actually in the queue */
    oC_QUEUE_Index_t    PutIndex;           /**< Index of the element to put */
    oC_QUEUE_Index_t    GetIndex;           /**< Index of the element to get */
} oC_QUEUE_t;

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MACROS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    EXTERN VARIABLES DEFINITIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    FUNCTION PROTOTYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern bool         oC_QUEUE_Init           ( oC_QUEUE_t * Queue , size_t ElementSize , oC_QUEUE_Index_t MaximumLength );
extern bool         oC_QUEUE_Deinit         ( oC_QUEUE_t * Queue );
extern bool         oC_QUEUE_IsInitialized  ( oC_QUEUE_t * Queue );
extern bool         oC_QUEUE_IsEmpty        ( oC_QUEUE_t * Queue );
extern bool         oC_QUEUE_IsFull         ( oC_QUEUE_t * Queue );
extern bool         oC_QUEUE_Put            ( oC_QUEUE_t * Queue , void * Element );
extern bool         oC_QUEUE_Get            ( oC_QUEUE_t * Queue , void * Element );

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INLINE FUNCTIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif /* SYSTEM_LIBRARIES_QUEUE_OC_QUEUE_H_ */
