/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_queue.c
 *
 *    @brief      The file with source of the queue basic functions
 *
 *    @author     Patryk Kubiak - (Created on: 20 paź 2014 16:49:57)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INCLUDES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This module
#include <oc_gen_macros.h>     // For assertions
#include <oc_queue.h>

// Core
#include <string.h>     // For memcpy
#include <stdlib.h>     // For malloc
#include <oc_assert.h>

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL CONSTS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL TYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MACROS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL VARIABLES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL FUNCTION PROTOTYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static inline bool          _IsEmpty                ( oC_QUEUE_t * Queue );
static inline bool          _IsFull                 ( oC_QUEUE_t * Queue );
static inline bool          _Put                    ( oC_QUEUE_t * Queue , void * Element );
static inline bool          _Get                    ( oC_QUEUE_t * Queue , void * ElementDestination );

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INTERFACE FUNCTIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//==========================================================================================================================================
/**
 * The function initializes the queue. It allocate a memory for storing the elements, and set default values for container fields.
 *
 * @param Queue                 Reference to a Queue container.
 * @param ElementSize           Size of the one element stored in the queue.
 * @param MaximumLength         Maximum length of the queue. It is number of elements stored in the queue.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_QUEUE_Init( oC_QUEUE_t * Queue , size_t ElementSize , oC_QUEUE_Index_t MaximumLength )
{
    bool result = false;

    if ( Queue != NULL )
    {
        Queue->Elements = malloc( ElementSize * MaximumLength );

        if ( Queue->Elements != NULL )
        {
            Queue->MaximumLength    = MaximumLength;
            Queue->ElementSize      = ElementSize;
            Queue->ElementsInQueue  = 0;
            Queue->PutIndex         = 0;
            Queue->GetIndex         = 0;
            Queue->This             = (void*)Queue;

            result                  = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function deinitializes the queue. It deallocate a memory needed for a elements. It also check, if queue was initialized.
 *
 * @param Queue                 Reference to a Queue container.
 *
 * @return true if success, false if queue was not initialized before.
 */
//==========================================================================================================================================
bool         oC_QUEUE_Deinit         ( oC_QUEUE_t * Queue )
{
    bool result = false;

    if ( oC_QUEUE_IsInitialized(Queue) )
    {
        free( Queue->Elements );
        Queue->This         = NULL;

        result              = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function checks if the queue is correctly initialized by a Init function.
 *
 * @param Queue                 The reference to the queue container
 *
 * @return true if container was initialized
 */
//==========================================================================================================================================
bool oC_QUEUE_IsInitialized( oC_QUEUE_t * Queue )
{
    bool result = false;

    if ( Queue != NULL )
    {
        if ( Queue->This == Queue )
        {
            result = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function checks if the queue is empty.
 *
 * @param Queue             The reference to the initialized queue container.
 *
 * @return true if empty
 */
//==========================================================================================================================================
bool         oC_QUEUE_IsEmpty        ( oC_QUEUE_t * Queue )
{
    oC_ASSERT( oC_QUEUE_IsInitialized( Queue ) );   // The queue wasn't initialized first!

    return _IsEmpty( Queue );
}

//==========================================================================================================================================
/**
 * The function checks if the queue is full.
 *
 * @param Queue             The reference to the initialized queue container.
 *
 * @return true if full
 */
//==========================================================================================================================================
bool         oC_QUEUE_IsFull         ( oC_QUEUE_t * Queue )
{
    oC_ASSERT( oC_QUEUE_IsInitialized( Queue ) );   // The queue wasn't initialized first!

    return _IsFull( Queue );
}

//==========================================================================================================================================
/**
 * The function puts a element to the queue. It operates on the copy of the element.
 *
 * @param Queue             The reference to the initialized queue container.
 * @param Element           The reference to the element which should be put to the queue.
 *
 * @return true if success, false if the queue is full
 */
//==========================================================================================================================================
bool         oC_QUEUE_Put            ( oC_QUEUE_t * Queue , void * Element )
{
    oC_ASSERT( oC_QUEUE_IsInitialized( Queue ) );   // The queue wasn't initialized first!
    oC_ASSERT( Element != NULL );                   // The element cannot be NULL !

    return _Put( Queue , Element );
}

//==========================================================================================================================================
/**
 * The function gets a element from the queue. It operates on the copy of the element, so the Element argument should be allocated reference.
 *
 * @param Queue             The reference to the initialized queue container.
 * @param Element           The reference to the element destination. It must be allocated first
 *
 * @return true if success, false if the queue is empty
 */
//==========================================================================================================================================
bool         oC_QUEUE_Get            ( oC_QUEUE_t * Queue , void * ElementDestination )
{
    bool result = false;

    oC_ASSERT( oC_QUEUE_IsInitialized( Queue ) );   // The queue wasn't initialized first!
    oC_ASSERT( ElementDestination != NULL );        // The element destination cannot be NULL !

    return _Get( Queue , ElementDestination );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL FUNCTION 
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//==========================================================================================================================================
/**
 * The function checks if the queue is empty without checking if queue is valid.
 *
 * @param Queue             Reference to the valid queue
 *
 * @return true if empty
 */
//==========================================================================================================================================
static inline bool          _IsEmpty                ( oC_QUEUE_t * Queue )
{
    return Queue->ElementsInQueue == 0;
}

//==========================================================================================================================================
/**
 * The function checks if the queue is full without checking if the queue reference is valid.
 *
 * @param Queue             Reference to the valid queue
 *
 * @return true if full
 */
//==========================================================================================================================================
static inline bool          _IsFull                 ( oC_QUEUE_t * Queue )
{
    return Queue->ElementsInQueue >= Queue->MaximumLength;
}

//==========================================================================================================================================
/**
 * The function puts the element to the queue, if it is not full. It not checks if the queue is correct.
 *
 * @param Queue             The reference to the valid queue
 * @param Element           The element for the copy to the queue
 *
 * @return true if success, false if queue is full
 */
//==========================================================================================================================================
static inline bool          _Put                    ( oC_QUEUE_t * Queue , void * Element )
{
    bool result;

    if ( !_IsFull( Queue ) )
    {
        memcpy( &Queue->Elements [ Queue->PutIndex ] , Element , Queue->ElementSize );

        Queue->PutIndex          = (Queue->PutIndex + Queue->ElementSize) % ( Queue->ElementSize * Queue->MaximumLength );
        Queue->ElementsInQueue  += 1;

        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function gets a element from the queue, if it is not empty. It not checks if the queue is correct.
 *
 * @param Queue                 The reference to the valid queue
 * @param ElementDestination    The reference to the destination of the element
 *
 * @return true if success, false if queue is empty
 */
//==========================================================================================================================================
static inline bool          _Get                    ( oC_QUEUE_t * Queue , void * ElementDestination )
{
    bool result;

    if ( !_IsEmpty( Queue ) )
    {
        memcpy( ElementDestination , &Queue->Elements[ Queue->GetIndex ] , Queue->ElementSize );

        Queue->GetIndex         = (Queue->GetIndex + Queue->ElementSize) % ( Queue->ElementSize * Queue->MaximumLength );
        Queue->ElementsInQueue -= 1;

        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}
