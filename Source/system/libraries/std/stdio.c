/*******************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          stdio.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 15 sty 2015 16:22:29) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <stdio.h>
#include <stdarg.h>
#include <oc_gen_types.h>
#include <oc_os.h>
#include <oc_string.h>
#include <oc_vfs.h>
#include <oc_assert.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Type of specifiers available in the printf function
 */
//==========================================================================================================================================
typedef enum
{
    Specifier_SignedDecimalInteger_d ,             //!< d   Signed decimal integer  (example: 392)
    Specifier_SignedDecimalInteger_i ,             //!< i   Signed decimal integer  (example: 392)
    Specifier_UnsignedDecimalInteger ,             //!< u	Unsigned decimal integer	(example: 7235)
    Specifier_UnsignedOctal ,                      //!< o	Unsigned octal	(example: 610)
    Specifier_UnsignedHexadecimalInteger ,         //!< x	Unsigned hexadecimal integer	(example: 7fa)
    Specifier_UnsignedHexadecimalIntegerUppercase ,//!< X	Unsigned hexadecimal integer (uppercase)	(example: 7FA)
    Specifier_DecimalFloatingPointLowercase ,      //!< f	Decimal floating point, lowercase	(example: 392.65)
    Specifier_DecimalFloatingPointUppercase ,      //!< F	Decimal floating point, uppercase	(example: 392.65)
    Specifier_ScientifixNotationLowercase ,        //!< e	Scientific notation (mantissa/exponent), lowercase	(example: 3.9265e+2)
    Specifier_ScientifixNotationUppercase ,        //!< E	Scientific notation (mantissa/exponent), uppercase	(example: 3.9265E+2)
    Specifier_ShortestRepresentationLowercase ,    //!< g	Use the shortest representation: %e or %f	(example: 392.65)
    Specifier_ShortestRepresentationUppercase ,    //!< G	Use the shortest representation: %E or %F	(example: 392.65)
    Specifier_HexadecimalFloatingPointLowercase ,  //!< a	Hexadecimal floating point, lowercase	(example: -0xc.90fep-2)
    Specifier_HexadecimalFloatingPointUppercase ,  //!< A	Hexadecimal floating point, uppercase	(example: -0XC.90FEP-2)
    Specifier_Character ,                          //!< c	Character	(example: a)
    Specifier_String ,                             //!< s	String of characters	(example: sample)
    Specifier_Pointer ,                            //!< p	Pointer address	(example: 0x8000000)
    Specifier_PercentSign ,                        //!< %   A % followed by another % character will write a single % to the stream.
    Specifier_NumberOfSpecifiers ,                 //!< Number of defined specifiers
    Specifier_Unknown                              //!< Unknown specifier, not handle in the system
} Specifier_t;

//==========================================================================================================================================
/**
 * Array of characters for detecting specifier
 */
//==========================================================================================================================================
const char Specifiers[Specifier_NumberOfSpecifiers] = {
                           'd' ,
                           'i' ,
                           'u' ,
                           'o' ,
                           'x' ,
                           'X' ,
                           'f' ,
                           'F' ,
                           'e' ,
                           'E' ,
                           'g' ,
                           'G' ,
                           'a' ,
                           'A' ,
                           'c' ,
                           's' ,
                           'p' ,
                           '%' ,
};

//==========================================================================================================================================
/**
 * Type of function to push data to a line according to a format, from the Args list
 *
 * @param Line          reference to a line string
 * @param Format        String with format
 * @param Args          List of function arguments
 *
 */
//==========================================================================================================================================
typedef void (*PushFunction_t)(oC_STRING_t * Line , const char * Format , va_list *Args);
//==========================================================================================================================================
/**
 * Type of function to read data to a line according to a format, from the Args list
 *
 * @param Line          reference to a line string
 * @param Format        String with format
 * @param Args          List of function arguments
 *
 */
//==========================================================================================================================================
typedef void (*ReadFunction_t)(oC_STRING_t * Line , const char * Format , va_list *Args);

//==========================================================================================================================================
/**
 * Type for storing informations about flags in specifier
 */
//==========================================================================================================================================
typedef struct
{
   unsigned char       LeftJustify:1;       /**< Left-justify within the given field width; Right justification is the default (see width sub-specifier). */
   unsigned char       PlusMinusSign:1;     /**< Forces to preceed the result with a plus or minus sign (+ or -) even for positive numbers. By default, only negative numbers are preceded with a - sign. */
   unsigned char       Space:1;             /**< If no sign is going to be written, a blank space is inserted before the value. */
   unsigned char       Hash:1;              /**< Used with o, x or X specifiers the value is preceeded with 0, 0x or 0X respectively for values different than zero.
                                                 Used with a, A, e, E, f, F, g or G it forces the written output to contain a decimal point even if no more digits follow. By default, if no digits follow, no decimal point is written. */
   unsigned char       Zero:1;              /**< Left-pads the number with zeroes (0) instead of spaces when padding is specified (see width sub-specifier). */
} Flags_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/
#define ENTER 13

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static inline int           Print                                   ( oC_FileDescriptor_t FileDescriptor , const char *Format, va_list *Args );
static inline int           Scan                                    ( oC_FileDescriptor_t FileDescriptor , const char *Format, va_list *Args );
static inline const char *  PushData                                ( oC_STRING_t * Line ,const char * Format , va_list *Args );
static inline const char *  ReadData                                ( oC_STRING_t * Line ,const char * Format , va_list *Args );
static inline Specifier_t   ReadFirstSpecifier                      ( const char * Format );
static inline const char *  MoveToEndOfSpecifier                    ( const char * Format );
static inline Flags_t       ReadFlags                               ( const char * Format );
static inline const char *  CopyToSpecifier                         ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        int           ReadNumberOfCharactersToPrint           ( const char * Format );
static        int           ReadNumberOfCharactersToScan            ( const char * Format );
static        int           ReadPrecision                           ( const char * Format );
static inline bool          IsSpecifier                             ( char Sign );
static inline bool          IsDigit                                 ( char Sign );

/* Push Functions */
static        void          PushSignedDecimalInteger                 ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushUnsignedDecimalInteger               ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushUnsignedOctal                        ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushHexadecimalInteger                   ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushHexadecimalIntegerUppercase          ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushDecimalFloatingPointLowercase        ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushDecimalFloatingPointUppercase        ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushScientifixNotationLowercase          ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushScientifixNotationUppercase          ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushShortestRepresentationLowercase      ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushShortestRepresentationUppercase      ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushHexadecimalFloatingPointLowercase    ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushHexadecimalFloatingPointUppercase    ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushCharacter                            ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushString                               ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushPointer                              ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushPercentSign                          ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          PushUnknown                              ( oC_STRING_t * Line , const char * Format , va_list *Args );

/* Read Functions */
static        void          ReadSignedDecimalInteger                 ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadUnsignedDecimalInteger               ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadUnsignedOctal                        ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadHexadecimalInteger                   ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadHexadecimalIntegerUppercase          ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadDecimalFloatingPointLowercase        ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadDecimalFloatingPointUppercase        ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadScientifixNotationLowercase          ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadScientifixNotationUppercase          ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadShortestRepresentationLowercase      ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadShortestRepresentationUppercase      ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadHexadecimalFloatingPointLowercase    ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadHexadecimalFloatingPointUppercase    ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadCharacter                            ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadString                               ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadPointer                              ( oC_STRING_t * Line , const char * Format , va_list *Args );
static        void          ReadUnknown                              ( oC_STRING_t * Line , const char * Format , va_list *Args );
/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/


//==========================================================================================================================================
/**
 * List of functions for pushing data into a line string according format for each specifier.
 */
//==========================================================================================================================================
const PushFunction_t PushFunctions[Specifier_Unknown + 1] = {
#if oC_PRINTF_USE_SPECIFIER_d == true
                                           PushSignedDecimalInteger                 ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_i == true
                                           PushSignedDecimalInteger                 ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_u == true
                                           PushUnsignedDecimalInteger               ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_o == true
                                           PushUnsignedOctal                        ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_x == true
                                           PushHexadecimalInteger                   ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_X == true
                                           PushHexadecimalIntegerUppercase          ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_f == true
                                           PushDecimalFloatingPointLowercase        ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_F == true
                                           PushDecimalFloatingPointUppercase        ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_e == true
                                           PushScientifixNotationLowercase          ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_E == true
                                           PushScientifixNotationUppercase          ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_g == true
                                           PushShortestRepresentationLowercase      ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_G == true
                                           PushShortestRepresentationUppercase      ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_a == true
                                           PushHexadecimalFloatingPointLowercase    ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_A == true
                                           PushHexadecimalFloatingPointUppercase    ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_c == true
                                           PushCharacter                            ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_s == true
                                           PushString                               ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_p == true
                                           PushPointer                              ,
#else
                                           PushUnknown                              ,
#endif
#if oC_PRINTF_USE_SPECIFIER_percent == true
                                           PushPercentSign                          ,
#else
                                           PushUnknown                              ,
#endif
                                           PushUnknown                              ,
                                           PushUnknown
};
//==========================================================================================================================================
/**
 * List of functions for reading data into a line string according format for each specifier.
 */
//==========================================================================================================================================
const ReadFunction_t ReadFunctions[Specifier_Unknown + 1] = {
#if oC_SCANF_USE_SPECIFIER_d == true
                                           ReadSignedDecimalInteger                 ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_i == true
                                           ReadSignedDecimalInteger                 ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_u == true
                                           ReadUnsignedDecimalInteger               ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_o == true
                                           ReadUnsignedOctal                        ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_x == true
                                           ReadHexadecimalInteger                   ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_X == true
                                           ReadHexadecimalIntegerUppercase          ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_f == true
                                           ReadDecimalFloatingPointLowercase        ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_F == true
                                           ReadDecimalFloatingPointUppercase        ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_e == true
                                           ReadScientifixNotationLowercase          ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_E == true
                                           ReadScientifixNotationUppercase          ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_g == true
                                           ReadShortestRepresentationLowercase      ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_G == true
                                           ReadShortestRepresentationUppercase      ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_a == true
                                           ReadHexadecimalFloatingPointLowercase    ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_A == true
                                           ReadHexadecimalFloatingPointUppercase    ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_c == true
                                           ReadCharacter                            ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_s == true
                                           ReadString                               ,
#else
                                           ReadUnknown                              ,
#endif
#if oC_SCANF_USE_SPECIFIER_p == true
                                           ReadPointer                              ,
#else
                                           ReadUnknown                              ,
#endif
                                           ReadUnknown
};
/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Redefinition of the printf function. To see description check printf docs.
 *
 * @param Format        String to print via std output
 *
 * @return number of written bytes
 */
//==========================================================================================================================================
int printf( const char * Format , ... )
{
    va_list args;

    va_start( args, Format );
    int status = Print( oC_FileDescriptor_StdOutput , Format, &args );

    return status;
}

//==========================================================================================================================================
/**
 * Redefinition of the printf function. To see description check printf docs
 *
 * @param Str           String to print
 *
 * @return number of written bytes
 */
//==========================================================================================================================================
int puts( const char * Str )
{
    return oC_OS_SendToStream( oC_FileDescriptor_StdOutput , Str , oC_STRING_Length( Str ) );
}

//==========================================================================================================================================
/**
 * Reads string from standard input stream (it checks size of string, if there is '\0' sign at the end of it
 *
 * @param Str           String pointer
 *
 * @return On success it returns Str, otherwise it return NULL pointer
 */
//==========================================================================================================================================
char * gets( char * Str )
{
    char * retStr = NULL;

    if ( oC_OS_StandardInput( Str , oC_STRING_Length( Str ) ) > 0 )
    {
        retStr  = Str;
    }

    return retStr;
}

//==========================================================================================================================================
/**
 * Reads one character from a stream
 *
 * @param Stream        Stream to read
 *
 * @return first character from stream
 */
//==========================================================================================================================================
int getc( FILE * Stream )
{
    char c;

    if ( oC_OS_StandardInput( &c , 1 ) == 1 )
    {
        return c;
    }
    else
    {
        return 0;
    }
}

//==========================================================================================================================================
/**
 * Reads formated data from standard input stream
 *
 * @param Format        Format to read
 *
 * @return number of received bytes
 */
//==========================================================================================================================================
int scanf( const char * Format , ... )
{
    va_list Args;
    va_start( Args , Format );

    int status = Scan( oC_FileDescriptor_StdInput , Format, &Args );

    return status;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Pushes data according to specifier to a line.
 *
 * @param Line          Reference to a line string
 * @param Format        Format string
 * @param Args          list of function arguments
 *
 * @return Format after push
 */
//==========================================================================================================================================
static inline const char * PushData( oC_STRING_t * Line ,const char * Format , va_list *Args )
{
    Specifier_t specifier = ReadFirstSpecifier( Format );

    oC_ASSERT( specifier <= Specifier_Unknown );

    PushFunctions[specifier]( Line , Format , Args );

    return MoveToEndOfSpecifier( Format );
}

//==========================================================================================================================================
/**
 * Read data according to specifier to a line.
 *
 * @param Line          Reference to a line string
 * @param Format        Format string
 * @param Args          list of function arguments
 *
 * @return
 */
//==========================================================================================================================================
static inline const char * ReadData( oC_STRING_t * Line ,const char * Format , va_list *Args )
{
    Specifier_t specifier = ReadFirstSpecifier( Format );

    oC_ASSERT( specifier <= Specifier_Unknown );

    ReadFunctions[specifier]( Line , Format , Args );

    return MoveToEndOfSpecifier( Format );
}
//==========================================================================================================================================
/**
 * Print formated text to file or stream
 *
 * @param File          File descriptor
 * @param Format        Formated text
 * @param Args          List of function arguments
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
static inline int Print( oC_FileDescriptor_t FileDescriptor , const char *Format, va_list *Args )
{
    int sentBytes;
    oC_STRING_t line = oC_STRING_NewWithSize(0);

    for( ; *Format != '\0' ; )
    {
        if ( *Format == '%' )
        {
            Format = PushData( &line , ++Format , Args );
        }
        else
        {
            Format = CopyToSpecifier( &line , Format , Args );
        }
    }

    sentBytes = oC_OS_SendToStream( FileDescriptor , line , oC_STRING_Length(line) );

    oC_STRING_Delete( &line );

    va_end( *Args );

    return sentBytes;
}
//==========================================================================================================================================
/**
 * Scan formated text from file or stream
 *
 * @param File          File descriptor
 * @param Format        Formated text
 * @param Args          List of function arguments
 *
 * @return number of read bytes
 */
//==========================================================================================================================================
static inline int Scan( oC_FileDescriptor_t FileDescriptor , const char *Format, va_list *Args )
{
    int readBytes = 0;
    oC_STRING_t Line = oC_STRING_NewWithSize(0);

    for( ; *Format != '\0' ; )
    {
        if ( *Format == '%' )
        {
            Format = ReadData( &Line , ++Format , Args );
        }
        else
        {
            Format = CopyToSpecifier( &Line , Format , Args );
        }
    }

    readBytes = oC_STRING_Size(Line);

    oC_STRING_Delete( &Line );
    va_end( *Args );

    return readBytes;
}

//==========================================================================================================================================
/**
 * Function reads first specifier from the format string
 *
 * @param Format        String with the format to print
 *
 * @return first specifier in the format string
 */
//==========================================================================================================================================
static inline Specifier_t ReadFirstSpecifier( const char * Format )
{
    Specifier_t specifier = Specifier_Unknown;

    while( specifier == Specifier_Unknown && oC_OS_IsAddressCorrect( Format ))
    {
        for (Specifier_t spec = 0; spec < Specifier_NumberOfSpecifiers ; spec++)
        {
            if ( *Format == Specifiers[spec] )
            {
                specifier   = spec;
                break;
            }
        }
        Format++;
    }

    return specifier;
}

//==========================================================================================================================================
/**
 * Moves format pointer to
 *
 * @param Format        String with the format to print
 *
 * @return reference to end of specifer in format
 */
//==========================================================================================================================================
static inline const char* MoveToEndOfSpecifier( const char * Format )
{
    while( *Format != '\0' )
    {

        if ( IsSpecifier( *Format ) )
        {
            Format++;
            break;
        }

        Format++;
    }

    return Format;
}

//==========================================================================================================================================
/**
 * Reads flags from the first specifier in Format
 *
 * @param Format
 *
 * @return
 */
//==========================================================================================================================================
static inline Flags_t ReadFlags( const char * Format )
{
    Flags_t flags = { .LeftJustify = 0 , .PlusMinusSign = 0 , .Space = 0 , .Hash = 0 , .Zero = 0 };

    for(;;)
    {
        if ( *Format == '-' )
        {
            flags.LeftJustify = 1;
        }
        else if ( *Format == '+' )
        {
            flags.PlusMinusSign= 1;
        }
        else if ( *Format == ' ' )
        {
            flags.Space       = 1;
        }
        else if ( *Format == '#' )
        {
            flags.Hash        = 1;
        }
        else if ( *Format == '0' )
        {
            flags.Zero        = 1;
        }
        else
        {
            break;
        }
        Format++;
    }

    return flags;
}

//==========================================================================================================================================
/**
 * Copy string to the Line for next specifier
 *
 * @param Line              Reference to the Line string
 * @param Format            Format string
 * @param Args              List of arguments
 *
 * @return reference to the Format string afer copy
 */
//==========================================================================================================================================
static inline const char * CopyToSpecifier( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    const char *startFormat = Format;
    int len = 0;

    oC_UNUSED_ARG( Args );

    while( *Format != '\0' )
    {
        if ( *Format == '%' )
        {
            break;
        }
        else
        {
            Format++;
            len++;
        }
    }

    if ( len > 0 )
    {
        oC_STRING_t str = oC_STRING_NewWithSize( len );

        for(int i = 0; i < len ; i++)
        {
            str[i] = startFormat[i];
        }

        oC_STRING_PushBack(Line , str);

        oC_STRING_Delete( &str );
    }

    return Format;
}

//==========================================================================================================================================
/**
 * The function reads number of characters to print from first specifier
 *
 * @param Format        Format string
 *
 * @return number of characters to print
 */
//==========================================================================================================================================
static int ReadNumberOfCharactersToPrint( const char * Format )
{
    int charactersToPrint   = 0;
    int multiplier = 1;

    while( *Format != '\0' )
    {
        if ( IsSpecifier( *Format ) )
        {
            break;
        }
        else if ( IsDigit( *Format ) )
        {
            while( IsDigit(*Format) )
            {
                charactersToPrint += multiplier * ((*Format) - '\0');
                multiplier *= 10;
                Format++;
            }
            break;
        }
        Format++;
    }

    return charactersToPrint;
}
//==========================================================================================================================================
/**
 * The function reads number of characters to print from first specifier
 *
 * @param Format        Format string
 *
 * @return number of characters to print
 */
//==========================================================================================================================================
static int ReadNumberOfCharactersToScan( const char * Format )
{
    int charactersToScan   = -1;
    int multiplier = 1;

    while( *Format != '\0' )
    {
        if ( IsSpecifier( *Format ) )
        {
            break;
        }
        else if ( IsDigit( *Format ) )
        {
            charactersToScan = 0;
            while( IsDigit(*Format) )
            {
                charactersToScan += multiplier * ((*Format) - '\0') - '0';
                multiplier *= 10;
                Format++;
            }
            break;
        }
        Format++;
    }

    return charactersToScan;
}

//==========================================================================================================================================
/**
 * For a, A, e, E, f and F specifiers: this is the number of digits to be printed after the decimal point (by default, this is 6).
 * For g and G specifiers: This is the maximum number of significant digits to be printed.
 * For s: this is the maximum number of characters to be printed. By default all characters are printed until the ending null character is encountered.
 * If the period is specified without an explicit value for precision, 0 is assumed.
 *
 * @param Format        String with format
 *
 * @return number of digits after point
 */
//==========================================================================================================================================
static int ReadPrecision( const char * Format )
{
    int precision = 6;

    while( *Format != '\0' && !IsSpecifier(*Format) )
    {
        if ( *Format == '.' )
        {
            Format++;

            if ( IsDigit( *Format ) )
            {
                precision   = (*Format) - '0';
                break;
            }
        }
        Format++;
    }

    return precision;
}

//==========================================================================================================================================
/**
 * Checks if character is specifier
 *
 * @param Sign      Sign to check
 *
 * @return true if specifier
 */
//==========================================================================================================================================
static inline bool IsSpecifier( char Sign )
{
    bool isSpecifier = false;

    for(Specifier_t spec = 0 ; spec < Specifier_NumberOfSpecifiers ; spec++)
    {
        if ( Specifiers[spec] == Sign )
        {
            isSpecifier = true;
        }
    }

    return isSpecifier;
}

//==========================================================================================================================================
/**
 * Checks if sign is digit
 *
 * @param Sign      Sign to check
 *
 * @return true if digit
 */
//==========================================================================================================================================
static inline bool IsDigit( char Sign )
{
    return (Sign >= '0') && (Sign <= '9');
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushSignedDecimalInteger( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    int charactersToPrint   = ReadNumberOfCharactersToPrint( Format );
    int value               = va_arg( *Args , int );

    oC_STRING_t valueString = oC_STRING_FromInt( charactersToPrint , value , flags.PlusMinusSign , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes unsigned decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushUnsignedDecimalInteger( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    int charactersToPrint   = ReadNumberOfCharactersToPrint( Format );
    unsigned int value      = va_arg( *Args , unsigned int );

    oC_STRING_t valueString = oC_STRING_FromUInt( charactersToPrint , value , flags.PlusMinusSign , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes unsigned octal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushUnsignedOctal( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    int charactersToPrint   = ReadNumberOfCharactersToPrint( Format );
    unsigned int value      = va_arg( *Args , unsigned int );

    oC_STRING_t valueString = oC_STRING_FromUIntToOctal( charactersToPrint , value , flags.PlusMinusSign , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes unsigned hexadecimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushHexadecimalInteger( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    int charactersToPrint   = ReadNumberOfCharactersToPrint( Format );
    unsigned int value      = va_arg( *Args , unsigned int );

    oC_STRING_t valueString = oC_STRING_FromUIntToHex( charactersToPrint , value , flags.PlusMinusSign , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushHexadecimalIntegerUppercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    int charactersToPrint   = ReadNumberOfCharactersToPrint( Format );
    unsigned int value      = va_arg( *Args , unsigned int );

    oC_STRING_t valueString = oC_STRING_FromUIntToHex( charactersToPrint , value , flags.PlusMinusSign , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushDecimalFloatingPointLowercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    double value             = va_arg( *Args , double );

    oC_STRING_t valueString = oC_STRING_FromFloat( ReadNumberOfCharactersToPrint( Format ) , value , ReadPrecision( Format ) , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushDecimalFloatingPointUppercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    double value             = va_arg( *Args , double );

    oC_STRING_t valueString = oC_STRING_FromFloat( ReadNumberOfCharactersToPrint( Format ) , value , ReadPrecision( Format ) , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushScientifixNotationLowercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    double value             = va_arg( *Args , double );

    oC_STRING_t valueString = oC_STRING_FromFloat( ReadNumberOfCharactersToPrint( Format ) , value , ReadPrecision( Format ) , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushScientifixNotationUppercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    double value             = va_arg( *Args , double );

    oC_STRING_t valueString = oC_STRING_FromFloat( ReadNumberOfCharactersToPrint( Format ) , value , ReadPrecision( Format ) , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushShortestRepresentationLowercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    double value             = va_arg( *Args , double );

    oC_STRING_t valueString = oC_STRING_FromFloat( ReadNumberOfCharactersToPrint( Format ) , value , ReadPrecision( Format ) , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushShortestRepresentationUppercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    double value             = va_arg( *Args , double );

    oC_STRING_t valueString = oC_STRING_FromFloat( ReadNumberOfCharactersToPrint( Format ) , value , ReadPrecision( Format ) , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushHexadecimalFloatingPointLowercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    double value            = va_arg( *Args , double );

    oC_STRING_t valueString = oC_STRING_FromFloat( ReadNumberOfCharactersToPrint( Format ) , value , ReadPrecision( Format ) , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushHexadecimalFloatingPointUppercase( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    double value             = va_arg( *Args , double );

    oC_STRING_t valueString = oC_STRING_FromFloat( ReadNumberOfCharactersToPrint( Format ) , value , ReadPrecision( Format ) , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushCharacter( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    oC_UNUSED_ARG( Format );

    oC_STRING_Resize( Line , oC_STRING_Size( * Line ) + 1 , va_arg( *Args , int ));
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushString( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags = ReadFlags( Format );
    int charactersToPrint   = ReadNumberOfCharactersToPrint( Format );
    oC_STRING_t string = va_arg( *Args , char * );

    if ( charactersToPrint > 0 )
    {
        oC_STRING_t justified;
        if ( flags.LeftJustify )
        {
            justified = oC_STRING_LeftJustified( string , charactersToPrint , ' ' );
        }
        else
        {
            justified = oC_STRING_RightJustified( string , charactersToPrint , ' ' );
        }

        oC_STRING_PushBack( Line , justified );
        oC_STRING_Delete( &justified );
    }
    else
    {
        oC_STRING_PushBack( Line , string );
    }


}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushPointer( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Flags_t flags           = ReadFlags( Format );
    int charactersToPrint   = ReadNumberOfCharactersToPrint( Format );
    void * value            = va_arg( *Args , void * );

    oC_STRING_t valueString = oC_STRING_FromPointer( charactersToPrint , value , flags.PlusMinusSign , flags.Zero );

    oC_STRING_PushBack( Line , valueString );
    oC_STRING_Delete( &valueString );
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushPercentSign( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    oC_UNUSED_ARG( Format );
    oC_UNUSED_ARG( *Args );
    oC_STRING_Resize( Line , oC_STRING_Size( * Line ) + 1 , '%');
}

//==========================================================================================================================================
/**
 * Pushes signed decimal integer to the line
 *
 * @param Line          Reference to the string with output line
 * @param Format        Format to print
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void PushUnknown( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    Specifier_t specifier = ReadFirstSpecifier( Format );

    switch(specifier)
    {
        case Specifier_SignedDecimalInteger_d :
        case Specifier_SignedDecimalInteger_i :
        {
            volatile int val = va_arg( *Args , int );
            oC_UNUSED_ARG( val );
            break;
        }
        case Specifier_UnsignedDecimalInteger :
        case Specifier_UnsignedOctal :
        case Specifier_UnsignedHexadecimalInteger :
        case Specifier_UnsignedHexadecimalIntegerUppercase :
        {
            volatile unsigned int val = va_arg( *Args , unsigned int );
            oC_UNUSED_ARG( val );
            break;
        }
        case Specifier_DecimalFloatingPointLowercase :
        case Specifier_DecimalFloatingPointUppercase :
        case Specifier_ScientifixNotationLowercase :
        case Specifier_ScientifixNotationUppercase :
        case Specifier_ShortestRepresentationLowercase :
        case Specifier_ShortestRepresentationUppercase :
        case Specifier_HexadecimalFloatingPointLowercase :
        case Specifier_HexadecimalFloatingPointUppercase :
        {
            volatile double val = va_arg( *Args , double );
            oC_UNUSED_ARG( val );
            break;
        }
        case Specifier_Character :
        {
            volatile int val = va_arg( *Args , int );
            oC_UNUSED_ARG( val );
            break;
        }
        case Specifier_String :
        {
            volatile char* val = va_arg( *Args , char * );
            oC_UNUSED_ARG( val );
            break;
        }
        case Specifier_Pointer :
        {
            volatile void * val = va_arg( *Args , void *);
            oC_UNUSED_ARG( val );
            break;
        }
        case Specifier_PercentSign :
        default:
            break;
    }

    CopyToSpecifier( Line , Format , Args );
}
//==========================================================================================================================================
/**
 * Read signed decimal integer from the line
 *
 * @param Line          Reference to the input string
 * @param Format        Format to scan
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void ReadSignedDecimalInteger( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    char DataRet[1];
    int charactersToScan = ReadNumberOfCharactersToScan( Format );
    int Idx=0;
    signed int *value = va_arg( *Args, int*);

    while( ( DataRet[0] != ENTER ) && ( charactersToScan != Idx ) )
    {
        if( oC_OS_StandardInput( &DataRet[0] , 2 ) > 1 )
        {
            if( DataRet[0] != ENTER )
            {
                Idx++;
                oC_STRING_t NewData = oC_STRING_NewWithSize(1);
                NewData[0] = DataRet[0];
                oC_STRING_Append(Line, NewData);
                oC_STRING_Delete(&NewData);
            }
        }
    }

    *value = oC_STRING_ToInt(*Line);
}
//==========================================================================================================================================
/**
 * Read unsigned decimal integer from the line
 *
 * @param Line          Reference to the input string
 * @param Format        Format to scan
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void ReadUnsignedDecimalInteger( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    char DataRet[1];
    int charactersToScan = ReadNumberOfCharactersToScan( Format );
    int Idx=0;
    unsigned int *value = va_arg( *Args, int*);

    while( ( DataRet[0] != ENTER ) && ( charactersToScan != Idx ) )
    {
        if( oC_OS_StandardInput( &DataRet[0] , 2 ) > 1 )
        {
            if( DataRet[0] != ENTER )
            {
                Idx++;
                oC_STRING_t NewData = oC_STRING_NewWithSize(1);
                NewData[0] = DataRet[0];
                oC_STRING_Append(Line, NewData);
                oC_STRING_Delete(&NewData);
            }
        }
    }

    *value = oC_STRING_ToUInt(*Line);
}
//==========================================================================================================================================
/**
 * Read unsigned octal integer from the line
 *
 * @param Line          Reference to the input string
 * @param Format        Format to scan
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void ReadUnsignedOctal( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    char DataRet[1];
    int charactersToScan = ReadNumberOfCharactersToScan( Format );
    int Idx=0;
    unsigned int *value = va_arg( *Args, int*);

    while( ( DataRet[0] != ENTER ) && ( charactersToScan != Idx ) )
    {
        if( oC_OS_StandardInput( &DataRet[0] , 2 ) > 1 )
        {
            if( DataRet[0] != ENTER )
            {
                Idx++;
                oC_STRING_t NewData = oC_STRING_NewWithSize(1);
                NewData[0] = DataRet[0];
                oC_STRING_Append(Line, NewData);
                oC_STRING_Delete(&NewData);
            }
        }
    }

    *value = oC_STRING_ToUOct(*Line);
}
//==========================================================================================================================================
/**
 * Read character from the line
 *
 * @param Line          Reference to the input string
 * @param Format        Format to scan
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void ReadCharacter( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    char DataRet[1];
    int charactersToScan = ReadNumberOfCharactersToScan( Format );
    int Idx=0;
    char* value = va_arg( *Args, char* );

    while( ( DataRet[0] != ENTER ) && ( charactersToScan != Idx ) )
    {
        if( oC_OS_StandardInput( &DataRet[0] , 2 ) > 1 )
        {
            if( DataRet[0] != ENTER )
            {
                Idx++;
                oC_STRING_t NewData = oC_STRING_NewWithSize(1);
                NewData[0] = DataRet[0];
                oC_STRING_Append(Line, NewData);
                oC_STRING_Delete(&NewData);
            }
        }
    }

    *value = oC_STRING_ToChar(*Line);
}
//==========================================================================================================================================
/**
 * Read string from the line
 *
 * @param Line          Reference to the input string
 * @param Format        Format to scan
 * @param Args          List of arguments
 *
 */
//==========================================================================================================================================
static void ReadString( oC_STRING_t * Line , const char * Format , va_list *Args )
{
    char DataRet[1];
    int charactersToScan = ReadNumberOfCharactersToScan( Format );
    int Idx = 0;
    char* value = va_arg( *Args, char* );

    while( ( DataRet[0] != ENTER ) && ( charactersToScan != Idx ) )
    {
        if( oC_OS_StandardInput( &DataRet[0] , 2 ) > 1 )
        {
            if( DataRet[0] != ENTER )
            {
                value[Idx++] = DataRet[0];
            }
        }
    }
}

// Case for the future
static void ReadHexadecimalInteger ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}

static void ReadHexadecimalIntegerUppercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}

static void ReadDecimalFloatingPointLowercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadDecimalFloatingPointUppercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadScientifixNotationLowercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadScientifixNotationUppercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadShortestRepresentationLowercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadShortestRepresentationUppercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadHexadecimalFloatingPointLowercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadHexadecimalFloatingPointUppercase ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadPointer ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}
static void ReadUnknown ( oC_STRING_t * Line , const char * Format , va_list *Args )
{

}

