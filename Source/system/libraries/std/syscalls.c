#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <FreeRTOS.h>
#include <oc_vfs.h>

#define STDIN_FILE_DESCRIPTOR               (0)
#define STDOUT_FILE_DESCRIPTOR              (1)
#define STDERR_FILE_DESCRIPTOR              (2)
#define MIN_FILE_DESCRIPTOR                 (3)

#undef errno
extern int  errno;

void _exit (int  rc)
{

  while (1)
    {
    }
}       /* _exit () */

int _close (int   file)
{
  errno = EBADF;

  return -1;                    /* Always fails */
}       /* _close () */

/**
 * Executing a program
 * @param name
 * @param argv
 * @param env
 * @return
 */
int _execve (char  *name,
         char **argv,
         char **env)
{
  errno = ENOMEM;
  return -1;                    /* Always fails */

}       /* _execve () */

/**
 * Create a child process
 * @return
 */
int _fork ()
{
  errno = EAGAIN;
  return -1;                    /* Always fails */

}       /* _fork () */

int _fstat (int          file,
        struct stat *st)
{
  st->st_mode = S_IFCHR;
  return  0;
}       /* _fstat () */

/**
 * Get process ID
 * @return
 */
int _getpid ()
{
  return  1;                            /* Success */

}       /* _getpid () */

int _isatty (int   file)
{
  return  1;

}       /* _isatty () */

int _kill (int  pid, int  sig)
{
  errno = EINVAL;
  return -1;                    /* Always fails */

}       /* _kill () */

int _link ( char *old , char *new )
{
  errno = EMLINK;
  return -1;                    /* Always fails */

}       /* _link () */

int _lseek ( int file, int offset, int whence )
{
  return  0;
}       /* _lseek () */


int _open ( const char *name, int flags, int mode)
{
  errno = ENOSYS;
  return -1;                    /* Always fails */

}       /* _open () */

int _read (int file, char *ptr, int len)
{
  int bytes_counter = 0;

#if oC_USE_STREAMS
#if oC_USE_STD_INPUT_STREAM
  if ( file == STDIN_FILE_DESCRIPTOR )
  {
      bytes_counter = oC_STREAMS_ReceiveViaStdio( ptr , len );
  }
#endif
#endif

  if ( file >= MIN_FILE_DESCRIPTOR )
  {

  }

  return  bytes_counter;                            /* EOF */
}       /* _read () */


void *_sbrk (int nbytes)
{
    return pvPortMalloc(nbytes);
}       /* _sbrk () */

int _stat (char *file, struct stat *st)
{
  st->st_mode = S_IFCHR;
  return 0;

}       /* _stat () */

int _times (struct tms *buf)
{
  errno = EACCES;
  return  -1;
}       /* _times () */

int _unlink (char *name)
{
  errno = ENOENT;
  return -1;                    /* Always fails */
}       /* _unlink () */

int _write (int   file,char *buf,  int   nbytes)
{
  int bytes_counter = 0;

#if oC_USE_STREAMS
#   if oC_USE_STD_OUTPUT_STREAM
  if ( file == STDOUT_FILE_DESCRIPTOR )
  {
      bytes_counter = oC_STREAMS_SendViaStdio( buf , nbytes );
  }
#   endif

#   if oC_USE_STD_ERROR_STREAM
  if ( file == STDERR_FILE_DESCRIPTOR )
  {
      bytes_counter = oC_STREAMS_SendViaStdError( buf , nbytes );
  }
#   endif
#endif

  if ( file >= MIN_FILE_DESCRIPTOR )
  {

  }

  return bytes_counter;

}       /* _write () */
