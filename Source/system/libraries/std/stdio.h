/*******************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       stdio.h
 *
 *    @brief
 *                File with standard input / output functions handling
 *
 *    @author     Patryk Kubiak - (Created on: 27 sty 2015 10:06:47) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_STD_STDIO_H_
#define SYSTEM_LIBRARIES_STD_STDIO_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_vfs_cfg.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/

#ifdef getc
#undef getc
#endif
#define stdin       oC_FileDescriptor_StdInput
#define stdout      oC_FileDescriptor_StdOutput
#define stderr      oC_FileDescriptor_StdError

/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Type of file descriptor
 */
//==========================================================================================================================================
typedef enum
{
    oC_FileDescriptor_StdInput          = 0 ,                                                               //!< Standard input stream
    oC_FileDescriptor_StdOutput         = 1 ,                                                               //!< Standard output stream
    oC_FileDescriptor_StdError          = 2 ,                                                               //!< Standard error stream
    oC_FileDescriptor_FilesBegin        = 3 ,                                                               //!< Begin of files descriptors section
    oC_FileDescriptor_FilesEnd          = (oC_MAXIMUM_NUMBER_OF_OPEN_FILES + oC_FileDescriptor_FilesBegin)  //!< End of files descriptors section
} oC_FileDescriptor_t;

typedef oC_FileDescriptor_t FILE;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define fputs(STR , FP)         printf(STR)
#define fputc(C , FP)           FP
#define fprintf(FILE,...)       printf(__VA_ARGS__)

/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern int                      printf              ( const char * Format , ... );
extern int                      puts                ( const char * Str );
extern char *                   gets                ( char * Str );
extern int                      getc                ( FILE * Stream );
extern int                      scanf               ( const char * Format , ... );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* SYSTEM_LIBRARIES_STD_STDIO_H_ */
