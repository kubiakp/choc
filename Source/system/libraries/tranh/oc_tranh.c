/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_tranh.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 2 gru 2014 14:39:01) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_tranh.h>
#include <stdlib.h>
#include <oc_instr_defs.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static bool             _NewBufferHandler           ( oC_TRANH_BufferHandler_t * Buffer );
static void             _DeleteBufferHandler        ( oC_TRANH_BufferHandler_t * Buffer );
static bool             _IsSendAll                  ( oC_TRANH_t * Context );
static bool             _IsReceivedAll              ( oC_TRANH_t * Context );
static void             _FillTxFIFO                 ( oC_TRANH_t * Context , bool * TaskWoken );
static void             _EmptyRxFIFO                ( oC_TRANH_t * Context , bool * TaskWoken );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Creates new transmission handler.
 *
 * @param Config    Reference to the configuration structure
 *
 * @return Reference to the context structure, or NULL if cannot be allocated.
 */
//==========================================================================================================================================
oC_TRANH_t * oC_TRANH_New( const oC_TRANH_Config_t * Config )
{
    oC_TRANH_t * Context = malloc( sizeof(oC_TRANH_t) );

    if ( Context )
    {
        Context->Config     = Config;
        Context->Parameter  = NULL;
        Context->Self       = Context;

        if ( !_NewBufferHandler( &Context->Rx ) || !_NewBufferHandler( &Context->Tx ) )
        {
            oC_TRANH_Delete(&Context);;
        }
    }

    return Context;
}

//==========================================================================================================================================
/**
 * Delete transmission context
 *
 * @param Context   Reference to the context pointer
 */
//==========================================================================================================================================
void oC_TRANH_Delete( oC_TRANH_t ** Context )
{
    if ( Context && *Context )
    {
        _DeleteBufferHandler( &(*Context)->Rx );
        _DeleteBufferHandler( &(*Context)->Tx );
        free(*Context);
        *Context        = NULL;
    }
}

//==========================================================================================================================================
/**
 * Waits for send available, and lock it when it is possible.
 *
 * @param Context   Reference to the context structure
 * @param Timeout   Maximum time for lock (in ticks)
 *
 * @return false if time expired
 */
//==========================================================================================================================================
bool oC_TRANH_LockSend( oC_TRANH_t * Context , oC_TickType_t Timeout )
{
    bool result = false;

    if ( oC_TRANH_IsContextCorrect( Context ) && oC_Semaphore_Take( Context->Tx.Finished , Timeout ))
    {
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Unlocks send functionality.
 *
 * @param Context   Reference to the context structure
 */
//==========================================================================================================================================
void oC_TRANH_UnlockSend( oC_TRANH_t * Context )
{
    if ( oC_TRANH_IsContextCorrect( Context ) )
    {
        oC_Semaphore_Give( Context->Tx.Finished , NULL );
    }
}

//==========================================================================================================================================
/**
 * Waits as long, as send is in progress.
 *
 * @param Context   Reference to the context structure
 * @param Timeout   Maximum time for wait
 *
 * @return false if time expired
 */
//==========================================================================================================================================
bool oC_TRANH_WaitForSendFinished( oC_TRANH_t * Context , oC_TickType_t Timeout )
{
    bool result = false;

    if ( oC_TRANH_IsContextCorrect( Context ) && oC_Semaphore_Take( Context->Tx.Finished , Timeout ))
    {
        result = true;
        oC_Semaphore_Give( Context->Tx.Finished , NULL );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Waits for receive available, and lock it when it is possible.
 *
 * @param Context   Reference to the context structure
 * @param Timeout   Maximum time for wait
 *
 * @return false if time expired
 */
//==========================================================================================================================================
bool oC_TRANH_LockReceive( oC_TRANH_t * Context , oC_TickType_t Timeout )
{
    bool result = false;

    if ( oC_TRANH_IsContextCorrect( Context ) && oC_Semaphore_Take( Context->Rx.Finished , Timeout ))
    {
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Unlock receive functionality.
 *
 * @param Context   Reference to the context structure
 */
//==========================================================================================================================================
void oC_TRANH_UnlockReceive( oC_TRANH_t * Context )
{
    if ( oC_TRANH_IsContextCorrect( Context ) )
    {
        oC_Semaphore_Give( Context->Rx.Finished , NULL );
    }
}

//==========================================================================================================================================
/**
 * Waits as long, as receive is in progress.
 *
 * @param Context   Reference to the context structure
 * @param Timeout   Maximum time for wait
 *
 * @return false if time expired
 */
//==========================================================================================================================================
bool oC_TRANH_WaitForReceiveFinished( oC_TRANH_t * Context , oC_TickType_t Timeout )
{
    bool result = false;

    if ( oC_TRANH_IsContextCorrect( Context ) && oC_Semaphore_Take( Context->Rx.Finished , Timeout ))
    {
        result = true;
        oC_Semaphore_Give( Context->Rx.Finished , NULL );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Sends a buffer
 *
 * @param Context   Reference to the context structure
 * @param Buffer    Buffer to send
 * @param Size      Size of the Buffer
 *
 * @return false if context is not correct, or transmission handler is not ready to send
 */
//==========================================================================================================================================
bool oC_TRANH_Send( oC_TRANH_t * Context , uint8_t * Buffer , size_t Size )
{
    bool result = false;

    if ( oC_TRANH_IsContextCorrect(Context) && Context->Tx.Ready )
    {
        Context->Tx.Ready       = false;
        Context->Tx.Buffer      = Buffer;
        Context->Tx.Size        = Size;
        Context->Tx.Idx         = 0;

        _FillTxFIFO( Context , NULL );

        result              = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Receive a buffer
 *
 * @param Context   Reference to the context structure
 * @param Buffer    Buffer for storing received data
 * @param Size      Size of the Buffer
 *
 * @return false if context is not correct or transmission handler is not ready to receive
 */
//==========================================================================================================================================
bool oC_TRANH_Receive( oC_TRANH_t * Context , uint8_t * Buffer , size_t Size )
{
    bool result = false;

    if ( oC_TRANH_IsContextCorrect(Context) && Context->Rx.Ready )
    {
        Context->Rx.Ready       = false;
        Context->Rx.Buffer      = Buffer;
        Context->Rx.Size        = Size;
        Context->Rx.Idx         = 0;

        _EmptyRxFIFO( Context , NULL );

        result              = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Handle Tx FIFO. It takes data from buffer to send, and fill a Tx FIFO using it.
 *
 * @param Context   Reference to the context structure
 * @param TaskWoken Reference to the task woken flag, or NULL if not used.
 */
//==========================================================================================================================================
void oC_TRANH_HandleTx( oC_TRANH_t * Context , bool * TaskWoken )
{
    _FillTxFIFO( Context , TaskWoken );
}

//==========================================================================================================================================
/**
 * Handle Rx FIFO. It takes data from Rx FIFO, and fill a receive buffer using it.
 *
 * @param Context   Reference to the context structure
 * @param TaskWoken Reference to the task woken flag, or NULL if not used.
 */
//==========================================================================================================================================
void oC_TRANH_HandleRx( oC_TRANH_t * Context , bool * TaskWoken )
{
    _EmptyRxFIFO( Context , TaskWoken );
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Creates buffer handler and initializes it.
 *
 * @param Buffer    Reference to the buffer to create
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _NewBufferHandler( oC_TRANH_BufferHandler_t * Buffer )
{
    bool result = false;

    Buffer->Finished    = oC_Semaphore_New( oC_Semaphore_Type_Binary , 1 );

    if ( Buffer->Finished )
    {
        Buffer->Buffer      = NULL;
        Buffer->Idx         = 0;
        Buffer->Ready       = true;
        Buffer->Size        = 0;

        result              = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Deletes buffer handler
 *
 * @param Buffer    Reference to the buffer to remove
 */
//==========================================================================================================================================
static void _DeleteBufferHandler( oC_TRANH_BufferHandler_t * Buffer )
{
    if ( Buffer->Finished )
    {
        oC_Semaphore_Delete( Buffer->Finished );
    }
}

//==========================================================================================================================================
/**
 * Checks if is send all buffer
 *
 * @param Context   Reference to the context structure
 *
 * @return true if all
 */
//==========================================================================================================================================
static bool _IsSendAll( oC_TRANH_t * Context )
{
    return Context->Tx.Idx >= Context->Tx.Size;
}

//==========================================================================================================================================
/**
 * Checks if is received all buffer
 *
 * @param Context   Reference to the context structure
 *
 * @return true if all
 */
//==========================================================================================================================================
static bool _IsReceivedAll( oC_TRANH_t * Context )
{
    return Context->Rx.Idx >= Context->Rx.Size;
}

//==========================================================================================================================================
/**
 * Copy data from send buffer to Tx FIFO
 *
 * @param Context   Reference to the context structure
 * @param TaskWoken Reference to the task woken flag. NULL if not used
 */
//==========================================================================================================================================
static void _FillTxFIFO(oC_TRANH_t * Context , bool * TaskWoken )
{
    oC_ENTER_CRITICAL();
    size_t sentBytes = Context->Config->Send( Context->Parameter , &Context->Tx.Buffer[Context->Tx.Idx] , Context->Tx.Size - Context->Tx.Idx );
    Context->Tx.Idx += sentBytes;
    oC_EXIT_CRITICAL();

    if ( _IsSendAll( Context ) )
    {
        oC_Semaphore_Give( Context->Tx.Finished , TaskWoken );
        Context->Tx.Ready   = true;
    }
}

//==========================================================================================================================================
/**
 * Copy data from Rx FIFO to receive buffer
 *
 * @param Context   Reference to the context structure
 * @param TaskWoken Reference to the task woken flag. NULL if not used
 */
//==========================================================================================================================================
static void _EmptyRxFIFO(oC_TRANH_t * Context , bool * TaskWoken )
{
    oC_ENTER_CRITICAL();
    size_t receivedBytes = Context->Config->Receive( Context->Parameter , &Context->Rx.Buffer[Context->Rx.Idx] , Context->Rx.Size - Context->Rx.Idx );
    Context->Rx.Idx += receivedBytes;
    oC_EXIT_CRITICAL();

    if ( _IsReceivedAll( Context ) )
    {
        oC_Semaphore_Give( Context->Rx.Finished , TaskWoken );
        Context->Rx.Ready   = true;
    }
}
