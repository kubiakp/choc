/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_tranh.h
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 2 gru 2014 13:35:51) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_TRANH_OC_TRANH_H_
#define SYSTEM_LIBRARIES_TRANH_OC_TRANH_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_os.h>
#include <oc_synch.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

typedef int             (*oC_TRANH_Send_t)      ( const void * Context , void * Buffer , size_t Size );
typedef int             (*oC_TRANH_Receive_t)   ( const void * Context , void * Buffer , size_t Size );

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct
{
    bool                        Ready;
    uint8_t*                    Buffer;
    size_t                      Size;
    size_t                      Idx;
    oC_Semaphore_t*             Finished;
} oC_TRANH_BufferHandler_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct
{
    oC_TRANH_Send_t             Send;
    oC_TRANH_Receive_t          Receive;
} oC_TRANH_Config_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct
{
    void *                      Self;
    const oC_TRANH_Config_t *   Config;
    oC_TRANH_BufferHandler_t    Rx;
    oC_TRANH_BufferHandler_t    Tx;
    const void *                Parameter;
} oC_TRANH_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern oC_TRANH_t *     oC_TRANH_New                    ( const oC_TRANH_Config_t * Config );
extern void             oC_TRANH_Delete                 ( oC_TRANH_t ** Context );
extern bool             oC_TRANH_LockSend               ( oC_TRANH_t * Context , oC_TickType_t Timeout );
extern void             oC_TRANH_UnlockSend             ( oC_TRANH_t * Context );
extern bool             oC_TRANH_WaitForSendFinished    ( oC_TRANH_t * Context , oC_TickType_t Timeout );
extern bool             oC_TRANH_LockReceive           ( oC_TRANH_t * Context , oC_TickType_t Timeout );
extern void             oC_TRANH_UnlockReceive          ( oC_TRANH_t * Context );
extern bool             oC_TRANH_WaitForReceiveFinished ( oC_TRANH_t * Context , oC_TickType_t Timeout );
extern bool             oC_TRANH_Send                   ( oC_TRANH_t * Context , uint8_t * Buffer , size_t Size );
extern bool             oC_TRANH_Receive                ( oC_TRANH_t * Context , uint8_t * Buffer , size_t Size );
extern void             oC_TRANH_HandleTx               ( oC_TRANH_t * Context , bool * TaskWoken );
extern void             oC_TRANH_HandleRx               ( oC_TRANH_t * Context , bool * TaskWoken );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Checks if context is correctly initialized
 *
 * @param Context       Reference to the context to check
 *
 * @return true if correct
 */
//==========================================================================================================================================
static inline bool oC_TRANH_IsContextCorrect( oC_TRANH_t * Context )
{
    return Context && Context->Self == Context;
}

//==========================================================================================================================================
/**
 * Sets parameter for the transmission handler. The parameter will be used for each function from config structure.
 *
 * @param Context       Reference to the context
 * @param Parameter     Parameter to set
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool oC_TRANH_SetParameter( oC_TRANH_t * Context , const void * Parameter )
{
    bool success = false;

    if ( oC_TRANH_IsContextCorrect(Context) )
    {
        Context->Parameter  = Parameter;
        success = true;
    }

    return success;
}

#endif /* SYSTEM_LIBRARIES_TRANH_OC_TRANH_H_ */
