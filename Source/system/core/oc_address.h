/** ****************************************************************************************************************************************
 *
 * @file       oc_address.h
 *
 * @brief      The file with functions that allows to checking addresses
 *
 * @author     Patryk Kubiak - (Created on: 2 kwi 2015 18:35:50) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_ADDRESS_H_
#define SYSTEM_CORE_OC_ADDRESS_H_

/** ****************************************************************************************************************************************
 * The section with external globals definitions
 */
#define _________________________________________GLOBAL_DEFINITIONS_SECTION_________________________________________________________________

extern const char __rom_start;
extern const char __rom_end;
extern const char __ram_start;
extern const char __ram_end;
extern const char __heap_start;
extern const char __heap_end;
extern const char __data_start;
extern const char __data_end;
extern const char __stack_start;
extern const char __stack_end;

/* END OF SECTION */
#undef  _________________________________________GLOBAL_DEFINITIONS_SECTION_________________________________________________________________

/** ****************************************************************************************************************************************
 * The section static inline functions
 */
#define _________________________________________STATIC_INLINE_FUNCTIONS_SECTION____________________________________________________________

static inline bool oC_Address_IsRom( const void * Address )
{
    void * romStart = (void*) &__rom_start;
    void * romEnd   = (void*) &__rom_end;
    return (Address > romStart) && (Address < romEnd);
}

static inline bool oC_Address_IsRam( const void * Address )
{
    void * ramStart = (void*) &__ram_start;
    void * ramEnd   = (void*) &__ram_end;
    return (Address > ramStart) && (Address < ramEnd);
}

static inline bool oC_Address_IsHeap( const void * Address )
{
    void * heapStart = (void*) &__heap_start;
    void * heapEnd   = (void*) &__heap_end;
    return (Address > heapStart) && (Address < heapEnd);
}

static inline bool oC_Address_IsData( const void * Address )
{
    void * dataStart = (void*) &__data_start;
    void * dataEnd   = (void*) &__data_end;
    return (Address > dataStart) && (Address < dataEnd);
}

static inline bool oC_Address_IsStack( const void * Address )
{
    void * stackStart = (void*) &__stack_start;
    void * stackEnd   = (void*) &__stack_end;
    return (Address > stackStart) && (Address < stackEnd);
}

static inline bool oC_Address_IsDynamicAllocated( const void * Address )
{
    return oC_Address_IsHeap(Address);
}

static inline bool oC_Address_IsFlash( const void * Address )
{
    return oC_Address_IsRom(Address);
}

static inline bool oC_Address_IsCorrect( const void * Address )
{
    return oC_Address_IsRom(Address) | oC_Address_IsRam(Address);
}

/* END OF SECTION */
#undef  _________________________________________STATIC_INLINE_FUNCTIONS_SECTION____________________________________________________________


#endif /* SYSTEM_CORE_OC_ADDRESS_H_ */
