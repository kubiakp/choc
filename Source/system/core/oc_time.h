/** ****************************************************************************************************************************************
 *
 * @file       oc_time.h
 *
 * @brief      The file with interface for handling all time operations
 *
 * @author     Patryk Kubiak - (Created on: 21 mar 2015 12:31:00) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Time Library Time - module for managing operations that handle time.
 *
 * @{
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_OC_TIME_H_
#define SYSTEM_CORE_OC_TIME_H_

#include <stdint.h>
#include <oc_errors.h>

/** ****************************************************************************************************************************************
 * The section with interface types
 */
#define _________________________________________INTERFACE_TYPES_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores the time in basic units (microseconds)
 *
 * The type is for storing stamp of the time. It is number of microseconds, that have elapsed since date 01.01.2015. You can use this type
 * when you want to quickly save a current time value. <br />
 * <br />
 * To get the time stamp, you should use #oC_Time_GetTimeStamp function, and #oC_Time_SetTimeStamp if you want to restore
 * time from the time stamp.<br />
 *
 * Time stamp's can be converted to a time structure by using functions #oC_Time_TimeStampToTime and #oC_Time_TimeStampToTimeExtended<br />
 * You also can calculate a time stamp from time structure by using functions #oC_Time_TimeToTimeStamp and #oC_Time_TimeExtendedToTimeStamp. <br />
 * <br />
 * You can use functions #oC_Time_TimeStampTo_hours and #oC_Time_HoursToTimeStamp (available also for other time units) to <br />
 * convert between time and time units.
 */
//==========================================================================================================================================
typedef uint64_t oC_Time_Stamp_t;

//==========================================================================================================================================
/**
 * @brief stores the time
 *
 * The type is for storing current time in most populate units. It cannot be greater than 24h. If you want to know the time in <br />
 * with more precision (with microseconds and milliseconds) you should use #oC_Time_Extended_t type time.<br />
 * <br />
 * If you want to know what time is it, use #oC_Time_GetCurrentTime function, and the #oC_Time_SetCurrentTime for current time <br />
 * configuration.<br />
 * <br />
 * You can check if time stored in the type is correct by function #oC_Time_IsTimeCorrect<br />
 *
 * @note The type is for configuration of the real time clock. You cannot use it to set time stamp, or the time greater than 24 hours!
 */
//==========================================================================================================================================
typedef struct
{
    uint8_t         h;      /**< Hours ( only values 0 - 23 ) */
    uint8_t         min;    /**< Minutes ( only values 0 - 59 ) */
    uint8_t         sec;    /**< seconds ( only values 0 - 59 ) */
} oC_Time_t;

//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
typedef struct
{
    uint8_t         h;
    uint8_t         min;
    uint8_t         sec;
    uint16_t        msec;
    uint16_t        usec;
} oC_Time_Extended_t;

/** @} */
/* END OF SECTION */
#undef  _________________________________________INTERFACE_TYPES_SECTION____________________________________________________________________


/** ****************************************************************************************************************************************
 * The section interface functions prototypes
 */
#define _________________________________________INTERFACE_FUNCTIONS_PROTOTYPES_SECTION_____________________________________________________

extern oC_ErrorCode_t       oC_Time_TurnOnModule                ( void );
extern oC_ErrorCode_t       oC_Time_TurnOffModule               ( void );

extern void                 oC_Time_DelayFor_us                 ( uint32_t Microseconds );
extern void                 oC_Time_DelayFor_ms                 ( uint32_t Milliseconds );
extern void                 oC_Time_DelayFor_s                  ( uint32_t Seconds );
extern void                 oC_Time_DelayFor_min                ( uint32_t Minutes );
extern void                 oC_Time_DelayFor_hour               ( uint32_t Hours );

extern oC_Time_Stamp_t      oC_Time_GetTimeStamp                ( void );
extern bool                 oC_Time_SetTimeStamp                ( oC_Time_Stamp_t TimeStamp );
extern oC_Time_t            oC_Time_GetCurrentTime              ( void );
extern oC_Time_Extended_t   oC_Time_GetCurrentTimeExtended      ( void );
extern bool                 oC_Time_SetCurrentTime              ( oC_Time_t CurrentTime );
extern bool                 oC_Time_SetCurrentTimeExtended      ( oC_Time_Extended_t CurrentTime );

extern void                 oC_Time_SleepFor_msec               ( uint32_t Milliseconds );
extern void                 oC_Time_SleepFor_sec                ( uint32_t Seconds );
extern void                 oC_Time_SleepFor_min                ( uint32_t Minutes );
extern void                 oC_Time_SleepFor_hour               ( uint32_t Hours );
extern void                 oC_Time_SleepForTime                ( oC_Time_t TimeToSleep );

extern bool                 oC_Time_WakeUpAtTime                ( oC_Time_t WakeUpTime );

/* END OF SECTION */
#undef  _________________________________________INTERFACE_FUNCTIONS_PROTOTYPES_SECTION_____________________________________________________

/** ****************************************************************************************************************************************
 * The section with inline functions
 *
 * @addtogroup Time
 * @{
 */
#define _________________________________________INLINE_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the time structure stores correct value
 *
 * @param Time  structure with the time to check
 *
 * @return true if the time is correct
 */
//==========================================================================================================================================
static inline bool
oC_Time_IsTimeCorrect(
                       IN   oC_Time_t Time
                       )
{
    return Time.h < 24 && Time.min < 60 && Time.sec < 60;
}
//==========================================================================================================================================
/**
 * @brief checks if the extended time structure stores correct time vaue
 *
 * @param TimeExtended  extended time structure to check
 *
 * @return true if the time is correct
 */
//==========================================================================================================================================
static inline bool
oC_Time_IsTimeExtendedCorrect(
                               IN   oC_Time_Extended_t TimeExtended
                               )
{
    return TimeExtended.h < 24 && TimeExtended.min < 60 && TimeExtended.sec < 60 && TimeExtended.msec < 1000 && TimeExtended.usec < 1000;
}

//==========================================================================================================================================
/**
 * @brief Converts time stamp to microseconds
 *
 * @param TimeStamp     stamp of the time
 *
 * @return time stamp in microseconds
 */
//==========================================================================================================================================
static inline uint64_t oC_Time_TimeStampTo_us ( oC_Time_Stamp_t TimeStamp )
{
    return (uint64_t)TimeStamp;
}

//==========================================================================================================================================
/**
 * @brief converts a microseconds to a time stamp
 *
 * @param us        Number of microseconds to convert to time stamp
 *
 * @return stamp of the time in microseconds
 */
//==========================================================================================================================================
static inline oC_Time_Stamp_t oC_Time_usToTimeStamp( uint64_t us )
{
    return (oC_Time_Stamp_t)us;
}

//==========================================================================================================================================
/**
 * @brief Converts time stamp to milliseconds
 *
 * @param TimeStamp     stamp of the time
 *
 * @return time stamp in milliseconds
 */
//==========================================================================================================================================
static inline uint64_t oC_Time_TimeStampTo_ms ( oC_Time_Stamp_t TimeStamp )
{
    return (uint64_t)(oC_Time_TimeStampTo_us(TimeStamp) / 1000);
}

//==========================================================================================================================================
/**
 * @brief converts a milliseconds to a time stamp
 *
 * @param ms        Number of milliseconds to convert to time stamp
 *
 * @return stamp of the time
 */
//==========================================================================================================================================
static inline oC_Time_Stamp_t oC_Time_msToTimeStamp( uint64_t ms )
{
    return (oC_Time_Stamp_t)ms * oC_Time_usToTimeStamp(1000);
}

//==========================================================================================================================================
/**
 * @brief Converts time stamp to seconds
 *
 * @param TimeStamp     stamp of the time
 *
 * @return time stamp in seconds
 */
//==========================================================================================================================================
static inline uint64_t oC_Time_TimeStampTo_s ( oC_Time_Stamp_t TimeStamp )
{
    return (uint64_t)oC_Time_TimeStampTo_ms(TimeStamp) / (uint64_t)1000;
}

//==========================================================================================================================================
/**
 * @brief converts the time in seconds to a time stamp
 *
 * @param s        Number of seconds to convert to time stamp
 *
 * @return stamp of the time
 */
//==========================================================================================================================================
static inline oC_Time_Stamp_t oC_Time_SecToTimeStamp( uint64_t s )
{
    return (oC_Time_Stamp_t)s * oC_Time_msToTimeStamp(1000);
}

//==========================================================================================================================================
/**
 * @brief Converts time stamp to minutes
 *
 * @param TimeStamp     stamp of the time
 *
 * @return time stamp in minutes
 */
//==========================================================================================================================================
static inline uint64_t oC_Time_TimeStampTo_min ( oC_Time_Stamp_t TimeStamp )
{
    return (uint64_t)oC_Time_TimeStampTo_s(TimeStamp) / (uint64_t)60;
}

//==========================================================================================================================================
/**
 * @brief converts the time in minutes to a time stamp
 *
 * @param min       Number of minutes to convert to time stamp
 *
 * @return stamp of the time
 */
//==========================================================================================================================================
static inline oC_Time_Stamp_t oC_Time_MinToTimeStamp( uint64_t min )
{
    return (oC_Time_Stamp_t)min * oC_Time_SecToTimeStamp(60);
}

//==========================================================================================================================================
/**
 * @brief Converts time stamp to hours
 *
 * @param TimeStamp     stamp of the time
 *
 * @return time stamp in hours
 */
//==========================================================================================================================================
static inline uint64_t oC_Time_TimeStampTo_hours ( oC_Time_Stamp_t TimeStamp )
{
    return (uint64_t)oC_Time_TimeStampTo_min(TimeStamp) / (uint64_t)60;
}

//==========================================================================================================================================
/**
 * @brief converts the time in hours to a time stamp
 *
 * @param hours      Number of hours to convert to time stamp
 *
 * @return stamp of the time
 */
//==========================================================================================================================================
static inline oC_Time_Stamp_t oC_Time_HoursToTimeStamp( uint64_t hours )
{
    return (oC_Time_Stamp_t)hours * oC_Time_MinToTimeStamp(60);
}

//==========================================================================================================================================
/**
 * @brief Converts the time stamp to time structure.
 *
 * @note The time structure can store only 24 hours, so this function will return time in this format. If you want to receive
 * full time of the time stamp, you should use functions #oC_Time_TimeStampTo_hours
 *
 * @param TimeStamp     Stamp of the time to convert
 *
 * @return time of the time stamp (lower than 24 h)
 */
//==========================================================================================================================================
static inline oC_Time_t oC_Time_TimeStampToTime( oC_Time_Stamp_t TimeStamp )
{
    oC_Time_t time;

    time.sec    = oC_Time_TimeStampTo_s(  TimeStamp) % 60;
    time.min    = oC_Time_TimeStampTo_min(TimeStamp) % 60;
    time.h      = oC_Time_TimeStampTo_hours(  TimeStamp) % 24;

    return time;
}

//==========================================================================================================================================
/**
 * @brief Converts the time stamp to the time extended structure
 *
 * @note The time structure can store only 24 hours, so this function will return time in this format. If you want to receive
 * full time of the time stamp, you should use functions #oC_Time_TimeStampTo_hours
 *
 * @param TimeStamp     Stamp of the time to convert
 *
 * @return time of the time stamp (lower than 24 h)
 */
//==========================================================================================================================================
static inline oC_Time_Extended_t oC_Time_TimeStampToTimeExtended( oC_Time_Stamp_t TimeStamp )
{
    oC_Time_Extended_t timeExtended;

    timeExtended.h      = oC_Time_TimeStampTo_hours(TimeStamp) % 24;
    timeExtended.min    = oC_Time_TimeStampTo_min(TimeStamp)   % 60;
    timeExtended.sec    = oC_Time_TimeStampTo_s(TimeStamp)   % 60;
    timeExtended.msec   = oC_Time_TimeStampTo_ms(TimeStamp)    % 1000;
    timeExtended.usec   = oC_Time_TimeStampTo_us(TimeStamp)    % 1000;

    return timeExtended;
}

//==========================================================================================================================================
/**
 * @brief Converts a time to the time stamp
 *
 * @note it not checks if the time structure is correct
 *
 * @param Time      The time structure
 *
 * @return time stamp
 */
//==========================================================================================================================================
static inline oC_Time_Stamp_t oC_Time_TimeToTimeStamp( oC_Time_t Time )
{
    return oC_Time_HoursToTimeStamp( Time.h ) + oC_Time_MinToTimeStamp(Time.min) + oC_Time_SecToTimeStamp(Time.sec);
}

//==========================================================================================================================================
/**
 * @brief Converts a time to the time stamp
 *
 * @note it not checks if the time structure is correct
 *
 * @param Time      The time structure
 *
 * @return time stamp
 */
//==========================================================================================================================================
static inline oC_Time_Stamp_t oC_Time_TimeExtendedToTimeStamp( oC_Time_Extended_t ExtendedTime )
{
    return
            oC_Time_HoursToTimeStamp(ExtendedTime.h) +
            oC_Time_MinToTimeStamp(ExtendedTime.min) +
            oC_Time_SecToTimeStamp(ExtendedTime.sec) +
            oC_Time_msToTimeStamp(ExtendedTime.msec) +
            oC_Time_usToTimeStamp(ExtendedTime.usec);
}

/** @} */
/* END OF SECTION */
#undef  _________________________________________INLINE_SECTION_____________________________________________________________________________


#endif /* SYSTEM_CORE_OC_TIME_H_ */
