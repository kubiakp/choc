/** ****************************************************************************************************************************************
 *
 * @file       oc_boot.h
 *
 * @brief      Header file with boot module interface
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 18:04:28) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_BOOT_H_
#define SYSTEM_CORE_OC_BOOT_H_

/** ****************************************************************************************************************************************
 * The section interface types
 */
#define _________________________________________INTERFACE_TYPES_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * The type for storing levels of the boot.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Boot_LevelNotAssigned,
    oC_Boot_Level0 ,
    oC_Boot_Level1 ,
    oC_Boot_Level2 ,
    oC_Boot_Level3 ,
    oC_Boot_Level4 ,
    oC_Boot_Level5 ,
    oC_Boot_LevelNotStarted ,
    oC_Boot_Level_DontRequireAnything               = oC_Boot_Level0 ,
    oC_Boot_Level_RequireMalloc                     = oC_Boot_Level1 ,
    oC_Boot_Level_RequireDriversInitializations     = oC_Boot_Level2 ,
    oC_Boot_Level_RequireOS                         = oC_Boot_Level3 ,
    oC_Boot_Level_RequireTimeModule                 = oC_Boot_Level3 ,
    oC_Boot_Level_RequireStdio                      = oC_Boot_Level4 ,
    oC_Boot_Level_RequireAnything                   = oC_Boot_Level5
} oC_Boot_Level_t;

//==========================================================================================================================================
/**
 * @brief       Prototype for boot loading function
 *
 * The function type is for storing pointers to the functions, that should be executed on the selected boot level.
 *
 * @param Level     Current level of booting
 */
//==========================================================================================================================================
typedef void (*oC_Boot_Function_t) ( oC_Boot_Level_t Level );

typedef struct
{
    oC_Boot_Level_t     Level;
    oC_Boot_Function_t  Function;
} oC_Boot_t;

/* END OF SECTION */
#undef  _________________________________________INTERFACE_TYPES_SECTION____________________________________________________________________


/** ****************************************************************************************************************************************
 * The section function prototypes
 */
#define _________________________________________FUNCTION_PROTOTYPES_SECTION________________________________________________________________

extern oC_Boot_Level_t  oC_Boot_CurrentLevel    ( void );

/* END OF SECTION */
#undef  _________________________________________FUNCTION_PROTOTYPES_SECTION________________________________________________________________


#endif /* SYSTEM_CORE_OC_BOOT_H_ */
