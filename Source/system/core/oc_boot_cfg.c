/** ****************************************************************************************************************************************
 *
 * @file          oc_boot_cfg.c
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 21:55:52) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_boot.h>
#include <oc_array.h>
#include <oc_drivers.h>

const oC_Boot_t oC_Boot_List[]  = {
                                   {
                                        .Level      = oC_Boot_Level0 ,
                                        .Function   = oC_Drivers_TurnOnDriversFromLevel
                                   } ,
                                   {
                                        .Level      = oC_Boot_Level1 ,
                                        .Function   = oC_Drivers_TurnOnDriversFromLevel
                                   } ,
                                   {
                                        .Level      = oC_Boot_Level2 ,
                                        .Function   = oC_Drivers_TurnOnDriversFromLevel
                                   } ,
                                   {
                                        .Level      = oC_Boot_Level0 ,
                                        .Function   = oC_Drivers_LoadDefaultConfigurations
                                   } ,
                                   {
                                        .Level      = oC_Boot_Level1 ,
                                        .Function   = oC_Drivers_LoadDefaultConfigurations
                                   } ,
                                   {
                                        .Level      = oC_Boot_Level2 ,
                                        .Function   = oC_Drivers_LoadDefaultConfigurations
                                   } ,
                                   {
                                        .Level      = oC_Boot_Level3 ,
                                        .Function   = oC_Drivers_LoadDefaultConfigurations
                                   } ,
                                   {
                                        .Level      = oC_Boot_Level4 ,
                                        .Function   = oC_Drivers_LoadDefaultConfigurations
                                   } ,
                                   {
                                        .Level      = oC_Boot_Level5 ,
                                        .Function   = oC_Drivers_LoadDefaultConfigurations
                                   } ,
};

const unsigned int oC_Boot_ListSize = oC_ARRAY_SIZE( oC_Boot_List );
