/** ****************************************************************************************************************************************
 *
 * @file          oc_boot.c
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 18:48:35) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_boot.h>
#include <oc_debug.h>
#include <oc_os.h>
#include <oc_address.h>
#include <oc_memory.h>
#include <stdio.h>

/** ****************************************************************************************************************************************
 * The section with local functions prototypes
 */
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void BootThread( void * p );

/* END OF SECTION */
#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ****************************************************************************************************************************************
 * The section external variables section
 */
#define _________________________________________EXTERNAL_VARIABLES_SECTION_________________________________________________________________

extern const oC_Boot_t      oC_Boot_List[];
extern const unsigned int   oC_Boot_ListSize;

/* END OF SECTION */
#undef  _________________________________________EXTERNAL_VARIABLES_SECTION_________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with local variables
 */
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

static oC_Boot_Level_t CurrentLevel = oC_Boot_LevelNotStarted;

/* END OF SECTION */
#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with main booting levels functions
 */
#define _________________________________________BOOTING_FUNCTIONS_SECTION__________________________________________________________________

//==========================================================================================================================================
/**
 * The function runs all functions from list, that are assigned to the current level
 *
 * @param Level     Current level of booting
 */
//==========================================================================================================================================
static void RunFunctionsFromLevel( oC_Boot_Level_t Level )
{
    CurrentLevel    = Level;

    printf("Running functions from %d level...\n" , Level);

    for(unsigned int i = 0 ; i < oC_Boot_ListSize ; i++)
    {
        if ( oC_Boot_List[i].Level == Level && oC_Address_IsCorrect(oC_Boot_List[i].Function) )
        {
            oC_Boot_List[i].Function(Level);
        }
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
void oC_Boot_Manager( void )
{
    oC_Memory_Init();

    RunFunctionsFromLevel(oC_Boot_Level0);

    oC_OS_Initialize();

    RunFunctionsFromLevel(oC_Boot_Level1);
    RunFunctionsFromLevel(oC_Boot_Level2);

    oC_OS_PID_t pid = oC_OS_Process_Create( oC_OS_Priority_Maximum );
    oC_OS_Thread_Create( pid , "boot" , BootThread , NULL , 512 , oC_OS_Priority_Parent , oC_OS_DefaultTTY() );

    oC_OS_StartScheduler();
}


/* END OF SECTION */
#undef  _________________________________________BOOTING_FUNCTIONS_SECTION__________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with local functions
 */
#define _________________________________________LOCAL_FUNCTION_SECTION_____________________________________________________________________

//==========================================================================================================================================
/**
 * Thread that initialize boot levels
 *
 * @param p         Not used parameter
 */
//==========================================================================================================================================
static void BootThread( void * p )
{
    (void)p;
    oC_OS_Terminal_ResetDevice();

    oC_PrintResult(oC_Time_TurnOnModule() , "Turning on time module: ");

    RunFunctionsFromLevel(oC_Boot_Level3);
    RunFunctionsFromLevel(oC_Boot_Level4);
    RunFunctionsFromLevel(oC_Boot_Level5);

    oC_OS_RegisterPrograms();

    printf("Run default program named %s...\n" , oC_OS_Program_GetName(oC_DEFAULT_PROGRAM));
    oC_OS_Program_ExecInNewProcess(
                                   oC_DEFAULT_PROGRAM ,
                                   0 ,
                                   0 ,
                                   0 ,
                                   NULL ,
                                   oC_DEFAULT_PRIORITY
                                   );

    while(1)
    {
        oC_Time_SleepFor_hour(12);
    }
}

/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTION_SECTION_____________________________________________________________________
