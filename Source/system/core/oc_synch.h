/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_synch.h
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 23 gru 2014 14:33:56) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_SYNCH_H_
#define SYSTEM_CORE_OC_SYNCH_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <stdbool.h>
#include <FreeRTOS.h>
#include <semphr.h>
#include <oc_dlist.h>
#include <oc_errors.h>
#include <oc_gen_types.h>
#include <oc_instr_defs.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Type of the semaphore
 */
//==========================================================================================================================================
typedef enum
{
    oC_Semaphore_Type_Binary = 1 ,  //!< Binary semaphore can be only in 2 states - free or taken
    oC_Semaphore_Type_Counting      //!< Counting semaphore can be taken so many times, as its maximum value
} oC_Semaphore_Type_t;

//==========================================================================================================================================
/**
 * Object of the semaphore
 */
//==========================================================================================================================================
typedef struct
{
    oC_Semaphore_Type_t     Type;           //!< Type of the semaphore
    SemaphoreHandle_t       Semaphore;      //!< Handle for FreeRTOS
    uint32_t                Counter;        //!< Current counter of semaphores
}oC_Semaphore_t;

typedef enum
{
    oC_Mutex_Type_Normal ,
    oC_Mutex_Type_Recursive
} oC_Mutex_Type_t;

typedef struct
{
    oC_Mutex_Type_t         Type;
    SemaphoreHandle_t       Mutex;
} oC_Mutex_t;

typedef void (*oC_Signal_Slot_t)( void * Parameters );

typedef struct
{
    oC_Semaphore_t*         Semaphore;          /**< Semaphore for wait for signal function */
    oC_DLIST_t*             Slots;              /**< List of slots connected to the signal */
    const void *            DefaultParameter;
} oC_Signal_t;

typedef struct
{
    oC_Semaphore_t*         SemaphoreActive;
    oC_Semaphore_t*         SemaphoreInactive;
    bool                    Active;
} oC_Event_t;

typedef struct
{
    oC_Event_t*             DataAvailable;
    oC_Event_t*             Full;
    uint8_t*                Data;
    uint32_t                GetIndex;
    uint32_t                PutIndex;
    uint32_t                NumberOfElements;
    uint32_t                Length;
    size_t                  ElementSize;
} oC_Queue_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern oC_Semaphore_t*  oC_Semaphore_New                ( oC_Semaphore_Type_t Type , uint32_t InitialValue );
extern bool             oC_Semaphore_Delete             ( oC_Semaphore_t * Semaphore );
extern bool             oC_Semaphore_Give               ( oC_Semaphore_t * Semaphore , bool * outTaskWoken );
extern bool             oC_Semaphore_Take               ( oC_Semaphore_t * Semaphore , oC_TickType_t Timeout );
extern oC_Mutex_t*      oC_Mutex_New                    ( oC_Mutex_Type_t Type );
extern bool             oC_Mutex_Delete                 ( oC_Mutex_t * Mutex );
extern bool             oC_Mutex_Give                   ( oC_Mutex_t * Mutex );
extern bool             oC_Mutex_Take                   ( oC_Mutex_t * Mutex , oC_TickType_t Timeout );
extern oC_Signal_t *    oC_Signal_New                   ( const void * DefaultParameter );
extern bool             oC_Signal_Delete                ( oC_Signal_t * Signal );
extern void             oC_Signal_SetDefaultParameter   ( oC_Signal_t * Signal , const void * Parameter );
extern bool             oC_Signal_EmitSignal            ( oC_Signal_t * Signal , bool * outTaskWoken);
extern bool             oC_Signal_WaitForSignal         ( oC_Signal_t * Signal , oC_TickType_t Timeout );
extern void             oC_Signal_Connect               ( oC_Signal_t * Signal , oC_Signal_Slot_t Slot , const void * Parameter );
extern void             oC_Signal_Disconnect            ( oC_Signal_t * Signal , oC_Signal_Slot_t Slot );
extern oC_Event_t*      oC_Event_New                    ( void );
extern bool             oC_Event_Delete                 ( oC_Event_t * Event );
extern bool             oC_Event_SetActive              ( oC_Event_t * Event );
extern bool             oC_Event_SetInactive            ( oC_Event_t * Event );
extern bool             oC_Event_IsActive               ( oC_Event_t * Event );
extern bool             oC_Event_WaitForEventActive     ( oC_Event_t * Event , oC_TickType_t Timeout );
extern bool             oC_Event_WaitForEventInactive   ( oC_Event_t * Event , oC_TickType_t Timeout );
extern oC_Queue_t*      oC_Queue_New                    ( uint32_t Length , size_t ItemSize );
extern bool             oC_Queue_Delete                 ( oC_Queue_t * Queue );
extern bool             oC_Queue_GetData                ( oC_Queue_t * Queue , oC_TickType_t Timeout , void * outData );
extern bool             oC_Queue_PutData                ( oC_Queue_t * Queue , oC_TickType_t Timeout , const void * Data );
extern bool             oC_Queue_IsFull                 ( oC_Queue_t * Queue );
extern bool             oC_Queue_IsEmpty                ( oC_Queue_t * Queue );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Begins critical section
 */
//==========================================================================================================================================
static inline void oC_EnterCritical( void )
{
    oC_ENTER_CRITICAL();
}

//==========================================================================================================================================
/**
 * Finishes critical section
 */
//==========================================================================================================================================
static inline void oC_ExitCritical( void )
{
    oC_EXIT_CRITICAL();
}

#endif /* SYSTEM_CORE_OC_SYNCH_H_ */
