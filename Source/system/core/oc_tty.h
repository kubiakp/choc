/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_tty.h
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 15 sty 2015 15:49:56) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_TTY_H_
#define SYSTEM_CORE_OC_TTY_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_errors.h>
#include <oc_string.h>
#include <oc_dlist.h>
#include <oc_drivers.h>
#include <oc_terminals.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Type of a function to transmit data (send/receive)
 *
 * @param Parameter         Additional parameter
 * @param Data              Data buffer
 * @param Length            length of the data buffer
 * @param Timeout           Maximum time for operation
 *
 * @return number of sent/received bytes
 */
//==========================================================================================================================================
typedef int (*oC_TTY_TransmitFunction_t)( const void * Parameter , char * Data , uint32_t Length , uint32_t Timeout );

//==========================================================================================================================================
/**
 * Structure with configuration of the stream
 */
//==========================================================================================================================================
typedef struct
{
    const oC_Driver_t *                 Driver;    /**< Reference to an interface of a driver */
    const void *                        Parameter;          /**< Additional parameter given to the Function */
    const oC_Terminal_IfCfg_t *         TerminalIf;         /**< Reference to the interface configuration for the stream */
    uint32_t                            Timeout;            /**< Maximum time for the operation given to the Function as Timeout parameter */
} oC_TTY_StreamConfig_t;

//==========================================================================================================================================
/**
 * Structure with configurations of the streams
 */
//==========================================================================================================================================
typedef struct
{
    oC_TTY_StreamConfig_t               StandardOutput;     /**< Configuration of the standard output stream */
    oC_TTY_StreamConfig_t               StandardInput;      /**< Configuration of the standard input stream */
    oC_TTY_StreamConfig_t               StandardError;      /**< Configuration of the standard error stream */
} oC_TTY_Config_t;

//==========================================================================================================================================
/**
 * Object of the TTY
 */
//==========================================================================================================================================
typedef struct
{
    void *                              Self;               /**< Reference to this object for verification */
    const oC_TTY_Config_t*              Config;             /**< Reference to the configuration structure */
    oC_DLIST_t *                        Lines;              /**< List of the oC_STRING_t pointers */
    bool                                StdOutActive;       /**< Flag if standard output stream is active */
    void *                              Mutex;              /**< Reference to the mutex object */
} oC_TTY_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern oC_TTY_t *                   oC_TTY_New                                    ( const oC_TTY_Config_t * Config , oC_ErrorCode_t * errorCode );
extern oC_TTY_t *                   oC_TTY_Clone                                  ( oC_TTY_t * TTY , oC_ErrorCode_t * outErrorCode );
extern void                         oC_TTY_Delete                                 ( oC_TTY_t * TTY );
extern bool                         oC_TTY_IsCorrect                              ( oC_TTY_t * TTY );
extern bool                         oC_TTY_IsConfigCorrect                        ( const oC_TTY_Config_t * Config );
extern int                          oC_TTY_StandardOutput                         ( oC_TTY_t * TTY , const char * Str , uint32_t Length );
extern int                          oC_TTY_StandardInput                          ( oC_TTY_t * TTY ,       char * Str , uint32_t Length );
extern int                          oC_TTY_StandardError                          ( oC_TTY_t * TTY , const char * Str , uint32_t Length );
extern void                         oC_TTY_Refresh                                ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetActive                              ( oC_TTY_t * TTY , bool Active );
extern bool                         oC_TTY_IsActive                               ( oC_TTY_t * TTY );
extern bool                         oC_TTY_ResetDevice                            ( oC_TTY_t * TTY );
extern bool                         oC_TTY_ClearScreen                            ( oC_TTY_t * TTY );
extern oC_Terminal_CursorPos_t      oC_TTY_GetCursorPosition                      ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EnableLineWrap                         ( oC_TTY_t * TTY );
extern bool                         oC_TTY_DisableLineWrap                        ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetCursorHome                          ( oC_TTY_t * TTY , oC_Terminal_CursorPos_t Position );
extern bool                         oC_TTY_CursorHome                             ( oC_TTY_t * TTY );
extern bool                         oC_TTY_CursorUp                               ( oC_TTY_t * TTY , int Count );
extern bool                         oC_TTY_CursorDown                             ( oC_TTY_t * TTY , int Count );
extern bool                         oC_TTY_CursorForward                          ( oC_TTY_t * TTY , int Count );
extern bool                         oC_TTY_CursorBackward                         ( oC_TTY_t * TTY , int Count );
extern bool                         oC_TTY_ForceCursorPosition                    ( oC_TTY_t * TTY , oC_Terminal_CursorPos_t Position );
extern bool                         oC_TTY_SaveCursorPosition                     ( oC_TTY_t * TTY );
extern bool                         oC_TTY_RestoreCursorPosition                  ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SaveCursorPositionAndAttrs             ( oC_TTY_t * TTY );
extern bool                         oC_TTY_RestoreCursorPositionAndAttrs          ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EnableScrollScreen                     ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EnableScrollScreenTo                   ( oC_TTY_t * TTY , int startRow , int endRow );
extern bool                         oC_TTY_ScrollUp                               ( oC_TTY_t * TTY );
extern bool                         oC_TTY_ScrollDown                             ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetTab                                 ( oC_TTY_t * TTY );
extern bool                         oC_TTY_ClearTab                               ( oC_TTY_t * TTY );
extern bool                         oC_TTY_ClearAllTabs                           ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EraseToEndOfLine                       ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EraseToStartOfLine                     ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EraseCurrentLine                       ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EraseUp                                ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EraseDown                              ( oC_TTY_t * TTY );
extern bool                         oC_TTY_EraseScreen                            ( oC_TTY_t * TTY );
extern bool                         oC_TTY_PrintScreen                            ( oC_TTY_t * TTY );
extern bool                         oC_TTY_PrintLine                              ( oC_TTY_t * TTY );
extern bool                         oC_TTY_StopPrintLog                           ( oC_TTY_t * TTY );
extern bool                         oC_TTY_StartPrintLog                          ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetKeyDefinition                       ( oC_TTY_t * TTY , char Key , char * String );
extern bool                         oC_TTY_ResetAllAttributes                     ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetBright                              ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetDim                                 ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetUnderscore                          ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetBlink                               ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetReverese                            ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetHidden                              ( oC_TTY_t * TTY );
extern bool                         oC_TTY_SetForegroundColor                     ( oC_TTY_t * TTY , oC_Terminal_Color_t Color );
extern bool                         oC_TTY_SetBackgroundColor                     ( oC_TTY_t * TTY , oC_Terminal_Color_t Color );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

#endif /* SYSTEM_CORE_OC_TTY_H_ */
