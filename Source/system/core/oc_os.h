/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_os.h
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 8 sty 2015 13:12:25) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_OS_H_
#define SYSTEM_CORE_OC_OS_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <oc_dlist.h>
#include <oc_errors.h>
#include <oc_string.h>
#include <oc_programs_cfg.h>
#include <oc_drivers.h>
#include <oc_tty.h>
#include <oc_general_cfg.h>
#include <oc_time.h>
#include <oc_assert.h>
#include <oc_debug.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
typedef uint32_t oC_OS_PID_t;
//==========================================================================================================================================
/**
 *
 * @param Argc
 * @param Argv
 *
 * @return
 */
//==========================================================================================================================================
typedef int (*oC_OS_MainFunction_t)( int Argc , char ** Argv );
//==========================================================================================================================================
/**
 *
 * @param Parameter
 */
//==========================================================================================================================================
typedef void (*oC_OS_ThreadFunction_t)( void * Parameter );
//==========================================================================================================================================
/**
 * Type of the cyclic function
 *
 * @param Parameter         A parameter given in the configuration
 *
 * @return true if cyclic function should be called again
 */
//==========================================================================================================================================
typedef bool (*oC_OS_CyclicFunction_t)( void * Parameter );
//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
typedef size_t oC_OS_StackSize_t;
//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
typedef TickType_t oC_OS_Tick_t;

//==========================================================================================================================================
/**
 *  The type represents priority for task / processes
 */
//==========================================================================================================================================
typedef enum
{
    oC_OS_Priority_Idle     = tskIDLE_PRIORITY ,            //!< Priority of the idle task
    oC_OS_Priority_Maximum  = configMAX_PRIORITIES ,        //!< Maximum priority
    oC_OS_Priority_Parent   = oC_OS_Priority_Maximum + 10,  //!< Priority of the parent
    oC_OS_Priority_Process  = oC_OS_Priority_Parent         //!< Priority of the process
} oC_OS_Priority_t;

//==========================================================================================================================================
/**
 * The type represents state of the process.
 */
//==========================================================================================================================================
typedef enum
{
    oC_OS_ProcessState_Normal ,                         //!< oC_OS_ProcessState_Normal
    oC_OS_ProcessState_Zombie ,                         //!< oC_OS_ProcessState_Zombie
    oC_OS_ProcessState_IndestructibleZombie             //!< oC_OS_ProcessState_IndestructibleZombie
} oC_OS_ProcessState_t;

//==========================================================================================================================================
/**
 * Type for storing thread object
 */
//==========================================================================================================================================
typedef struct
{
    oC_OS_PID_t             PID;                /**< Process ID */
    oC_OS_ThreadFunction_t  ThreadFunction;     /**< Function of the thread */
    void *                  Parameter;          /**< Additional parameter given for thread function */
    TaskHandle_t            TaskHandle;         /**< Task object handle */
    oC_TTY_t*               TTY;                /**< TTY object */
    bool                    IsTTYParent;        /**< Flag if the TTY object was created for this thread */
} oC_OS_Thread_t;

//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
typedef struct
{
    oC_DLIST_t*         Threads;                /**< List of the threads */
} oC_OS_ThreadsList_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define oC_OS_PROGRAM_BEGIN( ProgramName )          int ProgramName ( int Argc , char ** Argv ) {
#define oC_OS_PROGRAM_END                           }
#define oC_OS_ADD_PROGRAM_TO_PROTOTYPES( ProgramMain , Name , StackSize )      extern int ProgramMain( int Argc , char ** Argv );
#define oC_OS_DONT_USE(...)
#define oC_TTY_Config_(                                                                                                                     \
                            STDOUT_DRIVER , STDOUT_CFG , STDOUT_TERMINAL , STDOUT_TIMEOUT ,                                                                   \
                            STDIN_DRIVER  , STDIN_CFG  , STDIN_TERMINAL  , STDIN_TIMEOUT  ,                                                                   \
                            STDERR_DRIVER , STDERR_CFG , STDERR_TERMINAL , STDERR_TIMEOUT                                                                     \
                            )                                                                                                               \
                            { .StandardOutput = { .Driver = &oC_DRIVER_REGISTRATION_NAME( STDOUT_DRIVER ) , .TerminalIf = &oC_CAT_2(STDOUT_TERMINAL , _IfCfg ) ,.Parameter = &STDOUT_CFG , .Timeout = STDOUT_TIMEOUT } , \
                              .StandardInput  = { .Driver = &oC_DRIVER_REGISTRATION_NAME( STDIN_DRIVER )  , .TerminalIf = &oC_CAT_2(STDIN_TERMINAL  , _IfCfg ) ,.Parameter = &STDIN_CFG  , .Timeout = STDIN_TIMEOUT  } , \
                              .StandardError  = { .Driver = &oC_DRIVER_REGISTRATION_NAME( STDERR_DRIVER ) , .TerminalIf = &oC_CAT_2(STDERR_TERMINAL , _IfCfg ) ,.Parameter = &STDERR_CFG , .Timeout = STDERR_TIMEOUT }   \
                            }

/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

oC_SYSTEM_PROGRAMS_LIST( oC_OS_ADD_PROGRAM_TO_PROTOTYPES , oC_OS_DONT_USE )
oC_USER_PROGRAMS_LIST( oC_OS_ADD_PROGRAM_TO_PROTOTYPES , oC_OS_DONT_USE )

extern void                 oC_OS_Initialize                        ( void );
extern void                 oC_OS_Deinitialize                      ( void );
extern void                 oC_OS_RegisterPrograms                  ( void );
extern void                 oC_OS_StartScheduler                    ( void );
extern oC_OS_PID_t          oC_OS_Process_Create                    ( oC_OS_Priority_t Priority );
extern oC_ErrorCode_t       oC_OS_Process_Kill                      ( oC_OS_PID_t PID );
extern oC_ErrorCode_t       oC_OS_Process_WaitForFinish             ( oC_OS_PID_t PID , oC_OS_Tick_t Timeout );
extern oC_OS_PID_t          oC_OS_Process_Current                   ( void );
extern oC_OS_Thread_t*      oC_OS_Thread_Create                     ( oC_OS_PID_t PID , const oC_STRING_t Name , oC_OS_ThreadFunction_t ThreadFunction , void * Parameter , oC_OS_StackSize_t StackSize , oC_OS_Priority_t Priority , oC_TTY_t * TTY );
extern oC_OS_Thread_t*      oC_OS_Thread_Current                    ( void );
extern void                 oC_OS_Thread_Cancel                     ( oC_OS_Thread_t * Thread );
extern oC_TTY_t*            oC_OS_Thread_TTY                        ( void );
extern bool                 oC_OS_Thread_IsCorrect                  ( oC_OS_Thread_t * Thread );
extern int                  oC_OS_Program_Exec                      ( oC_OS_MainFunction_t MainFunction , int Argc , char ** Argv );
extern oC_OS_PID_t          oC_OS_Program_ExecInNewProcess          ( oC_OS_MainFunction_t MainFunction , int Argc , char ** Argv , int * outReturn , oC_TTY_t * TTY , oC_OS_Priority_t Priority );
extern bool                 oC_OS_Program_Register                  ( oC_OS_MainFunction_t MainFunction , const oC_STRING_t Name , oC_OS_StackSize_t StackSize );
extern const char *         oC_OS_Program_GetName                   ( oC_OS_MainFunction_t MainFunction );
extern void                 oC_OS_Program_Unregister                ( oC_OS_MainFunction_t MainFunction );
extern oC_ErrorCode_t       oC_OS_Program_Close                     ( oC_OS_PID_t PID );
extern void*                oC_OS_Cyclic_Start                      ( oC_OS_CyclicFunction_t CyclicFunction , const oC_STRING_t Name , void * Parameter , oC_OS_Tick_t Period , oC_OS_StackSize_t StackSize );
extern void                 oC_OS_Cyclic_Stop                       ( void * ObjectHandle );
extern oC_ErrorCode_t       oC_OS_RegisterGlobalVariable            ( void * Destination , size_t Size );
extern oC_ErrorCode_t       oC_OS_CloneTTY                          ( void );
extern oC_ErrorCode_t       oC_OS_ConfigureTTY                      ( const oC_TTY_Config_t * Config );
extern oC_ErrorCode_t       oC_OS_ConfigureTTYForThread             ( oC_OS_Thread_t * Thread , const oC_TTY_Config_t * Config );
extern oC_ErrorCode_t       oC_OS_SetTTY                            ( oC_TTY_t * TTY );
extern oC_ErrorCode_t       oC_OS_SetTTYForThread                   ( oC_OS_Thread_t * Thread , oC_TTY_t * TTY );
extern int                  oC_OS_StandardOutput                    ( const char * Str , uint32_t Length );
extern int                  oC_OS_StandardInput                     (       char * Str , uint32_t Length );
extern int                  oC_OS_StandardError                     ( const char * Str , uint32_t Length );
extern int                  oC_OS_SendToStream                      ( oC_FileDescriptor_t FileDescriptor , const oC_STRING_t String , uint32_t Length );
extern oC_TTY_t*            oC_OS_DefaultTTY                        ( void );
extern bool                 oC_OS_IsROMAddress                      ( const void * Address );
extern bool                 oC_OS_IsRAMAddress                      ( const void * Address );
extern bool                 oC_OS_IsDynamicAllocatedAddress         ( const void * Address );
extern bool                 oC_OS_IsAddressCorrect                  ( const void * Address );

extern bool                         oC_OS_Terminal_ResetDevice                            ( void );
extern bool                         oC_OS_Terminal_ClearScreen                            ( void );
extern oC_Terminal_CursorPos_t      oC_OS_Terminal_GetCursorPosition                      ( void );
extern bool                         oC_OS_Terminal_EnableLineWrap                         ( void );
extern bool                         oC_OS_Terminal_DisableLineWrap                        ( void );
extern bool                         oC_OS_Terminal_SetCursorHome                          ( oC_Terminal_CursorPos_t Position );
extern bool                         oC_OS_Terminal_CursorHome                             ( void );
extern bool                         oC_OS_Terminal_CursorUp                               ( int Count );
extern bool                         oC_OS_Terminal_CursorDown                             ( int Count );
extern bool                         oC_OS_Terminal_CursorForward                          ( int Count );
extern bool                         oC_OS_Terminal_CursorBackward                         ( int Count );
extern bool                         oC_OS_Terminal_SaveCursorPosition                     ( void );
extern bool                         oC_OS_Terminal_RestoreCursorPosition                  ( void );
extern bool                         oC_OS_Terminal_SaveCursorPositionAndAttrs             ( void );
extern bool                         oC_OS_Terminal_RestoreCursorPositionAndAttrs          ( void );
extern bool                         oC_OS_Terminal_EnableScrollScreen                     ( void );
extern bool                         oC_OS_Terminal_EnableScrollScreenTo                   ( int startRow , int endRow );
extern bool                         oC_OS_Terminal_ScrollUp                               ( void );
extern bool                         oC_OS_Terminal_ScrollDown                             ( void );
extern bool                         oC_OS_Terminal_SetTab                                 ( void );
extern bool                         oC_OS_Terminal_ClearTab                               ( void );
extern bool                         oC_OS_Terminal_ClearAllTabs                           ( void );
extern bool                         oC_OS_Terminal_EraseToEndOfLine                       ( void );
extern bool                         oC_OS_Terminal_EraseToStartOfLine                     ( void );
extern bool                         oC_OS_Terminal_EraseCurrentLine                       ( void );
extern bool                         oC_OS_Terminal_SetKeyDefinition                       ( char Key , char * String );
extern bool                         oC_OS_Terminal_ResetAllAttributes                     ( void );
extern bool                         oC_OS_Terminal_SetBright                              ( void );
extern bool                         oC_OS_Terminal_SetDim                                 ( void );
extern bool                         oC_OS_Terminal_SetUnderscore                          ( void );
extern bool                         oC_OS_Terminal_SetBlink                               ( void );
extern bool                         oC_OS_Terminal_SetReverese                            ( void );
extern bool                         oC_OS_Terminal_SetHidden                              ( void );
extern bool                         oC_OS_Terminal_SetForegroundColor                     ( oC_Terminal_Color_t Color );
extern bool                         oC_OS_Terminal_SetBackgroundColor                     ( oC_Terminal_Color_t Color );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Function to delay in tasks
 *
 * @param TimeMS        Time in ms to delay
 */
//==========================================================================================================================================
static inline void oC_OS_DelayMS( uint32_t TimeMS )
{
    vTaskDelay( oC_CONVERT_TIME_MS_TO_TICKS(TimeMS) );
}

//==========================================================================================================================================
/**
 * Function to delay in tasks
 *
 * @param Ticks        Ticks to delay
 */
//==========================================================================================================================================
static inline void oC_OS_Delay( oC_OS_Tick_t Ticks )
{
    vTaskDelay( Ticks );
}

#endif /* SYSTEM_CORE_OC_OS_H_ */
