/** ****************************************************************************************************************************************
 *
 * @file       oc_memory.h
 *
 * @brief      File with memory management definitions and functions
 *
 * @author     Patryk Kubiak - (Created on: 2 kwi 2015 21:05:16) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_MEMORY_H_
#define SYSTEM_CORE_OC_MEMORY_H_

#include <stdint.h>
#include <stdbool.h>

/** ****************************************************************************************************************************************
 * The section with macros definitions
 */
#define _________________________________________MACROS_DEFINITIONS_SECTION_________________________________________________________________

#ifndef NULL
#define NULL        (void*)0
#endif

/* END OF SECTION */
#undef  _________________________________________MACROS_DEFINITIONS_SECTION_________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with global variables
 */
#define _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

extern const char __rom_size;
extern const char __ram_size;
extern const char __heap_size;
extern const char __data_size;
extern const char __stack_size;

/* END OF SECTION */
#undef  _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with memory types
 */
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef uint32_t oC_Memory_Size_t;
typedef void * oC_Memory_Assignment_t;

/* END OF SECTION */
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with interface prototypes
 */
#define _________________________________________INTERFACE_PROTOTYPES_SECTION_______________________________________________________________

extern void             oC_Memory_Init                          ( void );
extern void *           oC_Memory_Alloc                         ( oC_Memory_Assignment_t Assignment , oC_Memory_Size_t Size );
extern void *           oC_Memory_Calloc                        ( oC_Memory_Assignment_t Assignment , oC_Memory_Size_t Size );
extern bool             oC_Memory_Free                          ( void * Pointer );
extern bool             oC_Memory_FreeFromAssignment            ( oC_Memory_Assignment_t Assignement );
extern void             oC_Memory_FreeAll                       ( void );
extern bool             oC_Memory_FreeAllWithoutAssignment      ( void );
extern oC_Memory_Size_t oC_Memory_GetFreeSize                   ( void );
extern oC_Memory_Size_t oC_Memory_GetAllocatedSize              ( void );
extern oC_Memory_Size_t oC_Memory_GetMaximumBlockSize           ( void );
extern oC_Memory_Size_t oC_Memory_GetAllocatedForAssignmentSize ( oC_Memory_Assignment_t Assignment );

/* END OF SECTION */
#undef  _________________________________________INTERFACE_PROTOTYPES_SECTION_______________________________________________________________


/** ****************************************************************************************************************************************
 * The section with static inline functions
 */
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


static inline oC_Memory_Size_t oC_Memory_GetRomSize( void )
{
    return (oC_Memory_Size_t) &__rom_size;
}
static inline oC_Memory_Size_t oC_Memory_GetRamSize( void )
{
    return (oC_Memory_Size_t) &__ram_size;
}
static inline oC_Memory_Size_t oC_Memory_GetHeapSize( void )
{
    return (oC_Memory_Size_t) &__heap_size;
}
static inline oC_Memory_Size_t oC_Memory_GetDataSize( void )
{
    return (oC_Memory_Size_t) &__data_size;
}
static inline oC_Memory_Size_t oC_Memory_GetStackSize( void )
{
    return (oC_Memory_Size_t) &__stack_size;
}

/* END OF SECTION */
#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


#endif /* SYSTEM_CORE_OC_MEMORY_H_ */
