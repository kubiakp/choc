/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_tty.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 16 sty 2015 13:08:08) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_tty.h>
#include <oc_os.h>
#include <stdlib.h>
#include <oc_synch.h>
#include <oc_general_cfg.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static inline bool  _IsStreamConfigCorrect  ( const oC_TTY_StreamConfig_t * Config );
static inline int   _SendViaStream          ( const oC_TTY_StreamConfig_t * Config , const char * Data , uint32_t Length );
static inline int   _ReceiveViaStream       ( const oC_TTY_StreamConfig_t * Config ,       char * Data , uint32_t Length );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Creates new tty object using the Config configuration
 *
 * @param Config            A configuration of the streams
 * @param outErrorCode      [out] Reference to the variable to store code of error, or NULL if not used
 *
 * @return NULL if error, otherwise reference to the TTY object
 */
//==========================================================================================================================================
oC_TTY_t* oC_TTY_New( const oC_TTY_Config_t * Config , oC_ErrorCode_t * outErrorCode )
{
    oC_TTY_t * tty = NULL;
    oC_ErrorCode_t errorCode;

    if ( oC_TTY_IsConfigCorrect( Config ) )
    {
        tty     = malloc( sizeof( oC_TTY_t ) );

        if( oC_OS_IsDynamicAllocatedAddress( tty ) )
        {
            tty->Self           = tty;
            tty->Config         = Config;
            tty->StdOutActive   = true;
            tty->Mutex          = oC_Mutex_New( oC_Mutex_Type_Normal );

            if ( oC_OS_IsAddressCorrect( tty->Mutex ) )
            {
                if ( _IsStreamConfigCorrect( &Config->StandardOutput ) )
                {
                    tty->Lines          = oC_DLIST_New();

                    if ( oC_DLIST_IsListCorrect( tty->Lines ) )
                    {
                        errorCode   = oC_ErrorCode_None;
                    }
                    else
                    {
                        oC_DLIST_Delete( tty->Lines );
                        free( tty );
                        tty         = NULL;
                        errorCode   = oC_ErrorCode_TTYCannotCreateDLIST;
                    }
                }
                else
                {
                    tty->Lines          = NULL;
                    errorCode           = oC_ErrorCode_None;
                }
            }
            else
            {
                free( tty );
                errorCode       = oC_ErrorCode_TTYCannotCreateMutex;
            }

        }
        else
        {
            errorCode       = oC_ErrorCode_AllocationError;
            tty             = NULL;
        }
    }
    else
    {
        errorCode           = oC_ErrorCode_TTYConfigurationIncorrect;
    }

    if ( oC_OS_IsRAMAddress( outErrorCode ) )
    {
        *outErrorCode       = errorCode;
    }

    return tty;
}

//==========================================================================================================================================
/**
 * The function creates new tty and copy variables from it
 *
 * @param TTY           TTY to clone
 * @param outErrorCode  [out] code of error
 *
 * @return
 */
//==========================================================================================================================================
oC_TTY_t * oC_TTY_Clone( oC_TTY_t * TTY , oC_ErrorCode_t * outErrorCode )
{
    oC_TTY_t * tty = NULL;

    if ( oC_TTY_IsCorrect( TTY ) && oC_TTY_IsConfigCorrect( TTY->Config ) )
    {
        tty     = oC_TTY_New( TTY->Config , outErrorCode );

        if ( oC_TTY_IsCorrect( tty ) )
        {
            foreach( oC_STRING_t * , line , TTY->Lines )
            {
                oC_DLIST_PushBack(tty->Lines , line , sizeof(oC_STRING_t *));
            }
        }
        else
        {
            oC_TTY_Delete( tty );
            tty     = NULL;
        }
    }

    return tty;
}

//==========================================================================================================================================
/**
 * Deletes TTY object
 *
 * @param TTY       The reference to the TTY object
 */
//==========================================================================================================================================
void oC_TTY_Delete( oC_TTY_t * TTY )
{
    if ( oC_TTY_IsCorrect( TTY ) )
    {
        oC_Mutex_Delete( TTY->Mutex );

        foreach( oC_STRING_t , line , TTY->Lines )
        {
            oC_STRING_Delete( &line );
        }

        oC_DLIST_Delete( TTY->Lines );

        free( TTY );
    }
}

//==========================================================================================================================================
/**
 * Checks if the object was correctly initialized
 *
 * @param TTY           Reference to the TTY object
 *
 * @return true if object is correct
 */
//==========================================================================================================================================
bool oC_TTY_IsCorrect( oC_TTY_t * TTY )
{
    return oC_OS_IsRAMAddress( TTY ) && TTY->Self == TTY;
}

//==========================================================================================================================================
/**
 * Checks if the given configuration is correct (at least one of the streams should be configurred and function address should be set to ROM value
 *
 * @param Config        Reference to the TTY object
 *
 * @return true if TTY configuration is correct
 */
//==========================================================================================================================================
bool oC_TTY_IsConfigCorrect( const oC_TTY_Config_t * Config )
{
    bool configCorrect = false;

    if ( oC_OS_IsAddressCorrect( Config ) )
    {
        if (
                 _IsStreamConfigCorrect( &Config->StandardOutput )
             ||  _IsStreamConfigCorrect( &Config->StandardInput  )
             ||  _IsStreamConfigCorrect( &Config->StandardError  )
                )
        {
            configCorrect       = true;
        }
    }

    return configCorrect;
}

//==========================================================================================================================================
/**
 * Sends string via standard output stream
 *
 * @param TTY           Reference to the TTY object
 * @param Str           String to send
 * @param Length        Length of the buffer to send
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
int oC_TTY_StandardOutput( oC_TTY_t * TTY , const char * Str , uint32_t Length )
{
    int sentBytes   = 0;

    if ( oC_TTY_IsCorrect( TTY ) && oC_OS_IsAddressCorrect( Str ) && oC_DLIST_IsListCorrect( TTY->Lines ) && oC_Mutex_Take( TTY->Mutex , oC_TTY_DEFAULT_STDOUT_TIMEOUT ) )
    {
        if ( TTY->StdOutActive )
        {
            sentBytes   = _SendViaStream( &TTY->Config->StandardOutput , Str , Length );
        }

        oC_STRING_t line = oC_STRING_New( (const oC_STRING_t) Str );

        if ( oC_STRING_IsCorrect( line ) )
        {
            oC_DLIST_EmplacePushBack( TTY->Lines , &line , sizeof(oC_STRING_t*) );

            while( oC_DLIST_Count( TTY->Lines ) > oC_TTY_SCREEN_BUFFER_SIZE_HEIGHT )
            {
                oC_DLIST_RemoveFirst( TTY->Lines );

            }
        }

        oC_Mutex_Give( TTY->Mutex );
    }

    return sentBytes;
}

//==========================================================================================================================================
/**
 * Receives data via standard input stream.
 *
 * @param TTY           Reference to the TTY object
 * @param Str           Destination buffer
 * @param Length        length of the destination buffer
 *
 * @return number of received bytes
 */
//==========================================================================================================================================
int oC_TTY_StandardInput( oC_TTY_t * TTY , char * Str , uint32_t Length )
{
    int receivedBytes = 0;

    if ( oC_TTY_IsCorrect( TTY ) && oC_OS_IsAddressCorrect( Str ) && oC_Mutex_Take( TTY->Mutex , oC_TTY_DEFAULT_STDIN_TIMEOUT ))
    {
        receivedBytes   = _ReceiveViaStream( &TTY->Config->StandardInput , Str , Length );
        oC_Mutex_Give( TTY->Mutex );
    }

    return receivedBytes;
}

//==========================================================================================================================================
/**
 * Send data via standard error stream
 *
 * @param TTY           Reference to the TTY object
 * @param Str           String to send
 * @param Length        Length of the buffer to send
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
int oC_TTY_StandardError( oC_TTY_t * TTY , const char * Str , uint32_t Length )
{
    int sentBytes   = 0;

    if ( oC_TTY_IsCorrect( TTY ) && oC_OS_IsAddressCorrect( Str ) && oC_DLIST_IsListCorrect( TTY->Lines ) && oC_Mutex_Take( TTY->Mutex , oC_TTY_DEFAULT_STDERR_TIMEOUT ))
    {
        sentBytes   = _SendViaStream( &TTY->Config->StandardError , Str , Length );
        oC_Mutex_Give( TTY->Mutex );
    }

    return sentBytes;
}

//==========================================================================================================================================
/**
 * Resend data via standard output stream
 *
 * @param TTY          Reference to the TTY object
 */
//==========================================================================================================================================
void oC_TTY_Refresh( oC_TTY_t * TTY )
{
    if ( oC_TTY_IsCorrect( TTY ) && _IsStreamConfigCorrect( &TTY->Config->StandardOutput ) && TTY->StdOutActive && oC_Mutex_Take( TTY->Mutex , oC_TTY_DEFAULT_STDOUT_TIMEOUT ))
    {
        foreach( oC_STRING_t * , line , TTY->Lines )
        {
            _SendViaStream( &TTY->Config->StandardOutput  ,*line , oC_STRING_Count( *line ) );
        }

        oC_Mutex_Give( TTY->Mutex );
    }
}

//==========================================================================================================================================
/**
 * Sets standard output tty active or not active
 *
 * @param TTY           Reference to the TTY object
 * @param Active        Standard output active state
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetActive( oC_TTY_t * TTY , bool Active )
{
    bool activeSet = false;

    if ( oC_TTY_IsCorrect( TTY ) && oC_OS_IsAddressCorrect( TTY->Config ) && _IsStreamConfigCorrect( &TTY->Config->StandardOutput ) )
    {
        TTY->StdOutActive   = Active;
        activeSet = true;
    }

    return activeSet;
}

//==========================================================================================================================================
/**
 * Checks if a tty stdout is active
 *
 * @param TTY           Reference to the TTY object
 *
 * @return true if stdout is active
 */
//==========================================================================================================================================
bool oC_TTY_IsActive( oC_TTY_t * TTY )
{
    bool active = false;

    if ( oC_TTY_IsCorrect( TTY ) && oC_OS_IsAddressCorrect( TTY->Config ) && _IsStreamConfigCorrect( &TTY->Config->StandardOutput ) )
    {
        active          = TTY->StdOutActive;
    }

    return active;
}

//==========================================================================================================================================
/**
 * Send reset device sequence
 *
 * @param TTY               Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_ResetDevice( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->ResetDevice )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->ResetDevice();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Clears screen
 *
 * @param TTY               Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_ClearScreen( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->ClearScreen )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->ClearScreen();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Reads current position of the cursor
 *
 * @param TTY               Reference to the TTY object handle
 *
 * @return Position of the cursor (-1,-1 if error)
 */
//==========================================================================================================================================
oC_Terminal_CursorPos_t oC_TTY_GetCursorPosition( oC_TTY_t * TTY )
{
    oC_Terminal_CursorPos_t cursorPos = { .Column = -1 , .Row = -1 };

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->GetCursorPosition )
         )
    {
        cursorPos = TTY->Config->StandardOutput.TerminalIf->GetCursorPosition();
    }

    return cursorPos;
}

//==========================================================================================================================================
/**
 * Enables line wrapping
 *
 * @param TTY               Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_EnableLineWrap( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->EnableLineWrap )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->EnableLineWrap();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Disable line wrapping
 *
 * @param TTY               Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_DisableLineWrap( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->DisableLineWrap )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->DisableLineWrap();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets position of home for the cursor.
 *
 * @param TTY               Reference to the TTY object handle
 * @param Position          Position to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetCursorHome( oC_TTY_t * TTY , oC_Terminal_CursorPos_t Position )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetCursorHome )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetCursorHome( Position );
    }

    return success;
}

//==========================================================================================================================================
/**
 * Return cursor to home position
 *
 * @param TTY               Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_CursorHome( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->CursorHome )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->CursorHome();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Moves the cursor up by Count rows
 *
 * @param TTY       Reference to the TTY object handle
 * @param Count     Number of rows to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_CursorUp( oC_TTY_t * TTY , int Count )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->CursorUp )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->CursorUp(Count);
    }

    return success;
}

//==========================================================================================================================================
/**
 * Moves the cursor down by Count rows
 *
 * @param TTY       Reference to the TTY object handle
 * @param Count     Number of rows to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_CursorDown( oC_TTY_t * TTY , int Count )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->CursorDown )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->CursorDown( Count );
    }

    return success;
}

//==========================================================================================================================================
/**
 * Moves the cursor forward by Count rows
 *
 * @param TTY       Reference to the TTY object handle
 * @param Count     Number of columns to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_CursorForward( oC_TTY_t * TTY , int Count )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->CursorForward )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->CursorForward( Count );
    }

    return success;
}

//==========================================================================================================================================
/**
 * Moves the cursor backward by Count rows
 *
 * @param TTY       Reference to the TTY object handle
 * @param Count     Number of columns to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_CursorBackward( oC_TTY_t * TTY , int Count )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->CursorBackward )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->CursorBackward( Count );
    }

    return success;
}


//==========================================================================================================================================
/**
 * Save current position of the cursor for restoring it later
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SaveCursorPosition( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SaveCursorPosition )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SaveCursorPosition();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Restores cursor position from last save
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_RestoreCursorPosition( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->RestoreCursorPosition )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->RestoreCursorPosition();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Save cursor position and attributes for restoring it later
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SaveCursorPositionAndAttrs( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SaveCursorPositionAndAttrs )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SaveCursorPositionAndAttrs();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Restores cursor position from last save and attributes from last save
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_RestoreCursorPositionAndAttrs( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->RestoreCursorPositionAndAttrs )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->RestoreCursorPositionAndAttrs();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Enable scrolling for entire display.
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_EnableScrollScreen( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->EnableScrollScreen )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->EnableScrollScreen();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Enable scrolling from row {startRow} to row {endRow}.
 *
 * @param TTY       Reference to the TTY object handle
 * @param startRow  start row of scroll
 * @param endRow    end row of scroll
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_EnableScrollScreenTo( oC_TTY_t * TTY , int startRow , int endRow )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->EnableScrollScreenTo )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->EnableScrollScreenTo( startRow , endRow );
    }

    return success;
}

//==========================================================================================================================================
/**
 * Scrolls screen one line up
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_ScrollUp( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->ScrollUp )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->ScrollUp();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Scrolls screen one line down
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_ScrollDown( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->ScrollDown )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->ScrollDown();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets tab at current position
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetTab( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetTab )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetTab();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Clears tab at current position
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_ClearTab( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->ClearTab )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->ClearTab();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Clears all tabs
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_ClearAllTabs( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->ClearAllTabs )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->ClearAllTabs();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Erases from the current cursor position to the end of the current line.
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_EraseToEndOfLine( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->EraseToEndOfLine )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->EraseToEndOfLine();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Erases from the current cursor position to the start of the current line.
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_EraseToStartOfLine( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->EraseToStartOfLine )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->EraseToStartOfLine();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Erases current line
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_EraseCurrentLine( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->EraseCurrentLine )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->EraseCurrentLine();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Associates a string of text to a keyboard key. {key} indicates the key by its ASCII value.
 *
 * @param TTY       Reference to the TTY object handle
 * @param Key       Ascii code of key to define
 * @param String    String to associate with the Key
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetKeyDefinition( oC_TTY_t * TTY , char Key , char * String )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetKeyDefinition )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetKeyDefinition( Key , String );
    }

    return success;
}

//==========================================================================================================================================
/**
 * Reset all set attributes and restore default values
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_ResetAllAttributes( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->ResetAllAttributes )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->ResetAllAttributes();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets bright
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetBright( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetBright )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetBright();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets dim
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetDim( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetDim )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetDim();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets underscore
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetUnderscore( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetUnderscore )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetUnderscore();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets blink
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetBlink( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetBlink )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetBlink();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets reverse
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetReverese( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetReverese )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetReverese();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets hidden
 *
 * @param TTY       Reference to the TTY object handle
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetHidden( oC_TTY_t * TTY )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetHidden )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetHidden();
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets foreground color
 *
 * @param TTY       Reference to the TTY object handle
 * @param Color     Color to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetForegroundColor( oC_TTY_t * TTY , oC_Terminal_Color_t Color )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetForegroundColor )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetForegroundColor( Color );
    }

    return success;
}

//==========================================================================================================================================
/**
 * Sets background color
 *
 * @param TTY       Reference to the TTY object handle
 * @param Color     Color to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TTY_SetBackgroundColor( oC_TTY_t * TTY , oC_Terminal_Color_t Color )
{
    bool success = false;

    if (
            oC_TTY_IsCorrect(TTY)
         && _IsStreamConfigCorrect( &TTY->Config->StandardOutput )
         && oC_OS_IsROMAddress( TTY->Config->StandardOutput.TerminalIf->SetBackgroundColor )
         )
    {
        success = TTY->Config->StandardOutput.TerminalIf->SetBackgroundColor( Color );
    }

    return success;
}



/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Checks if the stream config is correct
 *
 * @param Config            Reference to a stream configuration
 *
 * @return true if correct
 */
//==========================================================================================================================================
static inline bool _IsStreamConfigCorrect( const oC_TTY_StreamConfig_t * Config )
{
    return      oC_OS_IsAddressCorrect( Config )
            &&  oC_OS_IsAddressCorrect( Config->Driver )
            &&  oC_OS_IsAddressCorrect( Config->Driver->If )
            &&
                (
                        oC_OS_IsROMAddress( Config->Driver->If->SendViaStreamFunction )
                     || oC_OS_IsROMAddress( Config->Driver->If->ReceiveViaStreamFunction )
                        );
}

//==========================================================================================================================================
/**
 * Function sends data via stream
 *
 * @param Config            Reference to the configuration
 * @param Data              Buffer to send
 * @param Length            Length of the buffer
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
static inline int _SendViaStream( const oC_TTY_StreamConfig_t * Config , const char * Data , uint32_t Length )
{
    int sentBytes = 0;

    if ( _IsStreamConfigCorrect( Config ) && oC_OS_IsROMAddress( Config->Driver->If->SendViaStreamFunction ) )
    {
        sentBytes = Config->Driver->If->SendViaStreamFunction( Config->Parameter , Data , Length , Config->Timeout );
    }

    return sentBytes;
}

//==========================================================================================================================================
/**
 * Receive data via stream
 *
 * @param Config        Reference to the stream configuration
 * @param Data          Destination buffer
 * @param Length        Length of the buffer
 *
 * @return number of received bytes
 */
//==========================================================================================================================================
static inline int _ReceiveViaStream( const oC_TTY_StreamConfig_t * Config , char * Data , uint32_t Length )
{
    int receivedBytes = 0;

    if ( _IsStreamConfigCorrect( Config ) && oC_OS_IsROMAddress( Config->Driver->If->ReceiveViaStreamFunction ) )
    {
        receivedBytes = Config->Driver->If->ReceiveViaStreamFunction( Config->Parameter , Data , Length , Config->Timeout );
    }

    return receivedBytes;
}
