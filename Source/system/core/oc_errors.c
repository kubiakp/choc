/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_errors.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 9 gru 2014 12:21:22) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_errors.h>
#include <oc_os.h>
#include <oc_array.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define _ADD_ERROR_TO_STRING_LIST( CODE_NAME , CODE_STR )           CODE_STR ,

/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

/**
 * List of error strings
 */
const char* oC_ErrorsStrings[oC_ErrorCode_NumberOfErrorsDefinitions] = {
                                                                        "Success" ,
                                                                        oC_ERRORS_LIST(_ADD_ERROR_TO_STRING_LIST)
                                                                        "Unknown error code"
};

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Prints error via standard error stream
 *
 * @param ErrorCode         Code of error to print
 */
//==========================================================================================================================================
void oC_PrintError( oC_ErrorCode_t ErrorCode )
{
#if oC_PRINT_ERRORS_DESCRIPTIONS
    if ( ErrorCode < oC_ErrorCode_NumberOfErrorsDefinitions )
    {
        oC_OS_StandardOutput(
                             oC_ErrorsStrings[ ErrorCode ] ,
                             oC_STRING_Length( oC_ErrorsStrings[ ErrorCode ] )
                             );
    }
    else
    {
        oC_OS_StandardOutput(
                             oC_ErrorsStrings[ oC_ErrorCode_NumberOfErrorsDefinitions ] ,
                             oC_STRING_Length( oC_ErrorsStrings[ oC_ErrorCode_NumberOfErrorsDefinitions ] )
                             );
    }
#else
    oC_STRING_t str = oC_STRING_FromUIntToHex( 6 , ErrorCode , true , 6 );
    oC_OS_StandardOutput( str , oC_STRING_Length(str));
    oC_STRING_Delete(&str);
#endif
}


//==========================================================================================================================================
/**
 * The function returns string of the error code, if it exists.
 *
 * @param ErrorCode
 *
 * @return reference to the error string
 */
//==========================================================================================================================================
const char * oC_GetErrorString           ( oC_ErrorCode_t ErrorCode )
{
    const char * string = NULL;
#if oC_PRINT_ERRORS_DESCRIPTIONS
    if ( ErrorCode < oC_ErrorCode_NumberOfErrorsDefinitions )
    {
        string = oC_ErrorsStrings[ErrorCode];
    }
    else
    {
        string = oC_ARRAY_LAST_ELEMENT(oC_ErrorsStrings);
    }
#else
    string = "error descriptions are turned off\n";
#endif
    return string;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
