/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_synch.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 23 gru 2014 15:44:23) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software{} you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation{} either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY{} without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program{} if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_synch.h>
#include <stdlib.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

typedef struct
{
    oC_Signal_Slot_t        Slot;
    const void *            Parameter;
} _SlotContainer_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/


//==========================================================================================================================================
/**
 * Function creates semaphore
 *
 * @param Type          Type of the semaphore to create. For binary set it to #oC_Semaphore_Type_Binary value, otherwise set it into
 *                      maximum count value for the semaphore
 *
 * @param InitialValue  Value to set after create of the semaphore.
 *
 * @return Reference to the semaphore, or NULL if error
 *
 * @example "Create binary semaphore"
 * @code
       oC_Semaphore_t * semaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 );
   @endcode
   @example "Create counting semaphore"
 * @code
       oC_Semaphore_t * semaphore = oC_Semaphore_New( 10 , 3 );
   @endcode
 */
//==========================================================================================================================================
oC_Semaphore_t* oC_Semaphore_New( oC_Semaphore_Type_t Type , uint32_t InitialValue )
{
    oC_Semaphore_t * semaphore = NULL;

    if ( Type > 0 )
    {
        semaphore = malloc( sizeof(oC_Semaphore_t) );

        if ( semaphore )
        {
            semaphore->Type = Type;

            if ( Type == oC_Semaphore_Type_Binary )
            {
                semaphore->Semaphore    = xSemaphoreCreateBinary();

                if ( semaphore->Semaphore )
                {
                    if ( InitialValue == 0 )
                    {
                        //xSemaphoreTake( semaphore->Semaphore , portMAX_DELAY );
                        semaphore->Counter    = 0;
                    }
                    else
                    {
                        xSemaphoreGive( semaphore->Semaphore );
                        semaphore->Counter    = 1;
                    }
                }
                else
                {
                    free( semaphore );
                    semaphore   = NULL;
                }
            }
            else
            {
                semaphore->Semaphore    = xSemaphoreCreateCounting( Type , InitialValue );

                if ( semaphore->Semaphore )
                {
                    semaphore->Counter    = (uint32_t)InitialValue;
                }
                else
                {
                    free( semaphore );
                    semaphore   = NULL;
                }
            }
        }

    }

    return semaphore;
}

//==========================================================================================================================================
/**
 * Deletes a semaphore
 *
 * @param Semaphore     Reference to the semaphore to delete
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Semaphore_Delete( oC_Semaphore_t * Semaphore )
{
    bool result = false;

    if ( Semaphore && (Semaphore->Counter == Semaphore->Type) )
    {
        vSemaphoreDelete(Semaphore->Semaphore);
        free( Semaphore );
        result  = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Give semaphore from ISR, or THREAD.
 *
 * @param Semaphore     Reference to the semaphore to give
 * @param outTaskWoken  [out]   Reference to the flag if task was woken. It should be used only in ISR. In threads set it as NULL
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Semaphore_Give( oC_Semaphore_t * Semaphore , bool * outTaskWoken )
{
    bool result = false;

    if ( Semaphore )
    {
        if ( outTaskWoken )
        {
            if ( xSemaphoreGiveFromISR( Semaphore->Semaphore , outTaskWoken ) )
            {
                result = true;
                if ( Semaphore->Counter < Semaphore->Type )
                {
                    Semaphore->Counter++;
                }
            }
        }
        else
        {
            if (xSemaphoreGive( Semaphore->Semaphore ) )
            {
                result = true;
                if ( Semaphore->Counter < Semaphore->Type )
                {
                    Semaphore->Counter++;
                }
            }
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Takes semaphore from THREAD.
 *
 * @param Semaphore     Reference to the semaphore
 * @param Timeout       Maximum time to take the semaphore
 *
 * @return 'false' if semaphore has not been taken in the time defined as 'timeout'
 */
//==========================================================================================================================================
bool oC_Semaphore_Take( oC_Semaphore_t * Semaphore , oC_TickType_t Timeout )
{
    bool result = false;

    if ( Semaphore && Semaphore->Semaphore)
    {
        if (xSemaphoreTake( Semaphore->Semaphore , Timeout ))
        {
            result = true;

            if ( Semaphore->Counter > 0 )
            {
                Semaphore->Counter--;
            }
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Creates new mutex object.
 *
 * @param Type          Type of the mutex to create.
 *
 * @return Pointer to the mutex, or NULL if error
 */
//==========================================================================================================================================
oC_Mutex_t* oC_Mutex_New( oC_Mutex_Type_t Type )
{
    oC_Mutex_t * mutex = malloc(sizeof(oC_Mutex_t));

    if ( mutex )
    {
        mutex->Type         = Type;

        if ( Type == oC_Mutex_Type_Recursive )
        {
            mutex->Mutex = xSemaphoreCreateRecursiveMutex();
        }
        else
        {
            mutex->Mutex = xSemaphoreCreateMutex();
        }

        if ( NULL == mutex->Mutex )
        {
            free(mutex);
            mutex = NULL;
        }
    }

    return mutex;
}

//==========================================================================================================================================
/**
 * Removes mutex object
 *
 * @param Mutex         Reference to the mutex object
 *
 * @return false if error
 */
//==========================================================================================================================================
bool oC_Mutex_Delete( oC_Mutex_t * Mutex )
{
    bool result = false;

    if ( Mutex )
    {
        vSemaphoreDelete( Mutex->Mutex );
        free(Mutex);
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Gives mutex (it cannot be used from ISR)
 *
 * @param Mutex         Reference to the mutex object
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Mutex_Give( oC_Mutex_t * Mutex )
{
    bool result = false;

    if ( Mutex )
    {
        if ( Mutex->Type == oC_Mutex_Type_Recursive )
        {
            if ( xSemaphoreGiveRecursive( Mutex->Mutex ) )
            {
                result = true;
            }
        }
        else
        {
            if ( xSemaphoreGive( Mutex->Mutex ) )
            {
                result = true;
            }
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Wait for the mutex available, and take it when possible.
 *
 * @param Mutex         Reference to the mutex to take
 * @param Timeout       Maximum time to wait for a mutex available
 *
 * @return true if mutex was available in the Timeout time
 */
//==========================================================================================================================================
bool oC_Mutex_Take( oC_Mutex_t * Mutex , oC_TickType_t Timeout )
{
    bool result = false;

    if ( Mutex )
    {
        if ( Mutex->Type == oC_Mutex_Type_Recursive )
        {
            if ( xSemaphoreTakeRecursive(Mutex->Mutex , Timeout) )
            {
                result = true;
            }
        }
        else
        {
            if ( xSemaphoreTake(Mutex->Mutex , Timeout) )
            {
                result = true;
            }
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Creates new signal object.
 *
 * @param DefaultParameter  Default parameter for slots
 *
 * @return reference to the signal object, or NULL if cannot be created
 */
//==========================================================================================================================================
oC_Signal_t * oC_Signal_New( const void * DefaultParameter  )
{
    oC_Signal_t * signal = malloc( sizeof(oC_Signal_t) );

    if ( signal )
    {
        signal->Semaphore           = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 );
        signal->Slots               = oC_DLIST_New();
        signal->DefaultParameter    = DefaultParameter;

        if ( signal->Semaphore == NULL || signal->Slots == NULL )
        {
            free(signal);
            signal = NULL;
        }
    }

    return signal;
}

//==========================================================================================================================================
/**
 * Deletes a signal
 *
 * @param Signal    Reference to the signal to delete
 *
 * @return true if success, false if signal was not correct
 */
//==========================================================================================================================================
bool oC_Signal_Delete( oC_Signal_t * Signal )
{
    bool result = false;

    if ( Signal )
    {
        oC_Semaphore_Delete( Signal->Semaphore );
        oC_DLIST_Delete( Signal->Slots );
        free(Signal);
        result          = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Sets default parameter for the slots
 *
 * @param Signal            Reference to the signal
 * @param DefaultParameter  Parameter to give for slots, where parameter is set to null
 */
//==========================================================================================================================================
void oC_Signal_SetDefaultParameter( oC_Signal_t * Signal , const void * DefaultParameter )
{
    if ( Signal && DefaultParameter )
    {
        Signal->DefaultParameter       = DefaultParameter;
    }
}

//==========================================================================================================================================
/**
 * Emits signal and execute slots connected to it.
 *
 * @param Signal            Reference to the signal object
 * @param outTaskWoken      [out] Flag if task was woken, it should be used only in interrupts, otherwise should be set to NULL
 */
//==========================================================================================================================================
bool oC_Signal_EmitSignal( oC_Signal_t * Signal , bool * outTaskWoken)
{
    bool result = false;

    if ( Signal )
    {
        if ( oC_Semaphore_Give( Signal->Semaphore , outTaskWoken ) )
        {
            result = true;
        }

        oC_DLIST_foreach( _SlotContainer_t* , slotContainer , Signal->Slots )
        {
            slotContainer->Slot( slotContainer->Parameter );
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Waits for emission of the signal.
 *
 * @param Signal            Reference to the signal object
 * @param Timeout           Maximum time to wait into the signal
 *
 * @return 'true' if the signal was emitted
 */
//==========================================================================================================================================
bool oC_Signal_WaitForSignal( oC_Signal_t * Signal , oC_TickType_t Timeout )
{
    bool result = false;

    if ( Signal )
    {
        if ( oC_Semaphore_Take( Signal->Semaphore , Timeout ) )
        {
            result = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Connects a slot-function to the signal.
 *
 * @param Signal            Reference to the signal object
 * @param Slot              Function to call when signal is emitted
 * @param Parameter         Parameter to give for slot, when signal is emitted
 */
//==========================================================================================================================================
void oC_Signal_Connect( oC_Signal_t * Signal , oC_Signal_Slot_t Slot , const void * Parameter )
{
    if ( Signal )
    {
        _SlotContainer_t * container = malloc( sizeof(_SlotContainer_t) );

        if ( container )
        {
            container->Parameter= (Parameter == NULL) ? Signal->DefaultParameter : Parameter;
            container->Slot     = Slot;

            oC_DLIST_EmplacePushBack( Signal->Slots , &container , sizeof(_SlotContainer_t*) );
        }
    }
}

//==========================================================================================================================================
/**
 * Disconnects a slot from the signal
 *
 * @param Signal            Reference to the signal object
 * @param Slot              Slot to remove from the signal slots list
 */
//==========================================================================================================================================
void oC_Signal_Disconnect( oC_Signal_t * Signal , oC_Signal_Slot_t Slot )
{
    if ( Signal )
    {
        oC_DLIST_foreach( _SlotContainer_t* , container , Signal->Slots )
        {
            if ( container->Slot == Slot )
            {
                oC_DLIST_DeleteElement( _element , true );
                break;
            }
        }
    }
}

//==========================================================================================================================================
/**
 * Creates new event object
 *
 * @return NULL if error
 */
//==========================================================================================================================================
oC_Event_t* oC_Event_New( void )
{
    oC_Event_t * event = malloc( sizeof(oC_Event_t) );

    if ( event )
    {
        event->SemaphoreActive  = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 );
        event->SemaphoreInactive= oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 );
        event->Active           = false;

        if ( event->SemaphoreActive == NULL || event->SemaphoreInactive == NULL )
        {
            oC_Semaphore_Delete( event->SemaphoreActive );
            oC_Semaphore_Delete( event->SemaphoreInactive );
            free(event);
            event   = NULL;
        }
    }

    return event;
}

//==========================================================================================================================================
/**
 * Deletes an event object
 *
 * @param Event         Reference to the event object
 *
 * @return false if error
 */
//==========================================================================================================================================
bool oC_Event_Delete( oC_Event_t * Event )
{
    bool result = false;

    if ( Event )
    {
        oC_Semaphore_Delete(Event->SemaphoreActive);
        oC_Semaphore_Delete(Event->SemaphoreInactive);

        free(Event);

        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Sets the event as active
 *
 * @param Event         Reference to the event object
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Event_SetActive( oC_Event_t * Event )
{
    bool result = false;

    if ( Event )
    {
        Event->Active   = true;
        result = oC_Semaphore_Give( Event->SemaphoreActive , NULL );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Sets the event as inactive
 *
 * @param Event         Reference to the event object
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Event_SetInactive( oC_Event_t * Event )
{
    bool result = false;

    if ( Event )
    {
        Event->Active   = false;
        result = oC_Semaphore_Give( Event->SemaphoreInactive , NULL );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Checks if the event is active
 *
 * @param Event         Reference to the event object
 *
 * @return true if active
 */
//==========================================================================================================================================
bool oC_Event_IsActive( oC_Event_t * Event )
{
    return Event->Active;
}

//==========================================================================================================================================
/**
 * Waits for activation of the event.
 *
 * @param Event         Reference to the event
 * @param Timeout       maximum time to wait (in ticks)
 *
 * @return false if event is not active after timeout
 */
//==========================================================================================================================================
bool oC_Event_WaitForEventActive( oC_Event_t * Event , oC_TickType_t Timeout )
{
    bool result = false;

    if ( Event )
    {
        if ( Event->Active )
        {
            result = true;
        }
        else if ( oC_Semaphore_Take( Event->SemaphoreActive , Timeout ) )
        {
            oC_Semaphore_Give( Event->SemaphoreActive , NULL );
            result = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Waits for inactivation of the event.
 *
 * @param Event         Reference to the event
 * @param Timeout       maximum time to wait (in ticks)
 *
 * @return false if event is not active after timeout
 */
//==========================================================================================================================================
bool oC_Event_WaitForEventInactive( oC_Event_t * Event , oC_TickType_t Timeout )
{
    bool result = false;

    if ( Event )
    {
        if ( Event->Active == false)
        {
            result = true;
        }
        else if ( oC_Semaphore_Take( Event->SemaphoreInactive , Timeout ) )
        {
            oC_Semaphore_Give( Event->SemaphoreInactive , NULL );
            result = true;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Creates new queue object
 *
 * @param Length        Length of the queue
 * @param ItemSize      Size of each element
 *
 * @return reference to the queue object, or NULL if error
 */
//==========================================================================================================================================
oC_Queue_t* oC_Queue_New( uint32_t Length , size_t ItemSize )
{
    oC_Queue_t * queue = NULL;

    if ( Length > 0 && ItemSize > 0 )
    {
        queue = malloc( sizeof(oC_Queue_t) );

        if ( queue )
        {
            queue->Data     = malloc( ItemSize * Length );

            if ( queue->Data )
            {
                queue->DataAvailable    = oC_Event_New();
                queue->Full             = oC_Event_New();

                if ( queue->DataAvailable && queue->Full )
                {
                    oC_Event_SetInactive( queue->DataAvailable );
                    oC_Event_SetInactive( queue->Full );

                    queue->GetIndex         = 0;
                    queue->PutIndex         = 0;
                    queue->Length           = Length;
                    queue->NumberOfElements = 0;
                    queue->ElementSize      = ItemSize;
                }
                else
                {
                    if ( queue->DataAvailable ) oC_Event_Delete( queue->DataAvailable );
                    if ( queue->Full )          oC_Event_Delete( queue->Full );
                    free(queue->Data);
                    free(queue);
                    queue = NULL;
                }

            }
            else
            {
                free(queue);
                queue = NULL;
            }
        }
    }

    return queue;
}

//==========================================================================================================================================
/**
 * Deletes queue object
 *
 * @param Queue         Reference to the queue object
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Queue_Delete( oC_Queue_t * Queue )
{
    bool result = false;

    if ( Queue )
    {
        if ( Queue->Data ) free( Queue->Data );
        oC_Event_Delete( Queue->DataAvailable ) &&
        oC_Event_Delete( Queue->Full );
        free( Queue );

        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Waits and reads data from the queue.
 *
 * @param Queue         Reference to the queue object
 * @param Timeout       Maximum time to wait for data
 * @param outData       [out] Reference to the data destination
 *
 * @return true if data received
 */
//==========================================================================================================================================
bool oC_Queue_GetData( oC_Queue_t * Queue , oC_TickType_t Timeout , void * outData )
{
    bool result = false;

    if ( Queue && outData && oC_Event_WaitForEventActive( Queue->DataAvailable , Timeout ) )
    {
        memcpy( outData , &Queue->Data[ Queue->GetIndex * Queue->ElementSize ] , Queue->ElementSize );

        Queue->GetIndex = (Queue->GetIndex + 1) % Queue->Length;
        Queue->NumberOfElements--;

        oC_Event_SetInactive( Queue->Full );

        if ( oC_Queue_IsEmpty(Queue) )
        {
            oC_Event_SetInactive( Queue->DataAvailable );
        }

        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Puts data to the queue.
 *
 * @param Queue         Reference to the queue object
 * @param Timeout       Maximum time to wait for queue available
 * @param Data          Data to put into the queue
 *
 * @return true if success, false if queue is full after the Timeout time
 */
//==========================================================================================================================================
bool oC_Queue_PutData( oC_Queue_t * Queue , oC_TickType_t Timeout , const void * Data )
{
    bool result = false;

    if ( Queue && Data && oC_Event_WaitForEventInactive( Queue->Full , Timeout ) )
    {
        memcpy( &Queue->Data[Queue->PutIndex * Queue->ElementSize] , Data , Queue->ElementSize );

        Queue->PutIndex = (Queue->PutIndex + 1) % Queue->Length;
        Queue->NumberOfElements++;

        oC_Event_SetActive( Queue->DataAvailable );

        if ( oC_Queue_IsFull(Queue) )
        {
            oC_Event_SetActive( Queue->Full );
        }

        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Checks if queue is full
 *
 * @param Queue         Reference to the queue object
 *
 * @return true if queue is correct and full
 */
//==========================================================================================================================================
bool oC_Queue_IsFull( oC_Queue_t * Queue )
{
    return Queue && Queue->NumberOfElements >= Queue->Length;
}

//==========================================================================================================================================
/**
 * Checks if queue is empty
 *
 * @param Queue         Reference to the queue object
 *
 * @return true if queue is correct and empty
 */
//==========================================================================================================================================
bool oC_Queue_IsEmpty( oC_Queue_t * Queue )
{
    return Queue && Queue->NumberOfElements == 0;
}


/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
