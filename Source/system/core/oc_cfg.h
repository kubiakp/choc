/** ****************************************************************************************************************************************
 *
 * @file       oc_cfg.h
 *
 * @brief      Helper macros for configurations files
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 22:29:15) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup CFG Configuration
 * @{
 * To see details check #CFG
 * @page CFG
 * The ChocoOS system contains many configure definitions, that can be set to some kind of variables by user. All configuration files
 * are placed in the <b>config</b> directory. <br />
 * <br />
 * There are many definitions to set, but following rules helps you to quickly learn how to use them. First of all note, that
 * each configuration definition begins from <b>CFG_</b> prefix, for example #CFG_ENABLE_ASSERTIONS. This is easy way to distinguish
 * the system configuration from other definitions. Second part of configuration names is a type of a definition, for example <b>ENABLE</b>
 * in the #CFG_ENABLE_DEBUG_PRINT definition. Thanks to that you will know, what values can you assign to the definition. <br />
 * Here you have list of configuration types:<br />
 *              Type    | Values            | Description
 *              --------|-------------------|------------------------------------------------------------------------
 *              ENABLE  | ON / OFF          | Enables or disables functionality.
 *              LEVEL   | LEVEL( Lvl )      | Determines level of a functionality. To set you should use macro LEVEL
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_CFG_H_
#define SYSTEM_CORE_OC_CFG_H_

#include <stdbool.h>

/**
 * @brief Function is turned on
 */
#define ON      2
/**
 * @brief Function is turned off
 */
#define OFF     1

/**
 * @brief Function is set to LEVEL
 */
#define LEVEL(Lvl)          Lvl

/**
 * @}
 */
#endif /* SYSTEM_CORE_OC_CFG_H_ */
