/** ****************************************************************************************************************************************
 *
 * @file          oc_debug.c
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 25 mar 2015 08:25:21) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_debug.h>
#include <stdio.h>

/** ****************************************************************************************************************************************
 * The section interface functions
 */
#define _________________________________________INTERFACE_FUNCTIOS_SECTION_________________________________________________________________

//==========================================================================================================================================
/**
 * The function is for printing debug messages, only in case when the #CFG_ENABLE_DEBUG_PRINT definition in #oc_debug_cfg.h is set to ON
 *
 * @param Format        Format to print
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
int _Debug ( char * Format , ... )
{
    int sentBytes = 0;
#if CFG_ENABLE_DEBUG_PRINT == ON
#else
#endif
    return sentBytes;
}

/* END OF SECTION */
#undef  _________________________________________INTERFACE_FUNCTIOS_SECTION_________________________________________________________________
