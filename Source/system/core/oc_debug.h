/** ****************************************************************************************************************************************
 *
 * @file          oc_debug.h
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 22:15:56) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_OC_DEBUG_H_
#define SYSTEM_CORE_OC_DEBUG_H_

#include <oc_debug_cfg.h>

#if !defined(CFG_ENABLE_DEBUG_PRINT)
#   error CFG_ENABLE_DEBUG_PRINT is not defined in oc_debug_cfg.h !
#elif CFG_ENABLE_DEBUG_PRINT == ON
#   define  oC_Debug(...)       _Debug( __VA_ARGS__ )
#else
#   define  oC_Debug(...)
#endif

extern int _Debug( char * Format , ... );

#endif /* SYSTEM_CORE_OC_DEBUG_H_ */
