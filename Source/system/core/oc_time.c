/** ****************************************************************************************************************************************
 *
 * @file       oc_time.c
 *
 * @brief      The file with interface functions for the time module
 *
 * @author     Patryk Kubiak - (Created on: 21 mar 2015 16:02:55) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_time.h>
#include <FreeRTOS.h>
#include <oc_dlist.h>
#include <oc_synch.h>
#include <oc_tim.h>
#include <oc_assert.h>

/** ****************************************************************************************************************************************
 * The section with local types
 */
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
typedef struct
{
    oC_Time_Stamp_t         TimeToWakeUp;
    oC_Semaphore_t *        Semaphore;
} WakeUp_t;

/* END OF SECTION */
#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with global variables
 */
#define _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

struct
{
    bool                    Configured;
    uint16_t                TimeStampUs;
    oC_Time_Stamp_t         TimeStamp;
    oC_TIM_Channel_t        Channel;
    /**
     * @brief list of threads to wake up (each #WakeUp_t type)
     */
    oC_DLIST_t *            WakeUpList;
    oC_Event_t *            ConfigurationPossibleEvent;
    oC_Event_t *            ConfigurationActiveEvent;
    uint32_t                LockNestingCounter;
} Time;

/* END OF SECTION */
#undef  _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with interrupts functions prototypes
 */
#define _________________________________________INTERRUPTS_FUNCTIONS_PROTOTYPES_SECTION____________________________________________________

static bool         InterruptHandler_1ms                ( void * );
static bool         InterruptHandler_1s                 ( void * );

/* END OF SECTION */
#undef  _________________________________________INTERRUPTS_FUNCTIONS_PROTOTYPES_SECTION____________________________________________________


/** ****************************************************************************************************************************************
 * The section with constant definitions
 */
#define _________________________________________CONSTANT_SECTION___________________________________________________________________________

static const oC_TIM_Config_t TIMConfig  = {
                                           .Channel             = oC_TIM_Channel_FirstAvailable ,
                                           .Size                = oC_TIM_Size_16Bits ,
                                           .TickSource          = oC_TIM_TickSource_MainClock ,
                                           .TickPeriod          = oC_TIM_TickPeriod_us(1) ,
                                           .CountsDir           = oC_TIM_CountsDir_DontMatter ,
                                           .Mode                = oC_TIM_Mode_Periodic ,
                                           .MaxValue            = 1000 ,
                                           .MatchValue          = 0 ,
                                           .MatchCallback       = oC_TIM_Callback_NotUsed ,
                                           .TimeoutCallback     = InterruptHandler_1ms
};

/* END OF SECTION */
#undef  _________________________________________CONSTANT_SECTION___________________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with prototypes of local functions
 */
#define _________________________________________LOCAL_FUNCTIONS_PROTOTYPES_SECTION_________________________________________________________

static inline void              UpdateTimeStamp             ( void );
static inline bool              IsModuleConfigured          ( void );
static inline void              LockTimeConfiguration       ( void );
static inline void              UnlockTimeConfiguration     ( void );
static inline void              BeginTimeConfiguration      ( void );
static inline void              FinishTimeConfiguration     ( void );
static inline oC_Time_Stamp_t   GetTimeStampOfMidnight      ( void );

/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTIONS_PROTOTYPES_SECTION_________________________________________________________


/** ****************************************************************************************************************************************
 * The section with interface functions of the module
 *
 * @addtogroup Time
 * @{
 */
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * The function turn on the module if it is already not running.
 *
 * @return code of error or oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Time_TurnOnModule( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCodeIfFalse( &errorCode , !IsModuleConfigured() , oC_ErrorCode_TimeAlreadyTurnedOn )
         && oC_AssignErrorCode( &errorCode , oC_TIM_Configure( &TIMConfig ) )
            )
    {
        if ( oC_AssignErrorCode( &errorCode , oC_TIM_ReadChannelOfConfiguration( &TIMConfig , &Time.Channel ) ) )
        {
            Time.WakeUpList                 = oC_DLIST_New();
            Time.ConfigurationPossibleEvent = oC_Event_New();
            Time.ConfigurationActiveEvent   = oC_Event_New();

            if (
                    oC_AssignErrorCodeIfFalse( &errorCode , Time.WakeUpList         != NULL , oC_ErrorCode_TimeCannotCreateWakeUpList )
                 && oC_AssignErrorCodeIfFalse( &errorCode ,  Time.ConfigurationPossibleEvent != NULL
                                                          && Time.ConfigurationActiveEvent   != NULL , oC_ErrorCode_TimeCannotCreateConfigEvent )
                    )
            {
                errorCode   = oC_ErrorCode_None;
            }

            oC_Event_SetInactive( Time.ConfigurationActiveEvent  );
            oC_Event_SetActive(   Time.ConfigurationPossibleEvent);

            Time.TimeStamp          = 0;
            Time.LockNestingCounter = 0;
            Time.Configured         = true;
        }
        else
        {
            oC_TIM_Unconfigure( &TIMConfig );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function turn off the module if it is already not running.
 *
 * @return code of error or oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Time_TurnOffModule( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCodeIfFalse( &errorCode , IsModuleConfigured() , oC_ErrorCode_TimeAlreadyTurnedOff )
         && oC_AssignErrorCode( &errorCode , oC_TIM_Unconfigure( &TIMConfig ) )
            )
    {
        errorCode   = oC_ErrorCode_None;
    }

    oC_DLIST_Delete( Time.WakeUpList );
    oC_Event_Delete( Time.ConfigurationPossibleEvent );
    oC_Event_Delete( Time.ConfigurationActiveEvent   );

    Time.WakeUpList = false;
    Time.Configured = false;

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function delays for the given microseconds.
 *
 * @param Microseconds      number of microseconds for delay
 */
//==========================================================================================================================================
void oC_Time_DelayFor_us( uint32_t Microseconds )
{
    if ( IsModuleConfigured() )
    {
//        LockTimeConfiguration();
        oC_Time_Stamp_t finishTimeStamp = oC_Time_GetTimeStamp() + oC_Time_usToTimeStamp(Microseconds);

        while(finishTimeStamp > oC_Time_GetTimeStamp());
//        UnlockTimeConfiguration();
    }
}

//==========================================================================================================================================
/**
 * The function delays for the given milliseconds
 *
 * @param Milliseconds time to delay
 */
//==========================================================================================================================================
void oC_Time_DelayFor_ms( uint32_t Milliseconds )
{
    if ( IsModuleConfigured() )
    {
        LockTimeConfiguration();
        oC_Time_Stamp_t finishTimeStamp = oC_Time_GetTimeStamp() + oC_Time_msToTimeStamp(Milliseconds);

        while(finishTimeStamp > oC_Time_GetTimeStamp());
        UnlockTimeConfiguration();
    }
}

//==========================================================================================================================================
/**
 * The function delays for the given seconds
 *
 * @param Seconds time to delay
 */
//==========================================================================================================================================
void oC_Time_DelayFor_s( uint32_t Seconds )
{
    if ( IsModuleConfigured() )
    {
        LockTimeConfiguration();
        oC_Time_Stamp_t finishTimeStamp = oC_Time_GetTimeStamp() + oC_Time_SecToTimeStamp(Seconds);

        while(finishTimeStamp > oC_Time_GetTimeStamp());
        UnlockTimeConfiguration();
    }
}

//==========================================================================================================================================
/**
 * Delays for the given time in minutes
 *
 * @param Minutes   time to delay
 */
//==========================================================================================================================================
void oC_Time_DelayFor_min( uint32_t Minutes )
{
    if ( IsModuleConfigured() )
    {
        LockTimeConfiguration();
        oC_Time_Stamp_t finishTimeStamp = oC_Time_GetTimeStamp() + oC_Time_MinToTimeStamp(Minutes);

        while(finishTimeStamp > oC_Time_GetTimeStamp());
        UnlockTimeConfiguration();
    }
}

//==========================================================================================================================================
/**
 * Delays for the given time in hours
 *
 * @param Hours     time to delay
 */
//==========================================================================================================================================
void oC_Time_DelayFor_hour( uint32_t Hours )
{
    if ( IsModuleConfigured() )
    {
        LockTimeConfiguration();
        oC_Time_Stamp_t finishTimeStamp = oC_Time_GetTimeStamp() + oC_Time_HoursToTimeStamp(Hours);

        while(finishTimeStamp > oC_Time_GetTimeStamp());
        UnlockTimeConfiguration();
    }
}

//==========================================================================================================================================
/**
 * Returns current time stamp
 *
 * @return time stamp
 */
//==========================================================================================================================================
oC_Time_Stamp_t oC_Time_GetTimeStamp( void )
{
    oC_Time_Stamp_t timeStamp = 0;

    if ( IsModuleConfigured() )
    {
        UpdateTimeStamp();
        timeStamp   = Time.TimeStamp + Time.TimeStampUs;
    }

    return timeStamp;
}

//==========================================================================================================================================
/**
 * Sets time stamp
 *
 * @param TimeStamp         new TimeStamp to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Time_SetTimeStamp( oC_Time_Stamp_t TimeStamp )
{
    bool timeChanged = false;

    if( IsModuleConfigured() )
    {
        BeginTimeConfiguration( );
        Time.TimeStamp  = TimeStamp;
        timeChanged     = true;
        FinishTimeConfiguration();
    }

    return timeChanged;
}

//==========================================================================================================================================
/**
 * Returns current time
 *
 * @return structure with current time
 */
//==========================================================================================================================================
oC_Time_t oC_Time_GetCurrentTime( void )
{
    oC_Time_t currentTime = { 0 , 0 , 0 };

    if ( IsModuleConfigured() )
    {
        currentTime = oC_Time_TimeStampToTime( Time.TimeStamp );
    }

    return currentTime;
}

//==========================================================================================================================================
/**
 * Returns current time
 *
 * @return structure with current time
 */
//==========================================================================================================================================
oC_Time_Extended_t oC_Time_GetCurrentTimeExtended( void )
{
    oC_Time_Extended_t currentTime = { 0 , 0 , 0 , 0 , 0 };

    if ( IsModuleConfigured() )
    {
        currentTime = oC_Time_TimeStampToTimeExtended( Time.TimeStamp );
    }

    return currentTime;
}

//==========================================================================================================================================
/**
 * Sets current time
 *
 * @param CurrentTime       current time (maximum 24 h) to set
 *
 * @return true if time changed
 */
//==========================================================================================================================================
bool oC_Time_SetCurrentTime( oC_Time_t CurrentTime )
{
    bool timeChanged = false;

    if ( IsModuleConfigured() && oC_Time_IsTimeCorrect(CurrentTime) )
    {
        BeginTimeConfiguration();
        oC_Time_Stamp_t timeStamp   = GetTimeStampOfMidnight() + oC_Time_TimeToTimeStamp(CurrentTime);
        Time.TimeStamp              = timeStamp;
        FinishTimeConfiguration();
        timeChanged                 = true;
    }

    return timeChanged;
}

//==========================================================================================================================================
/**
 * Sets current time
 *
 * @param CurrentTime       current time (maximum 24 h) to set
 *
 * @return true if time changed
 */
//==========================================================================================================================================
bool oC_Time_SetCurrentTimeExtended( oC_Time_Extended_t CurrentTime )
{
    bool timeChanged = false;

    if ( IsModuleConfigured() && oC_Time_IsTimeExtendedCorrect(CurrentTime) )
    {
        BeginTimeConfiguration();
        oC_Time_Stamp_t timeStamp   = GetTimeStampOfMidnight() + oC_Time_TimeExtendedToTimeStamp(CurrentTime);
        Time.TimeStamp              = timeStamp;
        FinishTimeConfiguration();
        timeChanged                 = true;
    }

    return timeChanged;
}

//==========================================================================================================================================
/**
 * Sleeps current thread for the given time
 *
 * @param Milliseconds  time to sleep in milliseconds
 */
//==========================================================================================================================================
void oC_Time_SleepFor_msec( uint32_t Milliseconds )
{
    vTaskDelay( oC_CONVERT_TIME_MS_TO_TICKS(Milliseconds) );
}

//==========================================================================================================================================
/**
 * Sleeps current thread for the given time
 *
 * @param Seconds time to sleep
 */
//==========================================================================================================================================
void oC_Time_SleepFor_sec( uint32_t Seconds )
{
    if ( IsModuleConfigured() )
    {
        LockTimeConfiguration();
        oC_Time_Stamp_t finishTimeStamp = oC_Time_GetTimeStamp() + oC_Time_SecToTimeStamp( Seconds );
        while(finishTimeStamp > oC_Time_GetTimeStamp())
        {
            oC_Time_SleepFor_msec(1000);
        }
        UnlockTimeConfiguration();
    }
    else
    {
        volatile uint32_t secondsLeft = Seconds;

        while( secondsLeft > 0 )
        {
            oC_Time_SleepFor_msec( 1000 );
            secondsLeft--;
        }
    }
}

//==========================================================================================================================================
/**
 * Sleeps current thread for the given time
 *
 * @param Minutes time to sleep
 */
//==========================================================================================================================================
void oC_Time_SleepFor_min( uint32_t Minutes )
{
    volatile uint32_t minutesLeft = Minutes;

    while( minutesLeft > 0 )
    {
        oC_Time_SleepFor_msec(60000);
        minutesLeft--;
    }
}

//==========================================================================================================================================
/**
 * Sleeps current thread for the given time
 *
 * @param Hours     time to sleep
 */
//==========================================================================================================================================
void oC_Time_SleepFor_hour( uint32_t Hours )
{
    volatile uint32_t hoursLeft = Hours;

    while( hoursLeft > 0 )
    {
        oC_Time_SleepFor_min(60);
        hoursLeft--;
    }
}

//==========================================================================================================================================
/**
 * Sleeps current thread for the given time
 *
 * @param TimeToSleep      time to sleep
 */
//==========================================================================================================================================
void oC_Time_SleepForTime( oC_Time_t TimeToSleep )
{
    oC_Time_SleepFor_hour(TimeToSleep.h);
    oC_Time_SleepFor_min(TimeToSleep.min);
    oC_Time_SleepFor_sec(TimeToSleep.sec);
}

//==========================================================================================================================================
/**
 * Sleps current thread and wake it at the given time
 *
 * @param TimeToWakeUp
 *
 * @return true if it was woken up
 */
//==========================================================================================================================================
bool oC_Time_WakeUpAtTime( oC_Time_t WakeUpTime )
{
    bool wokenUp = false;

    if ( IsModuleConfigured() )
    {
        WakeUp_t wakeUp;

        wakeUp.TimeToWakeUp = oC_Time_TimeToTimeStamp(WakeUpTime);
        wakeUp.Semaphore    = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 );

        if ( oC_DLIST_PushBack( Time.WakeUpList , &wakeUp , sizeof(WakeUp_t) ) )
        {
            wokenUp = oC_Semaphore_Take( wakeUp.Semaphore , oC_UINT32_MAX );

            oC_DLIST_Element_t * element = oC_DLIST_GetElementOfValue( Time.WakeUpList , &wakeUp );

            oC_DLIST_TakeOffElement( Time.WakeUpList , element );
        }

        oC_Semaphore_Delete( wakeUp.Semaphore );
    }

    return wokenUp;
}

/** @} */
/* END OF SECTION */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with interrupts handlers
 */
#define _________________________________________INTERRUPT_HANDLERS_SECTION_________________________________________________________________

//==========================================================================================================================================
/**
 * Interrupt handler which should be called with 1 ms period.
 *
 * @param __ not used param
 *
 * @return true if task was woken
 */
//==========================================================================================================================================
static bool InterruptHandler_1ms( void * p)
{
    bool taskWoken = false;

    oC_UNUSED_ARG(p);

    Time.TimeStamp += 1000;

    if ( (Time.TimeStamp % 1000000) == 0 )
    {
        taskWoken = InterruptHandler_1s(NULL);
    }

    return taskWoken;
}

//==========================================================================================================================================
/**
 * Interrupt handler which should be called with 1 second period.
 *
 * @param __ not used parameter
 *
 * @return true if task was woken
 */
//==========================================================================================================================================
static bool InterruptHandler_1s( void * p)
{
    bool taskWoken  = false;

    oC_UNUSED_ARG(p);

    foreach( WakeUp_t , wakeUp , Time.WakeUpList )
    {
        if ( wakeUp.TimeToWakeUp <= Time.TimeStamp )
        {
            oC_Semaphore_Give( wakeUp.Semaphore , &taskWoken );
        }
    }

    return taskWoken;
}

/* END OF SECTION */
#undef  _________________________________________INTERRUPT_HANDLERS_SECTION_________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with local functions
 */
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * Updates value of the current time stamp.
 */
//==========================================================================================================================================
static inline void UpdateTimeStamp( void )
{
//    LockTimeConfiguration();
    Time.TimeStampUs = 1000 - oC_TIM_GetValue( Time.Channel );
//    UnlockTimeConfiguration();
}

//==========================================================================================================================================
/**
 * Checks if the time module is configured.
 *
 * @return true if module is configured
 */
//==========================================================================================================================================
static inline bool IsModuleConfigured( void )
{
    return Time.Configured;
}

//==========================================================================================================================================
/**
 * Locking time configuration
 */
//==========================================================================================================================================
static inline void LockTimeConfiguration( void )
{
    oC_Event_WaitForEventInactive( Time.ConfigurationActiveEvent , oC_UINT32_MAX);
    oC_Event_SetInactive( Time.ConfigurationPossibleEvent );

    Time.LockNestingCounter++;

    /**
     * Assertion description:
     *
     * Oh, god!! More than 0xFFFFFFFFU threads is locking configurations.. It is not possible! I think...
     */
    oC_ASSERT( Time.LockNestingCounter < oC_UINT32_MAX );
}

//==========================================================================================================================================
/**
 * Unlocking time configuration
 */
//==========================================================================================================================================
static inline void UnlockTimeConfiguration( void )
{
    Time.LockNestingCounter--;

    if ( Time.LockNestingCounter == 0 )
    {
        oC_Event_SetActive( Time.ConfigurationPossibleEvent );
    }
}

//==========================================================================================================================================
/**
 * Begins time configuration ( locking delays )
 */
//==========================================================================================================================================
static inline void BeginTimeConfiguration( void )
{
    oC_Event_WaitForEventActive( Time.ConfigurationPossibleEvent , oC_UINT32_MAX );
    oC_Event_SetActive( Time.ConfigurationActiveEvent );
}

//==========================================================================================================================================
/**
 * Finishes time configuration ( unlocking delays )
 */
//==========================================================================================================================================
static inline void FinishTimeConfiguration( void )
{
    oC_Event_SetInactive( Time.ConfigurationActiveEvent );
}

//==========================================================================================================================================
/**
 * Calculate time stamp of today midnight (a 00:00:00 time of today)
 *
 * @return midnight time stamp
 */
//==========================================================================================================================================
static inline oC_Time_Stamp_t GetTimeStampOfMidnight( void )
{
    oC_Time_Stamp_t currentTimeStamp = Time.TimeStamp;
    oC_Time_t currentTime            = oC_Time_GetCurrentTime();
    oC_Time_Stamp_t midnightTimeStamp= currentTimeStamp - oC_Time_TimeToTimeStamp( currentTime );

    return midnightTimeStamp;
}

/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
