/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_os.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 8 sty 2015 13:12:34) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_os.h>
#include <oc_synch.h>
#include <oc_programs_cfg.h>
#include <oc_general_cfg.h>
#include <FreeRTOS.h>
#include <task.h>
#include <oc_time.h>
#include <oc_drivers.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

typedef struct
{
    oC_OS_MainFunction_t    MainFunction ;
    int                     Argc;
    char **                 Argv;
    int *                   outReturn;
    oC_OS_PID_t             PID;
} Program_t;

typedef struct
{
    oC_OS_CyclicFunction_t  CyclicFunction;
    void*                   Parameter;
    oC_OS_Tick_t            Delay;
    oC_OS_Thread_t *        Thread;
} Cyclic_t;

typedef struct
{
    oC_DLIST_t*             Globals;
    oC_OS_Priority_t        Priority;
    oC_OS_PID_t             PID;
    oC_OS_PID_t             ParentPID;
    oC_DLIST_t*             Threads;
    oC_OS_ProcessState_t    State;
    oC_Signal_t *           Finished;
    Program_t*              MainProgram;
} Process_t;

typedef struct
{
    TaskHandle_t            TaskHandle;
    oC_OS_PID_t             PID;
    oC_OS_Thread_t *        Thread;
} TaskPID_t;

typedef struct
{
    void*                   Destination;
    void*                   Value;
    size_t                  Size;
} Global_t;

typedef struct
{
    oC_OS_MainFunction_t    MainFunction;
    oC_OS_StackSize_t       StackSize;
    oC_STRING_t             Name;
} ProgramRegistration_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define _ADD_PROGRAM_TO_REGISTER( PROGRAM_MAIN , NAME , STACK_SIZE )       oC_OS_Program_Register( PROGRAM_MAIN , NAME , STACK_SIZE );
#define _ADD_DRIVER_TO_SEND_LIST( DRIVER_NAME )
#define _DONT_ADD(...)

/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Container object for the OS module
 */
//==========================================================================================================================================
struct
{
    oC_DLIST_t *            Processes;                  /**< List of processes (each 'Process_t *' type) */
    oC_DLIST_t *            ProgramsRegistrations;      /**< List of program registrations (each 'ProgramRegistration_t*' type) */
    oC_DLIST_t *            Tasks;                      /**< List of tasks running in the system (each 'TaskPID_t*' type) */
    oC_OS_PID_t             CurrentPID;                 /**< ID of current process */
    oC_OS_PID_t             MasterPID;                  /**< ID of master process */
    oC_OS_PID_t             CyclicPID;                  /**< ID of process for handling cyclic functions */
    oC_DLIST_t*             Cyclics;                    /**< List of cyclic functions (each 'Cyclic_t *' type) */
    oC_TTY_t*               DefaultTTY;                 /**< Reference to the default TTY object */
} OS;

//==========================================================================================================================================
/**
 * Configuration for default TTY
 */
//==========================================================================================================================================
const oC_TTY_Config_t _TTYDefaultConfig =   oC_TTY_Config_(
                                                          oC_TTY_DEFAULT_STDOUT_DRIVER , oC_TTY_DEFAULT_STDOUT_CONFIG_NAME  , oC_TTY_DEFAULT_STDOUT_TERMINAL_STANDARD , oC_TTY_DEFAULT_STDOUT_TIMEOUT ,
                                                          oC_TTY_DEFAULT_STDIN_DRIVER  , oC_TTY_DEFAULT_STDIN_CONFIG_NAME   , oC_TTY_DEFAULT_STDIN_TERMINAL_STANDARD  , oC_TTY_DEFAULT_STDIN_TIMEOUT  ,
                                                          oC_TTY_DEFAULT_STDERR_DRIVER , oC_TTY_DEFAULT_STDERR_CONFIG_NAME  , oC_TTY_DEFAULT_STDERR_TERMINAL_STANDARD , oC_TTY_DEFAULT_STDERR_TIMEOUT
                                                          );

extern const char __rom_size;
extern const char __rom_start;
extern const char __rom_end;
extern const char __ram_size;
extern const char __ram_start;
extern const char __ram_end;
extern const char __text_size;
extern const char __data_size;
extern const char __stack_size;
extern const char __heap_size;

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static inline oC_OS_PID_t               _CountNewPID( void );
static inline Process_t*                _GetProcess( oC_OS_PID_t PID );
static inline void                      _DeleteProcess( Process_t * Process );
static inline void                      _DeleteCyclic( Cyclic_t * Cyclic );
static        void                      _ProgramThread( Program_t * Program );
static        void                      _CyclicThread( Cyclic_t * Cyclic );
static        ProgramRegistration_t*    _GetProgramRegistration( oC_OS_MainFunction_t MainFunction );
static        void                      _SwitchContext( oC_OS_PID_t NewPID );
static        bool                      _SearchNewThreadParentForTTY( oC_TTY_t * TTY );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Initialization of the OS
 */
//==========================================================================================================================================
void oC_OS_Initialize( void )
{
    OS.Processes                = oC_DLIST_New();
    OS.ProgramsRegistrations    = oC_DLIST_New();
    OS.Tasks                    = oC_DLIST_New();
    OS.CurrentPID               = 0;
    OS.MasterPID                = 0;
    OS.CyclicPID                = 0;
    OS.Cyclics                  = oC_DLIST_New();
    OS.DefaultTTY               = oC_TTY_New( &_TTYDefaultConfig , NULL );

}

//==========================================================================================================================================
/**
 * Deinitialization of the OS
 */
//==========================================================================================================================================
void oC_OS_Deinitialize( void )
{
    foreach( Process_t * , process , OS.Processes )
    {
        oC_OS_Process_Kill( process->PID );
    }

    foreach( ProgramRegistration_t * , preg , OS.ProgramsRegistrations )
    {
        oC_OS_Program_Unregister( preg->MainFunction );
    }

    foreach( Cyclic_t *  , cyclic , OS.Cyclics )
    {
        _DeleteCyclic( cyclic );
    }

    oC_DLIST_Delete( OS.Processes );
    oC_DLIST_Delete( OS.ProgramsRegistrations );
    oC_DLIST_Delete( OS.Tasks );
    oC_DLIST_Delete( OS.Cyclics );

    oC_Time_TurnOffModule();
}


//==========================================================================================================================================
/**
 *  Register all programs from list
 */
//==========================================================================================================================================
void oC_OS_RegisterPrograms( void )
{
    oC_SYSTEM_PROGRAMS_LIST( _ADD_PROGRAM_TO_REGISTER , _DONT_ADD );
    oC_USER_PROGRAMS_LIST( _ADD_PROGRAM_TO_REGISTER , _DONT_ADD );
}

//==========================================================================================================================================
/**
 * Starts scheduling tasks
 */
//==========================================================================================================================================
void oC_OS_StartScheduler( void )
{
    OS.CyclicPID    = oC_OS_Process_Create( oC_CYCLIC_PRIORITY );
    vTaskStartScheduler();
}

//==========================================================================================================================================
/**
 * Creates new process and add it into processes list
 *
 * @param Priority          Priority of the process
 *
 * @return Process ID, or 0 if error
 */
//==========================================================================================================================================
oC_OS_PID_t oC_OS_Process_Create( oC_OS_Priority_t Priority )
{
    oC_OS_PID_t pid = 0;
    Process_t * process = malloc( sizeof(Process_t) );

    if ( process )
    {
        process->Globals    = oC_DLIST_New();
        process->Priority   = Priority;
        process->PID        = _CountNewPID();
        process->ParentPID  = OS.CurrentPID;
        process->Threads    = oC_DLIST_New();
        process->State      = oC_OS_ProcessState_Normal;
        process->Finished   = oC_Signal_New( NULL );
        process->MainProgram= NULL;

        if ( process->Globals != NULL && process->Finished != NULL && process->PID > 0 && oC_DLIST_EmplacePushBack( OS.Processes , &process , sizeof(Process_t*) ) )
        {
            pid             = process->PID;

            if ( OS.MasterPID == 0 )
            {
                OS.MasterPID    = pid;
            }
        }
        else
        {
            oC_Signal_Delete(process->Finished);
            oC_DLIST_Delete( process->Threads );
            oC_DLIST_Delete( process->Globals );
            free(process);
        }
    }


    return pid;
}

//==========================================================================================================================================
/**
 * Kills a process and child processes
 *
 * @param PID               ID of the process to kill
 *
 * @return code of error if process cannot be killed, or #oC_ErrorCode_None if process killed
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_Process_Kill( oC_OS_PID_t PID )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_OSPIDNotExists;

    foreach( Process_t * , process , OS.Processes )
    {
        if ( process->ParentPID == PID )
        {
            if ( oC_OS_Process_Kill( process->PID ) != oC_ErrorCode_None )
            {
                errorCode   = oC_ErrorCode_OSCannotKillChildProcess;
            }
        }
        if ( process->PID == PID && process->State == oC_OS_ProcessState_Normal )
        {
            bool allThreadsCancelled   = true;

            if ( errorCode != oC_ErrorCode_OSPIDNotExists && errorCode != oC_ErrorCode_OSCannotKillChildProcess )
            {
                errorCode   = oC_ErrorCode_OSDuplicatePID;
            }

            foreach( oC_OS_Thread_t* , thread , process->Threads )
            {
                oC_OS_Thread_Cancel( thread );

                if ( !oC_DLIST_TakeOffElement( process->Threads , _element ) )
                {
                    allThreadsCancelled    = false;
                }
                else
                {
                    oC_DLIST_DeleteElement( _element , true );
                }
            }

            if ( allThreadsCancelled )
            {
                oC_DLIST_TakeOffElement( OS.Processes ,  _element );
                oC_DLIST_DeleteElement( _element , true );
                _DeleteProcess( process );
                errorCode       = oC_ErrorCode_None;
            }
            else
            {
                process->State  = oC_OS_ProcessState_Zombie;
                errorCode       = oC_ErrorCode_OSNotAllThreadsCancelled;
            }

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Waits for finish of a process with given ID
 *
 * @param PID               ID of process to waiting for
 * @param Timeout           Maximum time in ticks for waiting
 *
 * @return error of code or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_Process_WaitForFinish( oC_OS_PID_t PID , oC_OS_Tick_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_OSPIDNotExists;

    Process_t * process = _GetProcess( PID );

    if ( process )
    {
        if ( oC_Signal_WaitForSignal( process->Finished , Timeout ) )
        {
            errorCode   = oC_ErrorCode_None;
        }
        else
        {
            errorCode   = oC_ErrorCode_OSProcessNotFinished;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Returns active process ID
 *
 * @return id of a current process
 */
//==========================================================================================================================================
oC_OS_PID_t oC_OS_Process_Current( void )
{
    return OS.CurrentPID;
}

//==========================================================================================================================================
/**
 * Creates new thread for current process. It cannot be called when there is no process active
 *
 * @param PID                   ID of the process to create thread for (set it to 0 if thread should be created for current process)
 * @param Name                  Name for the thread
 * @param ThreadFunction        Address of the thread function
 * @param Parameter             Parameter which should be given for thread
 * @param StackSize             Size of the stack
 * @param Priority              Priority of the thread. If priority should be inherited from process, set it to #oC_OS_Priority_Parent
 *                              It can be also set the higher or lower priority than process using oC_OS_Priority_Parent +/- 10
 * @param TTY                   Reference to the TTY object (NULL if should be inherited)
 *
 * @return NULL if error, reference to the thread handle if success

   @example oC_OS_Thread_Create "Creating thread"
   @code
       void ThreadFunction( void * Parameter )
       {
           while(1);
       }

       int main(int Argc , char ** Argv )
       {
           oC_OS_Thread_t* thread = oC_OS_Thread_Create( ThreadFunction , NULL , oC_OS_StackSize_Minimum , oC_OS_Priority_Parent + 10 );

       }
   @endcode
 *
 */
//==========================================================================================================================================
oC_OS_Thread_t* oC_OS_Thread_Create( oC_OS_PID_t PID, const oC_STRING_t Name , oC_OS_ThreadFunction_t ThreadFunction , void * Parameter , oC_OS_StackSize_t StackSize , oC_OS_Priority_t Priority , oC_TTY_t * TTY )
{
    oC_OS_Thread_t* thread = NULL;
    Process_t * process    = _GetProcess( (PID == 0) ? OS.CurrentPID : PID );

    if ( process )
    {
        thread          = malloc( sizeof(oC_OS_Thread_t) );
        if ( thread )
        {
            oC_OS_Thread_t * currentThread = oC_OS_Thread_Current();

            if ( !oC_OS_IsAddressCorrect( TTY ) && oC_OS_IsAddressCorrect( currentThread ) )
            {
                thread->TTY         = currentThread->TTY;
            }
            else if ( oC_OS_IsAddressCorrect( TTY ) )
            {
                thread->TTY         = TTY;
            }
            else
            {
                thread->TTY         = OS.DefaultTTY;
            }

            thread->PID             = OS.CurrentPID;
            thread->ThreadFunction  = ThreadFunction;
            thread->Parameter       = Parameter;
            thread->IsTTYParent     = false;

            if ( Priority > oC_OS_Priority_Maximum )
            {
                Priority    = process->Priority + Priority - oC_OS_Priority_Parent;
            }

            TaskPID_t * taskPID     = malloc( sizeof(TaskPID_t) );

            if ( taskPID )
            {
                taskPID->PID        = thread->PID;
                taskPID->Thread     = thread;

                if ( xTaskCreate( ThreadFunction , Name , StackSize , Parameter , Priority , &thread->TaskHandle ) == pdPASS )
                {
                    taskPID->TaskHandle = thread->TaskHandle;

                    oC_DLIST_EmplacePushBack( process->Threads , &thread , sizeof(oC_OS_Thread_t*) );
                    oC_DLIST_EmplacePushBack( OS.Tasks , &taskPID , sizeof(TaskPID_t *) );
                }
                else
                {
                    free( taskPID );
                    free(thread);
                    thread      = NULL;
                }

            }
            else
            {
                free(thread);
                thread          = NULL;
            }
        }
    }

    return thread;
}

//==========================================================================================================================================
/**
 * The function return reference to the current thread
 *
 * @return reference to the current thread, or NULL if there is no thread active
 */
//==========================================================================================================================================
oC_OS_Thread_t* oC_OS_Thread_Current( void )
{
    oC_OS_Thread_t* currentThread = NULL;
    TaskHandle_t taskHandle = xTaskGetCurrentTaskHandle();

    foreach( TaskPID_t * , taskPID , OS.Tasks )
    {
        if ( taskPID->TaskHandle == taskHandle )
        {
            currentThread   = taskPID->Thread;
            break;
        }
    }

    return currentThread;
}

//==========================================================================================================================================
/**
 * Cancel and delete thread
 *
 * @param Thread        Thread to cancel
 *
 */
//==========================================================================================================================================
void oC_OS_Thread_Cancel( oC_OS_Thread_t * Thread )
{
    if ( oC_OS_IsRAMAddress( Thread ) )
    {
        foreach( TaskPID_t* , task , OS.Tasks )
        {
            if ( task->TaskHandle == Thread->TaskHandle )
            {
                oC_DLIST_TakeOffElement( OS.Tasks , _element );
                oC_DLIST_DeleteElement( _element , true );
            }
        }

        vTaskDelete( Thread->TaskHandle );

        if ( Thread->IsTTYParent && !_SearchNewThreadParentForTTY( Thread->TTY ) )
        {
            oC_TTY_Delete( Thread->TTY );
        }

        if ( oC_OS_IsDynamicAllocatedAddress( Thread ) )
        {
            free( Thread );
        }
    }

}

//==========================================================================================================================================
/**
 * The function returns tty of the current thread
 *
 * @return refernec to the TTY object, or NULL if error
 */
//==========================================================================================================================================
oC_TTY_t* oC_OS_Thread_TTY( void )
{
   oC_TTY_t * tty = NULL;
   oC_OS_Thread_t * thread = oC_OS_Thread_Current();

   if ( thread )
   {
       tty      = thread->TTY;
   }

   return tty;
}

//==========================================================================================================================================
/**
 * Checks if the thread object is correct
 *
 * @param Thread            Reference to the thread object
 *
 * @return true if correct
 */
//==========================================================================================================================================
bool oC_OS_Thread_IsCorrect( oC_OS_Thread_t * Thread )
{
    return oC_OS_IsAddressCorrect( Thread ) && Thread->PID > 0 && oC_OS_IsAddressCorrect( Thread->TaskHandle );
}

//==========================================================================================================================================
/**
 * The function runs program in current process
 *
 * @param MainFunction      Address of a main program function
 * @param Argc              Arguments counter
 * @param Argv              Arguments array
 *
 * @return -69 if MainFunction is NULL, otherwise result of program
 */
//==========================================================================================================================================
int oC_OS_Program_Exec( oC_OS_MainFunction_t MainFunction , int Argc , char ** Argv )
{
    int result = -69;

    if ( MainFunction )
    {
        result = MainFunction( Argc , Argv );
    }

    return result;
}

//==========================================================================================================================================
/**
 * Execute program in new process
 *
 * @param MainFunction      Address of the function to
 * @param Argc              Arguments counter
 * @param Argv              Array of arguments
 * @param outReturn         Reference for return variable (NULL if not used)
 * @param TTY               TTY object for the program (NULL if should be inherited)
 * @param Priority          Priority of the process
 *
 * @return 0 if error, otherwise id of the process
 */
//==========================================================================================================================================
oC_OS_PID_t oC_OS_Program_ExecInNewProcess( oC_OS_MainFunction_t MainFunction , int Argc , char ** Argv , int * outReturn , oC_TTY_t * TTY , oC_OS_Priority_t Priority )
{
    oC_OS_PID_t pid = 0;

    if ( MainFunction )
    {
        Program_t * program = malloc( sizeof(Program_t) );

        if ( program )
        {
            program->MainFunction   = MainFunction;
            program->outReturn      = outReturn;
            program->Argc           = Argc;
            program->Argv           = Argv;
            program->PID            = oC_OS_Process_Create( Priority );

            if ( program->PID == 0 )
            {
                free(program);
            }
            else
            {
                ProgramRegistration_t * registration = _GetProgramRegistration( MainFunction );
                Process_t * process = _GetProcess( program->PID );

                if ( registration && process )
                {
                    process->MainProgram    = program;

                    if (oC_OS_Thread_Create( program->PID , registration->Name , (oC_OS_ThreadFunction_t)_ProgramThread , program , registration->StackSize , Priority , TTY ) )
                    {
                        pid         = program->PID;
                    }

                }
                else
                {
                    oC_OS_Process_Kill( program->PID );
                }
            }

        }
    }

    return pid;
}

//==========================================================================================================================================
/**
 * Registers program in memory
 *
 * @param MainFunction          Address of the main function
 * @param Name                  Name of the program
 * @param StackSize             stack size for the program
 *
 * @return true if registered
 */
//==========================================================================================================================================
bool oC_OS_Program_Register( oC_OS_MainFunction_t MainFunction , const oC_STRING_t Name , oC_OS_StackSize_t StackSize )
{
    bool programRegistered = false;

    foreach( ProgramRegistration_t * , preg , OS.ProgramsRegistrations )
    {
        if ( preg->MainFunction == MainFunction )
        {
            preg->Name          = oC_STRING_New(Name);
            preg->StackSize     = StackSize;
            programRegistered   = true;
        }
    }

    if ( !programRegistered )
    {
        ProgramRegistration_t * registration = malloc( sizeof(ProgramRegistration_t) );

        if ( registration )
        {
            registration->MainFunction  = MainFunction;
            registration->Name          = oC_STRING_New(Name);
            registration->StackSize     = StackSize;

            oC_DLIST_EmplacePushBack( OS.ProgramsRegistrations , &registration , sizeof(ProgramRegistration_t*) );

            programRegistered   = true;
        }
    }

    return programRegistered;
}

//==========================================================================================================================================
/**
 * Returns name of the program if it is registered.
 *
 * @param MainFunction      pointer of the main program function
 *
 * @return name of the program
 */
//==========================================================================================================================================
const char * oC_OS_Program_GetName( oC_OS_MainFunction_t MainFunction )
{
    const char * programName = "unknown program";

    foreach( ProgramRegistration_t * , preg , OS.ProgramsRegistrations )
    {
        if ( preg->MainFunction == MainFunction )
        {
            programName = preg->Name;
        }
    }

    return programName;
}

//==========================================================================================================================================
/**
 * Remove program registration from list
 *
 * @param MainFunction          Address of main function to unregister
 */
//==========================================================================================================================================
void oC_OS_Program_Unregister( oC_OS_MainFunction_t MainFunction )
{
    foreach( ProgramRegistration_t* , preg , OS.ProgramsRegistrations )
    {
        if ( preg->MainFunction == MainFunction )
        {
            oC_DLIST_TakeOffElement( OS.ProgramsRegistrations , _element );
            oC_DLIST_DeleteElement( _element , true );
        }
    }
}

//==========================================================================================================================================
/**
 * The function closes program and childrens processes
 *
 * @param PID               Program main process ID
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_Program_Close( oC_OS_PID_t PID )
{
    return oC_OS_Process_Kill( PID );
}

//==========================================================================================================================================
/**
 * The function starts execute cyclic functions
 *
 * @param CyclicFunction        Function to call periodically
 * @param Name                  Name of the function to call
 * @param Parameter             Parameter to give for the CyclicFunction
 * @param Period                Period in ticks to call
 * @param StackSize             Size of the stack for the function
 *
 * @return NULL if error, otherwise object handle
 */
//==========================================================================================================================================
void* oC_OS_Cyclic_Start( oC_OS_CyclicFunction_t CyclicFunction , const oC_STRING_t Name , void * Parameter , oC_OS_Tick_t Period , oC_OS_StackSize_t StackSize )
{
    void * reference = NULL;

    if ( CyclicFunction != NULL )
    {
        Cyclic_t * cyclic = malloc( sizeof(Cyclic_t) );

        if ( cyclic )
        {
            cyclic->CyclicFunction  = CyclicFunction;
            cyclic->Delay           = Period;
            cyclic->Parameter       = Parameter;
            cyclic->Thread          = oC_OS_Thread_Create( OS.CyclicPID , Name , (oC_OS_ThreadFunction_t)_CyclicThread , cyclic ,StackSize , oC_OS_Priority_Parent , NULL );

            if ( cyclic->Thread != NULL )
            {
                reference           = cyclic;
                oC_DLIST_EmplacePushBack( OS.Cyclics , &cyclic , sizeof(Cyclic_t*) );
            }
            else
            {
                free(cyclic);
            }
        }
    }

    return reference;
}

//==========================================================================================================================================
/**
 * Stops calling of the cyclic function.
 *
 * @param ObjectHandle          Handle of the object from #oC_OS_Cyclic_Start function
 */
//==========================================================================================================================================
void oC_OS_Cyclic_Stop( void * ObjectHandle )
{
    Cyclic_t * cyclic = ObjectHandle;

    if ( oC_OS_IsRAMAddress(cyclic) )
    {
        oC_OS_Thread_Cancel( cyclic->Thread );
        _DeleteCyclic( cyclic );
    }
}

//==========================================================================================================================================
/**
 * Registers global variable, which will be switched according to process context
 *
 * @param Destination       Destination of the global variable
 * @param Size              Size of the global variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_RegisterGlobalVariable( void * Destination , size_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_WrongParameters;

    if ( Destination && Size > 0 )
    {
        Process_t * process = _GetProcess( OS.CurrentPID );

        if ( process )
        {
            Global_t * global = malloc( sizeof(Global_t) );

            if ( global )
            {
                global->Destination  = Destination;
                global->Value        = malloc( Size );
                global->Size         = Size;

                if ( global->Value )
                {
                    if ( oC_DLIST_EmplacePushBack( process->Globals , &global , sizeof(Global_t*) ) )
                    {
                        errorCode   = oC_ErrorCode_None;
                    }
                    else
                    {
                        errorCode   = oC_ErrorCode_OSGlobalVariableRegistrationError;
                    }
                }
                else
                {
                    free(global);
                    errorCode       = oC_ErrorCode_AllocationError;
                }
            }
            else
            {
                errorCode       = oC_ErrorCode_AllocationError;
            }
        }
        else
        {
            errorCode       = oC_ErrorCode_OSPIDNotExists;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function clones TTY from current thread and create new instance of it. (Lines will be clonned too)
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_CloneTTY( void )
{
    oC_ErrorCode_t errorCode;

    oC_OS_Thread_t * thread = oC_OS_Thread_Current();

    if ( oC_OS_IsRAMAddress( thread ) )
    {
        oC_TTY_t * tty = oC_TTY_Clone( thread->TTY , &errorCode);

        if ( oC_TTY_IsCorrect( tty ) )
        {
            if ( thread->IsTTYParent )
            {
                if ( !_SearchNewThreadParentForTTY( thread->TTY ) )
                {
                    oC_TTY_Delete( thread->TTY );
                }
            }

            thread->TTY             = tty;
            thread->IsTTYParent     = true;

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Configures stdio streams for current thread
 *
 * @param Config            Reference to a configuration of streams
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_ConfigureTTY( const oC_TTY_Config_t * Config )
{
    return oC_OS_ConfigureTTYForThread( oC_OS_Thread_Current() , Config );
}

//==========================================================================================================================================
/**
 * Configures stdio streams for a thread
 *
 * @param Thread            Reference to a thread object
 * @param Config            Reference to a configuration of streams
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_ConfigureTTYForThread( oC_OS_Thread_t * Thread , const oC_TTY_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotImplemented;

    if ( oC_OS_IsRAMAddress( Thread ) && oC_OS_IsAddressCorrect( Config ) && oC_TTY_IsConfigCorrect( Config ) )
    {

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Sets TTY object reference for current thread
 *
 * @param TTY           Reference to the TTY object
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_SetTTY( oC_TTY_t * TTY )
{
    return oC_OS_SetTTYForThread( oC_OS_Thread_Current() , TTY );
}

//==========================================================================================================================================
/**
 * Sets TTY object reference for a thread
 *
 * @param Thread        Reference for thread
 * @param TTY           Reference for TTY object
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_OS_SetTTYForThread( oC_OS_Thread_t * Thread , oC_TTY_t * TTY )
{
    oC_ErrorCode_t errorCode;

    if ( oC_OS_Thread_IsCorrect( Thread ) )
    {
        if ( oC_TTY_IsCorrect( TTY ) )
        {
            Thread->TTY = TTY;
            errorCode   = oC_ErrorCode_None;
        }
        else
        {
            errorCode   = oC_ErrorCode_OSWrongTTY;
        }
    }
    else
    {
        errorCode       = oC_ErrorCode_OSWrongThread;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Sends an array via standard output stream
 *
 * @param Str           Array to send
 * @param Length        Length of an array
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
int oC_OS_StandardOutput( const char * Str , uint32_t Length )
{
    int sentBytes = 0;

    oC_TTY_t * tty = oC_OS_Thread_Current()->TTY;

    if ( oC_OS_IsRAMAddress( tty ) )
    {
        sentBytes   = oC_TTY_StandardOutput( tty , Str , Length );
    }

    return sentBytes;
}

//==========================================================================================================================================
/**
 * Receives an array via standard input stream
 *
 * @param Str           Array to send
 * @param Length        Length of an array
 *
 * @return number of received bytes
 */
//==========================================================================================================================================
int oC_OS_StandardInput( char * Str , uint32_t Length )
{
    int receivedBytes = 0;

    oC_TTY_t * tty = oC_OS_Thread_Current()->TTY;

    if ( oC_OS_IsRAMAddress( tty ) )
    {
        receivedBytes   = oC_TTY_StandardInput( tty , Str , Length );
    }

    return receivedBytes;
}

//==========================================================================================================================================
/**
 * Sends a array via standard error stream
 *
 * @param Str           Array to send
 * @param Length        Length of an array
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
int oC_OS_StandardError( const char * Str , uint32_t Length )
{
    int sentBytes = 0;

    oC_TTY_t * tty = oC_OS_Thread_Current()->TTY;

    if ( oC_OS_IsRAMAddress( tty ) )
    {
        sentBytes   = oC_TTY_StandardError( tty , Str , Length );
    }

    return sentBytes;
}

//==========================================================================================================================================
/**
 * Function sends string via stream
 *
 * @param FileDescriptor            Descriptor of file
 * @param String                    String to send via stream
 * @param Length                    Lenght of a string
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
int oC_OS_SendToStream( oC_FileDescriptor_t FileDescriptor , const oC_STRING_t String , uint32_t Length  )
{
    int sentBytes = 0;

    if ( FileDescriptor == oC_FileDescriptor_StdOutput )
    {
        sentBytes   = oC_OS_StandardOutput( String , Length );
    }
    else if ( FileDescriptor == oC_FileDescriptor_StdError )
    {
        sentBytes   = oC_OS_StandardError( String , Length );
    }
    else if ( FileDescriptor == oC_FileDescriptor_StdInput )
    {
        /* Try to print to input stream  */
        oC_IMPLEMENT_FAILURE();
    }
    else if ( FileDescriptor >= oC_FileDescriptor_FilesBegin && FileDescriptor <= oC_FileDescriptor_FilesEnd )
    {
        /* Print to file */
    }
    else
    {
        /* Wrong file descriptor */
        oC_IMPLEMENT_FAILURE();
    }

    return sentBytes;
}

//==========================================================================================================================================
/**
 * Returns reference to the default TTY object
 *
 * @return default TTY reference
 */
//==========================================================================================================================================
oC_TTY_t* oC_OS_DefaultTTY( void )
{
    return OS.DefaultTTY;
}

//==========================================================================================================================================
/**
 * Checks if the given address is stored in ROM
 *
 * @param Address       Address to check
 *
 * @return true if the Address is in ROM
 */
//==========================================================================================================================================
bool oC_OS_IsROMAddress( const void * Address )
{
    void * romStart = (void*) &__rom_start;
    void * romEnd   = (void*) &__rom_end;

    return (Address > romStart) && (Address <= romEnd);
}

//==========================================================================================================================================
/**
 * Checks if the given address is stored in RAM
 *
 * @param Address       Address to check
 *
 * @return true if the Address is in RAM
 */
//==========================================================================================================================================
bool oC_OS_IsRAMAddress( const void * Address )
{
    void * ramStart = (void*) &__ram_start;
    void * ramEnd   = (void*) &__ram_end;

    return (Address >= ramStart) && (Address <= ramEnd);
}

//==========================================================================================================================================
/**
 *
 * @param Address
 *
 * @return
 */
//==========================================================================================================================================
bool oC_OS_IsDynamicAllocatedAddress( const void * Address )
{
    return oC_OS_IsRAMAddress( Address );
}

//==========================================================================================================================================
/**
 * The function checks if the address is correct. (If it is storred in ROM / RAM )
 *
 * @param Address           An address to check
 *
 * @return true if address is correct
 */
//==========================================================================================================================================
bool oC_OS_IsAddressCorrect( const void * Address )
{
    bool isRomAddress = oC_OS_IsROMAddress( Address );
    bool isRamAddress = oC_OS_IsRAMAddress( Address );
    return  isRomAddress || isRamAddress;
}

//==========================================================================================================================================
/**
 * Send reset device sequence
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_ResetDevice( void )
{
    return oC_TTY_ResetDevice( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Clears screen
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_ClearScreen( void )
{
    return oC_TTY_ClearScreen( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Reads current position of the cursor
 *
 * @return Position of the cursor (-1,-1 if error)
 */
//==========================================================================================================================================
oC_Terminal_CursorPos_t oC_OS_Terminal_GetCursorPosition( void )
{
    return oC_TTY_GetCursorPosition( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Enables line wrapping
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_EnableLineWrap( void )
{
    return oC_TTY_EnableLineWrap( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Disable line wrapping
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_DisableLineWrap( void )
{
    return oC_TTY_DisableLineWrap( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets position of home for the cursor.
 *
 * @param Position          Position to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetCursorHome( oC_Terminal_CursorPos_t Position )
{
    return oC_TTY_SetCursorHome( oC_OS_Thread_TTY() , Position );
}

//==========================================================================================================================================
/**
 * Return cursor to home position
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_CursorHome( void )
{
    return oC_TTY_CursorHome(oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Moves the cursor up by Count rows
 *
 * @param Count     Number of rows to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_CursorUp( int Count )
{
    return oC_TTY_CursorUp( oC_OS_Thread_TTY() , Count );
}

//==========================================================================================================================================
/**
 * Moves the cursor down by Count rows
 *
 * @param Count     Number of rows to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_CursorDown( int Count )
{
    return oC_TTY_CursorDown( oC_OS_Thread_TTY() , Count );
}

//==========================================================================================================================================
/**
 * Moves the cursor forward by Count rows
 *
 * @param Count     Number of columns to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_CursorForward( int Count )
{
    return oC_TTY_CursorForward( oC_OS_Thread_TTY() , Count );
}

//==========================================================================================================================================
/**
 * Moves the cursor backward by Count rows
 *
 * @param Count     Number of columns to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_CursorBackward( int Count )
{
    return oC_TTY_CursorBackward( oC_OS_Thread_TTY() , Count );
}

//==========================================================================================================================================
/**
 * Save current position of the cursor for restoring it later
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SaveCursorPosition( void )
{
    return oC_TTY_SaveCursorPosition( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Restores cursor position from last save
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_RestoreCursorPosition( void )
{
    return oC_TTY_RestoreCursorPosition( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Save cursor position and attributes for restoring it later
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SaveCursorPositionAndAttrs( void )
{
    return oC_TTY_SaveCursorPositionAndAttrs( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Restores cursor position from last save and attributes from last save
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_RestoreCursorPositionAndAttrs( void )
{
    return oC_TTY_RestoreCursorPositionAndAttrs( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Enable scrolling for entire display.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_EnableScrollScreen( void )
{
    return oC_TTY_EnableScrollScreen( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Enable scrolling from row {startRow} to row {endRow}.
 *
 * @param startRow      start row of scroll
 * @param endRow        end row of scroll
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_EnableScrollScreenTo( int startRow , int endRow )
{
    return oC_TTY_EnableScrollScreenTo( oC_OS_Thread_TTY() , startRow , endRow );
}

//==========================================================================================================================================
/**
 * Scrolls screen one line up
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_ScrollUp( void )
{
    return oC_TTY_ScrollUp( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Scrolls screen one line down
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_ScrollDown( void )
{
    return oC_TTY_ScrollDown( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets tab at current position
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetTab( void )
{
    return oC_TTY_SetTab( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Clears tab at current position
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_ClearTab( void )
{
    return oC_TTY_ClearTab( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Clears all tabs
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_ClearAllTabs( void )
{
    return oC_TTY_ClearAllTabs( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Erases from the current cursor position to the end of the current line.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_EraseToEndOfLine( void )
{
    return oC_TTY_EraseToEndOfLine( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Erases from the current cursor position to the start of the current line.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_EraseToStartOfLine( void )
{
    return oC_TTY_EraseToStartOfLine( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Erases current line
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_EraseCurrentLine( void )
{
    return oC_TTY_EraseCurrentLine( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Associates a string of text to a keyboard key. {key} indicates the key by its ASCII value.
 *
 * @param Key           Ascii code of key to define
 * @param String        String to associate with the Key
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetKeyDefinition( char Key , char * String )
{
    return oC_TTY_SetKeyDefinition( oC_OS_Thread_TTY() , Key , String );
}

//==========================================================================================================================================
/**
 * Reset all set attributes and restore default values
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_ResetAllAttributes( void )
{
    return oC_TTY_ResetAllAttributes( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets bright
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetBright( void )
{
    return oC_TTY_SetBright( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets dim
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetDim( void )
{
    return oC_TTY_SetDim( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets underscore
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetUnderscore( void )
{
    return oC_TTY_SetUnderscore( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets blink
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetBlink( void )
{
    return oC_TTY_SetBlink( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets reverse
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetReverese( void )
{
    return oC_TTY_SetReverese( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets hidden
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetHidden( void )
{
    return oC_TTY_SetHidden( oC_OS_Thread_TTY() );
}

//==========================================================================================================================================
/**
 * Sets foreground color
 *
 * @param Color         Color to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetForegroundColor( oC_Terminal_Color_t Color )
{
    return oC_TTY_SetForegroundColor( oC_OS_Thread_TTY() , Color );
}

//==========================================================================================================================================
/**
 * Sets background color
 *
 * @param Color         Color to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_OS_Terminal_SetBackgroundColor( oC_Terminal_Color_t Color )
{
    return oC_TTY_SetBackgroundColor( oC_OS_Thread_TTY() , Color);
}



/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Counts new process ID
 *
 * @return process ID for new process
 */
//==========================================================================================================================================
static inline oC_OS_PID_t _CountNewPID( void )
{
    oC_OS_PID_t pid;
    bool pidUsed        = false;

    for(pid = 1; pid < 0xFFFFFFFFU ; pid++ , pidUsed = false)
    {
        foreach( Process_t * , process , OS.Processes )
        {
            if ( process->PID == pid )
            {
                pidUsed     = true;
                break;
            }
        }

        if ( !pidUsed )
            break;
    }

    if ( pidUsed )
    {
        pid     = 0;
    }

    return pid;
}

//==========================================================================================================================================
/**
 * Find process object with given ID
 *
 * @param PID       process ID to search
 *
 * @return NULL if process not exists, otherwise reference to the process object
 */
//==========================================================================================================================================
static inline Process_t* _GetProcess( oC_OS_PID_t PID )
{
    Process_t* process = NULL;

    foreach( Process_t * , proc , OS.Processes )
    {
        if ( proc->PID == PID )
        {
            process = proc;
        }
    }

    return process;
}

//==========================================================================================================================================
/**
 * Deletes process variables
 *
 * @param Process           Process to delete
 *
 */
//==========================================================================================================================================
static inline void _DeleteProcess( Process_t * Process )
{
    oC_Signal_EmitSignal( Process->Finished , NULL );

    oC_DLIST_Delete( Process->Threads );
    oC_DLIST_Delete( Process->Globals );
    oC_Signal_Delete( Process->Finished );

    if ( Process->MainProgram )
    {
        free(Process->MainProgram);
    }

    free(Process);

}

//==========================================================================================================================================
/**
 * Deletes cyclic object, and removes it from cyclic list
 *
 * @param Cyclic        Reference to the object to delete
 */
//==========================================================================================================================================
static inline void _DeleteCyclic( Cyclic_t * Cyclic )
{
    foreach( Cyclic_t * , cyclic , OS.Cyclics )
    {
        if ( cyclic == Cyclic )
        {
            oC_DLIST_TakeOffElement( OS.Cyclics , _element );
        }
    }
    free(Cyclic);
}

//==========================================================================================================================================
/**
 * Thread for handling program
 *
 * @param Program       Reference to the program object
 */
//==========================================================================================================================================
static void _ProgramThread( Program_t * Program )
{
    int result = Program->MainFunction( Program->Argc , Program->Argv );

    if ( Program->outReturn )
    {
        *Program->outReturn = result;
    }
    oC_OS_PID_t pid = Program->PID;

    oC_OS_Process_Kill( pid );
}

//==========================================================================================================================================
/**
 * Thread for handling cyclic functions
 *
 * @param Cyclic       Reference to the cyclic object
 */
//==========================================================================================================================================
static void _CyclicThread( Cyclic_t * Cyclic )
{
    while(Cyclic->CyclicFunction( Cyclic->Parameter ))
    {
        oC_OS_Delay( Cyclic->Delay );
    }
    _DeleteCyclic( Cyclic );
}

//==========================================================================================================================================
/**
 * Function search and return program registration from main function address
 *
 * @param MainFunction      Address of the main function
 *
 * @return NULL if not found
 */
//==========================================================================================================================================
static ProgramRegistration_t* _GetProgramRegistration( oC_OS_MainFunction_t MainFunction )
{
    ProgramRegistration_t* registration = NULL;

    foreach( ProgramRegistration_t* , preg , OS.ProgramsRegistrations )
    {
        if ( preg->MainFunction == MainFunction )
        {
            registration    = preg;
            break;
        }
    }

    return registration;
}

//==========================================================================================================================================
/**
 * The function switches context for new process
 *
 * @param NewPID        new process ID
 */
//==========================================================================================================================================
static void _SwitchContext( oC_OS_PID_t NewPID )
{
    Process_t * process = _GetProcess( NewPID );

    if ( process )
    {
        foreach( Global_t * , global , process->Globals )
        {
            memcpy( global->Destination , global->Value , global->Size );
        }

        OS.CurrentPID       = NewPID;
    }
    else
    {
        // Try to switch context of not existing process
        oC_IMPLEMENT_FAILURE();
    }
}

//==========================================================================================================================================
/**
 * Search new thread, that use the TTY to set is as parent for it. It skip threads, that already are parents. If it will find a parent, then
 * it will be also set to it.
 *
 * @param TTY       Reference to the TTY object, that it should find a parent
 *
 * @return true if found new parent
 */
//==========================================================================================================================================
static bool _SearchNewThreadParentForTTY( oC_TTY_t * TTY )
{
    bool found = false;

    foreach( TaskPID_t * , taskPID , OS.Tasks )
    {
        if ( taskPID->Thread->TTY == TTY && !taskPID->Thread->IsTTYParent )
        {
            taskPID->Thread->IsTTYParent= true;
            found                       = true;
            break;
        }
    }

    return found;
}

//==========================================================================================================================================
/**
 * Tick hook called when
 */
//==========================================================================================================================================
void vApplicationTickHook( void )
{
    TaskHandle_t taskHandle = xTaskGetCurrentTaskHandle();
    oC_OS_PID_t pid = 0;

    foreach( TaskPID_t* , task , OS.Tasks )
    {
        if ( task->TaskHandle == taskHandle )
        {
            pid = task->PID;
            break;
        }
    }

    if ( pid != 0 && pid != OS.CurrentPID )
    {
        _SwitchContext( pid );
    }
}

//==========================================================================================================================================
/**
 * Hook for stack overflow
 */
//==========================================================================================================================================
void vApplicationStackOverflowHook( void *pxTask, signed portCHAR *pcTaskName )
{
    oC_UNUSED_ARG( pxTask );
    oC_UNUSED_ARG(pcTaskName);

    oC_IMPLEMENT_FAILURE();
}

//==========================================================================================================================================
/**
 * The idle hook is called repeatedly as long as the idle task is running.
 */
//==========================================================================================================================================
void vApplicationIdleHook( void )
{

}
