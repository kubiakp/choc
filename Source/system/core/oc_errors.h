/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          errh.h
 *
 *    @brief      File with definitions of code errors
 *
 *    @author     Patryk Kubiak - (Created on: 9 gru 2014 12:12:45) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_GEN_OC_ERRORS_H_
#define SYSTEM_GEN_OC_ERRORS_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_general_cfg.h>
#include <oc_gen_macros.h>
#include <stdint.h>
#include <stdio.h>

/*==========================================================================================================================================
//
//     ERROR CODES
//
//========================================================================================================================================*/


/**
 * List of errors in the system. To add new code of error use ADD macro with parameters:
 *  ADD( CODE_NAME , "ERROR DESCRIPTION" )
 *          CODE_NAME - no spaces name
 *          ERROR DESCRIPTION - string with description of the error
 */
#define oC_ERRORS_LIST(ADD) \
    ADD( AllocationError ,                                  "Cannot allocate memory" )\
    ADD( NotImplemented  ,                                  "Functionality not implemented" )\
    ADD( ImplementError  ,                                  "Implementation error" )\
    ADD( WrongParameters ,                                  "Given parameters are not correct" )\
    ADD( WrongFrequency ,                                   "Given frequency is not correct" )\
    ADD( WrongAddress ,                                     "Given address is not correct" )\
    ADD( OutputAddressNotInRAM ,                            "The address for output function variable is not pointing to RAM section." )\
    ADD( WrongChannel ,                                     "Given channel is not correct" )\
    ADD( DriverNotTurnedOn ,                                "The driver is not turned on!" )\
    ADD( DriverNotTurnedOff ,                               "The driver is not turned off!" )\
    ADD( DriverNotRegistered,                               "The given driver is not registered!" )\
    ADD( FunctionNotExists,                                 "Function not exists!" )\
    ADD( ModuleNotStartedYet,                               "Module has not started yet!" )\
    ADD( GPIOPinIncorrect ,                                 "Pin is incorrect" )\
    ADD( GPIOPinIsUsed ,                                    "Pin is used" )\
    ADD( GPIOModeConfigureError ,                           "Error while setting mode of GPIO pin" )\
    ADD( GPIOOutputCircuitConfigureError ,                  "Error while setting output circuit of GPIO pin" )\
    ADD( GPIOPullConfigureError ,                           "Error while setting pull of GPIO pin" )\
    ADD( GPIOCurrentConfigureError ,                        "Error while setting current of GPIO pin" )\
    ADD( GPIOSpeedConfigureError ,                          "Error while setting speed of GPIO pin" )\
    ADD( GPIOInterruptConfigureError ,                      "Error while setting interrupt of GPIO pin" )\
    ADD( GPIOADDInterruptToListError ,                      "Error while adding interrupt of GPIO pin to list" )\
    ADD( SYSUnknownOscillatorSource ,                       "Unknown oscillator source" )\
    ADD( SYSConfigurationError ,                            "Error while configuration of system clock" )\
    ADD( OSPIDNotExists ,                                   "Process with given ID not exists" )\
    ADD( OSNotAllThreadsCancelled,                          "One or more threads cannot be cancelled" )\
    ADD( OSDuplicatePID ,                                   "Process ID is duplicated" )\
    ADD( OSProcessNotFinished ,                             "Process is not finished" )\
    ADD( OSGlobalVariableRegistrationError ,                "Error while registration of the global variable" )\
    ADD( OSCannotKillChildProcess ,                         "At least one of children processes cannot be killed" )\
    ADD( OSWrongThread ,                                    "Not correct thread reference" )\
    ADD( OSWrongTTY,                                        "Not correct TTY reference" )\
    ADD( TTYConfigurationIncorrect ,                        "Configuration is not correct" )\
    ADD( TTYCannotCreateDLIST ,                             "Cannot create dynamic list" )\
    ADD( TTYCannotCreateMutex,                              "Cant create mutex object" )\
    ADD( UARTNotCorrectChannel,                             "The given channel number is not correct" )\
    ADD( UARTCannotPrepareTRAHN,                            "Error while creating TRANH object" )\
    ADD( UARTPinNotDefined,                                 "Given pin is not defined" )\
    ADD( UARTCantConfigurePin,                              "Cannot configure GPIO pin" )\
    ADD( UARTNotCorrectWordLength,                          "Given word length is not correct" )\
    ADD( UARTWordLengthNotSupported,                        "Given word length is not supported on selected architecture" )\
    ADD( UARTParityNotSupported,                            "Given parity mode is not supported on selected architecture" )\
    ADD( UARTStopBitNotSupported,                           "Given stop bit mode is not supported on selected architecture" )\
    ADD( UARTBitOrderNotSupported,                          "Given bit order mode is not supported on selected architecture" )\
    ADD( UARTInvertNotSupported,                            "Given invert mode is not supported on selected architecture" )\
    ADD( UARTBitRateNotSupported,                           "Given bit-rate is not supported on this architecture" )\
    ADD( TIMLockingLockedChannel,                           "Trying to lock locked channel" )\
    ADD( TIMUnlockingUnlockedChannel,                       "Trying to unlock channel which is not locked" )\
    ADD( TIMChannelNotConfigured,                           "The selected channel is not configured" )\
    ADD( TIMMatchCallbackNotCorrect,                        "The match callback function pointer is not correct" )\
    ADD( TIMMatchContextNotCorrect,                         "The match callback context pointer is not correct" )\
    ADD( TIMTimeoutCallbackNotCorrect,                      "The timeout callback function pointer is not correct" )\
    ADD( TIMTimeoutContextNotCorrect,                       "The timeout callback context pointer is not correct" )\
    ADD( TIMNoneOfChannelsAvailable,                        "None of channel available" )\
    ADD( TIMModeNotCorrect,                                 "Timer mode is not correct" )\
    ADD( TIMCountsDirNotCorrect,                            "CountsDir is not correct" )\
    ADD( TIMTickSourceNotSupported,                         "The selected TickSource is not supported" )\
    ADD( TIMSizeNotSupported,                               "The given size is not supported" )\
    ADD( TIMTickPeriodNotSupported,                         "The given tick period is not supported" )\
    ADD( TIMSizeNotSupportedOnSelectedChannel,              "The given size is not supported on selected channel" )\
    ADD( TIMMatchValueTooBig,                               "The given match value is too big" )\
    ADD( TIMMaxValueTooBig,                                 "The given max value is too big" )\
    ADD( TIMCantCalculatePrescaler,                         "Error while calculating prescaler" )\
    ADD( TIMTickPeriodTooSmall,                             "The given tick period is too small for this channel" )\
    ADD( TIMTickPeriodTooBig,                               "The given tick period is too big for this channel" )\
    ADD( TIMCountsDirNotSupported,                          "The given counts direction is not supported" )\
    ADD( TIMValueTooBig,                                    "The given timer value is too big for this channel" )\
    ADD( TimeAlreadyTurnedOn,                               "Time module is already turned on" )\
    ADD( TimeAlreadyTurnedOff,                              "Time module is already turned off" )\
    ADD( TimeCannotCreateWakeUpList,                        "Error while creating wake up list" )\
    ADD( TimeCannotCreateConfigEvent,                       "Error while creating configuration event" )\

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/
#define _ADD_ERROR_TO_ENUM( CODE_NAME , DESCR )     oC_CAT_2( oC_ErrorCode_ , CODE_NAME ) ,

/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

/**
 * List of errors codes in the system
 */
typedef enum
{
    oC_ErrorCode_None ,//!< oC_ErrorCode_None
    oC_ERRORS_LIST(_ADD_ERROR_TO_ENUM)
    oC_ErrorCode_NumberOfErrorsDefinitions
} oC_ErrorCode_t;

/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/

extern const char* oC_ErrorsStrings[oC_ErrorCode_NumberOfErrorsDefinitions];

/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern void         oC_PrintError               ( oC_ErrorCode_t ErrorCode );
extern const char * oC_GetErrorString           ( oC_ErrorCode_t ErrorCode );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function assign value for error code destination and return result of error
 *
 * @param outErrorCode      Reference to the error code destination
 * @param ErrorCode         code of error to assign
 *
 * @return true if error code is not error
 *
 * @code
   oC_ErrorCode_t errorCode = oC_ErrorCode_None;

   if (
         oC_AssignErrorCode( &errorCode , oC_SPI_Configure( SPI_Config ) )
      && oC_AssignErrorCode( &errorCode , oC_UART_Configure( UART_Config ) )
      )
   {
      // everything is OK
   }

   return errorCode;
   @endcode
 */
//==========================================================================================================================================
static inline bool oC_AssignErrorCode( oC_ErrorCode_t * outErrorCode , oC_ErrorCode_t ErrorCode )
{
    bool errorOccur = ErrorCode != oC_ErrorCode_None;
    if ( errorOccur )
    {
        outErrorCode[0] = ErrorCode;
    }
    return !errorOccur;
}

//==========================================================================================================================================
/**
 * The function assign an error code, if the Condition is false.
 *
 * @param outErrorCode      Destination for the error code
 * @param Condition         Condition to check
 * @param ErrorCode         Error code to assign if the condition is not true
 *
 * @return false if error occurs
 */
//==========================================================================================================================================
static inline bool oC_AssignErrorCodeIfFalse( oC_ErrorCode_t * outErrorCode , bool Condition , oC_ErrorCode_t ErrorCode )
{
    return oC_AssignErrorCode( outErrorCode , (Condition) ? oC_ErrorCode_None : ErrorCode );
}

//==========================================================================================================================================
/**
 * Checks if some error occurs
 *
 * @param ErrorCode     Code of error to check
 *
 * @return true if error occur
 */
//==========================================================================================================================================
static inline bool oC_ErrorOccur( oC_ErrorCode_t ErrorCode )
{
    return ErrorCode != oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * Prints error message if an error occurs.
 *
 * @param ErrorCode     Code of error to print
 *
 * @return true if error occurs
 */
//==========================================================================================================================================
static inline bool oC_PrintIfError( oC_ErrorCode_t ErrorCode )
{
    bool errorOccur = oC_ErrorOccur( ErrorCode );

    if ( errorOccur )
    {
        oC_PrintError(ErrorCode);
    }

    return errorOccur;
}

//==========================================================================================================================================
/**
 * Prints result of operation.
 *
 * @param ErrorCode             Code of error to print
 * @param OperationDescription  Description of the operation
 *
 * @return true if operation is success
 */
//==========================================================================================================================================
static inline bool oC_PrintResult( oC_ErrorCode_t ErrorCode , const char * OperationDescription )
{
    printf("%s: %s\n" , OperationDescription , oC_GetErrorString(ErrorCode));
    return !oC_ErrorOccur(ErrorCode);
}

#endif /* SYSTEM_GEN_OC_ERRORS_H_ */
