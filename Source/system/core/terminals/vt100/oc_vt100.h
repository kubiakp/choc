/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_vt100.h
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 19 lut 2015 23:48:38) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_TERMINALS_VT100_OC_VT100_H_
#define SYSTEM_CORE_TERMINALS_VT100_OC_VT100_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_terminals.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Type to store oC_VT100 sequences. To see possibilities to set it, use macros oC_VT100_SEQ_{SEQ_NAME} where {SEQ_NAME} is name of a sequence
 */
//==========================================================================================================================================
typedef char * oC_VT100_SEQ_t;

//==========================================================================================================================================
/**
 * Status of the device
 */
//==========================================================================================================================================
typedef enum
{
    oC_VT100_DeviceStatus_OK           = 0,//!< device is functioning correctly.
    oC_VT100_DeviceStatus_FAILURE      = 3 //!< device is functioning improperly.
} oC_VT100_DeviceStatus_t;

//==========================================================================================================================================
/**
 * Font for oC_VT100
 */
//==========================================================================================================================================
typedef enum
{
    oC_VT100_Font_G0 ,                     //!< standard font
    oC_VT100_Font_G1                       //!< alternate font
} oC_VT100_Font_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern bool                         oC_VT100_ResetDevice                            ( void );
extern bool                         oC_VT100_ClearScreen                            ( void );
extern oC_Terminal_CursorPos_t      oC_VT100_GetCursorPosition                      ( void );
extern bool                         oC_VT100_EnableLineWrap                         ( void );
extern bool                         oC_VT100_DisableLineWrap                        ( void );
extern bool                         oC_VT100_SetFont                                ( oC_VT100_Font_t Font );
extern bool                         oC_VT100_SetCursorHome                          ( oC_Terminal_CursorPos_t Position );
extern bool                         oC_VT100_CursorHome                             ( void );
extern bool                         oC_VT100_CursorUp                               ( int Count );
extern bool                         oC_VT100_CursorDown                             ( int Count );
extern bool                         oC_VT100_CursorForward                          ( int Count );
extern bool                         oC_VT100_CursorBackward                         ( int Count );
extern bool                         oC_VT100_ForceCursorPosition                    ( oC_Terminal_CursorPos_t Position );
extern bool                         oC_VT100_SaveCursorPosition                     ( void );
extern bool                         oC_VT100_RestoreCursorPosition                  ( void );
extern bool                         oC_VT100_SaveCursorPositionAndAttrs             ( void );
extern bool                         oC_VT100_RestoreCursorPositionAndAttrs          ( void );
extern bool                         oC_VT100_EnableScrollScreen                     ( void );
extern bool                         oC_VT100_EnableScrollScreenTo                   ( int startRow , int endRow );
extern bool                         oC_VT100_ScrollUp                               ( void );
extern bool                         oC_VT100_ScrollDown                             ( void );
extern bool                         oC_VT100_SetTab                                 ( void );
extern bool                         oC_VT100_ClearTab                               ( void );
extern bool                         oC_VT100_ClearAllTabs                           ( void );
extern bool                         oC_VT100_EraseToEndOfLine                       ( void );
extern bool                         oC_VT100_EraseToStartOfLine                     ( void );
extern bool                         oC_VT100_EraseCurrentLine                       ( void );
extern bool                         oC_VT100_EraseUp                                ( void );
extern bool                         oC_VT100_EraseDown                              ( void );
extern bool                         oC_VT100_EraseScreen                            ( void );
extern bool                         oC_VT100_PrintScreen                            ( void );
extern bool                         oC_VT100_PrintLine                              ( void );
extern bool                         oC_VT100_StopPrintLog                           ( void );
extern bool                         oC_VT100_StartPrintLog                          ( void );
extern bool                         oC_VT100_SetKeyDefinition                       ( char Key , char * String );
extern bool                         oC_VT100_ResetAllAttributes                     ( void );
extern bool                         oC_VT100_SetBright                              ( void );
extern bool                         oC_VT100_SetDim                                 ( void );
extern bool                         oC_VT100_SetUnderscore                          ( void );
extern bool                         oC_VT100_SetBlink                               ( void );
extern bool                         oC_VT100_SetReverese                            ( void );
extern bool                         oC_VT100_SetHidden                              ( void );
extern bool                         oC_VT100_SetForegroundColor                     ( oC_Terminal_Color_t Color );
extern bool                         oC_VT100_SetBackgroundColor                     ( oC_Terminal_Color_t Color );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* SYSTEM_CORE_TERMINALS_VT100_OC_VT100_H_ */
