/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_vt100.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 19 lut 2015 23:49:47) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_vt100.h>
#include <oc_vt100_seq.h>
#include <oc_terminals.h>
#include <oc_string.h>
#include <stdio.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Structure with configuration of interface for terminal
 */
//==========================================================================================================================================
const oC_Terminal_IfCfg_t VT100_IfCfg   = {
                                           .ResetDevice                     = oC_VT100_ResetDevice                            ,
                                           .ClearScreen                     = oC_VT100_ClearScreen                            ,
                                           .GetCursorPosition               = oC_VT100_GetCursorPosition                      ,
                                           .EnableLineWrap                  = oC_VT100_EnableLineWrap                         ,
                                           .DisableLineWrap                 = oC_VT100_DisableLineWrap                        ,
                                           .SetCursorHome                   = oC_VT100_SetCursorHome                          ,
                                           .CursorHome                      = oC_VT100_CursorHome                             ,
                                           .CursorUp                        = oC_VT100_CursorUp                               ,
                                           .CursorDown                      = oC_VT100_CursorDown                             ,
                                           .CursorForward                   = oC_VT100_CursorForward                          ,
                                           .CursorBackward                  = oC_VT100_CursorBackward                         ,
                                           .SaveCursorPosition              = oC_VT100_SaveCursorPosition                     ,
                                           .RestoreCursorPosition           = oC_VT100_RestoreCursorPosition                  ,
                                           .SaveCursorPositionAndAttrs      = oC_VT100_SaveCursorPositionAndAttrs             ,
                                           .RestoreCursorPositionAndAttrs   = oC_VT100_RestoreCursorPositionAndAttrs          ,
                                           .EnableScrollScreen              = oC_VT100_EnableScrollScreen                     ,
                                           .ScrollUp                        = oC_VT100_ScrollUp                               ,
                                           .ScrollDown                      = oC_VT100_ScrollDown                             ,
                                           .SetTab                          = oC_VT100_SetTab                                 ,
                                           .ClearTab                        = oC_VT100_ClearTab                               ,
                                           .ClearAllTabs                    = oC_VT100_ClearAllTabs                           ,
                                           .EraseToEndOfLine                = oC_VT100_EraseToEndOfLine                       ,
                                           .EraseToStartOfLine              = oC_VT100_EraseToStartOfLine                     ,
                                           .EraseCurrentLine                = oC_VT100_EraseCurrentLine                       ,
                                           .SetKeyDefinition                = oC_VT100_SetKeyDefinition                       ,
                                           .ResetAllAttributes              = oC_VT100_ResetAllAttributes                     ,
                                           .SetBright                       = oC_VT100_SetBright                              ,
                                           .SetDim                          = oC_VT100_SetDim                                 ,
                                           .SetUnderscore                   = oC_VT100_SetUnderscore                          ,
                                           .SetBlink                        = oC_VT100_SetBlink                               ,
                                           .SetReverese                     = oC_VT100_SetReverese                            ,
                                           .SetHidden                       = oC_VT100_SetHidden                              ,
                                           .SetForegroundColor              = oC_VT100_SetForegroundColor                     ,
                                           .SetBackgroundColor              = oC_VT100_SetBackgroundColor
};

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Sends sequence to the terminal.
 *
 * @param Sequence          Sequence to send. Use oC_VT100_SEQ_{SEQ_NAME} macros to set it
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool oC_VT100_SendSequence( const oC_VT100_SEQ_t Sequence )
{
    if ( puts( Sequence ) == oC_STRING_Length( Sequence ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

//==========================================================================================================================================
/**
 * Send reset device sequence
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_ResetDevice( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_RESET_DEVICE );
}

//==========================================================================================================================================
/**
 * Clears screen
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_ClearScreen( void )
{
    bool success ;

    success = oC_VT100_SendSequence( oC_VT100_SEQ_ERASE_SCREEN );

    return success;
}

//==========================================================================================================================================
/**
 * Reads current position of the cursor
 *
 * @return Position of the cursor (-1,-1 if error)
 */
//==========================================================================================================================================
oC_Terminal_CursorPos_t oC_VT100_GetCursorPosition( void )
{
    oC_Terminal_CursorPos_t position;

    position.Row    = -1;
    position.Column = -1;

    if ( oC_VT100_SendSequence( oC_VT100_SEQ_QUERY_CURSOR_POSITION ) )
    {
        scanf( oC_VT100_SEQ_REPORT_CURSOR_POSITION("%d" , "%d") , &position.Row , &position.Column );
    }

    return position;
}

//==========================================================================================================================================
/**
 * Enables line wrapping
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EnableLineWrap( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_ENABLE_LINE_WRAP );
}

//==========================================================================================================================================
/**
 * Disable line wrapping
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_DisableLineWrap( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_DISABLE_LINE_WRAP );
}

//==========================================================================================================================================
/**
 * The function sets font in the oC_VT100 console
 *
 * @param Font  Number of the font
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetFont( oC_VT100_Font_t Font )
{
    static const oC_VT100_SEQ_t sequences[] = { oC_VT100_SEQ_FONT_SET_G0 , oC_VT100_SEQ_FONT_SET_G1 };
    return oC_VT100_SendSequence( sequences[Font] );
}

//==========================================================================================================================================
/**
 * Sets position of home for the cursor.
 *
 * @param Position          Position to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetCursorHome( oC_Terminal_CursorPos_t Position )
{
    return printf( oC_VT100_SEQ_SET_CURSOR_HOME("%d" , "%d") , Position.Row , Position.Column ) > 0;
}

//==========================================================================================================================================
/**
 * Return cursor to home position
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_CursorHome( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_CURSOR_HOME );
}

//==========================================================================================================================================
/**
 * Moves the cursor up by Count rows
 *
 * @param Count     Number of rows to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_CursorUp( int Count )
{
    return printf( oC_VT100_SEQ_CURSOR_UP("%d") , Count ) > 0;
}

//==========================================================================================================================================
/**
 * Moves the cursor down by Count rows
 *
 * @param Count     Number of rows to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_CursorDown( int Count )
{
    return printf( oC_VT100_SEQ_CURSOR_DOWN("%d") , Count ) > 0;
}

//==========================================================================================================================================
/**
 * Moves the cursor forward by Count rows
 *
 * @param Count     Number of columns to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_CursorForward( int Count )
{
    return printf( oC_VT100_SEQ_CURSOR_FORWARD("%d") , Count ) > 0;
}

//==========================================================================================================================================
/**
 * Moves the cursor backward by Count rows
 *
 * @param Count     Number of columns to move
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_CursorBackward( int Count )
{
    return printf( oC_VT100_SEQ_CURSOR_BACKWARD("%d") , Count ) > 0;
}

//==========================================================================================================================================
/**
 * Sets position of the cursor
 *
 * @param Position  Position of the cursor
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_ForceCursorPosition( oC_Terminal_CursorPos_t Position )
{
    return printf( oC_VT100_SEQ_FORCE_CURSOR_POSITION("%d" , "%d") , Position.Row , Position.Column ) > 0;
}

//==========================================================================================================================================
/**
 * Save current position of the cursor for restoring it later
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SaveCursorPosition( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SAVE_CURSOR );
}

//==========================================================================================================================================
/**
 * Restores cursor position from last save
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_RestoreCursorPosition( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_UNSAVE_CURSOR );
}

//==========================================================================================================================================
/**
 * Save cursor position and attributes for restoring it later
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SaveCursorPositionAndAttrs( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SAVE_CURSOR_AND_ATTRS );
}

//==========================================================================================================================================
/**
 * Restores cursor position from last save and attributes from last save
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_RestoreCursorPositionAndAttrs( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_RESTORE_CURSOR_AND_ATTRS );
}

//==========================================================================================================================================
/**
 * Enable scrolling for entire display.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EnableScrollScreen( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SCROLL_SCREEN );
}

//==========================================================================================================================================
/**
 * Enable scrolling from row {startRow} to row {endRow}.
 *
 * @param startRow      start row of scroll
 * @param endRow        end row of scroll
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EnableScrollScreenTo( int startRow , int endRow )
{
    return printf( oC_VT100_SEQ_SCROLL_SCREEN_TO( "%d" , "%d" ) , startRow , endRow ) > 0;
}

//==========================================================================================================================================
/**
 * Scrolls screen one line up
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_ScrollUp( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SCROLL_UP );
}

//==========================================================================================================================================
/**
 * Scrolls screen one line down
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_ScrollDown( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SCROLL_DOWN );
}

//==========================================================================================================================================
/**
 * Sets tab at current position
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetTab( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SET_TAB );
}

//==========================================================================================================================================
/**
 * Clears tab at current position
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_ClearTab( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_CLEAR_TAB );
}

//==========================================================================================================================================
/**
 * Clears all tabs
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_ClearAllTabs( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_CLEAR_ALL_TABS );
}

//==========================================================================================================================================
/**
 * Erases from the current cursor position to the end of the current line.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EraseToEndOfLine( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_ERASE_END_OF_LINE );
}

//==========================================================================================================================================
/**
 * Erases from the current cursor position to the start of the current line.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EraseToStartOfLine( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_ERASE_START_OF_LINE );
}

//==========================================================================================================================================
/**
 * Erases current line
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EraseCurrentLine( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_ERASE_LINE );
}

//==========================================================================================================================================
/**
 * Erases the screen from the current line up to the top of the screen.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EraseUp( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_ERASE_UP );
}

//==========================================================================================================================================
/**
 * Erases the screen from the current line down to the bottom of the screen.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EraseDown( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_ERASE_DOWN );
}

//==========================================================================================================================================
/**
 * Erases the screen with the background colour and moves the cursor to home.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_EraseScreen( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_ERASE_SCREEN );
}

//==========================================================================================================================================
/**
 * Print the current screen.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_PrintScreen( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_PRINT_SCREEN );
}

//==========================================================================================================================================
/**
 * Print the current line.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_PrintLine( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_PRINT_LINE );
}

//==========================================================================================================================================
/**
 * Disable log.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_StopPrintLog( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_STOP_PRINT_LOG );
}

//==========================================================================================================================================
/**
 * Start log; all received text is echoed to a printer.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_StartPrintLog( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_START_PRINT_LOG );
}

//==========================================================================================================================================
/**
 * Associates a string of text to a keyboard key. {key} indicates the key by its ASCII value.
 *
 * @param Key           Ascii code of key to define
 * @param String        String to associate with the Key
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetKeyDefinition( char Key , char * String )
{
    return printf( oC_VT100_SEQ_SET_KEY_DEFINITION( "%d" , "%s" ) , Key , String );
}

//==========================================================================================================================================
/**
 * Reset all set attributes and restore default values
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_ResetAllAttributes( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( oC_VT100_DISPLAY_ATTRIBUTE(oC_VT100_DISPLAY_ATTRIBUTE_RESET_ALL_ATTRIBUTES) ) );
}

//==========================================================================================================================================
/**
 * Sets bright
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetBright( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( oC_VT100_DISPLAY_ATTRIBUTE(oC_VT100_DISPLAY_ATTRIBUTE_BRIGHT) ) );
}

//==========================================================================================================================================
/**
 * Sets dim
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetDim( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( oC_VT100_DISPLAY_ATTRIBUTE(oC_VT100_DISPLAY_ATTRIBUTE_DIM) ) );
}

//==========================================================================================================================================
/**
 * Sets underscore
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetUnderscore( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( oC_VT100_DISPLAY_ATTRIBUTE(oC_VT100_DISPLAY_ATTRIBUTE_UNDERSCORE) ) );
}

//==========================================================================================================================================
/**
 * Sets blink
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetBlink( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( oC_VT100_DISPLAY_ATTRIBUTE(oC_VT100_DISPLAY_ATTRIBUTE_BLINK) ) );
}

//==========================================================================================================================================
/**
 * Sets reverse
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetReverese( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( oC_VT100_DISPLAY_ATTRIBUTE(oC_VT100_DISPLAY_ATTRIBUTE_REVERSE) ) );
}

//==========================================================================================================================================
/**
 * Sets hidden
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetHidden( void )
{
    return oC_VT100_SendSequence( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( oC_VT100_DISPLAY_ATTRIBUTE(oC_VT100_DISPLAY_ATTRIBUTE_HIDDEN) ) );
}

//==========================================================================================================================================
/**
 * Sets foreground color
 *
 * @param Color         Color to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetForegroundColor( oC_Terminal_Color_t Color )
{
    static const char * colors[]       = {
                                     oC_VT100_DISPLAY_ATTRIBUTE_FG_COLOUR_BLACK  ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_FG_COLOUR_RED    ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_FG_COLOUR_GREEN  ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_FG_COLOUR_YELLOW ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_FG_COLOUR_BLUE   ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_FG_COLOUR_MAGENTA,
                                     oC_VT100_DISPLAY_ATTRIBUTE_FG_COLOUR_CYAN   ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_FG_COLOUR_WHITE
    };

    return printf( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( "%s" ) , colors[Color] ) > 0;
}

//==========================================================================================================================================
/**
 * Sets background color
 *
 * @param Color         Color to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VT100_SetBackgroundColor( oC_Terminal_Color_t Color )
{
    static const char * colors[]       = {
                                     oC_VT100_DISPLAY_ATTRIBUTE_BG_COLOUR_BLACK  ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_BG_COLOUR_RED    ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_BG_COLOUR_GREEN  ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_BG_COLOUR_YELLOW ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_BG_COLOUR_BLUE   ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_BG_COLOUR_MAGENTA,
                                     oC_VT100_DISPLAY_ATTRIBUTE_BG_COLOUR_CYAN   ,
                                     oC_VT100_DISPLAY_ATTRIBUTE_BG_COLOUR_WHITE
    };

    return printf( oC_VT100_SEQ_SET_ATTRIBUTE_MODE( "%s" ) , colors[Color] ) > 0;
}


/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
