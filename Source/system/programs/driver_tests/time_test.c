/** ****************************************************************************************************************************************
 *
 * @file          time_test.c
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 21 mar 2015 19:47:01) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <choco.h>
#include <stdio.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

oC_OS_PROGRAM_BEGIN( time_test )
{
    oC_OS_Terminal_ResetDevice();
    printf("Initializing time module: ");
    oC_PrintError(oC_Time_TurnOnModule());
    printf("\n\r");

    volatile oC_Time_Stamp_t timeStamp   = oC_Time_GetTimeStamp();
    volatile oC_Time_Extended_t currentTime;

    currentTime.h   = 11;
    currentTime.min = 03;
    currentTime.sec = 0;

    oC_Time_SetCurrentTimeExtended(currentTime);

    while(1)
    {
        currentTime = oC_Time_GetCurrentTimeExtended();
        printf("%d:%d:%d.%d %d us\n" , currentTime.h , currentTime.min , currentTime.sec , currentTime.msec , currentTime.usec);
        oC_Time_DelayFor_ms(500);
    }
}
oC_OS_PROGRAM_END

/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
