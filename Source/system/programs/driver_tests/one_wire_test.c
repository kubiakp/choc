/*******************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          1wire_test.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 1 kwi 2015 22:11:36) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// Core
#include <choco.h>
#include <oc_gpio_lld.h>


// Drivers

// Libraries

// Others

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/
    bool OneWireStatus;

    uint8_t ROM[8];
/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/


int one_wire_test( int Argc , char ** Argv )
{
    oC_Time_TurnOnModule();

    int *temperature;

    const oC_1WIRE_Config_t Test1Wire = {
    .PinLink=oC_GPIO_PinLink_PE3,
    .Mode = oC_1WIRE_Mode_Single};

    oC_DS18B20_Config_t DigitTemp;
    DigitTemp.OneWireConfig = &Test1Wire;
    DigitTemp.Scale = oC_DS18B20_TemperatureScale_Kelvin;


    if (
              oC_ErrorCode_None != oC_DS18B20_Configure( &DigitTemp   )
        )
    {
        oC_IMPLEMENT_FAILURE();
    }

    printf("\n\rThis is test program for DS18B20 driver\n\r");


    while(1)
    {

        oC_DS18B20_ReadTemp(&DigitTemp);
       // oC_Time_DelayFor_ms(100);


    }

    return -1;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
