/*******************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          gpio_test.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Krzysztof Chmielewski - (Created on: 2 gru 2014 20:00:21) 
 *
 *    @note       Copyright (C) 2014 Krzysztof Chmielewski <krzysztof.marek.chmielewski@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INCLUDES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Core
#include <choco.h>



// Drivers

// Libraries

// Others

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL CONSTS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL TYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * The type represents RGB diode colors
 */

typedef enum
{
    LED_Color_Black,
    LED_Color_Red,
    LED_Color_Green,
    LED_Color_Yellow,
    LED_Color_Blue,
    LED_Color_Violet,
    LED_Color_Turquoise,
    LED_Color_White,

}LED_Color_t;


/**
 * The type represents RGB diode blink speed
 */

typedef enum
{
    LED_BlinkSpeed_Contionous,
    LED_BlinkSpeed_Min,
    LED_BlinkSpeed_Half,
    LED_BlinkSpeed_Max,

}LED_BlinkSpeed_t;


/**
 * The type represents RGB diode parameters
 */

typedef struct
{
    oC_GPIO_Config_t* PinRed;
    oC_GPIO_Config_t* PinGreen;
    oC_GPIO_Config_t* PinBlue;
    LED_Color_t       ColorRGB;
    LED_BlinkSpeed_t  BlinkSpeed;

}LED_t;


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MACROS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define TIME_INTERVAL 25     /**< Definition of time interval */
#define STATE_HIGH    0xFF   /**< Definition of height state */
#define STATE_LOW     0x00   /**< Definition of low state */

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL VARIABLES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LED_BlinkSpeed_t DiodeRGB_BlinkSpeed = LED_BlinkSpeed_Min; /**< Declaration of variable to keeps actual set of diode blink speed */
LED_t DiodeRGB;

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL FUNCTION PROTOTYPES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void LED_On  (LED_t* LED);    /**< This function turn on diode */
static void LED_Off (LED_t* LED);    /**< This function turn off diode. */

static void LED_ChangeBlinkSpeed (const oC_GPIO_PinLink_t* const Switch2); /**< This changes speed of diode blinking. */
bool CyclicFunction1s( void * Parameter );

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INTERFACE FUNCTIONS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int gpio_test( int Argc , char ** Argv )
{


    /**
     * This section configures output pins
     */

    oC_GPIO_Config_t PinRed    = {.PinLink = oC_GPIO_PinLink_PF1 , .Mode = oC_GPIO_Mode_Output};
    oC_GPIO_Config_t PinGreen  = {.PinLink = oC_GPIO_PinLink_PF3 , .Mode = oC_GPIO_Mode_Output};
    oC_GPIO_Config_t PinBlue   = {.PinLink = oC_GPIO_PinLink_PF2 , .Mode = oC_GPIO_Mode_Output};


    /**
     * This section configures input pins
     */

    oC_GPIO_Config_t Switch1   = {.PinLink = oC_GPIO_PinLink_PF0 , .Mode = oC_GPIO_Mode_Input, .Trigger = oC_GPIO_IntTrigger_FallingEdge, .Pull = oC_GPIO_Pull_Up, .Protection=oC_GPIO_Protection_UnlockProtectedPins};
    oC_GPIO_Config_t Switch2   = {.PinLink = oC_GPIO_PinLink_PF4 , .Mode = oC_GPIO_Mode_Input, .Trigger = oC_GPIO_IntTrigger_FallingEdge, .Pull = oC_GPIO_Pull_Up};

    if (
              oC_ErrorCode_None != oC_GPIO_Configure( &PinRed   )
          ||  oC_ErrorCode_None != oC_GPIO_Configure( &PinGreen )
          ||  oC_ErrorCode_None != oC_GPIO_Configure( &PinBlue  )
          ||  oC_ErrorCode_None != oC_GPIO_Configure( &Switch1  )
          ||  oC_ErrorCode_None != oC_GPIO_Configure( &Switch2  )

        )
    {
        oC_IMPLEMENT_FAILURE();
    }

    oC_OS_Cyclic_Start(CyclicFunction1s , "Cyclic" , NULL , oC_CONVERT_TIME_MS_TO_TICKS(1000) , 64);

    /**
    * This section configures RGB diode
     */


    DiodeRGB.PinGreen   =   &PinGreen;
    DiodeRGB.PinBlue    =   &PinBlue;
    DiodeRGB.PinRed     =   &PinRed;
    DiodeRGB.ColorRGB   =   LED_Color_White;

    /**
    * This section shows how to work oC_GPIO_ConnectToTrigger function
    */

    oC_GPIO_ConnectToTrigger(Switch2.PinLink, LED_ChangeBlinkSpeed , NULL );

    /**
    * This section is main loop - shows how to work oC_GPIO_WaitForTrigger function
    */

    while(1)
    {
        LED_On  (&DiodeRGB);

        if(oC_GPIO_WaitForTrigger(Switch1.PinLink, TIME_INTERVAL*DiodeRGB_BlinkSpeed))
        {
            DiodeRGB.ColorRGB++;
            if(DiodeRGB.ColorRGB > LED_Color_White) DiodeRGB.ColorRGB = LED_Color_Red;
        }

        LED_Off (&DiodeRGB);

        if(oC_GPIO_WaitForTrigger(Switch1.PinLink, TIME_INTERVAL*DiodeRGB_BlinkSpeed))
        {
            DiodeRGB.ColorRGB++;
            if(DiodeRGB.ColorRGB > LED_Color_White) DiodeRGB.ColorRGB = LED_Color_Red;
        }
    }

    return -1;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    LOCAL FUNCTION
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool CyclicFunction1s( void * Parameter )
{
    DiodeRGB.ColorRGB++;
    return true;
}

/**
 * This is definition of LED_On Functions
 */

static void LED_On (LED_t* LED)
{
    LED_Off(LED); /**< Reset each one diode pins state*/

    if((LED->ColorRGB & LED_Color_Red)   == LED_Color_Red)   oC_GPIO_SetData(LED->PinRed   -> PinLink, STATE_HIGH);
    if((LED->ColorRGB & LED_Color_Green) == LED_Color_Green) oC_GPIO_SetData(LED->PinGreen -> PinLink, STATE_HIGH);
    if((LED->ColorRGB & LED_Color_Blue)  == LED_Color_Blue)  oC_GPIO_SetData(LED->PinBlue  -> PinLink, STATE_HIGH);
}

static void LED_Off (LED_t* LED)
{
    oC_GPIO_SetData(LED->PinRed  ->PinLink, STATE_LOW); /**< Turns off Red diode pin */
    oC_GPIO_SetData(LED->PinGreen->PinLink, STATE_LOW); /**< Turns off Green diode pin */
    oC_GPIO_SetData(LED->PinBlue ->PinLink, STATE_LOW); /**< Turns off Blue diode pin */
}


/**
 * This is definition of function triggered by pin ("Switch2")
 */

static void LED_ChangeBlinkSpeed (const oC_GPIO_PinLink_t* const Switch2)
{
    DiodeRGB_BlinkSpeed++;
    if(DiodeRGB_BlinkSpeed > LED_BlinkSpeed_Max) DiodeRGB_BlinkSpeed = LED_BlinkSpeed_Min;
}

