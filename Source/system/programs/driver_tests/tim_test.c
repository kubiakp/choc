/** ****************************************************************************************************************************************
 *
 * @file          tim_test.c
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 19 mar 2015 20:49:46) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// Core
#include <choco.h>

// Drivers

// Libraries

// Others

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static volatile int a = 0;

static bool TimeoutInterrupt( void * p )
{
    a++;
    return false;
}

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

oC_OS_PROGRAM_BEGIN(tim_test)
{
    static const oC_TIM_Config_t Config = {
                                           .Channel         = oC_TIM_Channel_WideTimer0 ,
                                           .TickSource      = oC_TIM_TickSource_MainClock ,
                                           .TickPeriod      = oC_TIM_TickPeriod_us(1) ,
                                           .CountsDir       = oC_TIM_CountsDir_Up ,
                                           .Mode            = oC_TIM_Mode_Periodic ,
                                           .Size            = oC_TIM_Size_32Bits ,
                                           .MaxValue        = 1000 ,
                                           .MatchValue      = 0 ,
                                           .TimeoutCallback = TimeoutInterrupt ,
                                           .MatchCallback   = NULL

    };

    oC_OS_Terminal_ResetDevice();

    printf("Timer configuration result: ");
    oC_PrintError( oC_TIM_Configure( &Config ) );
    printf("\n\r");

    while(1)
    {
        oC_OS_DelayMS(1000);
        printf("a = %d\n" , a);
    }

    return 0;
}
oC_OS_PROGRAM_END

/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
