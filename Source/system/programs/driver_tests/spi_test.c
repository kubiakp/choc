/*******************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       spi_test.c
 *
 *    @brief      Test program for SPI driver.
 *
 *    @author     Patryk Kubiak - (Created on: 1 gru 2014 18:43:52) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// Core
#include <choco.h>

// Drivers

// Libraries

// Others

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

int spi_test2( int Argc , char ** Argv )
{
    oC_UNUSED_ARG( Argc );
    oC_UNUSED_ARG( Argv );

    oC_GPIO_Config_t red    = {.PinLink = oC_GPIO_PinLink_PF1 , .Mode = oC_GPIO_Mode_Output};
    oC_GPIO_Config_t green  = {.PinLink = oC_GPIO_PinLink_PF3 , .Mode = oC_GPIO_Mode_Output};
    oC_GPIO_Config_t blue   = {.PinLink = oC_GPIO_PinLink_PF2 , .Mode = oC_GPIO_Mode_Output};
    oC_SPI_Config_t config;
    oC_SPI_Context_t context;
    oC_SPI_Data_t tx[10] , rx[10];

    config.Pins.CS      = oC_GPIO_PinLink_SSI1_Fss_PD1;
    config.Pins.Clk     = oC_GPIO_PinLink_SSI1_Clk_PD0;
    config.Pins.Rx      = oC_GPIO_PinLink_SSI1_Rx_PD2;
    config.Pins.Tx      = oC_GPIO_PinLink_SSI1_Tx_PD3;

    config.Channel      = oC_SPI_Channel_SSI1;

    config.ClockPhase   = oC_SPI_Phase_SecondEdge;
    config.ClockPolarity= oC_SPI_Polarity_LowWhenActive;
    config.DummyMessage = 0xAA;
    config.CSPolarity   = oC_SPI_Polarity_LowWhenActive;
    config.FrameWidth   = oC_SPI_FrameWidth_16Bits;
    config.Frequency    = oC_Frequency_MHz(1);

    if ( !oC_SPI_Configure( &config ) )
    {
        oC_GPIO_SetData(red.PinLink , 0xff);
        oC_IMPLEMENT_FAILURE();
    }

    if ( !oC_SPI_PrepareContext( &context , &config , tx , rx , 10 ) )
    {
        oC_GPIO_SetData(red.PinLink , 0xff);
        oC_IMPLEMENT_FAILURE();
    }

    for(int i = 0 ; i < 10 ; i++)
    {
        tx[i] = 0xAA;
    }

    while(1)
    {
        if ( !oC_SPI_Transceive( &context , 1000 ) )
        {
            oC_GPIO_SetData(red.PinLink , 0xff);
            oC_IMPLEMENT_FAILURE();
        }
        else
        {
            oC_GPIO_SetData(green.PinLink , 0xff);
        }
//        oC_CORE_DelayMS(50);

//        oC_SPI_SendViaStream( &context , "Test send" , 9 , 10 );

//        oC_CORE_DelayMS(50);
    }

    return -1;
}

int spi_test( int Argc , char ** Argv )
{
    oC_UNUSED_ARG( Argc );
    oC_UNUSED_ARG( Argv );


    oC_GPIO_Config_t red    = {.PinLink = oC_GPIO_PinLink_PF1 , .Mode = oC_GPIO_Mode_Output};
    oC_GPIO_Config_t green  = {.PinLink = oC_GPIO_PinLink_PF3 , .Mode = oC_GPIO_Mode_Output};
    oC_GPIO_Config_t blue   = {.PinLink = oC_GPIO_PinLink_PF2 , .Mode = oC_GPIO_Mode_Output};

    if (
              oC_ErrorCode_None != oC_GPIO_Configure( &red )
          ||  oC_ErrorCode_None != oC_GPIO_Configure( &green )
          ||  oC_ErrorCode_None != oC_GPIO_Configure( &blue )
        )
    {
        oC_IMPLEMENT_FAILURE();
    }
    oC_OS_Program_ExecInNewProcess( spi_test2 , 0 , NULL , NULL , NULL , oC_OS_Priority_Parent );

    oC_SPI_Config_t config;
    oC_SPI_Context_t context;
    oC_SPI_Data_t tx[10] , rx[10];

    config.Pins.CS      = oC_GPIO_PinLink_SSI0_Fss_PA3;
    config.Pins.Clk     = oC_GPIO_PinLink_SSI0_Clk_PA2;
    config.Pins.Rx      = oC_GPIO_PinLink_SSI0_Rx_PA4;
    config.Pins.Tx      = oC_GPIO_PinLink_SSI0_Tx_PA5;

    config.Channel      = oC_SPI_Channel_SSI0;

    config.ClockPhase   = oC_SPI_Phase_SecondEdge;
    config.ClockPolarity= oC_SPI_Polarity_LowWhenActive;
    config.DummyMessage = 0xAA;
    config.CSPolarity   = oC_SPI_Polarity_LowWhenActive;
    config.FrameWidth   = oC_SPI_FrameWidth_16Bits;
    config.Frequency    = oC_Frequency_MHz(1);

    if ( !oC_SPI_Configure( &config ) )
    {
        oC_GPIO_SetData(red.PinLink , 0xff);
        oC_IMPLEMENT_FAILURE();
    }

    if ( !oC_SPI_PrepareContext( &context , &config , tx , rx , 10 ) )
    {
        oC_GPIO_SetData(red.PinLink , 0xff);
        oC_IMPLEMENT_FAILURE();
    }

    for(int i = 0 ; i < 10 ; i++)
    {
        tx[i] = i+1;
    }

    while(1)
    {
        if ( !oC_SPI_Transceive( &context , 1000 ) )
        {
            oC_GPIO_SetData(red.PinLink , 0xff);
            oC_IMPLEMENT_FAILURE();
        }
        else
        {
            oC_GPIO_SetData(green.PinLink , 0xff);

        }

    }

    return -1;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
