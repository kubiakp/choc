/*******************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          welcome.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 23 lut 2015 18:17:29) 
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <choco.h>
#include <stdio.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

oC_OS_PROGRAM_BEGIN( Welcome )
{
    oC_OS_Terminal_ResetDevice();
    oC_OS_Terminal_SetBlink();

    oC_OS_Terminal_SetBackgroundColor( oC_Terminal_Color_White );
    oC_OS_Terminal_SetForegroundColor( oC_Terminal_Color_Black );
    printf("  o                         _____ _                        ____   _____ \r");
    printf("   \\   o   ______     o    / ____| |                      / __ \\ / ____|\r");
    printf("o   \\   \\ / - - /|   /    | |    | |__   ___   ___ ___   | |  | | (___  \r");
    printf(" \\   '---/ - - / /--'     | |    | '_ \\ / _ \\ / __/ _ \\  | |  | |\\___ \\ \r");
    printf("  '-----/ - - / /---o     | |____| | | | (_) | (_| (_) | | |__| |____) |\r");
    printf("       /_____/ /----.      \\_____|_| |_|\\___/ \\___\\___/   \\____/|_____/ \r");
    printf("  o----|_____|/--.   \\                                                  \r");
    printf("                  \\   o                                                 \r");
    printf("                   o                                                    \r");

    oC_OS_Terminal_SetBackgroundColor(oC_Terminal_Color_Black);
    oC_OS_Terminal_SetForegroundColor(oC_Terminal_Color_Red);
    printf("\r\tRegards from ChocoOS team...\r\r\r");
    oC_OS_Terminal_SetForegroundColor(oC_Terminal_Color_Green);
    printf("root@localhost $ ");
    oC_OS_Terminal_SetForegroundColor(oC_Terminal_Color_White);

    volatile int time_s = 0;
    oC_Terminal_CursorPos_t cursorHome = oC_OS_Terminal_GetCursorPosition();

    while(1)
    {
        char tab[2];
        oC_OS_DelayMS(1000);
        oC_OS_Terminal_SaveCursorPosition();
        oC_OS_Terminal_CursorUp(1);
        oC_OS_Terminal_EraseCurrentLine();
        oC_OS_Terminal_SetCursorHome( cursorHome );
        oC_OS_Terminal_CursorUp(1);
        time_s++;
        printf("System works %d sec...\r" , time_s);
        oC_OS_Terminal_RestoreCursorPosition();
    }
}
oC_OS_PROGRAM_END

/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
