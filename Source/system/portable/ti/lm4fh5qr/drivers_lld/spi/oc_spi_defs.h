/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_spi_defs.h
 *
 *    @brief      Definitions for the SPI for micro controller
 *
 *    @author     Patryk Kubiak - (Created on: 1 gru 2014 17:17:40) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SPI_OC_SPI_DEFS_H_
#define SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SPI_OC_SPI_DEFS_H_

/*==========================================================================================================================================
//
//     MICRO CONTROLLER DEFINITIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * List of channels in the micro controller. To add channel use macro:
 *      ADD_CHANNEL( CHANNEL_NAME )
 */
//==========================================================================================================================================
#define oC_SPI_ChannelsList( ADD_CHANNEL )                                                                                                  \
    ADD_CHANNEL( SSI0 )                                                                                                                     \
    ADD_CHANNEL( SSI1 )                                                                                                                     \
    ADD_CHANNEL( SSI2 )                                                                                                                     \
    ADD_CHANNEL( SSI3 )

//==========================================================================================================================================
/**
 * List of pins for SPI. To add pin use macro:
 *      ADD_PIN( CHANNEL_NAME , SIGNAL_NAME , PIN_NAME , PCTL_VALUE )
 *
 *              CHANNEL_NAME    - name of channel in SPI ( from oC_SPI_ChannelsList )
 *              SIGNAL_NAME     - name of signal in channel ( Clk / Fss / Rx / Tx )
 *              PIN_NAME        - name of pin ( PA2 , PF3 , ... )
 *              PCTL_VALUE      - value for GPIO PCTL register
 */
//==========================================================================================================================================
#define oC_SPI_PinsList( ADD_PIN )                                                                                                          \
    ADD_PIN( SSI0 , Clk , PA2 , 2 )                                                                                                         \
    ADD_PIN( SSI0 , Fss , PA3 , 2 )                                                                                                         \
    ADD_PIN( SSI0 , Rx  , PA4 , 2 )                                                                                                         \
    ADD_PIN( SSI0 , Tx  , PA5 , 2 )                                                                                                         \
    ADD_PIN( SSI2 , Clk , PB4 , 2 )                                                                                                         \
    ADD_PIN( SSI2 , Fss , PB5 , 2 )                                                                                                         \
    ADD_PIN( SSI2 , Rx  , PB6 , 2 )                                                                                                         \
    ADD_PIN( SSI2 , Tx  , PB7 , 2 )                                                                                                         \
    ADD_PIN( SSI3 , Clk , PD0 , 1 )                                                                                                         \
    ADD_PIN( SSI3 , Fss , PD1 , 1 )                                                                                                         \
    ADD_PIN( SSI3 , Rx  , PD2 , 1 )                                                                                                         \
    ADD_PIN( SSI3 , Tx  , PD3 , 1 )                                                                                                         \
    ADD_PIN( SSI1 , Clk , PD0 , 2 )                                                                                                         \
    ADD_PIN( SSI1 , Fss , PD1 , 2 )                                                                                                         \
    ADD_PIN( SSI1 , Rx  , PD2 , 2 )                                                                                                         \
    ADD_PIN( SSI1 , Tx  , PD3 , 2 )                                                                                                         \
    ADD_PIN( SSI1 , Rx  , PF0 , 2 )                                                                                                         \
    ADD_PIN( SSI1 , Tx  , PF1 , 2 )                                                                                                         \
    ADD_PIN( SSI1 , Clk , PF2 , 2 )                                                                                                         \
    ADD_PIN( SSI1 , Fss , PF3 , 2 )                                                                                                         \

#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SPI_OC_SPI_DEFS_H_ */
