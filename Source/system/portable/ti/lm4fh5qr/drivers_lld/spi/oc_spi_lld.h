/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_spi_lld.h
 *
 *    @brief      The file with lld interface of the SPI
 *
 *    @author     Patryk Kubiak - (Created on: 10 gru 2014 17:28:51) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SPI_OC_SPI_LLD_LLD_H_
#define SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SPI_OC_SPI_LLD_LLD_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_spi.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

/* REQUIRED PROTOTYPES */
extern void          oC_SPI_LLD_TurnOnDriver                                 ( void );
extern void          oC_SPI_LLD_TurnOffDriver                               ( void );

/* CONFIGURE PROTOTYPES */
extern bool          oC_SPI_LLD_Configure                                  ( const oC_SPI_Config_t * Config );

/* HELPER PROTOTYPES */
extern bool          oC_SPI_LLD_ConfigurePins                              ( const oC_SPI_Config_t * Config );
extern bool          oC_SPI_LLD_IsTxFIFOFull                               ( oC_SPI_Channel_t Channel );
extern bool          oC_SPI_LLD_IsRxFIFOEmpty                              ( oC_SPI_Channel_t Channel );
extern void          oC_SPI_LLD_PutToTx                                    ( oC_SPI_Channel_t Channel , oC_SPI_Data_t Data );
extern oC_SPI_Data_t oC_SPI_LLD_GetFromRx                                  ( oC_SPI_Channel_t Channel );
extern int           oC_SPI_LLD_Send                                       ( const oC_SPI_Config_t * Config , oC_SPI_Data_t * Buffer , size_t Size );
extern int           oC_SPI_LLD_Receive                                    ( const oC_SPI_Config_t * Config , oC_SPI_Data_t * Buffer , size_t Size );
extern void          oC_SPI_LLD_SetCSActive                                ( const oC_SPI_Config_t * Config );
extern void          oC_SPI_LLD_SetCSNotActive                             ( const oC_SPI_Config_t * Config );
extern bool          oC_SPI_LLD_RestoreConfig                              ( const oC_SPI_Config_t * Config );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SPI_OC_SPI_LLD_LLD_H_ */
