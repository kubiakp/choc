/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_spi_lld.c
 *
 *    @brief      The file with lld functions for the SPI driver
 *
 *    @author     Patryk Kubiak - (Created on: 1 gru 2014 17:22:56) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_spi.h>
#include <oc_gpio.h>
#include <oc_gpio_lld.h>
#include <oc_reg_defs.h>
#include <oc_dlist.h>
#include <oc_sys.h>
#include <oc_assert.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/

/**
 * Maximum frequency different between real and expected SPI frequency.
 */
#define oC_SPI_MAXIMUM_FREQUENCY_DELTA              oC_Frequency_kHz(500)

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

/**
 * Type for store quick configurations
 */
typedef struct
{
    const oC_SPI_Config_t * Config;
    oC_Frequency_t      ClockFrequency;
    oC_MemoryInt_t          CR0;
    oC_MemoryInt_t          CPSR;
} _QuickConfig_t;

typedef enum
{
    _PinFunction_Clk ,
    _PinFunction_Fss ,
    _PinFunction_Rx ,
    _PinFunction_Tx
} _PinFunction_t;

typedef struct
{
    const oC_GPIO_PinLink_t * const PinLink;
    oC_SPI_Channel_t                Channel;
    _PinFunction_t                  Function;
    uint8_t                         PCTL;
} _PinPCTL_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define _ADD_CHANNEL_TO_INTERRUPTS( CHANNEL_NAME )                  void oC_CAT_3( oC_INT_ , CHANNEL_NAME , Handler )(void) { if(oC_SPI_INT_TransmissionFinished( oC_CAT_2(oC_SPI_Channel_ , CHANNEL_NAME) )){ taskYIELD(); } }
#define _ADD_CHANNEL_TO_RMAPS( CHANNEL_NAME )                       oC_CAT_3( oC_REG_ , CHANNEL_NAME , _RMAP ) ,
#define _ADD_CHANNEL_TO_IRQ_LIST( CHANNEL_NAME )                    oC_CAT_2( oC_REG_NVIC_InterruptNumber_ , CHANNEL_NAME ) ,

//==========================================================================================================================================
/**
 * Macro for adding pin to pins definitions
 */
//==========================================================================================================================================
#define _SPI_ADD_PIN_TO_PINS( CHANNEL_NAME , SIGNAL_NAME , PIN_NAME , ... )   const oC_GPIO_PinLink_t * oC_CAT_6(oC_GPIO_PinLink_, CHANNEL_NAME , _ , SIGNAL_NAME , _ , PIN_NAME ) =  &PIN_NAME ;

//==========================================================================================================================================
/**
 * Macro for adding pin to structure which store information about PCTL for each SPI pin
 */
//==========================================================================================================================================
#define _SPI_ADD_PIN_TO_PINS_STRUCTURE( CHANNEL_NAME , SIGNAL_NAME , PIN_NAME , PCTL_VALUE )      { .PinLink =  &PIN_NAME , .Channel = oC_CAT_2(oC_SPI_Channel_ , CHANNEL_NAME)  , .Function = oC_CAT_2( _PinFunction_ , SIGNAL_NAME ) , .PCTL = PCTL_VALUE } ,

/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

/**
 * List of the references to the register maps
 */
static const oC_REG_SSI_RegisterMap_t * _RMaps[ oC_SPI_Channel_NumberOfChannels ] = { oC_SPI_ChannelsList(_ADD_CHANNEL_TO_RMAPS) };

/**
 * List of the interrupt numbers for each channel
 */
static const IRQn_Type _InterruptNumbers[ oC_SPI_Channel_NumberOfChannels ]    = { oC_SPI_ChannelsList(_ADD_CHANNEL_TO_IRQ_LIST) };

/**
 * List of CTL values for each pins
 */
static const _PinPCTL_t _PinCTL[] = { oC_SPI_PinsList( _SPI_ADD_PIN_TO_PINS_STRUCTURE ) };

/**
 * Container for handle lld variables
 */
static struct
{
    oC_DLIST_t *        QuickConfigures[ oC_SPI_Channel_NumberOfChannels ]; ///< List of QuickConfigures (_QuickConfig_t * type each element)
} SPI_LLD;

oC_SPI_PinsList(_SPI_ADD_PIN_TO_PINS)

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern bool oC_SPI_INT_TransmissionFinished( oC_SPI_Channel_t Channel );

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static inline oC_REG_SSI_RegisterMap_t *    _GetRMap                    ( oC_SPI_Channel_t Channel );
static inline bool                          _CountDivisors              ( oC_Frequency_t Frequency , oC_MemoryInt_t *CPSDVSR , oC_MemoryInt_t *SCR );
static inline void                          _SetClockPhase              ( oC_SPI_Phase_t Phase , oC_MemoryInt_t* CR0 );
static inline void                          _SetClockPolarity           ( oC_SPI_Polarity_t Polarity , oC_MemoryInt_t* CR0 );
static inline void                          _SetClockDivisor            ( oC_MemoryInt_t CPSDVSR , oC_MemoryInt_t* CPSR );
static inline void                          _SetClockRate               ( oC_MemoryInt_t SCR , oC_MemoryInt_t* CR0 );
static inline bool                          _ConfigureClock             ( oC_SPI_Phase_t Phase , oC_SPI_Polarity_t Polarity , oC_Frequency_t Frequency , oC_SPI_Channel_t Channel );
static inline bool                          _SetFrameWidth              ( oC_SPI_Channel_t Channel , oC_SPI_FrameWidth_t FrameWidth );
static inline void                          _SetFreescaleFormat         ( oC_SPI_Channel_t Channel );
static inline bool                          _IsBusy                     ( oC_SPI_Channel_t Channel );
static inline void                          _TurnOnInterrupt            ( oC_SPI_Channel_t Channel );
static inline void                          _TurnOnSSIOperations        ( oC_SPI_Channel_t Channel );
static inline void                          _TurnOffSSIOperations       ( oC_SPI_Channel_t Channel );
static inline void                          _TurnOnPowerForSSI          ( oC_SPI_Channel_t Channel );
static inline bool                          _SaveCurrentRegisterStates  (const oC_SPI_Config_t * Config );
static inline bool                          _RestoreRegisterStates      (const oC_SPI_Config_t * Config );
static        bool                          _GetPCTL                    ( const oC_GPIO_PinLink_t * PinLink , oC_SPI_Channel_t Channel , _PinFunction_t PinFunction , uint8_t * PCTL );
static        bool                          _ConfigurePin               ( const oC_GPIO_PinLink_t * PinLink , oC_SPI_Channel_t Channel , _PinFunction_t PinFunction );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Function called before system starts.
 */
//==========================================================================================================================================
void          oC_SPI_LLD_TurnOnDriver                                 ( void )
{
    for(oC_SPI_Channel_t channel = 0 ; channel < oC_SPI_Channel_NumberOfChannels ; channel++)
    {
        SPI_LLD.QuickConfigures[channel]    = oC_DLIST_New();
    }
}

//==========================================================================================================================================
/**
 * Function called before system restarts
 */
//==========================================================================================================================================
void          oC_SPI_LLD_TurnOffDriver                               ( void )
{
    for(oC_SPI_Channel_t channel = 0 ; channel < oC_SPI_Channel_NumberOfChannels ; channel++)
    {
        oC_DLIST_Delete(SPI_LLD.QuickConfigures[channel]);
    }
}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Reference to the configuration structure
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_SPI_LLD_Configure( const oC_SPI_Config_t * Config )
{
    bool result = false;

    _TurnOnPowerForSSI(Config->Channel);
    _TurnOffSSIOperations(Config->Channel);

    if (
             Config->BitOrder == oC_SPI_BitOrder_MSBFirst
        &&  _ConfigureClock( Config->ClockPhase , Config->ClockPolarity , Config->Frequency , Config->Channel )
        &&  _SetFrameWidth( Config->Channel , Config->FrameWidth )
        )
    {
        _SetFreescaleFormat(Config->Channel);
        _SaveCurrentRegisterStates( Config );
        _TurnOnInterrupt(Config->Channel);
        result = true;
    }

    _TurnOnSSIOperations(Config->Channel);

    return result;
}

//==========================================================================================================================================
/**
 * Function configures pins to work for SPI
 *
 * @param Config        Reference to the configuration structure
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_SPI_LLD_ConfigurePins( const oC_SPI_Config_t * Config )
{
    bool result = false;

    if ( Config )
    {
        oC_GPIO_Config_t cfg;

        oC_GPIO_SetDefaultValuesForConfig( &cfg );

        cfg.PinLink         = Config->Pins.CS;
        cfg.Mode            = oC_GPIO_Mode_Output;
        cfg.Protection      = Config->Pins.Protection;

        result =   oC_GPIO_Configure(&cfg)
                && _ConfigurePin( Config->Pins.Clk , Config->Channel , _PinFunction_Clk )
                && _ConfigurePin( Config->Pins.Rx  , Config->Channel , _PinFunction_Rx )
                && _ConfigurePin( Config->Pins.Tx  , Config->Channel , _PinFunction_Tx );

    }

    return result;
}

//==========================================================================================================================================
/**
 * Checks if Tx FIFO is full
 *
 * @param Channel       Channel to check
 *
 * @return true if full
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsTxFIFOFull( oC_SPI_Channel_t Channel )
{
    oC_ASSERT( Channel < oC_SPI_Channel_NumberOfChannels );

    oC_REG_SSI_RegisterMap_t *rmap = _GetRMap(Channel);
    return ((rmap->SR & oC_REG_SSI_SR_TNF) == 0) ? true : false;
}

//==========================================================================================================================================
/**
 * Checks if Rx FIFO is empty
 *
 * @param Channel       Channel to check
 *
 * @return true if empty
 */
//==========================================================================================================================================
bool          oC_SPI_LLD_IsRxFIFOEmpty                              ( oC_SPI_Channel_t Channel  )
{
    oC_ASSERT( Channel < oC_SPI_Channel_NumberOfChannels );

    oC_REG_SSI_RegisterMap_t *rmap = _GetRMap(Channel);
    return ((rmap->SR & oC_REG_SSI_SR_RNE) == 0) ? true : false;
}

//==========================================================================================================================================
/**
 * Puts data to the Tx FIFO
 *
 * @param Channel       Channel to put data into
 * @param Data          Data to put
 */
//==========================================================================================================================================
void          oC_SPI_LLD_PutToTx                                    ( oC_SPI_Channel_t Channel  , oC_SPI_Data_t Data )
{
    oC_ASSERT( Channel < oC_SPI_Channel_NumberOfChannels );

    oC_REG_SSI_RegisterMap_t *rmap = _GetRMap(Channel);
    rmap->DR = Data;
}

//==========================================================================================================================================
/**
 * Gets data from the Tx FIFO
 *
 * @param Channel       Channel read data from
 *
 * @return Data from Rx FIFO if success
 */
//==========================================================================================================================================
oC_SPI_Data_t oC_SPI_LLD_GetFromRx( oC_SPI_Channel_t Channel  )
{
    oC_ASSERT( Channel < oC_SPI_Channel_NumberOfChannels );
    oC_REG_SSI_RegisterMap_t *rmap = _GetRMap(Channel);

    return rmap->DR;
}

//==========================================================================================================================================
/**
 *
 * @param Config
 * @param Buffer
 * @param Size
 *
 * @return
 */
//==========================================================================================================================================
int oC_SPI_LLD_Send( const oC_SPI_Config_t * Config , oC_SPI_Data_t * Buffer , size_t Size )
{
    int sentBytes = 0;
    size_t bufferIdx;

    Size    /= sizeof(oC_SPI_Data_t); // Size is in bytes

    for(bufferIdx = 0; bufferIdx < Size
                        && !oC_SPI_LLD_IsTxFIFOFull( Config->Channel ); bufferIdx++)
    {
        oC_SPI_LLD_PutToTx( Config->Channel , Buffer[bufferIdx] );
    }

    sentBytes   = bufferIdx * sizeof(oC_SPI_Data_t);

    return sentBytes;
}

//==========================================================================================================================================
/**
 *
 * @param Config
 * @param Buffer
 * @param Size
 *
 * @return
 */
//==========================================================================================================================================
int oC_SPI_LLD_Receive( const oC_SPI_Config_t * Config , oC_SPI_Data_t * Buffer , size_t Size )
{
    int receivedBytes = 0;
    size_t bufferIdx;

    Size    *= sizeof(oC_SPI_Data_t); // Size is in bytes

    for(bufferIdx = 0; bufferIdx < Size
                        && !oC_SPI_LLD_IsRxFIFOEmpty( Config->Channel ); bufferIdx++)
    {
        Buffer[bufferIdx] = oC_SPI_LLD_GetFromRx( Config->Channel );
    }

    receivedBytes   = bufferIdx * sizeof(oC_SPI_Data_t);

    return receivedBytes;
}

//==========================================================================================================================================
/**
 * Activates CS
 *
 * @param Config        Reference to the configuration
 */
//==========================================================================================================================================
void oC_SPI_LLD_SetCSActive( const oC_SPI_Config_t * Config )
{
    if ( Config->CSPolarity == oC_SPI_Polarity_HighWhenActive )
    {
        oC_GPIO_SetData( Config->Pins.CS , oC_GPIO_Pin_All );
    }
    else
    {
        oC_GPIO_SetData( Config->Pins.CS , 0 );
    }
}
//==========================================================================================================================================
/**
 * Disactivates CS
 *
 * @param Config        Reference to the configuration
 */
//==========================================================================================================================================
void oC_SPI_LLD_SetCSNotActive( const oC_SPI_Config_t * Config )
{
    if ( Config->CSPolarity == oC_SPI_Polarity_HighWhenActive )
    {
        oC_GPIO_SetData( Config->Pins.CS , 0 );
    }
    else
    {
        oC_GPIO_SetData( Config->Pins.CS , oC_GPIO_Pin_All );
    }
}

//==========================================================================================================================================
/**
 * Restores configuration from quick saved
 *
 * @param Config        Reference to the configuration to restore
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_SPI_LLD_RestoreConfig( const oC_SPI_Config_t * Config )
{
    return _RestoreRegisterStates( Config );
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Returns register map reference according to the channel number
 *
 * @param Channel       Number of a channel
 *
 * @return reference to the register map
 */
//==========================================================================================================================================
static inline oC_REG_SSI_RegisterMap_t * _GetRMap( oC_SPI_Channel_t Channel )
{
    return _RMaps[Channel];
}

//==========================================================================================================================================
/**
 * Function counts divisors for the clock
 *
 * @param Frequency     Frequency of the SPI to configure
 * @param CPSDVSR       Reference to the register
 * @param SCR           Reference to the register
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool  _CountDivisors( oC_Frequency_t Frequency , oC_MemoryInt_t *CPSDVSR , oC_MemoryInt_t *SCR )
{
    bool result = false;
    oC_MemoryInt_t _CPSDVSR = 0;
    oC_MemoryInt_t _SCR = 0;

    oC_Frequency_t SysClk   = oC_SYS_GetSysClock();
    oC_Frequency_t CLKRatio = SysClk / Frequency;
    _CPSDVSR                    = 0x2;
    _SCR                        = CLKRatio / _CPSDVSR - 1;
    oC_Frequency_t RealFreq = SysClk / (_CPSDVSR * (1 + _SCR));

    while( (_CPSDVSR < 254) && ( (oC_ABS(RealFreq , Frequency) > oC_SPI_MAXIMUM_FREQUENCY_DELTA) || (Frequency < RealFreq) ) )
    {
        _CPSDVSR++;
        _SCR        = CLKRatio / _CPSDVSR - 1;
        RealFreq    = SysClk / ( _CPSDVSR * (1 + _SCR) );
    }

    if ( (oC_ABS( RealFreq ,  Frequency) <= oC_SPI_MAXIMUM_FREQUENCY_DELTA) && (Frequency >= RealFreq) )
    {
        result  = true;
    }

    *CPSDVSR    = _CPSDVSR;
    *SCR        = _SCR;

    return result;
}

//==========================================================================================================================================
/**
  * The function sets clock phase in CR0 register. It also checks parameters by assertions
  *
  * @param Phase        Phase of the clock
  * @param CR0          reference to CR0 register
  */
//==========================================================================================================================================
static inline void _SetClockPhase  ( oC_SPI_Phase_t Phase , oC_MemoryInt_t* CR0 )
{
    if ( Phase == oC_SPI_Phase_FirstEdge )
    {
        oC_CLR_BIT( *CR0 , oC_REG_SSI_CR0_SPH );
    }
    else
    {
        oC_SET_BIT( *CR0 , oC_REG_SSI_CR0_SPH );
    }
}

//==========================================================================================================================================
/**
  * The function sets clock polarity in CR0 register. It also checks parameters by assertions
  *
  * @param polarity Polarity of the clock
  * @param CR0 reference to CR0 register
  */
//==========================================================================================================================================
static inline void _SetClockPolarity(oC_SPI_Polarity_t Polarity , oC_MemoryInt_t* CR0 )
{
    if ( Polarity == oC_SPI_Polarity_HighWhenActive )
    {
        oC_CLR_BIT( *CR0 , oC_REG_SSI_CR0_SPO );
    }
    else
    {
        oC_SET_BIT( *CR0 , oC_REG_SSI_CR0_SPO );
    }
}

//==========================================================================================================================================
/**
  * The function sets clock divisor in CPSR register. It also checks parameters by assertions
  *
  * @param CPSDVSR clock divisor, it should be in range 2-254
  * @param CPSR reference to CPSR register
  */
//==========================================================================================================================================
static inline void _SetClockDivisor( oC_MemoryInt_t CPSDVSR , oC_MemoryInt_t* CPSR )
{
    oC_WRITE_WITH_MASK(*CPSR , oC_REG_SSI_CPSR_CPSDVSR , CPSDVSR);
}

//==========================================================================================================================================
/**
  * The function sets clock rate in CR0 register. It also checks parameters by assertions
  *
  * @param SCR clock rate
  * @param CR0 reference to CR0 register
  */
//==========================================================================================================================================
static inline void _SetClockRate   ( oC_MemoryInt_t SCR , oC_MemoryInt_t* CR0 )
{
    oC_ASSERT( SCR <= 255 );

    oC_WRITE_WITH_MASK(*CR0 , oC_REG_SSI_CR0_SCR , SCR << 8);
}


//==========================================================================================================================================
/**
 * Configures clock of the SPI
 *
 * @param Phase         Phase of the clock
 * @param Polarity      Polarity of the clock
 * @param Frequency     Frequency of the SPI
 * @param Channel       Channel of the SPI
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool _ConfigureClock( oC_SPI_Phase_t Phase , oC_SPI_Polarity_t Polarity , oC_Frequency_t Frequency , oC_SPI_Channel_t Channel )
 {
     bool retVal = false;
     oC_MemoryInt_t SCR;
     oC_MemoryInt_t CPSDVSR;

     if ( true == _CountDivisors(Frequency , &CPSDVSR , &SCR) )
     {
         oC_REG_SSI_RegisterMap_t* rmap;

         rmap    = _GetRMap(Channel);

         oC_MemoryInt_t CR0 , CPSR;

         CR0     = rmap->CR0;
         CPSR    = rmap->CPSR;

         _SetClockPhase( Phase , &CR0 );
         _SetClockPolarity(Polarity , &CR0);
         _SetClockDivisor( CPSDVSR , &CPSR );
         _SetClockRate( SCR , &CR0 );

         rmap->CR0   = CR0;
         rmap->CPSR  = CPSR;

         retVal      = true;
     }

     return retVal;
 }

//==========================================================================================================================================
/**
 * The function sets width of frame in transmission. It also checks parameters and return result NOT OK when parameters wrong
 *
 * @param Channel           Channel of the spi
 * @param FrameWidth        Width of the frame
 *
 * @return false when frameWidth is not ok
 */
//==========================================================================================================================================
static inline bool _SetFrameWidth( oC_SPI_Channel_t Channel , oC_SPI_FrameWidth_t FrameWidth )
{
    bool result = false;

    oC_REG_SSI_RegisterMap_t * rmap = _GetRMap( Channel );

    if ( (FrameWidth >= oC_SPI_FrameWidth_4Bits) && (FrameWidth <= oC_SPI_FrameWidth_16Bits) )
    {
        oC_WRITE_WITH_MASK(rmap->CR0 , oC_REG_SSI_CR0_DSS , (FrameWidth - 1));
        result = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * Sets Freescale format on selected channel
 *
 * @param Channel   Channel to set
 */
//==========================================================================================================================================
static inline void  _SetFreescaleFormat( oC_SPI_Channel_t Channel )
{
    oC_REG_SSI_RegisterMap_t *rmap = _GetRMap(Channel);
    oC_WRITE_WITH_MASK(rmap->CR0 , oC_REG_SSI_CR0_FRF , 0);
}

//==========================================================================================================================================
/**
 * The function checks if channel is busy
 *
 * @param Channel           Channel to SPI
 */
//==========================================================================================================================================
static inline bool _IsBusy( oC_SPI_Channel_t Channel )
{
    oC_REG_SSI_RegisterMap_t *rmap = _GetRMap(Channel);
    return ((rmap->SR & oC_REG_SSI_SR_BSY)==0) ? true : false;
}

//==========================================================================================================================================
/**
  * The function configures interrupt for channel
  *
  * @param Channel          Channel of SPI
  */
//==========================================================================================================================================
static inline void _TurnOnInterrupt( oC_SPI_Channel_t Channel )
{
    oC_REG_SSI_RegisterMap_t* rmap = _GetRMap(Channel);

    oC_SET_BIT( rmap->CR1 , oC_REG_SSI_CR1_EOT );
    oC_SET_BIT( rmap->IM , oC_REG_SSI_IM_TXIM );
    oC_SET_BIT( rmap->IM , oC_REG_SSI_IM_RXIM );

    NVIC_EnableIRQ( _InterruptNumbers[Channel] );
}

//==========================================================================================================================================
/**
 * The function turns on SSI operations
 *
 * @param Channel           Channel of SPI
 */
//==========================================================================================================================================
static inline void _TurnOnSSIOperations( oC_SPI_Channel_t Channel )
 {
     oC_REG_SSI_RegisterMap_t* rmap = _GetRMap(Channel);
     oC_SET_BIT( rmap->CR1 , oC_REG_SSI_CR1_SSE );
 }

//==========================================================================================================================================
/**
 * The function turns off SSI operations
 *
 * @param Channel           Channel of SPI
 */
//==========================================================================================================================================
static inline void _TurnOffSSIOperations( oC_SPI_Channel_t Channel )
{
    oC_REG_SSI_RegisterMap_t* rmap = _GetRMap(Channel);
    oC_CLR_BIT( rmap->CR1 , oC_REG_SSI_CR1_SSE );
}

//==========================================================================================================================================
/**
  * The function is turning on power for selected SSI channel
  *
  * @param Channel          Channel of SPI
  */
//==========================================================================================================================================
static inline void _TurnOnPowerForSSI( oC_SPI_Channel_t Channel )
{
    oC_SET_BIT(oC_REG_SysCtrl_RMAP->RCGCSSI , 1<<Channel);
    oC_SYS_Delay(3);
}

//==========================================================================================================================================
/**
 * Add current register states to quick config list
 *
 * @param Config            Reference to configuration
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool _SaveCurrentRegisterStates(const oC_SPI_Config_t * Config )
{
    bool saved = false;
    oC_REG_SSI_RegisterMap_t * rmap = _GetRMap( Config->Channel );
    _QuickConfig_t * quickRef = NULL;

    foreach( _QuickConfig_t * , quick , SPI_LLD.QuickConfigures[Config->Channel] )
    {
        if ( quick->Config == Config )
        {
            quickRef                = quick;
            break;
        }
    }

    if ( quickRef == NULL )
    {
        quickRef    = malloc( sizeof(_QuickConfig_t) );
    }

    /* cannot be in else condition */
    if ( quickRef != NULL )
    {
        quickRef->ClockFrequency   = oC_SYS_GetSysClock();
        quickRef->Config           = Config;
        quickRef->CR0              = rmap->CR0;
        quickRef->CPSR             = rmap->CPSR;
        saved                      = true;
    }

    return saved;
}

//==========================================================================================================================================
/**
 * Function restores saved configuration registers from quick config list
 *
 * @param Config        Reference to the configuration structure
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool  _RestoreRegisterStates(const oC_SPI_Config_t * Config )
{
    bool restored = false;
    oC_REG_SSI_RegisterMap_t * rmap = _GetRMap( Config->Channel );

    foreach( _QuickConfig_t * , quick , SPI_LLD.QuickConfigures[Config->Channel] )
    {
        if ( quick->Config == Config )
        {
            if ( quick->ClockFrequency == oC_SYS_GetSysClock() )
            {
                rmap->CR0       = quick->CR0;
                rmap->CPSR      = quick->CPSR;
                restored        = true;
            }
            break;
        }
    }

    return restored;
}

//==========================================================================================================================================
/**
 * Reads value for PCTL register
 *
 * @param PinLink           Reference to the pin
 * @param Channel           Channel of the SPI to use on pin
 * @param PinFunction       Function of the pin in SPI channel
 * @param PCTL              [out] reference to a PCTL
 *
 * @return true if pin is on the list
 */
//==========================================================================================================================================
static bool _GetPCTL( const oC_GPIO_PinLink_t * PinLink , oC_SPI_Channel_t Channel , _PinFunction_t PinFunction , uint8_t * PCTL )
{
    bool result = false;

    for(int i = 0 ; i < oC_SIZE_OF_ARRAY(_PinCTL) ; i++)
    {
        if ( _PinCTL[i].PinLink == PinLink && _PinCTL[i].Channel == Channel && _PinCTL[i].Function == PinFunction)
        {
            *PCTL   = _PinCTL[i].PCTL;
            result  = true;
            break;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Configures pin for spi
 *
 * @param PinLink           Reference to the pin to configure
 * @param Channel           Channel of the SPI to configure
 * @param PinFunction       Function of the SPI pin
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool _ConfigurePin( const oC_GPIO_PinLink_t * PinLink , oC_SPI_Channel_t Channel , _PinFunction_t PinFunction )
{
    bool result = false;

    if ( oC_GPIO_IsPinCorrect( PinLink ) )
    {
        uint8_t PCTL = 0;

        if ( _GetPCTL( PinLink , Channel , PinFunction , &PCTL ) )
        {
            result = oC_GPIO_ConfigureAlternative( PinLink , PCTL );
        }
    }

    return result;
}

/*==========================================================================================================================================
//
//     INTERRUPTS
//
//========================================================================================================================================*/

oC_SPI_ChannelsList(_ADD_CHANNEL_TO_INTERRUPTS)
