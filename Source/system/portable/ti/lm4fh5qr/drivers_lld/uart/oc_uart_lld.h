/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld.h Ver 1.0.0
 *
 *    @file       oc_uart2.h
 *
 *    @brief      File with interface functions for the UART2 driver
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-14 - 20:37:04)
 *
 *    @note       Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_UART2_LLD_H
#define _OC_UART2_LLD_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_uart.h>
#include <oc_uart_defs.h>
#include <oc_reg_defs.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/

extern const oC_REG_UART_RegisterMap_t * oC_UART_LLD_RMaps[oC_UART_Channel_NumberOfChannels];

/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

/* REQUIRED PROTOTYPES */
extern void              oC_UART_LLD_TurnOnDriver                               ( void );
extern void              oC_UART_LLD_TurnOffDriver                              ( void );

/* CONFIGURE PROTOTYPES */
extern oC_ErrorCode_t    oC_UART_LLD_Configure                                  ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t    oC_UART_LLD_Unconfigure                                ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t    oC_UART_LLD_SetWordLength                              ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t    oC_UART_LLD_SetBitRate                                 ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t    oC_UART_LLD_SetParity                                  ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t    oC_UART_LLD_SetStopBit                                 ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t    oC_UART_LLD_SetBitOrder                                ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t    oC_UART_LLD_SetInvert                                  ( const oC_UART_Config_t * Config );
extern oC_ErrorCode_t    oC_UART_LLD_ConfigurePins                              ( oC_UART_Channel_t Channel , const oC_UART_Pins_t * const Pins );
extern int               oC_UART_LLD_Send                                       ( const oC_UART_Config_t * Config , uint8_t * Buffer, int Size );
extern int               oC_UART_LLD_Receive                                    ( const oC_UART_Config_t * Config , uint8_t * Buffer, int Size );
extern oC_ErrorCode_t    oC_UART_LLD_SetLoopback                                ( const oC_UART_Config_t * Config , bool Loopback );
extern oC_ErrorCode_t    oC_UART_LLD_TurnOnChannel                              ( oC_UART_Channel_t Channel );
extern oC_ErrorCode_t    oC_UART_LLD_TurnOffChannel                             ( oC_UART_Channel_t Channel );
extern bool              oC_UART_LLD_IsChannelTurnedOn                          ( oC_UART_Channel_t Channel );
extern oC_ErrorCode_t    oC_UART_LLD_EnableChannel                              ( oC_UART_Channel_t Channel );
extern oC_ErrorCode_t    oC_UART_LLD_DisableChannel                             ( oC_UART_Channel_t Channel );
extern bool              oC_UART_LLD_IsChannelEnabled                           ( oC_UART_Channel_t Channel );

extern bool              oC_UART_INT_InterruptHandler                           ( oC_UART_Channel_t Channel );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function returns register map for selected channel. It not checks if channel number is correct. Do not use this function outside of LLD layer.
 *
 * @param Channel       Number of channel
 *
 * @return reference to the register map
 */
//==========================================================================================================================================
static inline oC_REG_UART_RegisterMap_t* oC_UART_LLD_GetRMap( oC_UART_Channel_t Channel )
{
    return oC_UART_LLD_RMaps[Channel];
}

#endif /* _OC_UART2_LLD_H */
