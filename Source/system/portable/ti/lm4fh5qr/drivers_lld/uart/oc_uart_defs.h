/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 * 	  File based on lld_defs.h Ver 1.0.1
 *
 *    @file       
 *
 *    @brief      Definitions for the UART2 for micro controller
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-14 - 20:37:04)
 *
 *    @note       Copyright (C) Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_UART2_OC_UART2_DEFS_H_
#define SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_UART2_OC_UART2_DEFS_H_

/*==========================================================================================================================================
//
//     MICRO CONTROLLER DEFINITIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * List of channels in the micro controller. To add channel use macro:
 *      ADD_CHANNEL( CHANNEL_NAME )
 */
//==========================================================================================================================================
#define oC_UART_ChannelsList( ADD_CHANNEL )                                                                                                \
    ADD_CHANNEL( UART0 )                                                                                                                   \
    ADD_CHANNEL( UART1 )                                                                                                                   \
    ADD_CHANNEL( UART2 )                                                                                                                   \
    ADD_CHANNEL( UART3 )                                                                                                                   \
    ADD_CHANNEL( UART4 )                                                                                                                   \
    ADD_CHANNEL( UART5 )                                                                                                                   \
    ADD_CHANNEL( UART6 )                                                                                                                   \
    ADD_CHANNEL( UART7 )                                                                                                                   \

//==========================================================================================================================================
/**
 * List of pins for UART. To add pin use macro:
 *      ADD_PIN( CHANNEL_NAME , SIGNAL_NAME , PIN_NAME , PCTL_VALUE )
 *
 *              CHANNEL_NAME    - name of channel in UART ( from oC_UART_ChannelsList )
 *              SIGNAL_NAME     - name of signal in channel ( Rx / Tx )
 *              PIN_NAME        - name of pin ( PA2 , PF3 , ... )
 *              PCTL_VALUE      - value for GPIO PCTL register
 */
//==========================================================================================================================================
#define oC_UART_PinsList( ADD_PIN )                                                                                                          \
    ADD_PIN( UART0, Rx  , PA0 , 1 )                                                                                                         \
    ADD_PIN( UART0, Tx  , PA1 , 1 )                                                                                                         \
    ADD_PIN( UART1, Rx  , PB0 , 1 )                                                                                                         \
    ADD_PIN( UART1, Tx  , PB1 , 1 )                                                                                                         \
    ADD_PIN( UART4, Rx  , PC4 , 1 )                                                                                                         \
    ADD_PIN( UART4, Tx  , PC5 , 1 )                                                                                                         \
    ADD_PIN( UART1, Rx  , PC4 , 2 )                                                                                                         \
    ADD_PIN( UART1, Tx  , PC5 , 2 )                                                                                                         \
    ADD_PIN( UART3, Rx  , PC6 , 1 )                                                                                                         \
    ADD_PIN( UART3, Tx  , PC7 , 1 )                                                                                                         \
    ADD_PIN( UART6, Rx  , PD4 , 1 )                                                                                                         \
    ADD_PIN( UART6, Tx  , PD5 , 1 )                                                                                                         \
    ADD_PIN( UART2, Rx  , PD6 , 1 )                                                                                                         \
    ADD_PIN( UART2, Tx  , PD7 , 1 )                                                                                                         \
    ADD_PIN( UART7, Rx  , PE0 , 1 )                                                                                                         \
    ADD_PIN( UART7, Tx  , PE1 , 1 )                                                                                                         \
    ADD_PIN( UART5, Rx  , PE4 , 1 )                                                                                                         \
    ADD_PIN( UART5, Tx  , PE5 , 1 )                                                                                                         \


#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_UART2_OC_UART2_DEFS_H_ */
