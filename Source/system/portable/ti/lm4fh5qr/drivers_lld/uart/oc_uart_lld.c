/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld.c Ver 1.0.1
 *
 *    @file       oc_uart2.h
 *
 *    @brief      File with source of functions of the LLD layer
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-14 - 20:37:04)
 *
 *    @note       Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
 
/*==========================================================================================================================================
//
//    INCLUDES
//
//========================================================================================================================================*/

// General
#include <oc_uart.h>
#include <oc_reg_defs.h>
#include <oc_gpio_lld.h>
#include <oc_os.h>          /// For checking addresses
#include <oc_sys.h>
#include <oc_dlist.h>
#include <oc_uart_lld.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Array with definitions of register maps
 */
//==========================================================================================================================================
const oC_REG_UART_RegisterMap_t * oC_UART_LLD_RMaps[oC_UART_Channel_NumberOfChannels] = {
#define ADD(CHANNEL_NAME)       oC_CAT_3( oC_REG_ , CHANNEL_NAME , _RMAP ) ,
                                                                                         oC_UART_ChannelsList(ADD)
#undef ADD
};

//==========================================================================================================================================
/**
 * Array with definitions of interrupt numbers for each channel
 */
//==========================================================================================================================================
static const IRQn_Type IRQNumbers[oC_UART_Channel_NumberOfChannels] = {
#define ADD(CHANNEL_NAME)                       oC_CAT_2( oC_REG_NVIC_InterruptNumber_ , CHANNEL_NAME ) ,
                                                                       oC_UART_ChannelsList(ADD)
#undef ADD
};

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Function of the pin for configuration
 */
//==========================================================================================================================================
typedef enum
{
    _PinFunction_Rx ,           //!< Pin should works as Rx
    _PinFunction_Tx             //!< Pin should works as Tx
} _PinFunction_t;

//==========================================================================================================================================
/**
 * PCTL function list
 */
//==========================================================================================================================================
typedef struct
{
    const oC_GPIO_PinLink_t * const PinLink;            //!< Reference to the pin link structure
    oC_UART_Channel_t               Channel;            //!< Channel of the UART
    _PinFunction_t                  Function;           //!< Function of the pin
    uint8_t                         PCTL;               //!< Value of PCTL
} _PinPCTL_t;

//==========================================================================================================================================
/**
 * The structure for storing Baud Rate Divisor (specific for baud-rate clock in lm4f120h5qr uC, p. 854)
 */
//==========================================================================================================================================
typedef struct
{
    uint16_t        IntegerPart;                        //!< integer part of the divisor
    uint8_t         FractionalPart:6;                   //!< fractional part of the divisor
    bool            HSESet;                             //!< Flag if HSE bit should be set (for 16 or 8 divisor)
} BaudRateDivisor_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Definitions of PCTL values for UART pins
 */
//==========================================================================================================================================
static const _PinPCTL_t PinPCTLs[]     = {
#define ADD( CHANNEL , SIGNAL , PIN , PCTL_VALUE )    { .PinLink = &PIN , .Channel = oC_CAT_2(oC_UART_Channel_ , CHANNEL) , .Function = oC_CAT_2(_PinFunction_ , SIGNAL) , .PCTL = PCTL_VALUE } ,
                                   oC_UART_PinsList(ADD)
#undef ADD
};

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static inline oC_ErrorCode_t    ConfigurePins                   ( oC_UART_Channel_t Channel , const oC_UART_Pins_t * Pins );
static inline oC_ErrorCode_t    ConfigurePin                    ( const oC_GPIO_PinLink_t * const PinLink , oC_UART_Channel_t Channel , _PinFunction_t PinFunction );
static inline bool              SearchPCTL                      ( const oC_GPIO_PinLink_t * const PinLink , oC_UART_Channel_t Channel , _PinFunction_t PinFunction , uint8_t * outPCTL );
static inline void              TurnOnChannel                   ( oC_UART_Channel_t Channel );
static inline void              TurnOffChannel                  ( oC_UART_Channel_t Channel );
static inline void              DisableChannel                  ( oC_UART_Channel_t Channel );
static inline void              EnableChannel                   ( oC_UART_Channel_t Channel );
static inline bool              IsChannelEnable                 ( oC_UART_Channel_t Channel );
static inline bool              IsChannelTurnedOn               ( oC_UART_Channel_t Channel );
static inline oC_ErrorCode_t    SetWordLength                   ( oC_REG_UART_RegisterMap_t * RMap , oC_UART_WordLength_t WordLength );
static inline oC_ErrorCode_t    SetParity                       ( oC_REG_UART_RegisterMap_t * RMap , oC_UART_Parity_t Parity );
static inline oC_ErrorCode_t    SetStopBit                      ( oC_REG_UART_RegisterMap_t * RMap , oC_UART_StopBit_t StopBit );
static inline oC_ErrorCode_t    SetBitOrder                     ( oC_REG_UART_RegisterMap_t * RMap , oC_UART_BitOrder_t BitOrder );
static inline oC_ErrorCode_t    SetInvert                       ( oC_REG_UART_RegisterMap_t * RMap , oC_UART_Invert_t Invert );
static inline oC_ErrorCode_t    SetBitRate                      ( oC_REG_UART_RegisterMap_t * RMap , uint32_t BitRate );
static inline void              ConfigureInterrupts             ( oC_UART_Channel_t Channel , oC_REG_UART_RegisterMap_t * RMap );
static inline void              FinishConfiguration             ( oC_UART_Channel_t Channel , oC_REG_UART_RegisterMap_t * RMap );
static inline bool              IsTxFull                        ( oC_REG_UART_RegisterMap_t * RMap );
static inline bool              IsRxEmpty                       ( oC_REG_UART_RegisterMap_t * RMap );
static inline void              SendByte                        ( oC_REG_UART_RegisterMap_t * RMap , uint8_t Data );
static inline uint8_t           ReadByte                        ( oC_REG_UART_RegisterMap_t * RMap );
static inline void              ClearInterrupts                 ( oC_UART_Channel_t Channel );

/* uC Specific */
static inline oC_ErrorCode_t    CountBaudRateDivisor            ( uint32_t BitRate , BaudRateDivisor_t * outBaudRateDivisor );
static inline float             CountBRD                        ( uint8_t ClkDiv , uint32_t BaudRate );
static inline bool              IsBRDCorrect                    ( float BRD );
static inline void              SetBaudRateDivisor              ( oC_REG_UART_RegisterMap_t * RMap , BaudRateDivisor_t * BaudRateDivisor );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Function called before system starts.
 */
//==========================================================================================================================================
void oC_UART_LLD_TurnOnDriver( void )
{

}

//==========================================================================================================================================
/**
 * Function called before system restarts
 */
//==========================================================================================================================================
void oC_UART_LLD_TurnOffDriver( void )
{

}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Reference to the configuration structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_Configure( const oC_UART_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , oC_OS_IsAddressCorrect( Config ) ?                     oC_ErrorCode_None : oC_ErrorCode_WrongAddress )
         && oC_AssignErrorCode( &errorCode , Config->Channel < oC_UART_Channel_NumberOfChannels ?   oC_ErrorCode_None : oC_ErrorCode_UARTNotCorrectChannel )
            )
    {
        bool                        channelWasTurnedOn  = IsChannelTurnedOn(    Config->Channel );
        bool                        channelWasEnabled   = (channelWasTurnedOn) ? IsChannelEnable(    Config->Channel ) : false;
        oC_REG_UART_RegisterMap_t * rmap                = oC_UART_LLD_GetRMap(  Config->Channel );

        TurnOnChannel(  Config->Channel );
        DisableChannel( Config->Channel );

        if (
                   oC_AssignErrorCode( &errorCode , ConfigurePins( Config->Channel , &Config->Pins ))
                && oC_AssignErrorCode( &errorCode , SetBitRate(    rmap , Config->BitRate    ) )
                && oC_AssignErrorCode( &errorCode , SetWordLength( rmap , Config->WordLength ) )
                && oC_AssignErrorCode( &errorCode , SetParity(     rmap , Config->Parity     ) )
                && oC_AssignErrorCode( &errorCode , SetStopBit(    rmap , Config->StopBit    ) )
                && oC_AssignErrorCode( &errorCode , SetBitOrder(   rmap , Config->BitOrder   ) )
                && oC_AssignErrorCode( &errorCode , SetInvert(     rmap , Config->Invert     ) )
                )
        {
            FinishConfiguration( Config->Channel , rmap );
            EnableChannel( Config->Channel );

            errorCode   = oC_ErrorCode_None;
        }
        else
        {
            if ( !channelWasTurnedOn )  TurnOffChannel( Config->Channel );
            if ( channelWasEnabled )    EnableChannel( Config->Channel );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Turns off uart channel, and unconfigures others
 *
 * @param Config        Reference to the configuration structure to unconfigure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_Unconfigure( const oC_UART_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            oC_AssignErrorCode( &errorCode , oC_OS_IsAddressCorrect( Config ) ?                     oC_ErrorCode_None : oC_ErrorCode_WrongAddress )
         && oC_AssignErrorCode( &errorCode , Config->Channel < oC_UART_Channel_NumberOfChannels ?   oC_ErrorCode_None : oC_ErrorCode_UARTNotCorrectChannel )
            )
    {
        if ( IsChannelTurnedOn(Config->Channel) )
        {
            if ( IsChannelEnable(Config->Channel) )
            {
                DisableChannel(Config->Channel);
            }

            TurnOffChannel(Config->Channel);
        }
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Refernce to the configuration structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetWordLength( const oC_UART_Config_t * Config )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Refernce to the configuration structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetBitRate   ( const oC_UART_Config_t * Config )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Refernce to the configuration structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetParity    ( const oC_UART_Config_t * Config )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Refernce to the configuration structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetStopBit   ( const oC_UART_Config_t * Config )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Refernce to the configuration structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetBitOrder  ( const oC_UART_Config_t * Config )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Refernce to the configuration structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetInvert    ( const oC_UART_Config_t * Config )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * The function prepares pins to work with UART
 *
 * @param Channel       Channel of the UART to configure
 * @param Pins          Structure with definitions of pins
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ConfigurePins( oC_UART_Channel_t Channel , const oC_UART_Pins_t * const Pins )
{
    return ConfigurePins( Channel , Pins );
}

//==========================================================================================================================================
/**
 * The function for sending data via UART. The selected channel should be configured first.
 *
 * @param Config        Reference to the configuration
 * @param Buffer        Buffer to send
 * @param Size          Size of the buffer to send
 *
 * @return number of send bytes
 */
//==========================================================================================================================================
int oC_UART_LLD_Send( const oC_UART_Config_t * Config , uint8_t * Buffer, int Size )
{
    int bytesSent = 0;

    if ( oC_OS_IsAddressCorrect(Config) && oC_UART_LLD_IsChannelEnabled( Config->Channel ) )
    {
        oC_REG_UART_RegisterMap_t * rmap = oC_UART_LLD_GetRMap(Config->Channel);

        for(bytesSent = 0 ; (bytesSent < Size) && !IsTxFull(rmap) ; bytesSent++)
        {
            SendByte(rmap , Buffer[bytesSent]);
        }
    }

    return bytesSent;
}

//==========================================================================================================================================
/**
 * The function receives buffer from Rx FIFO.
 *
 * @param Config        [in]    Reference to the configuration
 * @param Buffer        [out]   buffer destination for data
 * @param Size          [in]    size of the buffer
 *
 * @return number of received bytes
 */
//==========================================================================================================================================
int oC_UART_LLD_Receive( const oC_UART_Config_t * Config , uint8_t * Buffer, int Size )
{
    int bytesRecevived = 0;

    if ( oC_OS_IsAddressCorrect(Config) && oC_UART_LLD_IsChannelEnabled( Config->Channel ) )
    {
        oC_REG_UART_RegisterMap_t * rmap = oC_UART_LLD_GetRMap(Config->Channel);

        for(bytesRecevived = 0 ; (bytesRecevived < Size) && !IsRxEmpty(rmap) ; bytesRecevived++)
        {
            Buffer[bytesRecevived] = ReadByte(rmap);
        }
    }

    return bytesRecevived;
}


//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Refernce to the configuration structure
 * @param Loopback      True if loopback should be set, or false if should be turned off
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetLoopback( const oC_UART_Config_t * Config , bool Loopback )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * Turns on power for the channel
 *
 * @param Channel       Channel to power on
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_TurnOnChannel( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Channel < oC_UART_Channel_NumberOfChannels )
    {
        TurnOnChannel( Channel );
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTNotCorrectChannel;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Turns off power for the channel
 *
 * @param Channel       Channel to power off
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_TurnOffChannel( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Channel < oC_UART_Channel_NumberOfChannels )
    {
        TurnOffChannel( Channel );
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTNotCorrectChannel;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Checks if channel is turned on
 *
 * @param Channel       Channel to check
 *
 * @return true if turned on (false if turned off or channel is not correct)
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsChannelTurnedOn( oC_UART_Channel_t Channel )
{
    bool turnedOn = false;

    if ( Channel < oC_UART_Channel_NumberOfChannels )
        turnedOn  = IsChannelTurnedOn(Channel);

    return turnedOn;
}

//==========================================================================================================================================
/**
 * Enable operations on selected channel. There is difference between #oC_UART_LLD_TurnOnChannel and #oC_UART_LLD_EnableChannel
 *
 * When channel is turned on, then it can be configured,
 * and when channel is enabled, then it can send and receive messages.
 *
 * @param Channel   Channel to enable
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_EnableChannel( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Channel < oC_UART_Channel_NumberOfChannels )
    {
        EnableChannel( Channel );
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTNotCorrectChannel;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Disable operations on selected channel. There is difference between #oC_UART_LLD_TurnOffChannel and #oC_UART_LLD_DisableChannel
 *
 * When channel is turned on, then it can be configured,
 * and when channel is enabled, then it can send and receive messages.
 *
 * @param Channel   Channel to disable
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_DisableChannel( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Channel < oC_UART_Channel_NumberOfChannels )
    {
        DisableChannel( Channel );
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTNotCorrectChannel;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Checks if channel is enabled. There is difference between #oC_UART_LLD_TurnOnChannel and #oC_UART_LLD_EnableChannel
 *
 * When channel is turned on, then it can be configured,
 * and when channel is enabled, then it can send and receive messages.
 *
 * @param Channel   Channel to check
 *
 * @return true if channel is enabled (false if disabled or the channel is not correct)
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsChannelEnabled( oC_UART_Channel_t Channel )
{
    bool enabled = false;

    if ( Channel < oC_UART_Channel_NumberOfChannels && IsChannelTurnedOn(Channel) )
        enabled  = IsChannelEnable(Channel);

    return enabled;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function configures pins to work with UART
 *
 * @param Channel       Channel of the UART for pins
 * @param Pins          Reference to the pins to configure
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t ConfigurePins( oC_UART_Channel_t Channel , const oC_UART_Pins_t * Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCode( &errorCode , ConfigurePin( Pins->Rx , Channel , _PinFunction_Rx ) )
         && oC_AssignErrorCode( &errorCode , ConfigurePin( Pins->Tx , Channel , _PinFunction_Tx ) )
            )
    {
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Configures pin to work with UART
 *
 * @param PinLink           Reference to the pin link structure
 * @param Channel           Channel of the UART
 * @param PinFunction       Function of pin to configure
 * @param Protection        Protection of special pins
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t ConfigurePin(
                                          IN    const oC_GPIO_PinLink_t * const   PinLink ,
                                          IN    oC_UART_Channel_t                 Channel ,
                                          IN    _PinFunction_t                    PinFunction
                                         )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    uint8_t pctl;

    if ( SearchPCTL( PinLink , Channel , PinFunction , &pctl) )
    {
        if( oC_GPIO_ConfigureAlternative( PinLink , pctl ) )
        {
            errorCode   = oC_ErrorCode_None;
        }
        else
        {
            errorCode   = oC_ErrorCode_UARTCantConfigurePin;
        }
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTPinNotDefined;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Searches PCTL value for pin link to configure UART
 *
 * @param PinLink           Reference to the PinLink structure
 * @param Channel           Channel of UART
 * @param PinFunction       Function of the pin to configure
 * @param outPCTL           [out] reference to the PCTL destination
 *
 * @return true if found
 */
//==========================================================================================================================================
static inline bool SearchPCTL(
                              IN    const oC_GPIO_PinLink_t * const PinLink ,
                              IN    oC_UART_Channel_t               Channel ,
                              IN    _PinFunction_t                  PinFunction ,
                              OUT   uint8_t *                       outPCTL
                            )
{
    bool result = false;

    for(uint8_t i = 0 ; i < oC_SIZE_OF_ARRAY(PinPCTLs) ; i++)
    {
        if ( PinPCTLs[i].PinLink == PinLink && PinPCTLs[i].Channel == Channel && PinPCTLs[i].Function == PinFunction)
        {
            *outPCTL= PinPCTLs[i].PCTL;
            result  = true;
            break;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Turns on power for channel
 *
 * @param Channel   Channel to power on
 */
//==========================================================================================================================================
static inline void TurnOnChannel( oC_UART_Channel_t Channel )
{
    oC_SET_BIT( oC_REG_SysCtrl_RMAP->RCGCUART , 1<<Channel);
    oC_SYS_Delay(30);
}

//==========================================================================================================================================
/**
 * Turns off power for channel
 *
 * @param Channel   Channel to power off
 */
//==========================================================================================================================================
static inline void TurnOffChannel( oC_UART_Channel_t Channel )
{
    oC_CLR_BIT( oC_REG_SysCtrl_RMAP->RCGCUART , 1<<Channel);
    oC_SYS_Delay(30);
}

//==========================================================================================================================================
/**
 * The function disables all operations on selected channel
 *
 * @param Channel   Channel to disable
 */
//==========================================================================================================================================
static inline void DisableChannel( oC_UART_Channel_t Channel )
{
    oC_CLR_BIT( oC_UART_LLD_GetRMap(Channel)->CTL , oC_BIT_0 );
}

//==========================================================================================================================================
/**
 * The function enable operations on channel
 *
 * @param Channel   Channel to enable
 */
//==========================================================================================================================================
static inline void EnableChannel( oC_UART_Channel_t Channel )
{
    oC_SET_BIT( oC_UART_LLD_GetRMap(Channel)->CTL , oC_BIT_0 ); // Enable channel
    oC_SET_BIT( oC_UART_LLD_GetRMap(Channel)->CTL , oC_BIT_8 ); // Enable transmit
    oC_SET_BIT( oC_UART_LLD_GetRMap(Channel)->CTL , oC_BIT_9 ); // Enable receive
}

//==========================================================================================================================================
/**
 * Checks if channel is enable for operations
 *
 * @param Channel   Channel to check
 *
 * @return true if channel is enable and ready to work
 */
//==========================================================================================================================================
static inline bool IsChannelEnable( oC_UART_Channel_t Channel )
{
    return oC_IS_BIT_SET( oC_UART_LLD_GetRMap(Channel)->CTL , 0 );
}

//==========================================================================================================================================
/**
 * Checks if channel is turned on
 *
 * @param Channel   Channel to check
 *
 * @return true if channel is turned on
 */
//==========================================================================================================================================
static inline bool IsChannelTurnedOn( oC_UART_Channel_t Channel )
{
    return oC_IS_BIT_SET(oC_REG_SysCtrl_RMAP->RCGCUART , Channel);
}

//==========================================================================================================================================
/**
 * Sets word length in register map. It also checks if the given word length is correct
 *
 * @param RMap          Reference to the register map
 * @param WordLength    word length
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t SetWordLength( oC_REG_UART_RegisterMap_t * RMap , oC_UART_WordLength_t WordLength )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    const uint8_t supportedWordLength[] = {
                          [oC_UART_WordLength_5Bits] = 0 ,
                          [oC_UART_WordLength_6Bits] = 1 ,
                          [oC_UART_WordLength_7Bits] = 2 ,
                          [oC_UART_WordLength_8Bits] = 3
    };

    if ( WordLength < oC_SIZE_OF_ARRAY(supportedWordLength) )
    {
        oC_WRITE_WITH_MASK( RMap->LCRH , oC_BITS(5 , 6) , supportedWordLength[WordLength] << 5 );
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTWordLengthNotSupported;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Sets parity in the register map. It also checks if given parity is correct.
 *
 * @param RMap      Reference to the register map
 * @param Parity    Parity to set
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t SetParity( oC_REG_UART_RegisterMap_t * RMap , oC_UART_Parity_t Parity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    switch(Parity)
    {
        case oC_UART_Parity_None:
            oC_CLR_BIT( RMap->LCRH , oC_BIT_1 );
            errorCode   = oC_ErrorCode_None;
            break;
        case oC_UART_Parity_Odd:
            oC_SET_BIT( RMap->LCRH , oC_BIT_1 );
            oC_CLR_BIT( RMap->LCRH , oC_BIT_2 );
            errorCode   = oC_ErrorCode_None;
            break;
        case oC_UART_Parity_Even:
            oC_SET_BIT( RMap->LCRH , oC_BIT_1 );
            oC_SET_BIT( RMap->LCRH , oC_BIT_2 );
            errorCode   = oC_ErrorCode_None;
            break;
        default:
            errorCode   = oC_ErrorCode_UARTParityNotSupported;
            break;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Configures mode of the stop bit
 *
 * @param RMap          Reference to the register map
 * @param StopBit       Stop bit mode to configure
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t SetStopBit( oC_REG_UART_RegisterMap_t * RMap , oC_UART_StopBit_t StopBit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    switch(StopBit)
    {
        case oC_UART_StopBit_1Bit:
            oC_CLR_BIT( RMap->LCRH , oC_BIT_3 );
            errorCode   = oC_ErrorCode_None;
            break;
        case oC_UART_StopBit_2Bits:
            oC_SET_BIT( RMap->LCRH , oC_BIT_3 );
            errorCode   = oC_ErrorCode_None;
            break;
        default:
            errorCode   = oC_ErrorCode_UARTStopBitNotSupported;
            break;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Sets bit order in register map, it also checks if bit order is correct.
 *
 * @param RMap      Reference to the register map
 * @param BitOrder  bit order mode
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t SetBitOrder( oC_REG_UART_RegisterMap_t * RMap , oC_UART_BitOrder_t BitOrder )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_UNUSED_ARG( RMap );

    if ( BitOrder == oC_UART_BitOrder_LSBFirst )
    {
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTBitOrderNotSupported;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Sets inverted mode of UART. It also checks if the given invert mode is correct.
 *
 * @param RMap          Reference to the register map
 * @param Invert        Mode of invert
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t SetInvert( oC_REG_UART_RegisterMap_t * RMap , oC_UART_Invert_t Invert )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_UNUSED_ARG( RMap );

    if ( Invert == oC_UART_Invert_NotInverted )
    {
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTInvertNotSupported;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Sets bit rate for selected register map
 *
 * @param RMap      Reference to the register map
 * @param BitRate   Bit rate (or Baud-Rate) to set
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t SetBitRate( oC_REG_UART_RegisterMap_t * RMap , uint32_t BitRate )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    BaudRateDivisor_t BaudRateDivisor;

    if (
            oC_AssignErrorCode( &errorCode , CountBaudRateDivisor( BitRate , &BaudRateDivisor ) )
            )
    {
        SetBaudRateDivisor(RMap , &BaudRateDivisor);
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function configures interrupts to work with UART
 *
 * @param Channel   Channel of the UART
 * @param RMap      Reference to the register map
 */
//==========================================================================================================================================
static inline void ConfigureInterrupts( oC_UART_Channel_t Channel , oC_REG_UART_RegisterMap_t * RMap )
{
    oC_SET_BIT( RMap->IM , oC_BIT_5 );  // Enable transmit interrupt
    oC_SET_BIT( RMap->IM , oC_BIT_4 );  // Enable receive interrupt

    NVIC_EnableIRQ( IRQNumbers[Channel] );
}

//==========================================================================================================================================
/**
 * The function finishes configuration by setting additional bits like interrupt mask etc.
 *
 * @param Channel   Channel of the UART
 * @param RMap      Reference to the register map
 */
//==========================================================================================================================================
static inline void FinishConfiguration( oC_UART_Channel_t Channel , oC_REG_UART_RegisterMap_t * RMap )
{
    oC_SET_BIT( RMap->LCRH , oC_BIT_4 );    // Enable FIFO

    ConfigureInterrupts(Channel , RMap);
}

//==========================================================================================================================================
/**
 * Checks if tx is full
 *
 * @param RMap  Reference to the register map
 *
 * @return true if full
 */
//==========================================================================================================================================
static inline bool IsTxFull( oC_REG_UART_RegisterMap_t * RMap )
{
    return oC_IS_BIT_SET( RMap->FR , 5 );
}

//==========================================================================================================================================
/**
 * Checks if receive fifo is empty
 *
 * @param RMap  Reference to the register map
 *
 * @return true if empty
 */
//==========================================================================================================================================
static inline bool IsRxEmpty( oC_REG_UART_RegisterMap_t * RMap )
{
    return oC_IS_BIT_SET( RMap->FR , 4 );
}

//==========================================================================================================================================
/**
 * The function sends 1 byte to transmit FIFO
 *
 * @param RMap  Reference to the register map
 * @param Data  Data to send
 */
//==========================================================================================================================================
static inline void SendByte( oC_REG_UART_RegisterMap_t * RMap , uint8_t Data )
{
    RMap->DR = Data;
}

//==========================================================================================================================================
/**
 * The function reads one byte from Rx FIFO
 *
 * @param RMap  Reference to the register map
 *
 * @return data from Rx FIFO
 */
//==========================================================================================================================================
static inline uint8_t ReadByte( oC_REG_UART_RegisterMap_t * RMap )
{
    return (uint8_t)RMap->DR;
}

//==========================================================================================================================================
/**
 * Clears interrupt flags
 *
 * @param Channel   Channel to clear
 */
//==========================================================================================================================================
static inline void ClearInterrupts( oC_UART_Channel_t Channel )
{
   oC_REG_UART_RegisterMap_t * rmap = oC_UART_LLD_GetRMap(Channel);

   rmap->ICR    = 0xFFFFU;
}


//==========================================================================================================================================
/**
 * Counts Baud-Rate Divisor (it is described in lm4f120h5qr doc on page 854)
 *
 * @param BitRate               [in]    Bit rate to achieve
 * @param outBaudRateDivisor    [out]   Baud-Rate Divisor structure reference (the values for prescaler register settings)
 *
 * @return code of error
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t CountBaudRateDivisor(
                                                   IN   uint32_t BitRate ,
                                                   OUT  BaudRateDivisor_t * outBaudRateDivisor
                                                   )
{
    oC_ErrorCode_t      errorCode   = oC_ErrorCode_ImplementError;

    if ( BitRate > 0 )
    {
        if ( oC_OS_IsRAMAddress( outBaudRateDivisor ) )
        {
            float brdFloat              = CountBRD( 8 , BitRate );
            outBaudRateDivisor->HSESet  = true;

            if ( !IsBRDCorrect(brdFloat) )
            {
                brdFloat                    = CountBRD(16 , BitRate);
                outBaudRateDivisor->HSESet  = false;
            }

            if ( IsBRDCorrect(brdFloat) )
            {
                outBaudRateDivisor->IntegerPart     = (uint16_t)brdFloat;
                float       BRDF                    = brdFloat - (float)outBaudRateDivisor->IntegerPart;
                outBaudRateDivisor->FractionalPart  = (uint8_t)(BRDF * 64 + 0.5);
                errorCode                           = oC_ErrorCode_None;
            }
            else
            {
                errorCode   = oC_ErrorCode_UARTBitRateNotSupported;
            }
        }
        else
        {
            // The outBaudRateDivisor is not RAM address! (program should not be ever here!)
            oC_IMPLEMENT_FAILURE();
        }
    }
    else
    {
        errorCode   = oC_ErrorCode_UARTBitRateNotSupported;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function counts BRD factor according to equation
 *
 *      BRD = UARTSysClk / (ClkDiv * Baud Rate )
 *
 * @param ClkDiv        ClkDiv
 * @param BaudRate      (or BitRate) baud rate to achieve
 *
 * @return BRD factor
 */
//==========================================================================================================================================
static inline float CountBRD( uint8_t ClkDiv , uint32_t BaudRate )
{
    return oC_SYS_GetSysClock() / ((float)(ClkDiv * BaudRate));
}

//==========================================================================================================================================
/**
 * Checks if the BRD factor is correct
 *
 * @param BRD   BRD factor to check
 *
 * @return true if factor is supported on this micro-controller
 */
//==========================================================================================================================================
static inline bool IsBRDCorrect( float BRD )
{
    return (BRD) <= ((float) (0xFFFF + 0x0001) );
}

//==========================================================================================================================================
/**
 * Sets baud rate divisor in register map
 *
 * @param RMap              Reference to the correct register map
 * @param BaudRateDivisor   Baud-Rate Divisor factors
 *
 */
//==========================================================================================================================================
static inline void SetBaudRateDivisor( oC_REG_UART_RegisterMap_t * RMap , BaudRateDivisor_t * BaudRateDivisor )
{
    RMap->IBRD  = BaudRateDivisor->IntegerPart;
    RMap->FBRD  = BaudRateDivisor->FractionalPart;
    RMap->CC    = 0;

    if ( BaudRateDivisor->HSESet )
    {
        oC_SET_BIT( RMap->CTL , oC_BIT_5 );
    }
    else
    {
        oC_CLR_BIT( RMap->CTL , oC_BIT_5 );
    }
}

/*==========================================================================================================================================
//
//     INTERRUPTS
//
//========================================================================================================================================*/

#define ADD(CHANNEL_NAME) void oC_CAT_3( oC_INT_ , CHANNEL_NAME , Handler )(void) { ClearInterrupts( oC_CAT_2(oC_UART_Channel_ , CHANNEL_NAME) ); if(oC_UART_INT_InterruptHandler( oC_CAT_2(oC_UART_Channel_ , CHANNEL_NAME) )){ taskYIELD(); } }
oC_UART_ChannelsList(ADD)
#undef ADD
