/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld_types.h Ver 1.0.0
 *
 *    @file       oc_gpio.h
 *
 *    @brief      File with types for the LLD layer of the GPIO driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-11-13 - 10:58:46)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
 
#ifndef _OC_GPIO_LLD_TYPES_H
#define _OC_GPIO_LLD_TYPES_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// General
#include <oc_gpio_pinmap.h>
#include <oc_gen_macros.h>

// Drivers

// Libraries

// Others
#include <stdint.h>

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

/**
 * Creates enum value for the selected port
 *
 * @param PORT_NAME     name of the port ( A , B , C , etc)
 */
#define oC_GPIO_CREATE_ENUM_VALUE_FOR_PORT( PORT_NAME  )                        oC_CAT_2( oC_GPIO_Port_ , PORT_NAME )

/**
 * The macro for adding port for the list of the ports
 */
#define oC_GPIO_ADD_PORT_TO_ENUM( PORT_NAME , PIN_MASK )                        oC_GPIO_CREATE_ENUM_VALUE_FOR_PORT( PORT_NAME ) ,

/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

/**
 * The type represents list of masks for each GPIO pin
 */
typedef enum
{
    oC_GPIO_Pin_None     = 0 ,
    oC_GPIO_Pin_0        = oC_BIT_0  ,//!< oC_GPIO_Pin_0
    oC_GPIO_Pin_1        = oC_BIT_1  ,//!< oC_GPIO_Pin_1
    oC_GPIO_Pin_2        = oC_BIT_2  ,//!< oC_GPIO_Pin_2
    oC_GPIO_Pin_3        = oC_BIT_3  ,//!< oC_GPIO_Pin_3
    oC_GPIO_Pin_4        = oC_BIT_4  ,//!< oC_GPIO_Pin_4
    oC_GPIO_Pin_5        = oC_BIT_5  ,//!< oC_GPIO_Pin_5
    oC_GPIO_Pin_6        = oC_BIT_6  ,//!< oC_GPIO_Pin_6
    oC_GPIO_Pin_7        = oC_BIT_7  ,//!< oC_GPIO_Pin_7
    oC_GPIO_Pin_All      = 0xffU      //!< oC_GPIO_Pin_All
} oC_GPIO_Pin_t;

/**
 * The type represents ports available in the uC
 */
typedef enum
{
    oC_GPIO_PortsList( oC_GPIO_ADD_PORT_TO_ENUM )
    oC_GPIO_Port_NumberOfPorts
} oC_GPIO_Port_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/


#endif /* _OC_GPIO_LLD_TYPES_H */
