/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_gpio_defs.c
 *
 *    @brief      The file with definitions of the pin-map variables.
 *
 *    @author     Patryk Kubiak - (Created on: 13 lis 2014 14:40:01) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// Core
#include <oc_reg_defs.h>
#include <oc_gpio_types.h>
#include <oc_gpio_defs.h>

// Drivers

// Libraries

// Others

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/


#define _ADD_PIN_TO_PIN_LINK_DEFINITION( PORT_NAME , PIN_NUMBER )                     const oC_GPIO_PinLink_t oC_CAT_3( P , PORT_NAME , PIN_NUMBER ) = { .Port = oC_CAT_2( oC_GPIO_Port_ , PORT_NAME ) , .Pin = oC_CAT_2( oC_GPIO_Pin_ , PIN_NUMBER ) };
#define _ADD_PIN_TO_PIN_LINK_REFERENCE_DEFINITION( PORT_NAME , PIN_NUMBER )           const oC_GPIO_PinLink_t * const oC_CAT_3( oC_GPIO_PinLink_P , PORT_NAME , PIN_NUMBER ) = &oC_CAT_3( P , PORT_NAME , PIN_NUMBER );
#define _ADD_PORT_TO_RMAPS( PORT_NAME , PIN_MASK )                                    oC_CAT_4(oC_REG_GPIO , PORT_NAME , oC_GPIO_PERIPHERAL_BUS , _RMAP ) ,
#define _ADD_PORT_TO_IRQ_NUMBERS( PORT_NAME , PIN_MASK )                              oC_CAT_2(oC_REG_NVIC_InterruptNumber_GPIO , PORT_NAME ) ,

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

oC_GPIO_PinList( _ADD_PIN_TO_PIN_LINK_DEFINITION );
oC_GPIO_PinList( _ADD_PIN_TO_PIN_LINK_REFERENCE_DEFINITION );

const oC_REG_GPIO_RegisterMap_t* oC_GPIO_RMaps[] = { oC_GPIO_PortsList( _ADD_PORT_TO_RMAPS ) };
const IRQn_Type oC_GPIO_IRQ_Numbers[] = { oC_GPIO_PortsList( _ADD_PORT_TO_IRQ_NUMBERS ) };

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
