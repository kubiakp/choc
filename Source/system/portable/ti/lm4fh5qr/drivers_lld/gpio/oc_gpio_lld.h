/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld_interface.h Ver 1.0.0
 *
 *    @file       oc_gpio.h
 *
 *    @brief      File with interface of LLD layer of the GPIO driver
 *
 *    @author     Patryk Kubiak - (Created on: 2014-11-13 - 10:58:46)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
 
#ifndef _OC_GPIO_LLD_LLD_H
#define _OC_GPIO_LLD_LLD_H

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// Drivers
#include <oc_gpio_types.h>                   // access to types of the driver interface

// Libraries

// Others

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern void oC_GPIO_LLDDriverInit               ( void );
extern void oC_GPIO_LLDDriverDeinit             ( void );
extern void oC_GPIO_InitializeConfig            ( const oC_GPIO_Config_t * const Config );
extern void oC_GPIO_FinishConfig                ( const oC_GPIO_Config_t * const Config );
extern void oC_GPIO_FinishFailedConfig          ( const oC_GPIO_Config_t * const Config );
extern bool oC_GPIO_SetMode                     ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_Mode_t Mode );
extern bool oC_GPIO_SetOutputCircuit            ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_OutputCircuit_t OutputCircuit );
extern bool oC_GPIO_SetPull                     ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_Pull_t Pull );
extern bool oC_GPIO_SetCurrent                  ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_Current_t Current );
extern bool oC_GPIO_SetSpeed                    ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_Speed_t Speed );
extern bool oC_GPIO_SetInterrupt                ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_IntTrigger_t Trigger );
extern bool oC_GPIO_InterruptCallbacksHandler   ( oC_GPIO_Port_t Port , oC_GPIO_Pin_t Pins );
extern bool oC_GPIO_ConfigureAlternative        ( const oC_GPIO_PinLink_t * const PinLink , uint8_t AlternativeValue );
extern bool oC_GPIO_AddPinToUsed                ( const oC_GPIO_PinLink_t * const PinLink );
extern bool oC_GPIO_RemovePinFromUsed           ( const oC_GPIO_PinLink_t * const PinLink );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/


#endif /* _OC_GPIO_LLD_LLD_H */
