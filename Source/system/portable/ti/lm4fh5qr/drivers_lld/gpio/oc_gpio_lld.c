/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld.c Ver 1.0.0
 *
 *    @file       oc_gpio.h
 *
 *    @brief      File with source of functions of the LLD layer
 *
 *    @author     Patryk Kubiak - (Created on: 2014-11-13 - 10:58:46)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
 
/*==========================================================================================================================================
//
//    INCLUDES
//
//========================================================================================================================================*/

// General
#include <oc_drivers.h>            // Access to drivers interface
#include <oc_gpio_lld.h>           // Access to lld interface of this driver
#include <oc_gpio_lld_quick.h>
#include <oc_reg_defs.h>
#include <oc_gen_macros.h>          // UNUSED ARG macro
#include <oc_instr_defs.h>
#include <oc_assert.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define _ADD_PORT_TO_INTERRUPT_HANDLER( PORT_NAME , PIN_MASK )      void oC_CAT_3( oC_INT_GPIO, PORT_NAME , Handler)( void ) { _InterruptHandler( oC_CAT_2(oC_GPIO_Port_ , PORT_NAME) ); }

/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static inline void _TurnPowerForPort                    ( const oC_GPIO_PinLink_t * const PinLink );
static inline void _TurnOnBus                           ( const oC_GPIO_PinLink_t * const PinLink );
static inline bool _IsSpecialPin                        ( const oC_GPIO_PinLink_t * const PinLink );
static inline void _UnlockProtection                    ( const oC_GPIO_PinLink_t * const PinLink );
static inline bool _GetPinNumber                        ( oC_GPIO_Pin_t Pin , uint8_t * PinNumber );
static inline void _SetPinFunction                      ( oC_MemoryInt_t DigitalFunction , uint8_t PinNumber , oC_MemoryInt_t * PCTL );

/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Function for initialization lld layer. It is called before system starts
 */
//==========================================================================================================================================
void oC_GPIO_LLDDriverInit( void )
{

}

//==========================================================================================================================================
/**
 * Function for initialization lld layer. It is called before system starts
 */
//==========================================================================================================================================
void oC_GPIO_LLDDriverDeinit( void )
{

}

//==========================================================================================================================================
/** 
 * Function called before configuration. It should be called everytime before configuration. 
 * 
 * @param Config         Reference to the configuration to prepare driver
 *
 */
//==========================================================================================================================================
void oC_GPIO_InitializeConfig( const oC_GPIO_Config_t * const Config )
{
    oC_ASSERT( Config );

    _TurnPowerForPort( Config->PinLink );
    _TurnOnBus( Config->PinLink );

    // Unlock special pins
    if ( _IsSpecialPin( Config->PinLink ) )
    {
        // This pin is special, and can you must unlock it to configure
        oC_ASSERT( Config->Protection == oC_GPIO_Protection_UnlockProtectedPins );

        _UnlockProtection( Config->PinLink );
    }
}

//==========================================================================================================================================
/**
 * Function called after successful configuration
 *
 * @param Config         Reference to the configuration
 */
//==========================================================================================================================================
void oC_GPIO_FinishConfig( const oC_GPIO_Config_t * const Config )
{
    (void) Config;
}

//==========================================================================================================================================
/**
 * Function called after failed configuration
 *
 * @param Config         Reference to the configuration
 */
//==========================================================================================================================================
void oC_GPIO_FinishFailedConfig( const oC_GPIO_Config_t * const Config )
{
    (void) Config;
}

//==========================================================================================================================================
/**
 * Sets mode of the GPIO
 *
 * @param PinLink        Reference to the pin link structure
 * @param Mode           Mode of the pin
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_SetMode                     ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_Mode_t Mode )
{
    bool retVal = true;
    oC_REG_GPIO_RegisterMap_t* rmap;

    oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

    rmap            = __oC_GPIO_GetRegisterMap( PinLink->Port );

    switch(Mode)
    {
    case oC_GPIO_Mode_Default:
    case oC_GPIO_Mode_Output:
        oC_SET_BIT( rmap->DIR ,   PinLink->Pin );
        oC_CLR_BIT( rmap->AFSEL , PinLink->Pin );
        oC_SET_BIT( rmap->DEN ,   PinLink->Pin );
        break;
    case oC_GPIO_Mode_Input:
        oC_CLR_BIT( rmap->DIR ,   PinLink->Pin );
        oC_CLR_BIT( rmap->AFSEL , PinLink->Pin );
        oC_SET_BIT( rmap->DEN ,   PinLink->Pin );
        break;
    case oC_GPIO_Mode_Alternate:
        oC_SET_BIT( rmap->AFSEL , PinLink->Pin );
        oC_SET_BIT( rmap->DEN ,   PinLink->Pin );
        break;
    default:
        retVal  = false;
        break;
    }

    return retVal;
}

//==========================================================================================================================================
/**
 * Sets output circuit mode.
 *
 * @param PinLink        Reference to the pin link structure
 * @param OutputCircuit  Circuit of the output
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_SetOutputCircuit            ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_OutputCircuit_t OutputCircuit )
{
    bool retVal = true;
    oC_REG_GPIO_RegisterMap_t* rmap;

    oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

    rmap            = __oC_GPIO_GetRegisterMap( PinLink->Port );

    switch( OutputCircuit )
    {
    case oC_GPIO_OutputCircuit_Default:
    case oC_GPIO_OutputCircuit_OpenDrain:
        oC_SET_BIT(rmap->ODR , PinLink->Pin);
        break;
    case oC_GPIO_OutputCircuit_PushPull:
        oC_CLR_BIT(rmap->ODR , PinLink->Pin);
        break;
    default:
        retVal  = false;
    }

    return retVal;
}

//==========================================================================================================================================
/**
 * Sets pull of the GPIO
 *
 * @param PinLink       Reference to the pin link structure
 * @param Pull          Pull to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_SetPull                     ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_Pull_t Pull )
{
    bool retVal = true;
    oC_REG_GPIO_RegisterMap_t* rmap;

    oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

    rmap            = __oC_GPIO_GetRegisterMap( PinLink->Port );

    switch ( Pull )
    {
    case oC_GPIO_Pull_Default:
    case oC_GPIO_Pull_Up:
        oC_SET_BIT( rmap->PUR , PinLink->Pin );
        oC_CLR_BIT( rmap->PDR , PinLink->Pin );
        break;
    case oC_GPIO_Pull_Down:
        oC_CLR_BIT( rmap->PUR , PinLink->Pin );
        oC_SET_BIT( rmap->PDR , PinLink->Pin );
        break;
    default:
        retVal  = false;
        break;
    }

    return retVal;
}

//==========================================================================================================================================
/**
 * Sets output current on the pin
 *
 * @param PinLink       Reference to the pin link structure
 * @param Current       Output current to set
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_SetCurrent                  ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_Current_t Current )
{
    bool retVal = true;
    oC_REG_GPIO_RegisterMap_t* rmap;

    oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

    rmap            = __oC_GPIO_GetRegisterMap( PinLink->Port );

    switch( Current )
    {
    case oC_GPIO_Current_Default:
    case oC_GPIO_Current_Maximum:
        oC_CLR_BIT( rmap->DR2R , PinLink->Pin );
        oC_CLR_BIT( rmap->DR4R , PinLink->Pin );
        oC_SET_BIT( rmap->DR8R , PinLink->Pin );
        break;
    case oC_GPIO_Current_Medium:
        oC_CLR_BIT( rmap->DR2R , PinLink->Pin );
        oC_SET_BIT( rmap->DR4R , PinLink->Pin );
        oC_CLR_BIT( rmap->DR8R , PinLink->Pin );
        break;
    case oC_GPIO_Current_Minimum:
        oC_SET_BIT( rmap->DR2R , PinLink->Pin );
        oC_CLR_BIT( rmap->DR4R , PinLink->Pin );
        oC_CLR_BIT( rmap->DR8R , PinLink->Pin );
        break;
    default:
        retVal  = false;
        break;
    }

    return retVal;
}

//==========================================================================================================================================
/**
 * Sets speed of the pin
 *
 * @param PinLink       Reference to the pin link structure
 * @param Speed         Speed of the pin
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_SetSpeed                    ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_Speed_t Speed )
{
    bool retVal = true;

    (void)Speed;
    (void)PinLink;

    return retVal;
}

//==========================================================================================================================================
/**
 * Configures interrupt for the pin
 *
 * @param PinLink       Reference to the pin link structure
 * @param Trigger       Trigger of the interrupt
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_SetInterrupt                ( const oC_GPIO_PinLink_t * const PinLink , oC_GPIO_IntTrigger_t Trigger )
{
    bool retVal = true;
        oC_REG_GPIO_RegisterMap_t* rmap;

        oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

        rmap            = __oC_GPIO_GetRegisterMap(PinLink->Port);

        switch( Trigger )
        {
        case oC_GPIO_IntTrigger_Default:
        case oC_GPIO_IntTrigger_Off:
            oC_CLR_BIT(rmap->IM , PinLink->Pin);
            break;
        case oC_GPIO_IntTrigger_BothEdges:
            NVIC_EnableIRQ(oC_GPIO_IRQ_Numbers[PinLink->Port]);

            oC_CLR_BIT( rmap->IS ,  PinLink->Pin );
            oC_SET_BIT( rmap->IBE , PinLink->Pin );
            oC_SET_BIT( rmap->IM ,  PinLink->Pin );

            break;
        case oC_GPIO_IntTrigger_BothLevels:
            NVIC_EnableIRQ(oC_GPIO_IRQ_Numbers[PinLink->Port]);

            oC_SET_BIT( rmap->IS ,  PinLink->Pin );
            oC_SET_BIT( rmap->IBE , PinLink->Pin );
            oC_SET_BIT( rmap->IM ,  PinLink->Pin );

            break;
        case oC_GPIO_IntTrigger_FallingEdge:
            NVIC_EnableIRQ(oC_GPIO_IRQ_Numbers[PinLink->Port]);

            oC_CLR_BIT( rmap->IS  , PinLink->Pin );
            oC_CLR_BIT( rmap->IBE , PinLink->Pin );
            oC_CLR_BIT( rmap->IEV , PinLink->Pin );
            oC_SET_BIT( rmap->IM ,  PinLink->Pin );

            break;
        case oC_GPIO_IntTrigger_RisingEdge:
            NVIC_EnableIRQ(oC_GPIO_IRQ_Numbers[PinLink->Port]);

            oC_CLR_BIT( rmap->IS  , PinLink->Pin );
            oC_CLR_BIT( rmap->IBE , PinLink->Pin );
            oC_SET_BIT( rmap->IEV , PinLink->Pin );
            oC_SET_BIT( rmap->IM ,  PinLink->Pin );

            break;
        case oC_GPIO_IntTrigger_HighLevel:
            NVIC_EnableIRQ(oC_GPIO_IRQ_Numbers[PinLink->Port]);

            oC_SET_BIT( rmap->IS  , PinLink->Pin );
            oC_CLR_BIT( rmap->IBE , PinLink->Pin );
            oC_SET_BIT( rmap->IEV , PinLink->Pin );
            oC_SET_BIT( rmap->IM ,  PinLink->Pin );

            break;
        case oC_GPIO_IntTrigger_LowLevel:
            NVIC_EnableIRQ(oC_GPIO_IRQ_Numbers[PinLink->Port]);

            oC_SET_BIT( rmap->IS  , PinLink->Pin );
            oC_CLR_BIT( rmap->IBE , PinLink->Pin );
            oC_CLR_BIT( rmap->IEV , PinLink->Pin );
            oC_SET_BIT( rmap->IM ,  PinLink->Pin );

            break;
        default:
            retVal = false;
            break;
        }

        return retVal;
}

//==========================================================================================================================================
/**
 *
 * @param PinLink               Reference to the pin link structure
 * @param AlternativeValue      Alternative function value, for the AFSEL register
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_GPIO_ConfigureAlternative        ( const oC_GPIO_PinLink_t * const PinLink , uint8_t AlternativeValue )
{
    bool result = true;
    oC_GPIO_Pin_t pins = PinLink->Pin;

    oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

    oC_REG_GPIO_RegisterMap_t* rmap;

    rmap    = __oC_GPIO_GetRegisterMap( PinLink->Port );

    _TurnPowerForPort( PinLink );
    _TurnOnBus( PinLink );

    uint8_t pinNumber = 0;

    oC_SET_BIT( rmap->AFSEL , PinLink->Pin );
    oC_CLR_BIT( rmap->ODR , PinLink->Pin );
    oC_SET_BIT( rmap->DEN , PinLink->Pin );

    while ( true == _GetPinNumber(pins , &pinNumber) )
    {
        _SetPinFunction( AlternativeValue , pinNumber , (oC_MemoryInt_t*)&rmap->PCTL );

        oC_CLR_BIT(pins , 1<<pinNumber);
    }

    if ( pinNumber == 0 )
    {
        // No one pin is given!
        result  = false;
    }

    if ( result )
    {
        oC_GPIO_AddPinToUsed( PinLink );
    }

    return result;
}

/*==========================================================================================================================================
//
//    LOCAL FUNCTIONS
//
//========================================================================================================================================*/


//==========================================================================================================================================
/**
 * The function turn-on power for port
 *
 * @param PinLink               Reference to the pin link structure
 */
//==========================================================================================================================================
static inline void _TurnPowerForPort                    ( const oC_GPIO_PinLink_t * const PinLink )
{
    oC_SET_BIT( oC_REG_SysCtrl_RMAP->RCGCGPIO , 1 << PinLink->Port);

    oC_NOP();
    oC_NOP();
    oC_NOP();
}

//==========================================================================================================================================
/**
 * The function turn-on power for bus
 *
 * @param PinLink               Reference to the pin link structure
 */
//==========================================================================================================================================
static inline void _TurnOnBus                           ( const oC_GPIO_PinLink_t * const PinLink )
{
#if oC_GPIO_PERIPHERAL_BUS == AHB
    oC_SET_BIT( oC_REG_SysCtrl_RMAP->GPIOHBCTL , 1 << PinLink->Port );
#else
    // APB is turned on by default
#endif
}

//==========================================================================================================================================
/**
 * Checks if the pin is special
 *
 * @param PinLink               Reference to the pin link structure
 *
 * @return true if special
 */
//==========================================================================================================================================
static inline bool _IsSpecialPin                        ( const oC_GPIO_PinLink_t * const PinLink )
{
    oC_REG_GPIO_RegisterMap_t* rmap;

    rmap            = __oC_GPIO_GetRegisterMap( PinLink->Port );

    return (~rmap->CR) & PinLink->Pin;
}

//==========================================================================================================================================
/**
 * Takes off special protection from the pin
 *
 * @param PinLink               Reference to the pin link structure
 */
//==========================================================================================================================================
static inline void _UnlockProtection                    ( const oC_GPIO_PinLink_t * const PinLink )
{
    oC_REG_GPIO_RegisterMap_t* rmap;

    rmap            = __oC_GPIO_GetRegisterMap( PinLink->Port );

    rmap->LOCK  = oC_REG_GPIO_LOCK_KEY;
    rmap->CR    |= PinLink->Pin;
    rmap->DEN   |= PinLink->Pin;
}

//==========================================================================================================================================
/**
 * Count number of the set pin.
 *
 * @param Pin
 * @param PinNumber
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool _GetPinNumber                        ( oC_GPIO_Pin_t Pin , uint8_t * PinNumber )
{
    bool result = false;

    oC_ASSERT( PinNumber != oC_NULL );

    *PinNumber  = 0;

    while( *PinNumber < 8 )
    {
        if( (1<<*PinNumber) & Pin )
        {
            result  = true;
            break;
        }
        else
        {
            *PinNumber = *PinNumber + 1;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * Sets digital function for selected pin.
 *
 * @param DigitalFunction       digital function from table on page 1143
 * @param PinNumber             number of the pin (0-7)
 * @param PCTL                  reference to PCTL register
 */
//==========================================================================================================================================
static inline void _SetPinFunction                      ( oC_MemoryInt_t DigitalFunction , uint8_t PinNumber , oC_MemoryInt_t * PCTL )
{
    oC_WRITE_WITH_MASK( *PCTL , 0xfU << (PinNumber*4) , DigitalFunction << (PinNumber*4) );
}


/*==========================================================================================================================================
//
//    INTERRUPTS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Common interrupt handler
 *
 * @param Port          Port of the interrupt
 */
//==========================================================================================================================================
static inline void _InterruptHandler( oC_GPIO_Port_t Port )
{
    oC_GPIO_Pin_t pins;
    oC_REG_GPIO_RegisterMap_t* rmap;

    rmap    = __oC_GPIO_GetRegisterMap(Port);

    // get pins of interrupt
    pins    = rmap->RIS & rmap->MIS;

    // Call Port callback handler
    bool yield = oC_GPIO_InterruptCallbacksHandler(Port , pins);

    // Clear interrupt status
    rmap->ICR   = pins;

    if ( yield )
    {
        taskYIELD();
    }
}

//==========================================================================================================================================
/**
 * Interrupts for the ports handlers
 */
//==========================================================================================================================================
oC_GPIO_PortsList( _ADD_PORT_TO_INTERRUPT_HANDLER )
