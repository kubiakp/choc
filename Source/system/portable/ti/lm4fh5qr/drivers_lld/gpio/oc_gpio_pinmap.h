/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_gpio_pinmap.h
 *
 *    @brief      File with the map of the pins
 *
 *    @author     Patryk Kubiak - (Created on: 13 lis 2014 11:02:59) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_GPIO_OC_GPIO_PINMAP_H_
#define SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_GPIO_OC_GPIO_PINMAP_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_gpio_types.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * List of ports in the micro-controller.
 *
 * To add port, use ADD_PORT( PORT_NAME , ALL_PINS_MASK )
 */
//==========================================================================================================================================
#define oC_GPIO_PortsList( ADD_PORT )                                                                                                       \
     ADD_PORT( A , 0xFFU )                                                                                                                  \
     ADD_PORT( B , 0xFFU )                                                                                                                  \
     ADD_PORT( C , 0xFFU )                                                                                                                  \
     ADD_PORT( D , 0xFFU )                                                                                                                  \
     ADD_PORT( E , 0x1FU )                                                                                                                  \
     ADD_PORT( F , 0x1FU )

//==========================================================================================================================================
/**
 * List of pins in the micro-controller.
 *
 * To add pin, use ADD_PIN( PORT_NAME , PIN_NUMBER )
 */
//==========================================================================================================================================
#define oC_GPIO_PinList( ADD_PIN )                                                                                                        \
     ADD_PIN( A , 0 )                                                                                                                     \
     ADD_PIN( A , 1 )                                                                                                                     \
     ADD_PIN( A , 2 )                                                                                                                     \
     ADD_PIN( A , 3 )                                                                                                                     \
     ADD_PIN( A , 4 )                                                                                                                     \
     ADD_PIN( A , 5 )                                                                                                                     \
     ADD_PIN( A , 6 )                                                                                                                     \
     ADD_PIN( A , 7 )                                                                                                                     \
     ADD_PIN( B , 0 )                                                                                                                     \
     ADD_PIN( B , 1 )                                                                                                                     \
     ADD_PIN( B , 2 )                                                                                                                     \
     ADD_PIN( B , 3 )                                                                                                                     \
     ADD_PIN( B , 4 )                                                                                                                     \
     ADD_PIN( B , 5 )                                                                                                                     \
     ADD_PIN( B , 6 )                                                                                                                     \
     ADD_PIN( B , 7 )                                                                                                                     \
     ADD_PIN( C , 0 )                                                                                                                     \
     ADD_PIN( C , 1 )                                                                                                                     \
     ADD_PIN( C , 2 )                                                                                                                     \
     ADD_PIN( C , 3 )                                                                                                                     \
     ADD_PIN( C , 4 )                                                                                                                     \
     ADD_PIN( C , 5 )                                                                                                                     \
     ADD_PIN( C , 6 )                                                                                                                     \
     ADD_PIN( C , 7 )                                                                                                                     \
     ADD_PIN( D , 0 )                                                                                                                     \
     ADD_PIN( D , 1 )                                                                                                                     \
     ADD_PIN( D , 2 )                                                                                                                     \
     ADD_PIN( D , 3 )                                                                                                                     \
     ADD_PIN( D , 4 )                                                                                                                     \
     ADD_PIN( D , 5 )                                                                                                                     \
     ADD_PIN( D , 6 )                                                                                                                     \
     ADD_PIN( D , 7 )                                                                                                                     \
     ADD_PIN( E , 0 )                                                                                                                     \
     ADD_PIN( E , 1 )                                                                                                                     \
     ADD_PIN( E , 2 )                                                                                                                     \
     ADD_PIN( E , 3 )                                                                                                                     \
     ADD_PIN( E , 4 )                                                                                                                     \
     ADD_PIN( E , 5 )                                                                                                                     \
     ADD_PIN( F , 0 )                                                                                                                     \
     ADD_PIN( F , 1 )                                                                                                                     \
     ADD_PIN( F , 2 )                                                                                                                     \
     ADD_PIN( F , 3 )                                                                                                                     \
     ADD_PIN( F , 4 )

//==========================================================================================================================================
/**
 * Name of the bus used by the driver
 *
 * (AHB / APB )
 */
//==========================================================================================================================================
#define oC_GPIO_PERIPHERAL_BUS          AHB

/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_GPIO_OC_GPIO_PINMAP_H_ */
