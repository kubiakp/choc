/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_gpio_defs.h
 *
 *    @brief      The file with definitions for the gpio.
 *
 *    @author     Patryk Kubiak - (Created on: 13 lis 2014 14:20:52) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_GPIO_OC_GPIO_DEFS_H_
#define SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_GPIO_OC_GPIO_DEFS_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// Core
#include <oc_gpio_types.h>
#include <oc_gpio_pinmap.h>
#include <oc_reg_defs.h>

// Drivers

// Libraries

// Others

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/

#define oC_GPIO_PinLink_NOT_USED                NULL

/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define oC_GPIO_ADD_PIN_TO_PIN_LINK_PROTOTYPES( PORT_NAME , PIN_NUMBER )                            extern const oC_GPIO_PinLink_t * const oC_CAT_3( oC_GPIO_PinLink_P , PORT_NAME , PIN_NUMBER );
#define oC_GPIO_ADD_PIN_TO_PIN_PROTOTYPES( PORT_NAME , PIN_NUMBER )                                 extern const oC_GPIO_PinLink_t oC_CAT_3( P , PORT_NAME , PIN_NUMBER );

/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/

oC_GPIO_PinList( oC_GPIO_ADD_PIN_TO_PIN_LINK_PROTOTYPES );
oC_GPIO_PinList( oC_GPIO_ADD_PIN_TO_PIN_PROTOTYPES );

extern const oC_REG_GPIO_RegisterMap_t* oC_GPIO_RMaps[];
extern const IRQn_Type oC_GPIO_IRQ_Numbers[];

/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_GPIO_OC_GPIO_DEFS_H_ */
