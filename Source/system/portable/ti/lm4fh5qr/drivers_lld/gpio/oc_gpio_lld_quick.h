/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld_quick.h Ver 1.0.0
 *
 *    @file       oc_gpio.h
 *
 *    @brief      File with interface of the quick access functions for the GPIO module.
 *
 *    @author     Patryk Kubiak - (Created on: 2014-11-13 - 10:58:46)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#ifndef _OC_GPIO_LLD_QUICK_H_
#define _OC_GPIO_LLD_QUICK_H_
/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

// General
#include <oc_assert.h>
#include <oc_compiler.h>
#include <oc_gpio_types.h>          // access to types of the module
#include <oc_reg_defs.h>            // access to definitions of the registers
#include <oc_gpio_defs.h>           // definitions of the GPIO RMaps

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function returns register map for port
 *
 * @param Port  Number of port to return register map
 *
 * @return Register map of the port
 */
//==========================================================================================================================================
static inline oC_REG_GPIO_RegisterMap_t* __oC_GPIO_GetRegisterMap( oC_GPIO_Port_t Port )
{
    return (oC_REG_GPIO_RegisterMap_t*)oC_GPIO_RMaps[Port];
}

//==========================================================================================================================================
/**
 * The function toggle data on the GPIO pin(s)
 *
 * @param PinLink           Reference to the PinLink structure
 * @param Data              Data to toggle on the Pin(s)
 *
 */
//==========================================================================================================================================
static inline void oC_GPIO_ToggleData( const oC_GPIO_PinLink_t * const PinLink , oC_MemoryInt_t Data )
{
    oC_REG_GPIO_RegisterMap_t *rmap;

    oC_ASSERT( PinLink != NULL );
    oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

    rmap        = __oC_GPIO_GetRegisterMap( PinLink->Port );

    rmap->DATA  ^= ((oC_MemoryInt_t) Data) & ((oC_MemoryInt_t) PinLink->Pin );
}

//==========================================================================================================================================
/**
 *  The function for write data for the GPIO port
 *
 * @param PinLink           Reference to the PinLink structure
 * @param Data              Data to set on the Pin(s)
 *
 */
//==========================================================================================================================================
static inline void oC_GPIO_SetData( const oC_GPIO_PinLink_t * const PinLink , oC_MemoryInt_t Data )
{
    oC_REG_GPIO_RegisterMap_t *rmap;

    oC_ASSERT( PinLink != NULL );
    oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

    rmap        = __oC_GPIO_GetRegisterMap( PinLink->Port );

    oC_WRITE_WITH_MASK(rmap->DATA , (oC_MemoryInt_t)PinLink->Pin , (oC_MemoryInt_t)Data );
}

//==========================================================================================================================================
/**
 *  The function for data from GPIO port
 *
 * @param PinLink           Reference to the PinLink structure
 *
 *  @return Data of the GPIO Port
 */
//==========================================================================================================================================
static inline oC_MemoryInt_t oC_GPIO_GetData( const oC_GPIO_PinLink_t * const PinLink )
{
    oC_REG_GPIO_RegisterMap_t *rmap;

    oC_ASSERT( PinLink != NULL );
    oC_ASSERT( PinLink->Port < oC_GPIO_Port_NumberOfPorts );

    rmap        = __oC_GPIO_GetRegisterMap( PinLink->Port );

    return (oC_MemoryInt_t)rmap->DATA & (oC_MemoryInt_t)PinLink->Pin;
}


#endif
