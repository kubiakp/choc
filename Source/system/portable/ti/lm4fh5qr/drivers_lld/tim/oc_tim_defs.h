/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 * 	  File based on lld_defs.h Ver 1.0.1
 *
 *    @file       
 *
 *    @brief      Definitions for the TIM for micro controller
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-25 - 19:00:21)
 *
 *    @note       Copyright (C) Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_TIM_OC_TIM_DEFS_H_
#define SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_TIM_OC_TIM_DEFS_H_

/*==========================================================================================================================================
//
//     MICRO CONTROLLER DEFINITIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * List of channels in the micro controller. To add channel use macro:
 *      ADD_CHANNEL( CHANNEL_NAME , FIRST_MODE , SECOND_MODE )
 *
 * Available modes:
 *          MODE_8BIT / MODE_16BIT / MODE_32BIT / MODE_64BIT
 */
//==========================================================================================================================================
#define oC_TIM_ChannelsList( ADD_CHANNEL )                                                                                                  \
    ADD_CHANNEL( Timer0     , SIZE16or32 )                                         \
    ADD_CHANNEL( Timer1     , SIZE16or32 )                                         \
    ADD_CHANNEL( Timer2     , SIZE16or32 )                                         \
    ADD_CHANNEL( Timer3     , SIZE16or32 )                                         \
    ADD_CHANNEL( Timer4     , SIZE16or32 )                                         \
    ADD_CHANNEL( Timer5     , SIZE16or32 )                                         \
    ADD_CHANNEL( WideTimer0 , SIZE32or64 )                                         \
    ADD_CHANNEL( WideTimer1 , SIZE32or64 )                                         \
    ADD_CHANNEL( WideTimer2 , SIZE32or64 )                                         \
    ADD_CHANNEL( WideTimer3 , SIZE32or64 )                                         \
    ADD_CHANNEL( WideTimer4 , SIZE32or64 )                                         \
    ADD_CHANNEL( WideTimer5 , SIZE32or64 )                                         \

#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_TIM_OC_TIM_DEFS_H_ */
