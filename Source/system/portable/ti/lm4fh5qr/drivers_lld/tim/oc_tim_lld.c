/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld.c Ver 1.0.1
 *
 *    @file       oc_tim.h
 *
 *    @brief      File with source of functions of the LLD layer
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-25 - 19:00:21)
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
 
/*==========================================================================================================================================
//
//    INCLUDES
//
//========================================================================================================================================*/

// General
#include <oc_tim.h>
#include <oc_tim_lld.h>
#include <oc_reg_defs.h>
#include <oc_sys.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/




/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

/** ****************************************************************************************************************************************
 * The section with local types definitions
 */
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

typedef oC_REG_GPTM_RegisterMap_t RegisterMap_t;

typedef enum
{
    TimerWidth_Full = 0 ,
    TimerWidth_Half = 0x4,
} TimerWidth_t;

#define SIZE16or32(CHANNEL)             oC_ONE_WORD_FROM_2(Timer16or32Index_ , CHANNEL) ,
#define SIZE32or64(CHANNEL)
#define ADD(CHANNEL , SIZE , ...)       SIZE(CHANNEL)
typedef enum
{
    oC_TIM_ChannelsList(ADD)
    Timer16or32Index_NumberOfElements
} Timer16or32Index_t;
#undef SIZE16or32
#undef SIZE32or64
#undef ADD

#define SIZE16or32(CHANNEL)
#define SIZE32or64(CHANNEL)             oC_ONE_WORD_FROM_2(Timer32or64Index_ , CHANNEL) ,
#define ADD(CHANNEL , SIZE , ...)       SIZE(CHANNEL)
typedef enum
{
    oC_TIM_ChannelsList(ADD)
    Timer32or64Index_NumberOfElements
} Timer32or64Index_t;
#undef SIZE16or32
#undef SIZE32or64
#undef ADD


/* END OF SECTION */
#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________


/** ****************************************************************************************************************************************
 * The section with local variables definitions
 */
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

#define ADD(CHANNEL , ...)                     oC_ONE_WORD_FROM_3(oC_REG_, CHANNEL , _RMAP) ,
static const RegisterMap_t * RMaps[ oC_TIM_Channel_NumberOfChannels ] = { oC_TIM_ChannelsList( ADD ) };
#undef ADD

#define ADD(CHANNEL , ...)                     oC_ONE_WORD_FROM_3( oC_REG_NVIC_InterruptNumber_, CHANNEL , A ) ,
static const IRQn_Type IRQNumbers_A[ oC_TIM_Channel_NumberOfChannels ] = { oC_TIM_ChannelsList( ADD ) };
#undef ADD
#define ADD(CHANNEL , ...)                     oC_ONE_WORD_FROM_3( oC_REG_NVIC_InterruptNumber_, CHANNEL , B ) ,
static const IRQn_Type IRQNumbers_B[ oC_TIM_Channel_NumberOfChannels ] = { oC_TIM_ChannelsList( ADD ) };
#undef ADD
#define SIZE16or32(CHANNEL)         oC_ONE_WORD_FROM_2(Timer16or32Index_ , CHANNEL) ,
#define SIZE32or64(CHANNEL)         oC_ONE_WORD_FROM_2(Timer32or64Index_ , CHANNEL) ,
#define ADD(CHANNEL , SIZE , ...)   SIZE(CHANNEL)
static const uint8_t TimersIndexes[ oC_TIM_Channel_NumberOfChannels ]  = { oC_TIM_ChannelsList( ADD ) };
#undef SIZE16or32
#undef SIZE32or64
#undef ADD

#define SIZE16or32(CHANNEL)         true ,
#define SIZE32or64(CHANNEL)         false ,
#define ADD(CHANNEL , SIZE , ...)   SIZE(CHANNEL)
static const bool Timer16or32BitsFlags[ oC_TIM_Channel_NumberOfChannels ] = { oC_TIM_ChannelsList( ADD ) };
#undef SIZE16or32
#undef SIZE32or64
#undef ADD

/* END OF SECTION */
#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________


/** ****************************************************************************************************************************************
 * The section contains local functions prototypes
 */
#define _________________________________________LOCAL_FUNCTIONS_PROTOTYPES_SECTION_________________________________________________________

static inline RegisterMap_t *       GetRMap                 ( oC_TIM_Channel_t Channel );
static inline IRQn_Type             GetIRQA                 ( oC_TIM_Channel_t Channel );
static inline IRQn_Type             GetIRQB                 ( oC_TIM_Channel_t Channel );
static inline void                  DisableTimer            ( RegisterMap_t * RMap );
static inline void                  EnableTimer             ( RegisterMap_t * RMap );
static inline void                  TurnOnPower             ( oC_TIM_Channel_t Channel );
static inline void                  TurnOffPower            ( oC_TIM_Channel_t Channel );
static inline bool                  IsChannelTurnedOn       ( oC_TIM_Channel_t Channel );
static inline void                  TurnOnInterrupts        ( oC_TIM_Channel_t Channel );
static inline void                  TurnOffInterrupts       ( oC_TIM_Channel_t Channel );
static inline void                  SetTimerWidth           ( RegisterMap_t * RMap , TimerWidth_t TimerWidth );
static inline TimerWidth_t          GetTimerWidth           ( RegisterMap_t * RMap );
static inline oC_ErrorCode_t        SetMode                 ( RegisterMap_t * RMap , oC_TIM_Mode_t Mode );
static inline oC_ErrorCode_t        SetCountsDir            ( RegisterMap_t * RMap , oC_TIM_CountsDir_t CountsDir );
static inline oC_ErrorCode_t        SetTickSource           ( RegisterMap_t * RMap , oC_TIM_TickSource_t TickSource , oC_TIM_TickPeriod_t TickPeriod , oC_TIM_Channel_t Channel );
static inline oC_ErrorCode_t        SetMatchValue           ( RegisterMap_t * RMap , uint64_t MatchValue , oC_TIM_Channel_t Channel );
static inline oC_ErrorCode_t        SetMaxValue             ( RegisterMap_t * RMap , uint64_t MaxValue , oC_TIM_Channel_t Channel );
static inline oC_ErrorCode_t        SetTickPeriod           ( RegisterMap_t * RMap , oC_TIM_TickPeriod_t TickPeriod , oC_TIM_Channel_t Channel );
static inline oC_ErrorCode_t        SetSize                 ( RegisterMap_t * RMap , oC_TIM_Size_t Size , oC_TIM_Channel_t Channel );
static inline oC_ErrorCode_t        SetValue                ( RegisterMap_t * RMap , oC_TIM_Channel_t Channel , uint64_t Value );
static inline oC_TIM_Size_t         GetSize                 ( RegisterMap_t * RMap , oC_TIM_Channel_t Channel );
static inline uint64_t              GetMaximumLimit         ( RegisterMap_t * RMap , oC_TIM_Channel_t Channel );
static inline bool                  IsSizePossible          ( oC_TIM_Channel_t Channel , oC_TIM_Size_t Size );
static inline bool                  IsTickSourcePossible    ( oC_TIM_Channel_t Channel , oC_TIM_Size_t Size , oC_TIM_TickSource_t TickSource , oC_TIM_TickPeriod_t TickPeriod , oC_TIM_CountsDir_t CountsDir );
static inline uint64_t              GetValue                ( RegisterMap_t * RMap , oC_TIM_Channel_t Channel );
static inline uint64_t              GetMaxValue             ( RegisterMap_t * RMap , oC_TIM_Channel_t Channel );
static inline uint64_t              GetMatchValue           ( RegisterMap_t * RMap , oC_TIM_Channel_t Channel );
static inline oC_TIM_CountsDir_t    GetCountsDir            ( RegisterMap_t * RMap );
static inline oC_ErrorCode_t        CalculatePrescaler      ( oC_TIM_TickPeriod_t TickPeriod , bool TimerSize16or32Bits , TimerWidth_t TimerWidth , oC_TIM_CountsDir_t CountsDir, uint32_t * outPrescaler );

/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTIONS_PROTOTYPES_SECTION_________________________________________________________


/** ****************************************************************************************************************************************
 * The section contains interface functions definitions
 */
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

void oC_TIM_LLD_TurnOnDriver( void )
{
//    TurnOnPower(0);
//    RegisterMap_t * rmap = GetRMap(0);
//    DisableTimer(rmap);
//    rmap->SYNC = 0xFFFFFFUL;
}

void oC_TIM_LLD_TurnOffDriver( void )
{

}

oC_ErrorCode_t oC_TIM_LLD_Configure( const oC_TIM_Config_t * Config , oC_TIM_Channel_t Channel )
{
    oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;
    RegisterMap_t * rmap        = GetRMap(Channel);

    TurnOnPower(Channel);
    DisableTimer(rmap);

    if (
            oC_AssignErrorCode( &errorCode , SetMode(           rmap , Config->Mode         ) )
         && oC_AssignErrorCode( &errorCode , SetSize(           rmap , Config->Size         , Channel ) )
         && oC_AssignErrorCode( &errorCode , SetCountsDir(      rmap , Config->CountsDir    ) )
         && oC_AssignErrorCode( &errorCode , SetTickSource(     rmap , Config->TickSource   , Config->TickPeriod , Channel) )
         && oC_AssignErrorCode( &errorCode , SetMatchValue(     rmap , Config->MatchValue   , Channel ) )
         && oC_AssignErrorCode( &errorCode , SetMaxValue(       rmap , Config->MaxValue     , Channel ) )
            )
    {
        TurnOnInterrupts(Channel);
//        SetValue(rmap , Channel , 0);
        EnableTimer(rmap);
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

oC_ErrorCode_t oC_TIM_LLD_Unconfigure( const oC_TIM_Config_t * Config , oC_TIM_Channel_t Channel )
{
    return oC_ErrorCode_NotImplemented;
}

bool oC_TIM_LLD_IsConfigurationPossible( oC_TIM_Channel_t Channel , const oC_TIM_Config_t * Config )
{
    bool configPossible     = false;

    if ( Channel < oC_TIM_Channel_NumberOfChannels )
    {
        if (
                IsSizePossible(Channel , Config->Size)
             && IsTickSourcePossible(Channel , Config->Size , Config->TickSource , Config->TickPeriod , Config->CountsDir)
            )
        {
            configPossible  = true;
        }
    }

    return configPossible;
}

void oC_TIM_LLD_RestoreDefaultStateOnChannel( oC_TIM_Channel_t Channel )
{
    if ( Channel < oC_TIM_Channel_NumberOfChannels )
    {
        if ( IsChannelTurnedOn(Channel) )
        {
            DisableTimer( GetRMap(Channel) );
            TurnOffPower(Channel);
        }
    }
}

oC_ErrorCode_t oC_TIM_LLD_StartTimer( oC_TIM_Channel_t Channel )
{
    return oC_ErrorCode_NotImplemented;
}

oC_ErrorCode_t oC_TIM_LLD_StopTimer( oC_TIM_Channel_t Channel )
{
    return oC_ErrorCode_NotImplemented;
}

bool oC_TIM_LLD_IsTimerRunning( oC_TIM_Channel_t Channel );
uint64_t oC_TIM_LLD_GetCurrentCount( oC_TIM_Channel_t Channel );
oC_ErrorCode_t    oC_TIM_LLD_ReadCurrentCount             ( oC_TIM_Channel_t Channel , uint64_t * outValue );
oC_ErrorCode_t    oC_TIM_LLD_SetCurrentCount              ( oC_TIM_Channel_t Channel , uint64_t Value );
oC_ErrorCode_t    oC_TIM_LLD_SetMaxValue                  ( oC_TIM_Channel_t Channel , uint64_t Value );
oC_ErrorCode_t    oC_TIM_LLD_ReadMaxValue                 ( oC_TIM_Channel_t Channel , uint64_t * Value );
uint64_t          oC_TIM_LLD_GetMaxValue                  ( oC_TIM_Channel_t Channel );
oC_ErrorCode_t    oC_TIM_LLD_SetMatchValue                ( oC_TIM_Channel_t Channel , uint64_t Value );
oC_ErrorCode_t    oC_TIM_LLD_ReadMatchValue               ( oC_TIM_Channel_t Channel , uint64_t * Value );
uint64_t          oC_TIM_LLD_GetMatchValue                ( oC_TIM_Channel_t Channel );

oC_ErrorCode_t    oC_TIM_LLD_SetValue                     ( oC_TIM_Channel_t Channel , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    RegisterMap_t * rmap = GetRMap( Channel );

    if ( oC_AssignErrorCode( &errorCode , SetValue( rmap , Channel , Value ) ) )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
oC_ErrorCode_t    oC_TIM_LLD_ReadValue                    ( oC_TIM_Channel_t Channel , uint64_t * Value );
uint64_t          oC_TIM_LLD_GetValue                     ( oC_TIM_Channel_t Channel )
{
    return GetValue( GetRMap(Channel) , Channel );
}
oC_ErrorCode_t    oC_TIM_LLD_TurnOnMatchInterrupt         ( oC_TIM_Channel_t Channel )
{
    return oC_ErrorCode_None;
}
oC_ErrorCode_t    oC_TIM_LLD_TurnOffMatchInterrupt        ( oC_TIM_Channel_t Channel )
{
    return oC_ErrorCode_None;
}
oC_ErrorCode_t    oC_TIM_LLD_TurnOnTimeoutInterrupt       ( oC_TIM_Channel_t Channel )
{
    return oC_ErrorCode_None;
}
oC_ErrorCode_t    oC_TIM_LLD_TurnOffTimeoutInterrupt      ( oC_TIM_Channel_t Channel )
{
    return oC_ErrorCode_None;
}


/* END OF SECTION */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with declarations of local functions
 */
#define _________________________________________LOCAL_FUNCTIONS_SOURCES_SECTION____________________________________________________________

static inline RegisterMap_t * GetRMap( oC_TIM_Channel_t Channel )
{
    return RMaps[Channel];
}

static inline IRQn_Type GetIRQA( oC_TIM_Channel_t Channel )
{
    return IRQNumbers_A[Channel];
}

static inline IRQn_Type GetIRQB( oC_TIM_Channel_t Channel )
{
    return IRQNumbers_B[Channel];
}

static inline void DisableTimer( RegisterMap_t * RMap )
{
    oC_CLR_BIT(RMap->CTL , oC_BIT_0 );
}

static inline void EnableTimer( RegisterMap_t * RMap )
{
    oC_SET_BIT(RMap->CTL , oC_BIT_0 );

    oC_SET_BIT(RMap->CTL , oC_BIT_9); // to freeze timer B counting when the program is halted by debugger
    oC_SET_BIT(RMap->CTL , oC_BIT_1); // to freeze timer A counting when the program is halted by debugger
}


static inline void TurnOnPower( oC_TIM_Channel_t Channel )
{
    if ( Timer16or32BitsFlags[Channel] )
    {
        oC_SET_BIT(oC_REG_SysCtrl_RMAP->RCGCTIMER , 1 << TimersIndexes[Channel] );
    }
    else
    {
        oC_SET_BIT(oC_REG_SysCtrl_RMAP->RCGCWTIMER , 1 << TimersIndexes[Channel] );
    }
    oC_SYS_Delay(10);
}

static inline void TurnOffPower( oC_TIM_Channel_t Channel )
{
    if ( Timer16or32BitsFlags[Channel] )
    {
        oC_CLR_BIT(oC_REG_SysCtrl_RMAP->RCGCTIMER , 1 << TimersIndexes[Channel] );
    }
    else
    {
        oC_CLR_BIT(oC_REG_SysCtrl_RMAP->RCGCWTIMER , 1 << TimersIndexes[Channel] );
    }
    oC_SYS_Delay(10);
}

static inline bool IsChannelTurnedOn( oC_TIM_Channel_t Channel )
{
    bool turnedOn = false;

    if ( Timer16or32BitsFlags[Channel] )
    {
        turnedOn  = oC_IS_BIT_SET( oC_REG_SysCtrl_RMAP->RCGCTIMER ,  TimersIndexes[Channel] );
    }
    else
    {
        turnedOn  = oC_IS_BIT_SET( oC_REG_SysCtrl_RMAP->RCGCWTIMER , TimersIndexes[Channel] );
    }

    return turnedOn;
}

static inline void TurnOnInterrupts( oC_TIM_Channel_t Channel )
{
    RegisterMap_t * rmap = GetRMap(Channel);

    NVIC_EnableIRQ( GetIRQA(Channel) );
    NVIC_EnableIRQ( GetIRQB(Channel) );
//
//    oC_SET_BIT( rmap->TAMR , oC_BIT_8 ); // interval load write
//    oC_SET_BIT( rmap->TBMR , oC_BIT_8 );

    oC_SET_BIT( rmap->TAMR , oC_BIT_5  ); // Match A interrupt mask
    oC_SET_BIT( rmap->TBMR , oC_BIT_5  ); // Match A interrupt mask

    oC_SET_BIT( rmap->IMR , oC_BIT_4  ); // Match A interrupt mask
    oC_SET_BIT( rmap->IMR , oC_BIT_11 ); // Match B interrupt mask
    oC_SET_BIT( rmap->IMR , oC_BIT_0  ); // Timeout A interrupt mask
    oC_SET_BIT( rmap->IMR , oC_BIT_8  ); // Timeout B interrupt mask


    // Sync in Module 0 timer.... for timeouts... shit it is stupid
    rmap->SYNC  |= (0x3) << (Channel * 2);
//    oC_REG_Timer0_RMAP->SYNC |= (0x3) << (Channel * 2);

}

static inline void TurnOffInterrupts( oC_TIM_Channel_t Channel )
{
    RegisterMap_t * rmap = GetRMap(Channel);

    NVIC_DisableIRQ( GetIRQA(Channel) );
    NVIC_DisableIRQ( GetIRQB(Channel) );

    oC_CLR_BIT( rmap->IMR , oC_BIT_4  ); // Match A interrupt mask
    oC_CLR_BIT( rmap->IMR , oC_BIT_11 ); // Match B interrupt mask
    oC_CLR_BIT( rmap->IMR , oC_BIT_0  ); // Timeout A interrupt mask
    oC_CLR_BIT( rmap->IMR , oC_BIT_8  ); // Timeout B interrupt mask
}

static inline void SetTimerWidth( RegisterMap_t * RMap , TimerWidth_t TimerWidth )
{
    RMap->CFG   = TimerWidth;

    oC_SET_BIT( RMap->CTL , oC_BIT_0 ); // enable timer A

    if ( TimerWidth == TimerWidth_Full )
    {
        oC_SET_BIT(RMap->CTL , oC_BIT_8); // enable timer B counting
    }
    else
    {
        oC_CLR_BIT(RMap->CTL , oC_BIT_8); // disable timer B counting
    }
}

static inline TimerWidth_t GetTimerWidth( RegisterMap_t * RMap )
{
    return RMap->CFG & oC_BITS(0,2);
}

static inline oC_ErrorCode_t SetMode( RegisterMap_t * RMap , oC_TIM_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Mode == oC_TIM_Mode_OneShot )
    {
        oC_SET_BIT( RMap->TAMR , oC_BIT_0 );
        oC_CLR_BIT( RMap->TAMR , oC_BIT_1 );
        oC_SET_BIT( RMap->TBMR , oC_BIT_0 );
        oC_CLR_BIT( RMap->TBMR , oC_BIT_1 );
        errorCode   = oC_ErrorCode_None;
    }
    else if ( Mode == oC_TIM_Mode_Periodic )
    {
        oC_CLR_BIT( RMap->TAMR , oC_BIT_0 );
        oC_SET_BIT( RMap->TAMR , oC_BIT_1 );
        oC_CLR_BIT( RMap->TBMR , oC_BIT_0 );
        oC_SET_BIT( RMap->TBMR , oC_BIT_1 );
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_TIMModeNotCorrect;
    }

    return errorCode;
}


static inline oC_ErrorCode_t SetCountsDir( RegisterMap_t * RMap , oC_TIM_CountsDir_t CountsDir )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;

    switch( CountsDir )
    {
        case oC_TIM_CountsDir_Up:
            oC_SET_BIT( RMap->TAMR , oC_BIT_4 );
            oC_SET_BIT( RMap->TBMR , oC_BIT_4 );
            errorCode   = oC_ErrorCode_None;
            break;
        case oC_TIM_CountsDir_DontMatter:
        case oC_TIM_CountsDir_Down:
            oC_CLR_BIT( RMap->TAMR , oC_BIT_4 );
            oC_CLR_BIT( RMap->TBMR , oC_BIT_4 );
            errorCode   = oC_ErrorCode_None;
            break;
        default:
            errorCode   = oC_ErrorCode_TIMCountsDirNotCorrect;
    }

    return errorCode;
}


static inline oC_ErrorCode_t SetTickSource( RegisterMap_t * RMap , oC_TIM_TickSource_t TickSource , oC_TIM_TickPeriod_t TickPeriod , oC_TIM_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( TickSource == oC_TIM_TickSource_MainClock )
    {
        errorCode   = SetTickPeriod( RMap , TickPeriod , Channel );
    }
    else
    {
        errorCode   = oC_ErrorCode_TIMTickSourceNotSupported;
    }

    return errorCode;
}

static inline oC_ErrorCode_t SetMatchValue( RegisterMap_t * RMap , uint64_t MatchValue , oC_TIM_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( MatchValue < GetMaximumLimit(RMap , Channel ) )
    {
        RMap->TAMATCHR  = (uint32_t)( MatchValue & oC_BITS(0,31));
        RMap->TBMATCHR  = (uint32_t)((MatchValue & oC_BITS(32,63))<<32);

        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_TIMMaxValueTooBig;
    }


    return errorCode;
}

static inline oC_ErrorCode_t SetMaxValue( RegisterMap_t * RMap , uint64_t MaxValue , oC_TIM_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( MaxValue < GetMaximumLimit(RMap , Channel ) )
    {
        RMap->TAILR = (uint32_t)(MaxValue & oC_BITS(0,31));
        RMap->TBILR = (uint32_t)((MaxValue & oC_BITS(32,63))<<32);

        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_TIMMaxValueTooBig;
    }

    return errorCode;
}

static inline oC_ErrorCode_t SetTickPeriod( RegisterMap_t * RMap , oC_TIM_TickPeriod_t TickPeriod , oC_TIM_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( TickPeriod == oC_TIM_TickPeriod_DontDivide )
    {
        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        uint32_t prescaler = 0;

        if ( oC_AssignErrorCode( &errorCode ,
                                 CalculatePrescaler( TickPeriod ,
                                                     Timer16or32BitsFlags[Channel] ,
                                                     GetTimerWidth(RMap) ,
                                                     GetCountsDir(RMap) ,
                                                     &prescaler
                                                     )
                               )
                )
        {
            errorCode   = oC_ErrorCode_None;
            RMap->TAPR  = prescaler;
        }
    }

    return errorCode;
}

static inline oC_ErrorCode_t SetSize( RegisterMap_t * RMap , oC_TIM_Size_t Size , oC_TIM_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    switch(Size)
    {
        case oC_TIM_Size_8Bits:
        case oC_TIM_Size_16Bits:
            SetTimerWidth( RMap , TimerWidth_Half );
            errorCode   = oC_ErrorCode_None;
            break;
        case oC_TIM_Size_32Bits:
            if ( Timer16or32BitsFlags[Channel] )
            {
                SetTimerWidth( RMap , TimerWidth_Full );
            }
            else
            {
                SetTimerWidth( RMap , TimerWidth_Half );
            }
            errorCode   = oC_ErrorCode_None;
            break;
        case oC_TIM_Size_64Bits:
            if ( Timer16or32BitsFlags[Channel] )
            {
                SetTimerWidth( RMap , TimerWidth_Full );
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                errorCode   = oC_ErrorCode_TIMSizeNotSupportedOnSelectedChannel;
            }
            break;
        default:
            errorCode   = oC_ErrorCode_TIMSizeNotSupported;
    }

    return errorCode;
}

static inline oC_ErrorCode_t SetValue( RegisterMap_t * RMap , oC_TIM_Channel_t Channel , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( Value < GetMaximumLimit(RMap , Channel ) )
    {

        RMap->TAV = (uint32_t)(Value & oC_BITS(0,31));
        RMap->TBV = (uint32_t)((Value & oC_BITS(32,63))<<32);

        errorCode   = oC_ErrorCode_None;
    }
    else
    {
        errorCode   = oC_ErrorCode_TIMValueTooBig;
    }

    return errorCode;
}

static inline oC_TIM_Size_t GetSize( RegisterMap_t * RMap , oC_TIM_Channel_t Channel )
{
    oC_TIM_Size_t size ;
    TimerWidth_t timerWidth = GetTimerWidth(RMap);

    if ( Timer16or32BitsFlags[Channel] )
    {
        if ( timerWidth == TimerWidth_Half )
        {
            size = oC_TIM_Size_16Bits;
        }
        else
        {
            size = oC_TIM_Size_32Bits;
        }
    }
    else
    {
        if ( timerWidth == TimerWidth_Half )
        {
            size = oC_TIM_Size_32Bits;
        }
        else
        {
            size = oC_TIM_Size_64Bits;
        }
    }

    return size;
}

static inline uint64_t GetMaximumLimit( RegisterMap_t * RMap , oC_TIM_Channel_t Channel )
{
    uint64_t        limit   = 0;
    oC_TIM_Size_t   size    = GetSize(RMap , Channel );

    switch(size)
    {
        case oC_TIM_Size_8Bits:
            limit   = oC_UINT8_MAX;
            break;
        case oC_TIM_Size_16Bits:
            limit   = oC_UINT16_MAX;
            break;
        case oC_TIM_Size_32Bits:
            limit   = oC_UINT32_MAX;
            break;
        case oC_TIM_Size_64Bits:
            limit   = oC_UINT64_MAX;
            break;
    }

    return limit;
}

static inline bool IsSizePossible( oC_TIM_Channel_t Channel , oC_TIM_Size_t Size )
{
    bool possible = false;

    switch(Size)
    {
        case oC_TIM_Size_8Bits:
        case oC_TIM_Size_16Bits:
        case oC_TIM_Size_32Bits:
            possible    = true;
            break;
        case oC_TIM_Size_64Bits:
            if ( Timer16or32BitsFlags[Channel] == false)
            {
                possible    = true;
            }
            break;
    }

    return possible;
}

static inline bool IsTickSourcePossible( oC_TIM_Channel_t Channel , oC_TIM_Size_t Size , oC_TIM_TickSource_t TickSource , oC_TIM_TickPeriod_t TickPeriod , oC_TIM_CountsDir_t CountsDir )
{
    bool possible = false;

    if ( TickSource == oC_TIM_TickSource_MainClock )
    {
        if ( Timer16or32BitsFlags[Channel] )
        {
            if (
                        ( Size == oC_TIM_Size_32Bits && TickPeriod == oC_TIM_TickPeriod_DontDivide )
                    ||  (   !oC_ErrorOccur(
                                         CalculatePrescaler( TickPeriod ,
                                                             true ,
                                                             (Size == oC_TIM_Size_32Bits) ? TimerWidth_Full : TimerWidth_Half ,
                                                             CountsDir ,
                                                             NULL
                                         )
                    )
                    )
            )
            {
                possible    = true;
            }
        }
        else
        {
            if (
                    (   Size == oC_TIM_Size_64Bits && TickPeriod == oC_TIM_TickPeriod_DontDivide )
                    || (   !oC_ErrorOccur(
                                         CalculatePrescaler( TickPeriod ,
                                                             false ,
                                                             (Size == oC_TIM_Size_64Bits) ? TimerWidth_Full : TimerWidth_Half ,
                                                             CountsDir ,
                                                             NULL
                                                            )
                                        )
                    )
            )
            {
                possible    = true;
            }
        }
    }
    else
    {
        possible    = false;
    }

    return possible;
}



static inline uint64_t GetValue( RegisterMap_t * RMap , oC_TIM_Channel_t Channel )
{
    uint64_t value = 0;

    value = RMap->TAR;

    if ( GetTimerWidth(RMap) == TimerWidth_Full )
    {
        value |= RMap->TBR << 32;
    }

    return value;
}

static inline uint64_t GetMaxValue( RegisterMap_t * RMap , oC_TIM_Channel_t Channel )
{
    uint64_t value = 0;

    value = RMap->TAILR;

    if ( GetTimerWidth(RMap) == TimerWidth_Full )
    {
        value |= ((uint64_t)RMap->TBILR) << 32;
    }

    return value;
}

static inline uint64_t GetMatchValue( RegisterMap_t * RMap , oC_TIM_Channel_t Channel )
{
    uint64_t value = 0;

    value = RMap->TAMATCHR;

    if ( GetTimerWidth(RMap) == TimerWidth_Full )
    {
        value |= ((uint64_t)RMap->TBMATCHR) << 32;
    }

    return value;
}

static inline oC_TIM_CountsDir_t GetCountsDir( RegisterMap_t * RMap )
{
    oC_TIM_CountsDir_t countsDir;

    if ( oC_IS_BIT_SET( RMap->TAMR , 4 ) )
    {
        countsDir   = oC_TIM_CountsDir_Up;
    }
    else
    {
        countsDir   = oC_TIM_CountsDir_Down;
    }

    return countsDir;
}

static inline oC_ErrorCode_t CalculatePrescaler( oC_TIM_TickPeriod_t TickPeriod , bool TimerSize16or32Bits , TimerWidth_t TimerWidth , oC_TIM_CountsDir_t CountsDir , uint32_t * outPrescaler )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Frequency_t sysClk       = oC_SYS_GetSysClock();

    if (
                oC_AssignErrorCodeIfFalse( &errorCode , TimerWidth == TimerWidth_Half          , oC_ErrorCode_TIMTickPeriodNotSupported )
            &&  oC_AssignErrorCodeIfFalse( &errorCode , sysClk > 0                             , oC_ErrorCode_TIMCantCalculatePrescaler )
            &&  oC_AssignErrorCodeIfFalse( &errorCode , CountsDir  == oC_TIM_CountsDir_Down      ||
                                                        CountsDir  == oC_TIM_CountsDir_DontMatter   , oC_ErrorCode_TIMCountsDirNotSupported )
    )
    {
        float sysPeriodInSeconds    = 1 / ((float)sysClk);
        float tickPeriodInSeconds   = oC_TIM_TickPeriod_To_s( TickPeriod );

        if (
                oC_AssignErrorCodeIfFalse( &errorCode , tickPeriodInSeconds > 0 , oC_ErrorCode_TIMTickPeriodTooSmall )
        )
        {
            float prescaler             = tickPeriodInSeconds / sysPeriodInSeconds;

            if ( oC_AssignErrorCodeIfFalse( &errorCode , prescaler >= 1 , oC_ErrorCode_TIMTickPeriodTooSmall ) )
            {
                if (
                            (  TimerSize16or32Bits && prescaler < ( (float)oC_UINT8_MAX  ) )
                        ||  ( !TimerSize16or32Bits && prescaler < ( (float)oC_UINT16_MAX ) )
                )
                {
                    if ( outPrescaler != NULL )
                    {
                        *outPrescaler   = (uint32_t)prescaler;
                    }
                    errorCode       = oC_ErrorCode_None;
                }
                else
                {
                    errorCode   = oC_ErrorCode_TIMTickPeriodTooBig;
                }
            }
        }
    }


    return errorCode;
}

/* END OF SECTION */
#undef  _________________________________________LOCAL_FUNCTIONS_SOURCES_SECTION____________________________________________________________

/** ****************************************************************************************************************************************
 * The section with interrupts
 */
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

static void TimerAHandler(oC_TIM_Channel_t Channel)
{
    bool taskWoken = false;

    RegisterMap_t * rmap = GetRMap( Channel );

    if ( oC_IS_BIT_SET( rmap->RIS , 0 ) )
    {
        // timer A timeout
        taskWoken = oC_TIM_INTHandler_TimeoutInterrupt( Channel );
        oC_SET_BIT( rmap->ICR , oC_BIT_0 );
    }
    if ( oC_IS_BIT_SET( rmap->RIS , 4 ) )
    {
        // timer A match
        taskWoken = oC_TIM_INTHandler_MatchInterrupt( Channel ) || taskWoken;

        oC_SET_BIT( rmap->ICR , oC_BIT_4 );
    }

    if ( taskWoken )
    {
        taskYIELD();
    }
}

static void TimerBHandler(oC_TIM_Channel_t Channel)
{
    bool taskWoken = false;

    RegisterMap_t * rmap = GetRMap( Channel );

    if ( oC_IS_BIT_SET( rmap->RIS , 8 ) )
    {
        // timer B timeout

        if ( GetTimerWidth(rmap) == TimerWidth_Full )
        {
           taskWoken = oC_TIM_INTHandler_TimeoutInterrupt( Channel );
        }
        oC_SET_BIT( rmap->ICR , oC_BIT_8 );
    }
    if ( oC_IS_BIT_SET( rmap->RIS , 11 ) )
    {
        // timer B match
        if ( GetTimerWidth(rmap) == TimerWidth_Full )
        {
            taskWoken = oC_TIM_INTHandler_MatchInterrupt( Channel ) || taskWoken;
        }
        oC_SET_BIT( rmap->ICR , oC_BIT_11 );
    }

    if ( taskWoken )
    {
        taskYIELD();
    }
}

#define ADD(CHANNEL, ...)    \
    void oC_ONE_WORD_FROM_3(oC_INT_ , CHANNEL , AHandler)(void)\
    {\
        TimerAHandler( oC_ONE_WORD_FROM_2(oC_TIM_Channel_ , CHANNEL) );\
    }

oC_TIM_ChannelsList(ADD)

#undef ADD
#define ADD(CHANNEL, ...)    \
    void oC_ONE_WORD_FROM_3(oC_INT_ , CHANNEL , BHandler)(void)\
    {\
        TimerBHandler( oC_ONE_WORD_FROM_2(oC_TIM_Channel_ , CHANNEL) );\
    }

oC_TIM_ChannelsList(ADD)

#undef ADD

/* END OF SECTION */
#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________
