/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld.h Ver 1.0.0
 *
 *    @file       oc_tim.h
 *
 *    @brief      File with interface functions for the TIM driver.
 *
 *    @author     Patryk Kubiak - (Created on: 2015-02-25 - 19:00:21)
 *
 *    @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef _OC_TIM_LLD_H
#define _OC_TIM_LLD_H

#include <oc_tim.h>

/*==========================================================================================================================================
//
//     CONSTS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     EXTERN VARIABLES DEFINITIONS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

/* REQUIRED PROTOTYPES */
extern void              oC_TIM_LLD_TurnOnDriver                 ( void );
extern void              oC_TIM_LLD_TurnOffDriver                ( void );

/* CONFIGURE PROTOTYPES */
extern oC_ErrorCode_t    oC_TIM_LLD_Configure                    ( const oC_TIM_Config_t * Config , oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_Unconfigure                  ( const oC_TIM_Config_t * Config , oC_TIM_Channel_t Channel );

extern bool              oC_TIM_LLD_IsConfigurationPossible      ( oC_TIM_Channel_t Channel , const oC_TIM_Config_t * Config );

extern void              oC_TIM_LLD_RestoreDefaultStateOnChannel ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_StartTimer                   ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_StopTimer                    ( oC_TIM_Channel_t Channel );
extern bool              oC_TIM_LLD_IsTimerRunning               ( oC_TIM_Channel_t Channel );
extern uint64_t          oC_TIM_LLD_GetCurrentCount              ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_ReadCurrentCount             ( oC_TIM_Channel_t Channel , uint64_t * outValue );
extern oC_ErrorCode_t    oC_TIM_LLD_SetCurrentCount              ( oC_TIM_Channel_t Channel , uint64_t Value );
extern oC_ErrorCode_t    oC_TIM_LLD_SetMaxValue                  ( oC_TIM_Channel_t Channel , uint64_t Value );
extern oC_ErrorCode_t    oC_TIM_LLD_ReadMaxValue                 ( oC_TIM_Channel_t Channel , uint64_t * Value );
extern uint64_t          oC_TIM_LLD_GetMaxValue                  ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_SetMatchValue                ( oC_TIM_Channel_t Channel , uint64_t Value );
extern oC_ErrorCode_t    oC_TIM_LLD_ReadMatchValue               ( oC_TIM_Channel_t Channel , uint64_t * Value );
extern uint64_t          oC_TIM_LLD_GetMatchValue                ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_SetValue                     ( oC_TIM_Channel_t Channel , uint64_t Value );
extern oC_ErrorCode_t    oC_TIM_LLD_ReadValue                    ( oC_TIM_Channel_t Channel , uint64_t * Value );
extern uint64_t          oC_TIM_LLD_GetValue                     ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_TurnOnMatchInterrupt         ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_TurnOffMatchInterrupt        ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_TurnOnTimeoutInterrupt       ( oC_TIM_Channel_t Channel );
extern oC_ErrorCode_t    oC_TIM_LLD_TurnOffTimeoutInterrupt      ( oC_TIM_Channel_t Channel );

/* INTERRUPT HANDLERS */
extern bool              oC_TIM_INTHandler_MatchInterrupt       ( oC_TIM_Channel_t Channel );
extern bool              oC_TIM_INTHandler_TimeoutInterrupt     ( oC_TIM_Channel_t Channel );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/



#endif /* _OC_TIM_LLD_H */
