/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    File based on lld.c Ver 1.0.1
 *
 *    @file       oc_sys.h
 *
 *    @brief      File with source of functions of the LLD layer
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-21 - 22:31:22)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
 
/*==========================================================================================================================================
//
//    INCLUDES
//
//========================================================================================================================================*/

// General
#include <oc_sys.h>
#include <oc_reg_defs.h>
#include <oc_gen_types.h>
#include <oc_assert.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/

#define oC_SYS_InternalOscillatorFrequency                  oC_Frequency_MHz(16)
#define oC_SYS_PLL_Frequency                                ( oC_Frequency_MHz(400) )
#define oC_SYS_LowFrequencyInternalOscillator               ( 30000UL )

/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

typedef enum
{
    _RCC    = 0 ,
    _RCC2   = oC_REG_SysCtrl_RCC2_USERCC2
} _RCC_t;

typedef enum
{
    _PLL_OUT_200MHz ,
    _PLL_OUT_400MHz
} _PLL_OUT_t;

typedef enum
{
    _BYPASS_FROM_PLL ,
    _BYPASS_FROM_OSCSRC
} _BYPASS_t;

typedef enum
{
    _USESYSDIV_DONT_USE_SYSDIV = 0,
    _USESYSDIV_USE_SYSDIV      = oC_REG_SysCtrl_RCC_USESYSDIV
} _USESYSDIV_t;

typedef enum
{
    _OSCSRC_MainOscillator                          = oC_REG_SysCtrl_RCC_OSCSRC_MainOscillator,
    _OSCSRC_PrecisionInternalOscillator             = oC_REG_SysCtrl_RCC_OSCSRC_PrecisionInternalOscillator ,
    _OSCSRC_PrecisionInternalOscillatorDividedBy4   = oC_REG_SysCtrl_RCC_OSCSRC_PrecisionInternalOscillator4 ,
    _OSCSRC_LowFrequencyOscillator                  = oC_REG_SysCtrl_RCC_OSCSRC_LowFrequencyInternalOscillator ,
    _OSCSRC_32768kHz                                = (0x7 << 4) /* external oscillator */
} _OSCSRC_t;

typedef enum
{
    _XTAL_4MHz        = oC_REG_SysCtrl_RCC_XTAL_4MHz        ,
    _XTAL_4_096kHz    = oC_REG_SysCtrl_RCC_XTAL_4_096kHz    ,
    _XTAL_4_915_200Hz = oC_REG_SysCtrl_RCC_XTAL_4_915_200Hz ,
    _XTAL_5MHz        = oC_REG_SysCtrl_RCC_XTAL_5MHz        ,
    _XTAL_5_120kHz    = oC_REG_SysCtrl_RCC_XTAL_5_120kHz    ,
    _XTAL_6MHz        = oC_REG_SysCtrl_RCC_XTAL_6MHz        ,
    _XTAL_6_144kHz    = oC_REG_SysCtrl_RCC_XTAL_6_144kHz    ,
    _XTAL_7_372_800Hz = oC_REG_SysCtrl_RCC_XTAL_7_372_800Hz ,
    _XTAL_8MHz        = oC_REG_SysCtrl_RCC_XTAL_8MHz        ,
    _XTAL_8_192kHz    = oC_REG_SysCtrl_RCC_XTAL_8_192kHz    ,
    _XTAL_10MHz       = oC_REG_SysCtrl_RCC_XTAL_10MHz       ,
    _XTAL_12MHz       = oC_REG_SysCtrl_RCC_XTAL_12MHz       ,
    _XTAL_12_288kHz   = oC_REG_SysCtrl_RCC_XTAL_12_288kHz   ,
    _XTAL_13_560kHz   = oC_REG_SysCtrl_RCC_XTAL_13_560kHz   ,
    _XTAL_14_318_180Hz= oC_REG_SysCtrl_RCC_XTAL_14_318_180Hz,
    _XTAL_16MHz       = oC_REG_SysCtrl_RCC_XTAL_16MHz       ,
    _XTAL_16_384kHz   = oC_REG_SysCtrl_RCC_XTAL_16_384kHz   ,
    _XTAL_18MHz       = oC_REG_SysCtrl_RCC_XTAL_18MHz       ,
    _XTAL_20MHz       = oC_REG_SysCtrl_RCC_XTAL_20MHz       ,
    _XTAL_24MHz       = oC_REG_SysCtrl_RCC_XTAL_24MHz       ,
    _XTAL_25MHz       = oC_REG_SysCtrl_RCC_XTAL_25MHz
} _XTAL_t;

typedef enum
{
    _PLL_POWER_UP       = 0 ,
    _PLL_POWER_DOWN     = oC_REG_SysCtrl_RCC_PWRDN
} _PLL_POWER_t;

typedef struct
{
    oC_uint32_t         divisor;
    _USESYSDIV_t        usesysdiv;
    _XTAL_t             xtal;
    _OSCSRC_t           oscsrc;
    _BYPASS_t           bypass;
    _RCC_t              rcc;
    _PLL_POWER_t        pll_power;
    _PLL_OUT_t          pll_out;
} _ClockConfig_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

static const oC_Frequency_t _XTAL_Frequency[]  = {
                4000000UL ,
                4096000UL ,
                4915200UL ,
                5000000UL ,
                5120000UL ,
                6000000UL ,
                6144000UL ,
                7372800UL ,
                8000000UL ,
                8192000UL ,
                10000000UL ,
                12000000UL ,
                12288000UL ,
                13560000UL ,
                14318180UL ,
                16000000UL ,
                16384000UL ,
                18000000UL ,
                20000000UL ,
                24000000UL ,
                25000000UL
};

oC_Frequency_t _RealFrequency   = oC_Frequency_MHz(16);

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

static oC_uint32_t        _CountDivisor( oC_Frequency_t WantedFrequency , oC_Frequency_t OscillatorFrequency );
static _XTAL_t            _SearchXTAL( oC_Frequency_t OscillatorFrequency );
static inline bool        _UpdateConfigWithDivisor( _ClockConfig_t* config , oC_uint32_t divisor );
static inline void        _Delay( volatile oC_uint32_t count );
static inline bool        _PLLIsLocked( void );
static inline void        _ConfigureClock( _ClockConfig_t* config );

/* REG Helpers */
static inline void          _SelectRCC( oC_MemoryInt_t *RCC2 , _RCC_t selection );
static inline void          _SelectBYPASS( oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2 , _BYPASS_t bypass );
static inline void          _SelectUSESYSDIV( oC_MemoryInt_t *RCC , _USESYSDIV_t usesysdiv );
static inline void          _SelectOSCSRC( oC_MemoryInt_t *RCC, oC_MemoryInt_t *RCC2 , _OSCSRC_t oscsrc );
static inline void          _TurnOnOSC( oC_MemoryInt_t *RCC , _OSCSRC_t oscsrc );
static inline void          _SelectXTAL(oC_MemoryInt_t *RCC , _XTAL_t xtal );
static inline void          _SelectPllOutSpeed( oC_MemoryInt_t *RCC2 , _PLL_OUT_t pll_out );
static inline void          _SelectPLLPower(oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2 , _PLL_POWER_t power );
static inline void          _SetClockDivisor( oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2, _RCC_t selected , _PLL_OUT_t pll_out , oC_uint8_t divisor );
static inline void          _ReadRegistersRCC( oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2 );
static inline void          _WriteRegistersRCC( oC_MemoryInt_t RCC , oC_MemoryInt_t  RCC2 );


/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Function called before system starts.
 */
//==========================================================================================================================================
void oC_SYS_LLD_TurnOnDriver( void )
{

}

//==========================================================================================================================================
/**
 * Function called before system restarts
 */
//==========================================================================================================================================
void oC_SYS_LLD_TurnOffDriver( void )
{

}

//==========================================================================================================================================
/**
 * Function called for configure driver
 *
 * @param Config        Refernce to the configuration structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_Configure( const oC_SYS_Config_t * Config )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * The function initialize clock to work with internal oscillator with specific frequency.
 *
 * @param WantedFrequency Frequency to achieve after clock configure
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_SYS_LLD_ConfigureInternalOscillator( oC_Frequency_t WantedFrequency )
{
    bool result = true;
    _ClockConfig_t  config = { 0 };
    oC_Frequency_t rFreq = 0;

    //
    //  SET FREQUENCY TO DEFAULT WHEN NOT SET
    //
    WantedFrequency = (WantedFrequency == 0) ? oC_Frequency_MHz(16) : WantedFrequency;

    if  ( WantedFrequency == oC_SYS_LowFrequencyInternalOscillator )
    {
        //
        //  CONFIGURE AS Low Frequency Internal Oscillator
        //
        config.oscsrc       = _OSCSRC_LowFrequencyOscillator;
        config.bypass       = _BYPASS_FROM_OSCSRC;
        config.pll_power    = _PLL_POWER_DOWN;
        config.pll_out      = _PLL_OUT_200MHz;
        config.divisor      = 1;
        rFreq               = oC_SYS_LowFrequencyInternalOscillator;
    }
    else
    {
        //
        //  Configure Precision Internal Oscillator
        //
        config.oscsrc               = _OSCSRC_PrecisionInternalOscillator;
        config.xtal                 = _XTAL_16MHz;
        config.divisor              = _CountDivisor( WantedFrequency , oC_SYS_PLL_Frequency );
        config.pll_out              = _PLL_OUT_400MHz;
        config.pll_power            = _PLL_POWER_UP;
        config.bypass               = _BYPASS_FROM_PLL;
        rFreq                       = oC_SYS_PLL_Frequency / config.divisor;
    }

    if ( false == _UpdateConfigWithDivisor(&config , config.divisor) )
    {
        result  = false;
    }

    if ( result == true )
    {
        _RealFrequency              = rFreq;
        _ConfigureClock( &config );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function initialize clock to work with external oscillator with specific frequency.
 *
 * @param WantedFrequency Frequency to achieve after clock configure
 * @param OscillatorFrequency Frequency of the external oscillator.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_SYS_LLD_ConfigureExternalOscillator( oC_Frequency_t WantedFrequency , oC_Frequency_t OscillatorFrequency )
{
    bool result = true;
    _ClockConfig_t  config;
    oC_Frequency_t rFreq = 0;
    oC_uint16_t multiplier = 0;

    config.xtal             = _SearchXTAL(OscillatorFrequency);
    config.oscsrc           = _OSCSRC_MainOscillator;

    if ( WantedFrequency <= OscillatorFrequency  )
    {
        // PLL NOT NEEDED
        config.bypass           = _BYPASS_FROM_OSCSRC;
        config.pll_power        = _PLL_POWER_DOWN;
        config.pll_out          = _PLL_OUT_200MHz;
        multiplier              = 1;
    }
    else
    {
        // PLL needed
        config.bypass           = _BYPASS_FROM_PLL;
        config.pll_out          = _PLL_OUT_400MHz;
        config.pll_power        = _PLL_POWER_UP;
        multiplier              = oC_SYS_PLL_Frequency / _XTAL_Frequency[(config.xtal - _XTAL_4MHz) >> 6];
    }

    config.divisor          = _CountDivisor(WantedFrequency , OscillatorFrequency * multiplier );
    rFreq                   = (OscillatorFrequency * multiplier) / config.divisor;

    if ( false == _UpdateConfigWithDivisor(&config , config.divisor) )
    {
        result  = false;
    }

    if ( result == true )
    {
        _RealFrequency      = rFreq;
        _ConfigureClock( &config );
    }

    return result;
}

//==========================================================================================================================================
/**
 * The function return current clock frequency
 *
 * @return Configured frequency of the system clock
 */
//==========================================================================================================================================
oC_Frequency_t oC_SYS_GetSysClock( void )
{
    return _RealFrequency;
}

//==========================================================================================================================================
/**
 *
 * @param WantedFrequency
 * @param OscillatorFrequency
 * @return
 */
//==========================================================================================================================================
static oC_uint32_t _CountDivisor(  oC_Frequency_t WantedFrequency , oC_Frequency_t OscillatorFrequency  )
{
    oC_uint32_t Prescaler = 1;

    if ( OscillatorFrequency > WantedFrequency )
    {
        Prescaler   = OscillatorFrequency / WantedFrequency;
    }

    return Prescaler;
}

//==========================================================================================================================================
/**
 * The function search XTAL configuration for selected oscillator frequency
 *
 * @param OscillatorFrequency Frequency of the external oscillator
 */
//==========================================================================================================================================
static _XTAL_t            _SearchXTAL( oC_Frequency_t OscillatorFrequency )
{
    _XTAL_t xtal;
    _XTAL_t retXtal = 0;
    oC_Frequency_t minDiff = oC_ABS(OscillatorFrequency , _XTAL_Frequency[1]);

    for( xtal = 1 ; xtal <= 21 ; xtal++ )
    {
        oC_Frequency_t diff = oC_ABS(OscillatorFrequency , _XTAL_Frequency[xtal]);

        if ( diff < minDiff )
        {
            retXtal = xtal;
            minDiff = diff;
        }
    }
    retXtal = (0x06U + retXtal) << 6;

    return retXtal;
}

//==========================================================================================================================================
/**
 * The function updates configuration according to new divisor
 *
 * @param config reference to configuration structure
 * @param divisor new divisor
 *
 * @return true when success
 */
//==========================================================================================================================================
static inline bool   _UpdateConfigWithDivisor( _ClockConfig_t* config , oC_uint32_t divisor )
{
    bool result = true;

    if ( config->divisor < 3 )
        {
            config->usesysdiv   = _USESYSDIV_DONT_USE_SYSDIV;
            config->divisor     = 3;// can not be less than 3
            config->rcc         = _RCC;
        }
        else if ( (config->divisor >= 3) && (config->divisor <= 64) )
        {
            config->usesysdiv   = _USESYSDIV_USE_SYSDIV;
            config->rcc         = _RCC2;
            config->divisor     = divisor;
        }
        else if ( config->divisor <= 128 )
        {
            config->usesysdiv   = _USESYSDIV_USE_SYSDIV;
            config->rcc         = _RCC2;
            config->pll_out     = _PLL_OUT_400MHz; // TO turn on lsb sysdiv bit
            config->divisor     = divisor;
        }
        else
        {
            result              = false;
        }

    return result;
}

//==========================================================================================================================================
/**
 * The function add simple delay for about count * 4 cycles. (it is depending from compiler)
 *
 * @param   count   how many iteration should be delayed
 */
//==========================================================================================================================================
static inline void          _Delay( volatile oC_uint32_t count )
{
    while( count-- );
}

/**
 * The function checks if PLL is locked
 *
 * @return  true if PLL is locked
 *          false if PLL is not locked
 */
static inline bool _PLLIsLocked( void )
{
    return ( oC_REG_SysCtrl_RMAP->RIS & oC_REG_SysCtrl_RIS_PLLLRIS );
}

//==========================================================================================================================================
/**
 * The function for configure clock.
 *
 * @param config reference to lld clock configuration structure
 */
//==========================================================================================================================================
static inline void        _ConfigureClock( _ClockConfig_t* config )
{
    oC_MemoryInt_t      RCC = 0;
    oC_MemoryInt_t      RCC2 = 0;

    oC_ASSERT(config);

    _ReadRegistersRCC(&RCC , &RCC2);

    _SelectBYPASS(&RCC , &RCC , _BYPASS_FROM_OSCSRC );
    _SelectUSESYSDIV(&RCC , _USESYSDIV_DONT_USE_SYSDIV );

    _WriteRegistersRCC(RCC , RCC2);

    // TODO : Turn on Main oscillator if used
    _TurnOnOSC(&RCC , config->oscsrc);

    _SelectOSCSRC(&RCC , &RCC2 , config->oscsrc);

    _SelectXTAL( &RCC , config->xtal );

    _SelectRCC(&RCC2 , config->rcc);

    _WriteRegistersRCC(RCC , RCC2);

    _Delay(16);

    _SelectPLLPower(&RCC , &RCC2 ,  config->pll_power);

    _SelectPllOutSpeed(&RCC2 , config->pll_out);

    // TODO Clear PLL lock interrupt SysCtl MISC -> PLL LOCK

    _WriteRegistersRCC(RCC , RCC2);

    // waiting for PLL
    while( !_PLLIsLocked() && (config->pll_power == _PLL_POWER_UP) );

    _SelectBYPASS(&RCC , &RCC2 , config->bypass);
    _SelectUSESYSDIV( &RCC , config->usesysdiv );

    // TODO: MOSCDIS

    _SetClockDivisor(&RCC , &RCC2 , config->rcc , config->pll_out , config->divisor);

    _WriteRegistersRCC(RCC , RCC2);

    _Delay(16);
}

//==========================================================================================================================================
/**
 * The function is for select between RCC and RCC2 registers
 *
 * @param RCC2 reference to RCC2 register
 * @param selection selection of the RCC register (_RCC or _RCC2)
 *
 */
//==========================================================================================================================================
static inline void          _SelectRCC( oC_MemoryInt_t *RCC2 , _RCC_t selection )
{
    oC_WRITE_WITH_MASK( *RCC2 , oC_REG_SysCtrl_RCC2_USERCC2 , selection );
}

//==========================================================================================================================================
/**
 * The function is for select source of the clock between from PLL output or OSCSRC
 *
 * @param RCC   reference to RCC register
 * @param RCC2  reference to RCC2 register
 * @param bypass bypass selection
 */
//==========================================================================================================================================
static inline void          _SelectBYPASS( oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2 , _BYPASS_t bypass )
{
    if ( bypass == _BYPASS_FROM_OSCSRC )
    {
        oC_SET_BIT( *RCC , oC_REG_SysCtrl_RCC_BYPASS );
        oC_SET_BIT( *RCC2 , oC_REG_SysCtrl_RCC2_BYPASS2 );
    }
    else
    {
        oC_CLR_BIT( *RCC , oC_REG_SysCtrl_RCC_BYPASS );
        oC_CLR_BIT( *RCC2 , oC_REG_SysCtrl_RCC2_BYPASS2 );
    }
}

//==========================================================================================================================================
/**
 * The function is for select if OSC should be divided by SYSDIV
 *
 * @param RCC   reference to RCC register
 * @param usesysdiv divide / not divide
 */
//==========================================================================================================================================
static inline void          _SelectUSESYSDIV( oC_MemoryInt_t *RCC , _USESYSDIV_t usesysdiv )
{
    oC_WRITE_WITH_MASK( *RCC , oC_REG_SysCtrl_RCC_USESYSDIV , usesysdiv );
}

//==========================================================================================================================================
/**
 * The function is for select oscillator source
 *
 * @param RCC   reference to RCC  register
 * @param RCC2  reference to RCC2 register
 * @param oscsrc source of the oscillator
 */
//==========================================================================================================================================
static inline void          _SelectOSCSRC( oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2 , _OSCSRC_t oscsrc )
{
    if ( oscsrc <= _OSCSRC_LowFrequencyOscillator )
    {
        oC_WRITE_WITH_MASK( *RCC  , oC_REG_SysCtrl_RCC_OSCSRC   , oscsrc );
    }

    oC_WRITE_WITH_MASK( *RCC2 , oC_REG_SysCtrl_RCC2_OSCSRC2 , oscsrc );
}

//==========================================================================================================================================
/**
 * The function for turning on oscillator (Main or Precision Internal)
 *
 * @param RCC reference to RCC register
 * @param oscsrc source of the oscillator
 */
//==========================================================================================================================================
static inline void          _TurnOnOSC( oC_MemoryInt_t *RCC ,  _OSCSRC_t oscsrc )
{
    if ( oscsrc == _OSCSRC_MainOscillator )
    {
        oC_CLR_BIT( *RCC , oC_REG_SysCtrl_RCC_MOSCDIS);
        oC_CLR_BIT( oC_REG_SysCtrl_RMAP->RCC , oC_REG_SysCtrl_RCC_MOSCDIS);

        // wait the worst case time
        _Delay(524288);
    }
}

//==========================================================================================================================================
/**
 * The function is for select oscillator frequency for PLL
 *
 * @param RCC   reference to RCC register
 * @param xtal oscillator frequency
 */
//==========================================================================================================================================
static inline void          _SelectXTAL(oC_MemoryInt_t *RCC , _XTAL_t xtal )
{
    oC_WRITE_WITH_MASK( *RCC , oC_REG_SysCtrl_RCC_XTAL , xtal );
}

//==========================================================================================================================================
/**
 * The function is for select output speed for PLL
 *
 * @param RCC   reference to RCC register
 * @param pll_out speed of PLL output
 */
//==========================================================================================================================================
static inline void          _SelectPllOutSpeed( oC_MemoryInt_t *RCC2 , _PLL_OUT_t pll_out )
{
    if ( pll_out == _PLL_OUT_200MHz )
    {
        oC_CLR_BIT(*RCC2 , oC_REG_SysCtrl_RCC2_DIV400);
    }
    else
    {
        oC_SET_BIT(*RCC2 , oC_REG_SysCtrl_RCC2_DIV400);
        oC_CLR_BIT(*RCC2 , oC_REG_SysCtrl_RCC2_SYSDIV2LSB);
    }
}

//==========================================================================================================================================
/**
 * The function is for power-up/power-down PLL
 *
 * @param RCC   reference to RCC register
 * @param RCC2   reference to RCC2 register
 * @param power if pll power should be turn-on
 */
//==========================================================================================================================================
static inline void          _SelectPLLPower(oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2 , _PLL_POWER_t power )
{
    oC_WRITE_WITH_MASK(*RCC  , oC_REG_SysCtrl_RCC_PWRDN   , power);
    oC_WRITE_WITH_MASK(*RCC2 , oC_REG_SysCtrl_RCC2_PWRDN2 , power);
}

//==========================================================================================================================================
/**
 * The function for set divisor in (SYSDIV) (RCC or RCC2)
 *
 * @param RCC       reference to RCC register
 * @param RCC2      reference to RCC2 register
 * @param selected  which RCC is selected
 * @param pll_out   which PLL output speed is selected
 * @param divisor   divisor to set
 */
//==========================================================================================================================================
static inline void          _SetClockDivisor( oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2 , _RCC_t selected  , _PLL_OUT_t pll_out , oC_uint8_t divisor )
{

    if ( selected == _RCC )
    {
        oC_ASSERT( (divisor <= 16) && (divisor > 2) );

        oC_WRITE_WITH_MASK( *RCC , oC_REG_SysCtrl_RCC_SYSDIV ,   (divisor - 1) << 23 );
    }
    else
    {
        if ( pll_out == _PLL_OUT_200MHz )
        {
            oC_ASSERT( (divisor <= 64) && (divisor > 2) );

            oC_WRITE_WITH_MASK( *RCC2 , oC_REG_SysCtrl_RCC2_SYSDIV2 , (divisor - 1) << 23 );
        }
        else
        {
            oC_ASSERT( (divisor <= 128) && (divisor > 2) );

            oC_WRITE_WITH_MASK( *RCC2 , oC_REG_SysCtrl_RCC2_SYSDIV2 , (divisor - 1) << 22 );
        }

    }
}

//==========================================================================================================================================
/**
 * The function reads current state of RCC and RCC2 registers
 *
 * @param RCC reference to memory int for returned value
 * @param RCC2 reference to memory int for returned value
 */
//==========================================================================================================================================
static inline void          _ReadRegistersRCC( oC_MemoryInt_t *RCC , oC_MemoryInt_t *RCC2 )
{
    *RCC    = oC_REG_SysCtrl_RMAP->RCC;
    *RCC2   = oC_REG_SysCtrl_RMAP->RCC2;
}

//==========================================================================================================================================
/**
 * The function writes new states of RCC and RCC2 registers
 *
 * @param RCC new value for RCC register
 * @param RCC2 new value for RCC2 register
 */
//==========================================================================================================================================
static inline void          _WriteRegistersRCC( oC_MemoryInt_t RCC , oC_MemoryInt_t  RCC2 )
{
    if ( RCC2 & oC_REG_SysCtrl_RCC2_USERCC2 )
    {
        // RCC2 is used
        oC_REG_SysCtrl_RMAP->RCC2   = RCC2;
        oC_REG_SysCtrl_RMAP->RCC    = RCC;
    }
    else
    {
        // RCC2 is not used
        oC_REG_SysCtrl_RMAP->RCC    = RCC;
        oC_REG_SysCtrl_RMAP->RCC2   = RCC2;
    }
}

/*==========================================================================================================================================
//
//     INTERRUPTS
//
//========================================================================================================================================*/


void oC_INT_HardFaultHandler(void)
{
    oC_IMPLEMENT_FAILURE();
}

void oC_INT_MPUFaultHandler(void)
{
    oC_IMPLEMENT_FAILURE();
}

void oC_INT_BUSFaultHandler(void)
{
    oC_IMPLEMENT_FAILURE();
}

void oC_INT_UsageFaultHandler(void)
{
    oC_IMPLEMENT_FAILURE();
}
