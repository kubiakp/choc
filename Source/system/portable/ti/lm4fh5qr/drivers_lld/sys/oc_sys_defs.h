/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 * 	  File based on lld_defs.h Ver 1.0.1
 *
 *    @file       
 *
 *    @brief      Definitions for the SYS for micro controller
 *
 *    @author     Patryk Kubiak - (Created on: 2014-12-21 - 22:31:22)
 *
 *    @note       Copyright (C) Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SYS_OC_SYS_DEFS_H_
#define SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SYS_OC_SYS_DEFS_H_

/*==========================================================================================================================================
//
//     MICRO CONTROLLER DEFINITIONS
//
//========================================================================================================================================*/



#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_DRIVERS_LLD_SYS_OC_SYS_DEFS_H_ */
