/*******************************************************************************
 * @author: Daniel Zorychta [danz@jabster.pl]
 *
 * Vector file for STM32F10x microcontroller series
 * compiler: arm-none-eabi-gcc
*******************************************************************************/

/** define weak assign to the default function */
#define WEAK_DEFAULT __attribute__ ((interrupt, weak, alias("__Default_Handler")))

/*============================================================================*/
/**
* \brief Default interrupt handler.
* \details Default interrupt handler, used for interrupts that don't have their
* own handler defined.
*/
/*============================================================================*/
static void __Default_Handler(void) __attribute__ ((interrupt));
static void __Default_Handler(void)
{
        while (1);
}

/*==============================================================================
Assign all unused interrupts to the default handler
==============================================================================*/
WEAK_DEFAULT void oC_INT_NMIHandler(void);
WEAK_DEFAULT void oC_INT_HardFaultHandler(void);
WEAK_DEFAULT void oC_INT_MPUFaultHandler(void);
WEAK_DEFAULT void oC_INT_BUSFaultHandler(void);
WEAK_DEFAULT void oC_INT_UsageFaultHandler(void);
WEAK_DEFAULT void oC_INT_SVC_Handler(void);
WEAK_DEFAULT void oC_INT_DebugMonitorHandler(void);
WEAK_DEFAULT void oC_INT_PendSV_Handler(void);
WEAK_DEFAULT void oC_INT_SysTick_Handler(void);

// GPIO
WEAK_DEFAULT void oC_INT_GPIOAHandler(void);
WEAK_DEFAULT void oC_INT_GPIOBHandler(void);
WEAK_DEFAULT void oC_INT_GPIOCHandler(void);
WEAK_DEFAULT void oC_INT_GPIODHandler(void);
WEAK_DEFAULT void oC_INT_GPIOEHandler(void);
WEAK_DEFAULT void oC_INT_GPIOFHandler(void);

// SSI
WEAK_DEFAULT void oC_INT_SSI0Handler(void);
WEAK_DEFAULT void oC_INT_SSI1Handler(void);
WEAK_DEFAULT void oC_INT_SSI2Handler(void);
WEAK_DEFAULT void oC_INT_SSI3Handler(void);

// UART
WEAK_DEFAULT void oC_INT_UART0Handler(void);
WEAK_DEFAULT void oC_INT_UART1Handler(void);
WEAK_DEFAULT void oC_INT_UART2Handler(void);
WEAK_DEFAULT void oC_INT_UART3Handler(void);
WEAK_DEFAULT void oC_INT_UART4Handler(void);
WEAK_DEFAULT void oC_INT_UART5Handler(void);
WEAK_DEFAULT void oC_INT_UART6Handler(void);
WEAK_DEFAULT void oC_INT_UART7Handler(void);

// TIMERS
WEAK_DEFAULT void oC_INT_Timer0AHandler(void);
WEAK_DEFAULT void oC_INT_Timer1AHandler(void);
WEAK_DEFAULT void oC_INT_Timer2AHandler(void);
WEAK_DEFAULT void oC_INT_Timer3AHandler(void);
WEAK_DEFAULT void oC_INT_Timer4AHandler(void);
WEAK_DEFAULT void oC_INT_Timer5AHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer0AHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer1AHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer2AHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer3AHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer4AHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer5AHandler(void);
WEAK_DEFAULT void oC_INT_Timer0BHandler(void);
WEAK_DEFAULT void oC_INT_Timer1BHandler(void);
WEAK_DEFAULT void oC_INT_Timer2BHandler(void);
WEAK_DEFAULT void oC_INT_Timer3BHandler(void);
WEAK_DEFAULT void oC_INT_Timer4BHandler(void);
WEAK_DEFAULT void oC_INT_Timer5BHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer0BHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer1BHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer2BHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer3BHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer4BHandler(void);
WEAK_DEFAULT void oC_INT_WideTimer5BHandler(void);

/* LM4F120 specific Interrupt Numbers ******************************************/
WEAK_DEFAULT void IntDefaultHandler(void);

/*==================================================================================================
Vector table
==================================================================================================*/
extern const char __main_stack_end;          /* imported main stack end (from linker script)      */

void Reset_Handler(void);                    /* import the address of Reset_Handler()             */

void (*const vectors[])(void) __attribute__ ((section(".vectors"))) =
{
   (void (*)(void))&__main_stack_end,        /* Main stack end address                            */
    Reset_Handler,                          // The reset handler
    oC_INT_NMIHandler,                      // The NMI handler
    oC_INT_HardFaultHandler,                // The hard fault handler
    oC_INT_MPUFaultHandler,                 // The MPU fault handler
    oC_INT_BUSFaultHandler,                 // The bus fault handler
    oC_INT_UsageFaultHandler,               // The usage fault handler
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    oC_INT_SVC_Handler,                     // SVCall handler
    oC_INT_DebugMonitorHandler,             // Debug monitor handler
    0,                                      // Reserved
    oC_INT_PendSV_Handler,                  // The PendSV handler
    oC_INT_SysTick_Handler,                 // The SysTick handler
    oC_INT_GPIOAHandler,                    // GPIO Port A
    oC_INT_GPIOBHandler,                    // GPIO Port B
    oC_INT_GPIOCHandler,                    // GPIO Port C
    oC_INT_GPIODHandler,                    // GPIO Port D
    oC_INT_GPIOEHandler,                    // GPIO Port E
    oC_INT_UART0Handler,                    // UART0 Rx and Tx
    oC_INT_UART1Handler,                    // UART1 Rx and Tx
    oC_INT_SSI0Handler,                     // SSI0 Rx and Tx
    IntDefaultHandler,                      // I2C0 Master and Slave
    IntDefaultHandler,                      // PWM Fault
    IntDefaultHandler,                      // PWM Generator 0
    IntDefaultHandler,                      // PWM Generator 1
    IntDefaultHandler,                      // PWM Generator 2
    IntDefaultHandler,                      // Quadrature Encoder 0
    IntDefaultHandler,                      // ADC Sequence 0
    IntDefaultHandler,                      // ADC Sequence 1
    IntDefaultHandler,                      // ADC Sequence 2
    IntDefaultHandler,                      // ADC Sequence 3
    IntDefaultHandler,                      // Watchdog timer
    oC_INT_Timer0AHandler,                      // Timer 0 subtimer A
    oC_INT_Timer0BHandler,                      // Timer 0 subtimer B
    oC_INT_Timer1AHandler,                      // Timer 1 subtimer A
    oC_INT_Timer1BHandler,                      // Timer 1 subtimer B
    oC_INT_Timer2AHandler,                      // Timer 2 subtimer A
    oC_INT_Timer2BHandler,                      // Timer 2 subtimer B
    IntDefaultHandler,                      // Analog Comparator 0
    IntDefaultHandler,                      // Analog Comparator 1
    IntDefaultHandler,                      // Analog Comparator 2
    IntDefaultHandler,                      // System Control (PLL, OSC, BO)
    IntDefaultHandler,                      // FLASH Control
    oC_INT_GPIOFHandler,                    // GPIO Port F
    IntDefaultHandler,                      // GPIO Port G
    IntDefaultHandler,                      // GPIO Port H
    oC_INT_UART2Handler,                    // UART2 Rx and Tx
    oC_INT_SSI1Handler,                     // SSI1 Rx and Tx
    oC_INT_Timer3AHandler,                      // Timer 3 subtimer A
    oC_INT_Timer3BHandler,                      // Timer 3 subtimer B
    IntDefaultHandler,                      // I2C1 Master and Slave
    IntDefaultHandler,                      // Quadrature Encoder 1
    IntDefaultHandler,                      // CAN0
    IntDefaultHandler,                      // CAN1
    IntDefaultHandler,                      // CAN2
    0,                                      // Reserved
    IntDefaultHandler,                      // Hibernate
    IntDefaultHandler,                      // USB0
    IntDefaultHandler,                      // PWM Generator 3
    IntDefaultHandler,                      // uDMA Software Transfer
    IntDefaultHandler,                      // uDMA Error
    IntDefaultHandler,                      // ADC1 Sequence 0
    IntDefaultHandler,                      // ADC1 Sequence 1
    IntDefaultHandler,                      // ADC1 Sequence 2
    IntDefaultHandler,                      // ADC1 Sequence 3
    0,                                      // Reserved
    0,                                      // Reserved
    IntDefaultHandler,                      // GPIO Port J
    IntDefaultHandler,                      // GPIO Port K
    IntDefaultHandler,                      // GPIO Port L
    oC_INT_SSI2Handler,                     // SSI2 Rx and Tx
    oC_INT_SSI3Handler,                     // SSI3 Rx and Tx
    oC_INT_UART3Handler,                    // UART3 Rx and Tx
    oC_INT_UART4Handler,                    // UART4 Rx and Tx
    oC_INT_UART5Handler,                    // UART5 Rx and Tx
    oC_INT_UART6Handler,                    // UART6 Rx and Tx
    oC_INT_UART7Handler,                    // UART7 Rx and Tx
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    IntDefaultHandler,                      // I2C2 Master and Slave
    IntDefaultHandler,                      // I2C3 Master and Slave
    oC_INT_Timer4AHandler,                      // Timer 4 subtimer A
    oC_INT_Timer4BHandler,                      // Timer 4 subtimer B
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    oC_INT_Timer5AHandler,                      // Timer 5 subtimer A
    oC_INT_Timer5BHandler,                      // Timer 5 subtimer B
    oC_INT_WideTimer0AHandler,                      // Wide Timer 0 subtimer A
    oC_INT_WideTimer0BHandler,                      // Wide Timer 0 subtimer B
    oC_INT_WideTimer1AHandler,                      // Wide Timer 1 subtimer A
    oC_INT_WideTimer1BHandler,                      // Wide Timer 1 subtimer B
    oC_INT_WideTimer2AHandler,                      // Wide Timer 2 subtimer A
    oC_INT_WideTimer2BHandler,                      // Wide Timer 2 subtimer B
    oC_INT_WideTimer3AHandler,                      // Wide Timer 3 subtimer A
    oC_INT_WideTimer3BHandler,                      // Wide Timer 3 subtimer B
    oC_INT_WideTimer4AHandler,                      // Wide Timer 4 subtimer A
    oC_INT_WideTimer4BHandler,                      // Wide Timer 4 subtimer B
    oC_INT_WideTimer5AHandler,                      // Wide Timer 5 subtimer A
    oC_INT_WideTimer5BHandler,                      // Wide Timer 5 subtimer B
    IntDefaultHandler,                      // FPU
    IntDefaultHandler,                      // PECI 0
    IntDefaultHandler,                      // LPC 0
    IntDefaultHandler,                      // I2C4 Master and Slave
    IntDefaultHandler,                      // I2C5 Master and Slave
    IntDefaultHandler,                      // GPIO Port M
    IntDefaultHandler,                      // GPIO Port N
    IntDefaultHandler,                      // Quadrature Encoder 2
    IntDefaultHandler,                      // Fan 0
    0,                                      // Reserved
    IntDefaultHandler,                      // GPIO Port P (Summary or P0)
    IntDefaultHandler,                      // GPIO Port P1
    IntDefaultHandler,                      // GPIO Port P2
    IntDefaultHandler,                      // GPIO Port P3
    IntDefaultHandler,                      // GPIO Port P4
    IntDefaultHandler,                      // GPIO Port P5
    IntDefaultHandler,                      // GPIO Port P6
    IntDefaultHandler,                      // GPIO Port P7
    IntDefaultHandler,                      // GPIO Port Q (Summary or Q0)
    IntDefaultHandler,                      // GPIO Port Q1
    IntDefaultHandler,                      // GPIO Port Q2
    IntDefaultHandler,                      // GPIO Port Q3
    IntDefaultHandler,                      // GPIO Port Q4
    IntDefaultHandler,                      // GPIO Port Q5
    IntDefaultHandler,                      // GPIO Port Q6
    IntDefaultHandler,                      // GPIO Port Q7
    IntDefaultHandler,                      // GPIO Port R
    IntDefaultHandler,                      // GPIO Port S
    IntDefaultHandler,                      // PWM 1 Generator 0
    IntDefaultHandler,                      // PWM 1 Generator 1
    IntDefaultHandler,                      // PWM 1 Generator 2
    IntDefaultHandler,                      // PWM 1 Generator 3
    IntDefaultHandler                       // PWM 1 Fault
};

/***************************************************************************************************
END OF FILE
***************************************************************************************************/
