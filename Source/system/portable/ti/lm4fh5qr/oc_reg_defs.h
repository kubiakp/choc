/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_reg_defs.h
 *
 *    @brief      The file with definitions of the registers
 *
 *    @author     Patryk Kubiak - (Created on: 7 pa� 2014 14:59:46)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef OC_REG_DEFS_H_
#define OC_REG_DEFS_H_

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INCLUDES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Core
#include <oc_memory_types.h>
#include <oc_gen_macros.h>

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    CONSTS
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  CMSIS
//
#define __NVIC_PRIO_BITS    3

/**
 * Type with number of interrupts for NVIC (p. 102)
 */
typedef enum
{
    NonMaskableInt_IRQn                                     = -14,    /*!< 2 Non Maskable Interrupt                             */
    MemoryManagement_IRQn                                   = -12,    /*!< 4 Cortex-M3 Memory Management Interrupt              */
    BusFault_IRQn                                           = -11,    /*!< 5 Cortex-M3 Bus Fault Interrupt                      */
    UsageFault_IRQn                                         = -10,    /*!< 6 Cortex-M3 Usage Fault Interrupt                    */
    SVCall_IRQn                                             = -5,     /*!< 11 Cortex-M3 SV Call Interrupt                       */
    DebugMonitor_IRQn                                       = -4,     /*!< 12 Cortex-M3 Debug Monitor Interrupt                 */
    PendSV_IRQn                                             = -2,     /*!< 14 Cortex-M3 Pend SV Interrupt                       */
    SysTick_IRQn                                            = -1,     /*!< 15 Cortex-M3 System Tick Interrupt                   */
    oC_REG_NVIC_InterruptNumber_GPIOA                       = 0,
    oC_REG_NVIC_InterruptNumber_GPIOB                       = 1,
    oC_REG_NVIC_InterruptNumber_GPIOC                       = 2,
    oC_REG_NVIC_InterruptNumber_GPIOD                       = 3,
    oC_REG_NVIC_InterruptNumber_GPIOE                       = 4,
    oC_REG_NVIC_InterruptNumber_UART0                       = 5,
    oC_REG_NVIC_InterruptNumber_UART1                       = 6,
    oC_REG_NVIC_InterruptNumber_SSI0                        = 7,
    oC_REG_NVIC_InterruptNumber_I2C0                        = 8,
    oC_REG_NVIC_InterruptNumber_ADC0_Sequence_0             = 14,
    oC_REG_NVIC_InterruptNumber_ADC0_Sequence_1             = 15,
    oC_REG_NVIC_InterruptNumber_ADC0_Sequence_2             = 16,
    oC_REG_NVIC_InterruptNumber_ADC0_Sequence_3             = 17,
    oC_REG_NVIC_InterruptNumber_Watchdog_Timers_0_and_1     = 18,
    oC_REG_NVIC_InterruptNumber_Timer0A         = 19,
    oC_REG_NVIC_InterruptNumber_Timer0B         = 20,
    oC_REG_NVIC_InterruptNumber_Timer1A         = 21,
    oC_REG_NVIC_InterruptNumber_Timer1B         = 22,
    oC_REG_NVIC_InterruptNumber_Timer2A         = 23,
    oC_REG_NVIC_InterruptNumber_Timer2B         = 24,
    oC_REG_NVIC_InterruptNumber_Analog_Comparator_0         = 25,
    oC_REG_NVIC_InterruptNumber_Analog_Comparator_1         = 26,
    oC_REG_NVIC_InterruptNumber_System_Control              = 28,
    oC_REG_NVIC_InterruptNumber_Flash_Memory_Control_and_EEPROM_Control = 29,
    oC_REG_NVIC_InterruptNumber_GPIOF                       = 30,
    oC_REG_NVIC_InterruptNumber_UART2                       = 33,
    oC_REG_NVIC_InterruptNumber_SSI1                        = 34,
    oC_REG_NVIC_InterruptNumber_Timer3A         = 35,
    oC_REG_NVIC_InterruptNumber_Timer3B         = 36,
    oC_REG_NVIC_InterruptNumber_I2C1                        = 37,
    oC_REG_NVIC_InterruptNumber_CAN0                        = 39,
    oC_REG_NVIC_InterruptNumber_HibernationModule           = 43,
    oC_REG_NVIC_InterruptNumber_USB                         = 44,
    oC_REG_NVIC_InterruptNumber_uDMA_Software               = 46,
    oC_REG_NVIC_InterruptNumber_uDMA_Error                  = 47,
    oC_REG_NVIC_InterruptNumber_ADC1_Sequence_0             = 48,
    oC_REG_NVIC_InterruptNumber_ADC1_Sequence_1             = 49,
    oC_REG_NVIC_InterruptNumber_ADC1_Sequence_2             = 50,
    oC_REG_NVIC_InterruptNumber_ADC1_Sequence_3             = 51,
    oC_REG_NVIC_InterruptNumber_SSI2                        = 57,
    oC_REG_NVIC_InterruptNumber_SSI3                        = 58,
    oC_REG_NVIC_InterruptNumber_UART3                       = 59,
    oC_REG_NVIC_InterruptNumber_UART4                       = 60,
    oC_REG_NVIC_InterruptNumber_UART5                       = 61,
    oC_REG_NVIC_InterruptNumber_UART6                       = 62,
    oC_REG_NVIC_InterruptNumber_UART7                       = 63,
    oC_REG_NVIC_InterruptNumber_I2C2                        = 68,
    oC_REG_NVIC_InterruptNumber_I2C3                        = 69,
    oC_REG_NVIC_InterruptNumber_Timer4A         = 70,
    oC_REG_NVIC_InterruptNumber_Timer4B         = 71,
    oC_REG_NVIC_InterruptNumber_Timer5A         = 92,
    oC_REG_NVIC_InterruptNumber_Timer5B         = 93,
    oC_REG_NVIC_InterruptNumber_WideTimer0A         = 94,
    oC_REG_NVIC_InterruptNumber_WideTimer0B         = 95,
    oC_REG_NVIC_InterruptNumber_WideTimer1A         = 96,
    oC_REG_NVIC_InterruptNumber_WideTimer1B         = 97,
    oC_REG_NVIC_InterruptNumber_WideTimer2A         = 98,
    oC_REG_NVIC_InterruptNumber_WideTimer2B         = 99,
    oC_REG_NVIC_InterruptNumber_WideTimer3A         = 100,
    oC_REG_NVIC_InterruptNumber_WideTimer3B         = 101,
    oC_REG_NVIC_InterruptNumber_WideTimer4A         = 102,
    oC_REG_NVIC_InterruptNumber_WideTimer4B         = 103,
    oC_REG_NVIC_InterruptNumber_WideTimer5A         = 104,
    oC_REG_NVIC_InterruptNumber_WideTimer5B         = 105,
    oC_REG_NVIC_InterruptNumber_SystemException             = 106,
} IRQn_Type;

#include <core_cm4.h>

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  COMMON
//

/**
 * Macro count size of reserved array in register map. Size cannot be less than 2!
 *
 * @param   _BEFORE_OFFSET_ offset of register before reserved memory
 * @param   _AFTER_OFFSET_ offset of register after reserved memory
 */
#define oC_REG_COUNT_RESERVED_SIZE( _BEFORE_OFFSET_ , _AFTER_OFFSET_ )      ( (_AFTER_OFFSET_ - _BEFORE_OFFSET_) / 4 - 1 )
#define oC_REG_COUNT_ARRAY_SIZE( _OFFSET_START_ , _OFFSET_END_ )            ( ( _OFFSET_END_  - _OFFSET_START_ ) / 4 )
#define oC_REG_RESERVED_ARRAY( _NR_ , _BEF_OFF_ , _AFT_OFF_ )               oC_Register_t _Reserved##_NR_[ oC_REG_COUNT_RESERVED_SIZE( _BEF_OFF_ , _AFT_OFF_ ) ]
#define oC_REG_RESERVED( _NR_ )                                             oC_Register_t _Reserved##_NR_

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  SysTick
//

/*
 *  BASES
 */
#define oC_REG_SysTick_BASE                         ( 0xE000E000UL )    /**< Base of the Register Map */

/**
 * Register map for SysTick (p. 132)
 */
typedef struct
{
oC_REG_RESERVED_ARRAY(0 , 0x0U , 0x10U);
    oC_Register_t       CTRL;                           /* SysTick Control and Status Register (p. 136) [Offset: 0x010] */
    oC_Register_t       RELOAD;                         /* SysTick Reload Value Register (p. 138) [Offset: 0x014] */
    oC_Register_t       CURRENT;                        /* SysTick Current Value Register (p. 139) [Offset: 0x018] */
} oC_REG_SysTick_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_SysTick_RMAP                         oC_CREATE_REGISTER_MAP_CAST( oC_REG_SysTick_RegisterMap_t , oC_REG_SysTick_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  NVIC
//

/*
 *  BASES
 */
#define oC_REG_NVIC_BASE                            ( 0xE000E000UL )    /**< Base of the Register Map */

/**
 * Register map for NVIC (p. 132)
 */
typedef struct
{
oC_REG_RESERVED_ARRAY(0 , 0x0U , 0x100U);
    oC_Register_t       EN[5];                          /* Interrupt 0-138 Set Enable (p. 140) [Offset: 0x100] */
oC_REG_RESERVED_ARRAY(1 , 0x110U , 0x180U);
    oC_Register_t       DIS[5];                         /* Interrupt 0-138 Clear Enable (p. 142) [Offset: 0x180] */
oC_REG_RESERVED_ARRAY(2 , 0x190U , 0x200U);
    oC_Register_t       PEND[5];                        /* Interrupt 0-138 Set Pending (p. 144) [Offset: 0x200] */
oC_REG_RESERVED_ARRAY(3 , 0x210U , 0x280U);
    oC_Register_t       UNPEND[5];                      /* Interrupt 0-31 Clear Pending (p. 146) [Offset: 0x280] */
oC_REG_RESERVED_ARRAY(4 , 0x290U , 0x300U);
    oC_Register_t       ACTIVE[5];                      /* Interrupt 0-31 Active Bit (p. 148) [Offset: 0x300] */
oC_REG_RESERVED_ARRAY(5 , 0x310U , 0x400U);
    oC_Register_t       PRI[35];                        /* Interrupt 0-138 Priority (p. 150) [Offset: 0x400] */
oC_REG_RESERVED_ARRAY(6 , 0x488U , 0xf00U);
    oC_Register_t       SWTRIG;                         /* Software Trigger Interrupt (p. 154) [Offset: 0xF00] */
} oC_REG_NVIC_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_NVIC_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_NVIC_RegisterMap_t , oC_REG_NVIC_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MPU
//

/*
 *  BASES
 */
#define oC_REG_MPU_BASE                             ( 0xE000E000UL )    /**< Base of the Register Map */

/**
 * Register map for MPU (p. 135)
 */
typedef struct
{
oC_REG_RESERVED_ARRAY(0 , 0x0U , 0xd90U);
    oC_Register_t       MPUTYPE;                        /* MPU Type (p. 184) [Offset: 0xD90] */
    oC_Register_t       MPUCTRL;                        /* MPU Control (p. 185) [Offset: 0xD94] */
    oC_Register_t       MPUNUMBER;                      /* MPU Region Number (p. 187) [Offset: 0xD98] */
    oC_Register_t       MPUBASE;                        /* MPU Region Base Address (p. 188) [Offset: 0xD9C] */
    oC_Register_t       MPUATTR;                        /* MPU Region Attribute and Size (p. 190) [Offset: 0xDA0] */
    oC_Register_t       MPUBASE1;                       /* MPU Region Base Address Alias 1 (p. 188) [Offset: 0xDA4] */
    oC_Register_t       MPUATTR1;                       /* MPU Region Attribute and Size Alias 1 (p. 190) [Offset: 0xDA8] */
    oC_Register_t       MPUBASE2;                       /* MPU Region Base Address Alias 2 (p. 188) [Offset: 0xDAC] */
    oC_Register_t       MPUATTR2;                       /* MPU Region Attribute and Size Alias 2 (p. 190) [Offset: 0xDB0] */
    oC_Register_t       MPUBASE3;                       /* MPU Region Base Address Alias 3 (p. 188) [Offset: 0xDB4] */
    oC_Register_t       MPUATTR3;                       /* MPU Region Attribute and Size Alias 3 (p. 190) [Offset: 0xDB8] */
} oC_REG_MPU_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_MPU_RMAP                             oC_CREATE_REGISTER_MAP_CAST( oC_REG_MPU_RegisterMap_t , oC_REG_MPU_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FPU
//

/*
 *  BASES
 */
#define oC_REG_FPU_BASE                             ( 0xE000E000UL )    /**< Base of the Register Map */

/**
 * Register map for FPU (p. 135)
 */
typedef struct
{
oC_REG_RESERVED_ARRAY(0 , 0x0U , 0xd88U);
    oC_Register_t       CPAC;                           /* Coprocessor Access Control (p. 193) [Offset: 0xD88] */
oC_REG_RESERVED_ARRAY(1 , 0xd88U , 0xf34U);
    oC_Register_t       FPCC;                           /* Floating-Point Context Control (p. 194) [Offset: 0xF34] */
    oC_Register_t       FPCA;                           /* Floating-Point Context Address (p. 196) [Offset: 0xF38] */
    oC_Register_t       FPDSC;                          /* Floating-Point Default Status Control (p. 197) [Offset: 0xF3C] */
} oC_REG_FPU_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_FPU_RMAP                             oC_CREATE_REGISTER_MAP_CAST( oC_REG_FPU_RegisterMap_t , oC_REG_FPU_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  System Control (SysCtrl) (p. 210)
//

/*
 *  BASES
 */
#define oC_REG_SysCtrl_BASE                         ( 0x400FE000UL )    /**< Base of the Register Map */

/**
 *  Register map for SysCtrl (p. 228)
 */
typedef struct
{
    oC_Register_t       DID0;                           /* Device Identification 0 (p. 233) [Offset: 0x000] */
    oC_Register_t       DID1;                           /* Device Identification 1 (p. 235) [Offset: 0x004] */
oC_REG_RESERVED_ARRAY(0 , 0x4U , 0x30U);
    oC_Register_t       PBORCTL;                        /* Brown-Out Reset Control (p. 237) [Offset: 0x030] */
oC_REG_RESERVED_ARRAY(1 , 0x30U , 0x50U);
    oC_Register_t       RIS;                            /* Raw Interrupt Status (p. 238) [Offset: 0x050] */
    oC_Register_t       IMC;                            /* Interrupt Mask Control (p. 241) [Offset: 0x054] */
    oC_Register_t       MISC;                           /* Masked Interrupt Status and Clear (p. 243) [Offset: 0x058] */
    oC_Register_t       RESC;                           /* Reset Cause (p. 246) [Offset: 0x05C] */
    oC_Register_t       RCC;                            /* Run-Mode Clock Configuration (p. 248) [Offset: 0x060] */
oC_REG_RESERVED_ARRAY(2 , 0x60U , 0x6cU);
    oC_Register_t       GPIOHBCTL;                      /* GPIO High-Performance Bus Control (p. 252) [Offset: 0x06C] */
    oC_Register_t       RCC2;                           /* Run-Mode Clock Configuration 2 (p. 254) [Offset: 0x070] */
oC_REG_RESERVED_ARRAY(3 , 0x70U , 0x7cU);
    oC_Register_t       MOSCCTL;                        /* Main Oscillator Control (p. 257) [Offset: 0x07C] */
oC_REG_RESERVED_ARRAY(4 , 0x7cU , 0x144U);
    oC_Register_t       DSLPCLKCFG;                     /* Deep Sleep Clock Configuration (p. 258) [Offset: 0x144] */
oC_REG_RESERVED(5);
    oC_Register_t       SYSPROP;                        /* System Properties (p. 260) [Offset: 0x14C] */
    oC_Register_t       PIOSCCAL;                       /* Precision Internal Oscillator Calibration (p. 261) [Offset: 0x150] */
    oC_Register_t       PIOSCSTAT;                      /* Precision Internal Oscillator Statistics (p. 263) [Offset: 0x154] */
oC_REG_RESERVED_ARRAY(6 , 0x154U , 0x160U);
    oC_Register_t       PLLFREQ0;                       /* PLL Frequency 0 (p. 264) [Offset: 0x160] */
    oC_Register_t       PLLFREQ1;                       /* PLL Frequency 1 (p. 265) [Offset: 0x164] */
    oC_Register_t       PLLSTAT;                        /* PLL Status (p. 266) [Offset: 0x168] */
oC_REG_RESERVED_ARRAY(7 , 0x168U , 0x300U);
    oC_Register_t       PPWD;                           /* Watchdog Timer Peripheral Present (p. 267) [Offset: 0x300] */
    oC_Register_t       PPTIMER;                        /* 16/32-Bit General-Purpose Timer Peripheral Present (p. 268) [Offset: 0x304] */
    oC_Register_t       PPGPIO;                         /* General-Purpose Input/Output Peripheral Present (p. 270) [Offset: 0x308] */
    oC_Register_t       PPDMA;                          /* Micro Direct Memory Access Peripheral Present (p. 273) [Offset: 0x30C] */
oC_REG_RESERVED_ARRAY(8 , 0x30cU , 0x31cU);
    oC_Register_t       PPSSI;                          /* Synchronous Serial Interface Peripheral Present (p. 277) [Offset: 0x31C] */
    oC_Register_t       PPI2C;                          /* Inter-Integrated Circuit Peripheral Present (p. 279) [Offset: 0x320] */
oC_REG_RESERVED(9);
    oC_Register_t       PPUSB;                          /* Universal Serial Bus Peripheral Present (p. 281) [Offset: 0x328] */
oC_REG_RESERVED_ARRAY(10 , 0x328U , 0x334U);
    oC_Register_t       PPCAN;                          /* Controller Area Network Peripheral Present (p. 282) [Offset: 0x334] */
    oC_Register_t       PPADC;                          /* Analog-to-Digital Converter Peripheral Present (p. 283) [Offset: 0x338] */
    oC_Register_t       PPACMP;                         /* Analog Comparator Peripheral Present (p. 284) [Offset: 0x33C] */
    oC_Register_t       PPPWM;                          /* Pulse Width Modulator Peripheral Present (p. 285) [Offset: 0x340] */
    oC_Register_t       PPQEI;                          /* Quadrature Encoder Interface Peripheral Present (p. 286) [Offset: 0x344] */
oC_REG_RESERVED_ARRAY(11 , 0x344U , 0x358U);
    oC_Register_t       PPEEPROM;                       /* EEPROM Peripheral Present 287 32/64-Bit Wide General-Purpose Timer Peripheral (p. 288) [Offset: 0x358] */
oC_REG_RESERVED_ARRAY(12 , 0x358U , 0x500U);
    oC_Register_t       SRWD;                           /* Watchdog Timer Software Reset (p. 290) [Offset: 0x500] */
    oC_Register_t       SRTIMER;                        /* 16/32-Bit General-Purpose Timer Software Reset (p. 292) [Offset: 0x504] */
    oC_Register_t       SRGPIO;                         /* General-Purpose Input/Output Software Reset (p. 294) [Offset: 0x508] */
    oC_Register_t       SRDMA;                          /* Micro Direct Memory Access Software Reset (p. 296) [Offset: 0x50C] */
oC_REG_RESERVED(13);
    oC_Register_t       SRHIB;                          /* Hibernation Software Reset 297 Universal Asynchronous Receiver/Transmitter Software (p. 298) [Offset: 0x514] */
oC_REG_RESERVED(14);
    oC_Register_t       SRSSI;                          /* Synchronous Serial Interface Software Reset (p. 300) [Offset: 0x51C] */
    oC_Register_t       SRI2C;                          /* Inter-Integrated Circuit Software Reset (p. 302) [Offset: 0x520] */
oC_REG_RESERVED(15);
    oC_Register_t       SRUSB;                          /* Universal Serial Bus Software Reset (p. 304) [Offset: 0x528] */
oC_REG_RESERVED_ARRAY(16 , 0x528U , 0x534U);
    oC_Register_t       SRCAN;                          /* Controller Area Network Software Reset (p. 305) [Offset: 0x534] */
    oC_Register_t       SRADC;                          /* Analog-to-Digital Converter Software Reset (p. 306) [Offset: 0x538] */
    oC_Register_t       SRACMP;                         /* Analog Comparator Software Reset (p. 308) [Offset: 0x53C] */
oC_REG_RESERVED_ARRAY(17 , 0x53cU , 0x558U);
    oC_Register_t       SREEPROM;                       /* EEPROM Software Reset (p. 309) [Offset: 0x558] */
    oC_Register_t       SRWTIMER;                       /* 32/64-Bit Wide General-Purpose Timer Software Reset (p. 310) [Offset: 0x55C] */
oC_REG_RESERVED_ARRAY(18 , 0x55cU , 0x604U);
    oC_Register_t       RCGCTIMER;                      /* 16/32-BitGeneral-Purpose Timer RunMode ClockGating Control (p. 313) [Offset: 0x604] */
    oC_Register_t       RCGCGPIO;                       /* Control General-Purpose Input/Output Run Mode Clock Gating (p. 315) [Offset: 0x608] */
oC_REG_RESERVED_ARRAY(19 , 0x608U , 0x614U);
    oC_Register_t       RCGCHIB;                        /* Hibernation Run Mode Clock Gating Control (p. 318) [Offset: 0x614] */
    oC_Register_t       RCGCUART;                       /* Missed in generation - added manually */
    oC_Register_t       RCGCSSI;                        /* Synchronous Serial Interface Run Mode Clock Gating Control  (p. 321) [Offset: 0x61C] */
    oC_Register_t       RCGCI2C;                        /* Inter-Integrated Circuit Run Mode Clock Gating Control (p. 323) [Offset: 0x620] */
oC_REG_RESERVED(21);
    oC_Register_t       RCGCUSB;                        /* Universal Serial Bus Run Mode Clock Gating Control (p. 325) [Offset: 0x628] */
oC_REG_RESERVED_ARRAY(22 , 0x628U , 0x634U);
    oC_Register_t       RCGCCAN;                        /* Controller Area Network RunMode Clock Gating Control (p. 326) [Offset: 0x634] */
    oC_Register_t       RCGCADC;                        /* Analog-to-Digital Converter Run Mode Clock Gating (p. 327) [Offset: 0x638] */
    oC_Register_t       RCGCACMP;                       /* Analog Comparator Run Mode Clock Gating Control (p. 328) [Offset: 0x63C] */
oC_REG_RESERVED_ARRAY(23 , 0x63cU , 0x658U);
    oC_Register_t       RCGCEEPROM;                     /* EEPROM Run Mode Clock Gating Control (p. 329) [Offset: 0x658] */
    oC_Register_t       RCGCWTIMER;                     /* Gating Control 32/64-BitWide General-Purpose Timer Run Mode Clock (p. 330) [Offset: 0x65C] */
oC_REG_RESERVED_ARRAY(24 , 0x65cU , 0x700U);
    oC_Register_t       SCGCWD;                         /* Watchdog Timer Sleep Mode Clock Gating Control (p. 332) [Offset: 0x700] */
    oC_Register_t       SCGCTIMER;                      /* Gating Control 16/32-Bit General-Purpose Timer Sleep Mode Clock (p. 333) [Offset: 0x704] */
    oC_Register_t       SCGCGPIO;                       /* Control General-Purpose Input/Output Sleep Mode Clock Gating (p. 335) [Offset: 0x708] */
    oC_Register_t       SCGCDMA;                        /* Control Micro Direct Memory Access Sleep Mode Clock Gating (p. 337) [Offset: 0x70C] */
oC_REG_RESERVED(25);
    oC_Register_t       SCGCHIB;                        /* Hibernation Sleep Mode Clock Gating Control (p. 338) [Offset: 0x714] */
    oC_Register_t       SCGCUART;                       /* Mode Clock Gating Control Universal Asynchronous Receiver/Transmitter Sleep (p. 339) [Offset: 0x718] */
    oC_Register_t       SCGCSSI;                        /* Control Synchronous Serial Interface Sleep Mode Clock Gating (p. 341) [Offset: 0x71C] */
    oC_Register_t       SCGCI2C;                        /* Inter-Integrated Circuit Sleep Mode Clock Gating Control (p. 343) [Offset: 0x720] */
oC_REG_RESERVED(26);
    oC_Register_t       SCGCUSB;                        /* Universal Serial Bus Sleep Mode Clock Gating Control (p. 345) [Offset: 0x728] */
oC_REG_RESERVED_ARRAY(27 , 0x728U , 0x734U);
    oC_Register_t       SCGCCAN;                        /* Control Controller Area Network Sleep Mode Clock Gating (p. 346) [Offset: 0x734] */
    oC_Register_t       SCGCADC;                        /* Control Analog-to-Digital Converter Sleep Mode Clock Gating (p. 347) [Offset: 0x738] */
    oC_Register_t       SCGCACMP;                       /* Analog Comparator Sleep Mode Clock Gating Control (p. 348) [Offset: 0x73C] */
oC_REG_RESERVED_ARRAY(28 , 0x73cU , 0x758U);
    oC_Register_t       SCGCEEPROM;                     /* EEPROM Sleep Mode Clock Gating Control (p. 349) [Offset: 0x758] */
    oC_Register_t       SCGCWTIMER;                     /* Gating Control 32/64-BitWideGeneral-Purpose Timer SleepMode Clock (p. 350) [Offset: 0x75C] */
oC_REG_RESERVED_ARRAY(29 , 0x75cU , 0x800U);
    oC_Register_t       DCGCWD;                         /* Watchdog Timer Deep-SleepMode Clock Gating Control (p. 352) [Offset: 0x800] */
    oC_Register_t       DCGCTIMER;                      /* Clock Gating Control 16/32-Bit General-Purpose Timer Deep-Sleep Mode (p. 353) [Offset: 0x804] */
    oC_Register_t       DCGCGPIO;                       /* Gating Control General-Purpose Input/Output Deep-Sleep Mode Clock (p. 355) [Offset: 0x808] */
    oC_Register_t       DCGCDMA;                        /* Gating Control Micro Direct Memory Access Deep-Sleep Mode Clock (p. 357) [Offset: 0x80C] */
oC_REG_RESERVED(30);
    oC_Register_t       DCGCHIB;                        /* Hibernation Deep-Sleep Mode Clock Gating Control (p. 358) [Offset: 0x814] */
    oC_Register_t       DCGCUART;                       /* Deep-Sleep Mode Clock Gating Control Universal Asynchronous Receiver/Transmitter (p. 359) [Offset: 0x818] */
    oC_Register_t       DCGCSSI;                        /* Gating Control Synchronous Serial Interface Deep-Sleep Mode Clock (p. 361) [Offset: 0x81C] */
    oC_Register_t       DCGCI2C;                        /* Control Inter-Integrated Circuit Deep-Sleep Mode Clock Gating (p. 363) [Offset: 0x820] */
oC_REG_RESERVED(31);
    oC_Register_t       DCGCUSB;                        /* Control Universal Serial Bus Deep-Sleep Mode Clock Gating (p. 365) [Offset: 0x828] */
oC_REG_RESERVED_ARRAY(32 , 0x828U , 0x834U);
    oC_Register_t       DCGCCAN;                        /* Control Controller Area Network Deep-SleepMode Clock Gating (p. 366) [Offset: 0x834] */
    oC_Register_t       DCGCADC;                        /* Gating Control Analog-to-Digital Converter Deep-Sleep Mode Clock (p. 367) [Offset: 0x838] */
    oC_Register_t       DCGCACMP;                       /* Control Analog Comparator Deep-Sleep Mode Clock Gating (p. 368) [Offset: 0x83C] */
oC_REG_RESERVED_ARRAY(33 , 0x83cU , 0x858U);
    oC_Register_t       DCGCEEPROM;                     /* EEPROM Deep-Sleep Mode Clock Gating Control (p. 369) [Offset: 0x858] */
    oC_Register_t       DCGCWTIMER;                     /* Clock Gating Control 32/64-BitWideGeneral-Purpose Timer Deep-SleepMode (p. 370) [Offset: 0x85C] */
oC_REG_RESERVED_ARRAY(34 , 0x85cU , 0xa00U);
    oC_Register_t       PRWD;                           /* Watchdog Timer Peripheral Ready (p. 372) [Offset: 0xA00] */
    oC_Register_t       PRTIMER;                        /* 16/32-Bit General-Purpose Timer Peripheral Ready (p. 373) [Offset: 0xA04] */
    oC_Register_t       PRGPIO;                         /* General-Purpose Input/Output Peripheral Ready (p. 375) [Offset: 0xA08] */
    oC_Register_t       PRDMA;                          /* Micro Direct Memory Access Peripheral Ready (p. 377) [Offset: 0xA0C] */
oC_REG_RESERVED(35);
    oC_Register_t       PRHIB;                          /* Hibernation Peripheral Ready (p. 378) [Offset: 0xA14] */
    oC_Register_t       PRUART;                         /* Ready Universal Asynchronous Receiver/Transmitter Peripheral (p. 379) [Offset: 0xA18] */
    oC_Register_t       PRSSI;                          /* Synchronous Serial Interface Peripheral Ready (p. 381) [Offset: 0xA1C] */
    oC_Register_t       PRI2C;                          /* Inter-Integrated Circuit Peripheral Ready (p. 383) [Offset: 0xA20] */
oC_REG_RESERVED(36);
    oC_Register_t       PRUSB;                          /* Universal Serial Bus Peripheral Ready (p. 385) [Offset: 0xA28] */
oC_REG_RESERVED_ARRAY(37 , 0xa28U , 0xa34U);
    oC_Register_t       PRCAN;                          /* Controller Area Network Peripheral Ready (p. 386) [Offset: 0xA34] */
    oC_Register_t       PRADC;                          /* Analog-to-Digital Converter Peripheral Ready (p. 387) [Offset: 0xA38] */
    oC_Register_t       PRACMP;                         /* Analog Comparator Peripheral Ready (p. 388) [Offset: 0xA3C] */
oC_REG_RESERVED_ARRAY(38 , 0xa3cU , 0xa58U);
    oC_Register_t       PREEPROM;                       /* EEPROM Peripheral Ready (p. 389) [Offset: 0xA58] */
    oC_Register_t       PRWTIMER;                       /* 32/64-BitWide General-Purpose Timer Peripheral Ready (p. 390) [Offset: 0xA5C] */

} oC_REG_SysCtrl_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_SysCtrl_RMAP                         oC_CREATE_REGISTER_MAP_CAST( oC_REG_SysCtrl_RegisterMap_t , oC_REG_SysCtrl_BASE)

/*
 *  Bits definitions for SysCtrl
 */
#define oC_REG_SysCtrl_RCC_ACG                      oC_BIT_27
#define oC_REG_SysCtrl_RCC_SYSDIV                   oC_BITS(23 , 26)
#define oC_REG_SysCtrl_RCC_USESYSDIV                oC_BIT_22
#define oC_REG_SysCtrl_RCC_PWRDN                    oC_BIT_13
#define oC_REG_SysCtrl_RCC_BYPASS                   oC_BIT_11
#define oC_REG_SysCtrl_RCC_XTAL                     oC_BITS(6 , 10)
#define oC_REG_SysCtrl_RCC_OSCSRC                   oC_BITS(4 , 5)
#define oC_REG_SysCtrl_RCC_MOSCDIS                  oC_BIT_0

#define oC_REG_SysCtrl_RCC2_USERCC2                 oC_BIT_31
#define oC_REG_SysCtrl_RCC2_DIV400                  oC_BIT_30
#define oC_REG_SysCtrl_RCC2_SYSDIV2                 oC_BITS(23 , 28)
#define oC_REG_SysCtrl_RCC2_SYSDIV2LSB              oC_BIT_22
#define oC_REG_SysCtrl_RCC2_USBPWRDN                oC_BIT_14
#define oC_REG_SysCtrl_RCC2_PWRDN2                  oC_BIT_13
#define oC_REG_SysCtrl_RCC2_BYPASS2                 oC_BIT_11
#define oC_REG_SysCtrl_RCC2_OSCSRC2                 oC_BITS(4 , 6)

#define oC_REG_SysCtrl_RIS_BOR0RIS                  oC_BIT_11
#define oC_REG_SysCtrl_RIS_VDDARIS                  oC_BIT_10
#define oC_REG_SysCtrl_RIS_MOSCPUPRIS               oC_BIT_8
#define oC_REG_SysCtrl_RIS_USBPLLLRIS               oC_BIT_7
#define oC_REG_SysCtrl_RIS_PLLLRIS                  oC_BIT_6
#define oC_REG_SysCtrl_RIS_MOFRIS                   oC_BIT_3
#define oC_REG_SysCtrl_RIS_BOR1RIS                  oC_BIT_1


/*
 *  Values For SysCtrl
 */
#define oC_REG_SysCtrl_RCC_OSCSRC_MainOscillator                    ( 0 )
#define oC_REG_SysCtrl_RCC_OSCSRC_PrecisionInternalOscillator       ( 0x1 << 4 )
#define oC_REG_SysCtrl_RCC_OSCSRC_PrecisionInternalOscillator4      ( 0x2 << 4 )
#define oC_REG_SysCtrl_RCC_OSCSRC_LowFrequencyInternalOscillator    ( 0x3 << 4 )

#define oC_REG_SysCtrl_RCC_XTAL_4MHz                                ( 0x06U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_4_096kHz                            ( 0x07U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_4_915_200Hz                         ( 0x08U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_5MHz                                ( 0x09U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_5_120kHz                            ( 0x0AU << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_6MHz                                ( 0x0BU << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_6_144kHz                            ( 0x0CU << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_7_372_800Hz                         ( 0x0DU << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_8MHz                                ( 0x0EU << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_8_192kHz                            ( 0x0FU << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_10MHz                               ( 0x10U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_12MHz                               ( 0x11U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_12_288kHz                           ( 0x12U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_13_560kHz                           ( 0x13U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_14_318_180Hz                        ( 0x14U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_16MHz                               ( 0x15U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_16_384kHz                           ( 0x16U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_18MHz                               ( 0x17U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_20MHz                               ( 0x18U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_24MHz                               ( 0x19U << 6 )
#define oC_REG_SysCtrl_RCC_XTAL_25MHz                               ( 0x1AU << 6 )

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  System Exception Module (SysExc) (p. 448)
//

/*
 *  BASES
 */
#define oC_REG_SysExc_BASE                          ( 0x400F9000UL )    /**< Base of the Register Map */

/**
 *  Register map for SysCtrl (p. 448)
 */
typedef struct
{
    oC_Register_t       RIS;                            /**< Raw Interrupt Status (p. 449) */
    oC_Register_t       IM;                             /**< Interrupt Mask (p. 451) */
    oC_Register_t       MIS;                            /**< Masked Interrupt Status (p. 453) */
    oC_Register_t       CIC;                            /**< Interrupt Clear (p. 455) */
} oC_REG_SysExc_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_SysExc_RMAP                          oC_CREATE_REGISTER_MAP_CAST( oC_REG_SysExc_RegisterMap_t , oC_REG_SysExc_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Hibernation (HIB) (p. 448)
//

/*
 *  BASES
 */
#define oC_REG_HIB_BASE                             ( 0x400FC000UL )    /**< Base of the Register Map */

/**
 *  Register map for SysCtrl (p. 468)
 */
typedef struct
{
    oC_Register_t       RTCC;                           /**< RTC Counter (p. 470) */
    oC_Register_t       RTCM0;                          /**< RTC Match 0 (p. 471) */
    oC_REG_RESERVED(0);
    oC_Register_t       RTCLD;                          /**< RTC Load (p. 472) */
    oC_Register_t       CTL;                            /**< Control (p. 473) */
    oC_Register_t       IM;                             /**< Interrupt Mask (p. 477) */
    oC_Register_t       RIS;                            /**< Raw Interrupt Status (p. 479) */
    oC_Register_t       MIS;                            /**< Masked Interrupt Status (p. 481) */
    oC_Register_t       IC;                             /**< Interrupt Clear (p. 483) */
    oC_Register_t       RTCT;                           /**< RTC Trim (p. 484) */
    oC_Register_t       RTCSS;                          /**< RTC Sub Seconds (p. 485) */
    oC_Register_t       DATA[oC_REG_COUNT_ARRAY_SIZE(0x030U, 0x06FU)];
} oC_REG_HIB_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_HIB_RMAP                             oC_CREATE_REGISTER_MAP_CAST( oC_REG_HIB_RegisterMap_t , oC_REG_HIB_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Flash (FLASH) (p. 502)
//

/*
 *  BASES
 */
#define oC_REG_FLASH_BASE                           ( 0x400FD000UL )    /**< Base of the Register Map */

/**
 *  Register map for FLASH (p. 502)
 */
typedef struct
{
    oC_Register_t       MA;                             /**< Memory Address (p. 504) */
    oC_Register_t       MD;                             /**< Memory Data (p. 505) */
    oC_Register_t       MC;                             /**< Memory Control (p. 506) */
    oC_Register_t       CRIS;                           /**< Controller Raw Interrupt Status (p. 508) */
    oC_Register_t       CIM;                            /**< Controller Interrupt Mask (p. 511) */
    oC_Register_t       CMISC;                          /**< Controller Masked Interrupt Status and Clear (p. 513) */
    oC_REG_RESERVED_ARRAY(0 , 0x014U , 0x020U);
    oC_Register_t       MC2;                            /**< Memory Control (p. 516) */
    oC_REG_RESERVED_ARRAY(1 , 0x020U , 0x030U);
    oC_Register_t       WBVAL;                          /**< Flash Write Buffer Valid (p. 517) */
    oC_REG_RESERVED_ARRAY(2 , 0x030U , 0x100U);
    oC_Register_t       WBn[oC_REG_COUNT_ARRAY_SIZE(0x100U, 0x17CU)];
    oC_REG_RESERVED_ARRAY(3 , 0x17CU , 0xFC0U);
    oC_Register_t       FSIZE;                          /**< Flash size (p. 519) */
    oC_Register_t       SSIZE;                          /**< SRAM size (p. 520) */
    oC_Register_t       ROMSWMAP;                       /**< ROM Software Map (p. 521) */
} oC_REG_FLASH_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_FLASH_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_FLASH_RegisterMap_t , oC_REG_FLASH_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  EEPROM (EEPROM) (p. 502)
//

/*
 *  BASES
 */
#define oC_REG_EEPROM_BASE                          ( 0x400AF000UL )    /**< Base of the Register Map */

/**
 *  Register map for EEPROM (p. 502)
 */
typedef struct
{
    oC_Register_t       SIZE;                           /**< EEPROM Size Information (p. 522) */
    oC_Register_t       BLOCK;                          /**< EEPROM Current Block (p. 523) */
    oC_Register_t       OFFSET;                         /**< EEPROM Current Offset (p. 524) */
    oC_Register_t       RDWR;                           /**< EEPROM Read-Write (p. 525) */
    oC_Register_t       RDWRINC;                        /**< EEPROM Read-Write with Increment (p. 526) */
    oC_Register_t       DONE;                           /**< EEPROM Done Status (p. 527) */
    oC_Register_t       SUPP;                           /**< EEPROM Support Control and Status (p. 529) */
    oC_Register_t       UNLOCK;                         /**< EEPROM Unlock (p. 531) */
    oC_REG_RESERVED_ARRAY(0 , 0x020U , 0x030U);
    oC_Register_t       PROT;                           /**< EEPROM Protection (p. 532) */
    oC_Register_t       PASS0;                          /**< EEPROM Password (p. 534) */
    oC_Register_t       PASS1;                          /**< EEPROM Password (p. 534) */
    oC_Register_t       PASS2;                          /**< EEPROM Password (p. 534) */
    oC_Register_t       PASS3;                          /**< EEPROM Password (p. 534) */
    oC_Register_t       INT;                            /**< EEPROM Interrupt (p. 535) */
    oC_REG_RESERVED_ARRAY(1 , 0x040U , 0x050U);
    oC_Register_t       HIDE;                           /**< EEPROM Block hide (p. 536) */
    oC_REG_RESERVED_ARRAY(2 , 0x050U , 0x080U);
    oC_Register_t       DBGME1;                          /**< EEPROM Debug Mass Erase (p. 537) */
    oC_REG_RESERVED_ARRAY(3 , 0x080U , 0xFC0U);
    oC_Register_t       DBGME2;                          /**< EEPROM Peripheral Properties (p. 538) */
} oC_REG_EEPROM_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_EEPROM_RMAP                          oC_CREATE_REGISTER_MAP_CAST( oC_REG_EEPROM_RegisterMap_t , oC_REG_EEPROM_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  uDMA (uDMA) (p. 568)
//

/*
 *  BASES
 */
#define oC_REG_uDMA_BASE                            ( 0x400FF000UL )    /**< Base of the Register Map */

/**
 * Register Map for uDMA Channel Control Structure (p. 569)
 */
typedef struct
{
    oC_Register_t       SRCENDP;                        /**< uDMA Channel Source Address End Pointer (p. 571) */
    oC_Register_t       DSTENDP;                        /**< uDMA Channel Destination Address End Pointer (p. 572) */
    oC_Register_t       CHCTL;                          /**< uDMA Channel Control Word (p. 573) */
} oC_REG_uDMACCS_RegisterMap_t;

/**
 * Register map for uDMA (p. 569)
 */
typedef struct
{
    oC_Register_t       STAT;                           /**< DMA Status (p. 578) [Offset: 0x000] */
    oC_Register_t       CFG;                            /**< DMA Configuration (p. 580) [Offset: 0x004] */
    oC_Register_t       CTLBASE;                        /**< DMA Channel Control Base Pointer (p. 581) [Offset: 0x008] */
    oC_Register_t       ALTBASE;                        /**< DMA Alternate Channel Control Base Pointer (p. 582) [Offset: 0x00C] */
    oC_Register_t       WAITSTAT;                       /**< DMA Channel Wait-on-Request Status (p. 583) [Offset: 0x010] */
    oC_Register_t       SWREQ;                          /**< DMA Channel Software Request (p. 584) [Offset: 0x014] */
    oC_Register_t       USEBURSTSET;                    /**< DMA Channel Useburst Set (p. 585) [Offset: 0x018] */
    oC_Register_t       USEBURSTCLR;                    /**< DMA Channel Useburst Clear (p. 586) [Offset: 0x01C] */
    oC_Register_t       REQMASKSET;                     /**< DMA Channel Request Mask Set (p. 587) [Offset: 0x020] */
    oC_Register_t       REQMASKCLR;                     /**< DMA Channel Request Mask Clear (p. 588) [Offset: 0x024] */
    oC_Register_t       ENASET;                         /**< DMA Channel Enable Set (p. 589) [Offset: 0x028] */
    oC_Register_t       ENACLR;                         /**< DMA Channel Enable Clear (p. 590) [Offset: 0x02C] */
    oC_Register_t       ALTSET;                         /**< DMA Channel Primary Alternate Set (p. 591) [Offset: 0x030] */
    oC_Register_t       ALTCLR;                         /**< DMA Channel Primary Alternate Clear (p. 592) [Offset: 0x034] */
    oC_Register_t       PRIOSET;                        /**< DMA Channel Priority Set (p. 593) [Offset: 0x038] */
    oC_Register_t       PRIOCLR;                        /**< DMA Channel Priority Clear (p. 594) [Offset: 0x03C] */
oC_REG_RESERVED_ARRAY(0 , 0x3cU , 0x4cU);
    oC_Register_t       ERRCLR;                         /**< DMA Bus Error Clear (p. 595) [Offset: 0x04C] */
oC_REG_RESERVED_ARRAY(1 , 0x4cU , 0x500U);
    oC_Register_t       CHASGN;                         /**< DMA Channel Assignment (p. 596) [Offset: 0x500] */
    oC_Register_t       CHIS;                           /**< DMA Channel Interrupt Status (p. 597) [Offset: 0x504] */
oC_REG_RESERVED_ARRAY(2 , 0x504U , 0x510U);
    oC_Register_t       CHMAP0;                         /**< DMA Channel Map Select 0 (p. 598) [Offset: 0x510] */
    oC_Register_t       CHMAP1;                         /**< DMA Channel Map Select 1 (p. 599) [Offset: 0x514] */
    oC_Register_t       CHMAP2;                         /**< DMA Channel Map Select 2 (p. 600) [Offset: 0x518] */
    oC_Register_t       CHMAP3;                         /**< DMA Channel Map Select 3 (p. 601) [Offset: 0x51C] */
oC_REG_RESERVED_ARRAY(3 , 0x51cU , 0xfd0U);
    oC_Register_t       DMAPeriphID4;                   /**< DMA Peripheral Identification 4 (p. 606) [Offset: 0xFD0] */
oC_REG_RESERVED_ARRAY(4 , 0xfd0U , 0xfe0U);
    oC_Register_t       PeriphID0;                      /**< DMA Peripheral Identification 0 (p. 602) [Offset: 0xFE0] */
    oC_Register_t       PeriphID1;                      /**< DMA Peripheral Identification 1 (p. 603) [Offset: 0xFE4] */
    oC_Register_t       PeriphID2;                      /**< DMA Peripheral Identification 2 (p. 604) [Offset: 0xFE8] */
    oC_Register_t       PeriphID3;                      /**< DMA Peripheral Identification 3 (p. 605) [Offset: 0xFEC] */
    oC_Register_t       PCellID0;                       /**< DMA PrimeCell Identification 0 (p. 607) [Offset: 0xFF0] */
    oC_Register_t       PCellID1;                       /**< DMA PrimeCell Identification 1 (p. 608) [Offset: 0xFF4] */
    oC_Register_t       PCellID2;                       /**< DMA PrimeCell Identification 2 (p. 609) [Offset: 0xFF8] */
    oC_Register_t       PCellID3;                       /**< DMA PrimeCell Identification 3 (p. 610) [Offset: 0xFFC] */
} oC_REG_uDMA_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_uDMA_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_uDMA_RegisterMap_t , oC_REG_uDMA_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  GPIO (GPIO) (p. 620)
//

/*
 *  BASES
 */
#define oC_REG_GPIOAAPB_BASE                        ( 0x40004000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOBAPB_BASE                        ( 0x40005000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOCAPB_BASE                        ( 0x40006000UL )    /**< Base of the Register Map */
#define oC_REG_GPIODAPB_BASE                        ( 0x40007000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOEAPB_BASE                        ( 0x40024000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOFAPB_BASE                        ( 0x40025000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOAAHB_BASE                        ( 0x40058000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOBAHB_BASE                        ( 0x40059000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOCAHB_BASE                        ( 0x4005A000UL )    /**< Base of the Register Map */
#define oC_REG_GPIODAHB_BASE                        ( 0x4005B000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOEAHB_BASE                        ( 0x4005C000UL )    /**< Base of the Register Map */
#define oC_REG_GPIOFAHB_BASE                        ( 0x4005D000UL )    /**< Base of the Register Map */

/**
 * Register map for GPIO (p. 621)
 */
typedef struct
{
oC_REG_RESERVED_ARRAY(0 , 0x0U , 0x400U);
    oC_Register_t       DATA;                           /**< GPIO Data (p. 623) [Offset: 0x000] */
    oC_Register_t       DIR;                            /**< GPIO Direction (p. 624) [Offset: 0x400] */
    oC_Register_t       IS;                             /**< GPIO Interrupt Sense (p. 625) [Offset: 0x404] */
    oC_Register_t       IBE;                            /**< GPIO Interrupt Both Edges (p. 626) [Offset: 0x408] */
    oC_Register_t       IEV;                            /**< GPIO Interrupt Event (p. 627) [Offset: 0x40C] */
    oC_Register_t       IM;                             /**< GPIO Interrupt Mask (p. 628) [Offset: 0x410] */
    oC_Register_t       RIS;                            /**< GPIO Raw Interrupt Status (p. 629) [Offset: 0x414] */
    oC_Register_t       MIS;                            /**< GPIO Masked Interrupt Status (p. 630) [Offset: 0x418] */
    oC_Register_t       ICR;                            /**< GPIO Interrupt Clear (p. 631) [Offset: 0x41C] */
    oC_Register_t       AFSEL;                          /**< GPIO Alternate Function Select (p. 632) [Offset: 0x420] */
oC_REG_RESERVED_ARRAY(1 , 0x420U , 0x500U);
    oC_Register_t       DR2R;                           /**< GPIO 2-mA Drive Select (p. 634) [Offset: 0x500] */
    oC_Register_t       DR4R;                           /**< GPIO 4-mA Drive Select (p. 635) [Offset: 0x504] */
    oC_Register_t       DR8R;                           /**< GPIO 8-mA Drive Select (p. 636) [Offset: 0x508] */
    oC_Register_t       ODR;                            /**< GPIO Open Drain Select (p. 637) [Offset: 0x50C] */
    oC_Register_t       PUR;                            /**< GPIO Pull-Up Select (p. 638) [Offset: 0x510] */
    oC_Register_t       PDR;                            /**< GPIO Pull-Down Select (p. 640) [Offset: 0x514] */
    oC_Register_t       SLR;                            /**< GPIO Slew Rate Control Select (p. 642) [Offset: 0x518] */
    oC_Register_t       DEN;                            /**< GPIO Digital Enable (p. 643) [Offset: 0x51C] */
    oC_Register_t       LOCK;                           /**< GPIO Lock (p. 645) [Offset: 0x520] */
    oC_Register_t       CR;                             /**< GPIO Commit (p. 646) [Offset: 0x524] */
    oC_Register_t       AMSEL;                          /**< GPIO Analog Mode Select (p. 648) [Offset: 0x528] */
    oC_Register_t       PCTL;                           /**< GPIO Port Control (p. 649) [Offset: 0x52C] */
    oC_Register_t       ADCCTL;                         /**< GPIO ADC Control (p. 651) [Offset: 0x530] */
    oC_Register_t       DMACTL;                         /**< GPIO DMA Control (p. 652) [Offset: 0x534] */
oC_REG_RESERVED_ARRAY(2 , 0x534U , 0xfd0U);
    oC_Register_t       PeriphID4;                      /**< GPIO Peripheral Identification 4 (p. 653) [Offset: 0xFD0] */
    oC_Register_t       PeriphID5;                      /**< GPIO Peripheral Identification 5 (p. 654) [Offset: 0xFD4] */
    oC_Register_t       PeriphID6;                      /**< GPIO Peripheral Identification 6 (p. 655) [Offset: 0xFD8] */
    oC_Register_t       PeriphID7;                      /**< GPIO Peripheral Identification 7 (p. 656) [Offset: 0xFDC] */
    oC_Register_t       PeriphID0;                      /**< GPIO Peripheral Identification 0 (p. 657) [Offset: 0xFE0] */
    oC_Register_t       PeriphID1;                      /**< GPIO Peripheral Identification 1 (p. 658) [Offset: 0xFE4] */
    oC_Register_t       PeriphID2;                      /**< GPIO Peripheral Identification 2 (p. 659) [Offset: 0xFE8] */
    oC_Register_t       PeriphID3;                      /**< GPIO Peripheral Identification 3 (p. 660) [Offset: 0xFEC] */
    oC_Register_t       PCellID0;                       /**< GPIO PrimeCell Identification 0 (p. 661) [Offset: 0xFF0] */
    oC_Register_t       PCellID1;                       /**< GPIO PrimeCell Identification 1 (p. 662) [Offset: 0xFF4] */
    oC_Register_t       PCellID2;                       /**< GPIO PrimeCell Identification 2 (p. 663) [Offset: 0xFF8] */
    oC_Register_t       PCellID3;                       /**< GPIO PrimeCell Identification 3 (p. 664) [Offset: 0xFFC] */
} oC_REG_GPIO_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_GPIOAAPB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOAAPB_BASE)
#define oC_REG_GPIOBAPB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOBAPB_BASE)
#define oC_REG_GPIOCAPB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOCAPB_BASE)
#define oC_REG_GPIODAPB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIODAPB_BASE)
#define oC_REG_GPIOEAPB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOEAPB_BASE)
#define oC_REG_GPIOFAPB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOFAPB_BASE)
#define oC_REG_GPIOAAHB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOAAHB_BASE)
#define oC_REG_GPIOBAHB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOBAHB_BASE)
#define oC_REG_GPIOCAHB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOCAHB_BASE)
#define oC_REG_GPIODAHB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIODAHB_BASE)
#define oC_REG_GPIOEAHB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOEAHB_BASE)
#define oC_REG_GPIOFAHB_RMAP                        oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPIO_RegisterMap_t , oC_REG_GPIOFAHB_BASE)

/*
 * Additional definitions for GPIO
 */
#define oC_REG_GPIO_LOCK_KEY                       (0x4C4F434BUL)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  General-Purpose Timers (GPTM) (p. 687)
//

/*
 *  BASES
 */
#define oC_REG_GPTM0_BASE                           ( 0x40030000UL )    /**< Base of the Register Map */
#define oC_REG_GPTM1_BASE                           ( 0x40031000UL )    /**< Base of the Register Map */
#define oC_REG_GPTM2_BASE                           ( 0x40032000UL )    /**< Base of the Register Map */
#define oC_REG_GPTM3_BASE                           ( 0x40033000UL )    /**< Base of the Register Map */
#define oC_REG_GPTM4_BASE                           ( 0x40034000UL )    /**< Base of the Register Map */
#define oC_REG_GPTM5_BASE                           ( 0x40035000UL )    /**< Base of the Register Map */
#define oC_REG_GPTMW0_BASE                          ( 0x40036000UL )    /**< Base of the Register Map */
#define oC_REG_GPTMW1_BASE                          ( 0x40037000UL )    /**< Base of the Register Map */
#define oC_REG_GPTMW2_BASE                          ( 0x4004C000UL )    /**< Base of the Register Map */
#define oC_REG_GPTMW3_BASE                          ( 0x4004D000UL )    /**< Base of the Register Map */
#define oC_REG_GPTMW4_BASE                          ( 0x4004E000UL )    /**< Base of the Register Map */
#define oC_REG_GPTMW5_BASE                          ( 0x4004F000UL )    /**< Base of the Register Map */

/**
 * Register map for GPIO (p. 686)
 */
typedef struct
{
    oC_Register_t       CFG;                            /**< GPTM Configuration (p. 688) [Offset: 0x000] */
    oC_Register_t       TAMR;                           /**< GPTM Timer A Mode (p. 690) [Offset: 0x004] */
    oC_Register_t       TBMR;                           /**< GPTM Timer B Mode (p. 694) [Offset: 0x008] */
    oC_Register_t       CTL;                            /**< GPTM Control (p. 698) [Offset: 0x00C] */
    oC_Register_t       SYNC;                           /**< GPTM Synchronize (p. 702) [Offset: 0x010] */
oC_REG_RESERVED(0);
    oC_Register_t       IMR;                            /**< GPTM Interrupt Mask (p. 706) [Offset: 0x018] */
    oC_Register_t       RIS;                            /**< GPTM Raw Interrupt Status (p. 709) [Offset: 0x01C] */
    oC_Register_t       MIS;                            /**< GPTM Masked Interrupt Status (p. 712) [Offset: 0x020] */
    oC_Register_t       ICR;                            /**< GPTM Interrupt Clear (p. 715) [Offset: 0x024] */
    oC_Register_t       TAILR;                          /**< GPTM Timer A Interval Load (p. 717) [Offset: 0x028] */
    oC_Register_t       TBILR;                          /**< GPTM Timer B Interval Load (p. 718) [Offset: 0x02C] */
    oC_Register_t       TAMATCHR;                       /**< GPTM Timer A Match (p. 719) [Offset: 0x030] */
    oC_Register_t       TBMATCHR;                       /**< GPTM Timer B Match (p. 720) [Offset: 0x034] */
    oC_Register_t       TAPR;                           /**< GPTM Timer A Prescale (p. 721) [Offset: 0x038] */
    oC_Register_t       TBPR;                           /**< GPTM Timer B Prescale (p. 722) [Offset: 0x03C] */
    oC_Register_t       TAPMR;                          /**< GPTM TimerA Prescale Match (p. 723) [Offset: 0x040] */
    oC_Register_t       TBPMR;                          /**< GPTM TimerB Prescale Match (p. 724) [Offset: 0x044] */
    oC_Register_t       TAR;                            /**< GPTM Timer A (p. 725) [Offset: 0x048] */
    oC_Register_t       TBR;                            /**< GPTM Timer B (p. 726) [Offset: 0x04C] */
    oC_Register_t       TAV;                            /**< GPTM Timer A Value (p. 727) [Offset: 0x050] */
    oC_Register_t       TBV;                            /**< GPTM Timer B Value (p. 728) [Offset: 0x054] */
    oC_Register_t       RTCPD;                          /**< GPTM RTC Predivide (p. 729) [Offset: 0x058] */
    oC_Register_t       TAPS;                           /**< GPTM Timer A Prescale Snapshot (p. 730) [Offset: 0x05C] */
    oC_Register_t       TBPS;                           /**< GPTM Timer B Prescale Snapshot (p. 731) [Offset: 0x060] */
    oC_Register_t       TAPV;                           /**< GPTM Timer A Prescale Value (p. 732) [Offset: 0x064] */
    oC_Register_t       TBPV;                           /**< GPTM Timer B Prescale Value (p. 733) [Offset: 0x068] */
oC_REG_RESERVED_ARRAY(1 , 0x68U , 0xfc0U);
    oC_Register_t       PP;                             /**< GPTM Peripheral Properties (p. 734) [Offset: 0xFC0] */
} oC_REG_GPTM_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_Timer0_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTM0_BASE)
#define oC_REG_Timer1_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTM1_BASE)
#define oC_REG_Timer2_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTM2_BASE)
#define oC_REG_Timer3_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTM3_BASE)
#define oC_REG_Timer4_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTM4_BASE)
#define oC_REG_Timer5_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTM5_BASE)
#define oC_REG_WideTimer0_RMAP                          oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTMW0_BASE)
#define oC_REG_WideTimer1_RMAP                          oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTMW1_BASE)
#define oC_REG_WideTimer2_RMAP                          oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTMW2_BASE)
#define oC_REG_WideTimer3_RMAP                          oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTMW3_BASE)
#define oC_REG_WideTimer4_RMAP                          oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTMW4_BASE)
#define oC_REG_WideTimer5_RMAP                          oC_CREATE_REGISTER_MAP_CAST( oC_REG_GPTM_RegisterMap_t , oC_REG_GPTMW5_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Watchdog Timers (WDG) (p. 687)
//

/*
 *  BASES
 */
#define oC_REG_WDG0_BASE                            ( 0x40000000UL )    /**< Base of the Register Map */
#define oC_REG_WDG1_BASE                            ( 0x40001000UL )    /**< Base of the Register Map */

/**
 * Register Map for Watchdog (p. 738)
 */
typedef struct
{
    oC_Register_t       LOAD;                           /**< Watchdog Load (p. 739) [Offset: 0x000] */
    oC_Register_t       VALUE;                          /**< Watchdog Value (p. 740) [Offset: 0x004] */
    oC_Register_t       CTL;                            /**< WDTICR WO - Watchdog Interrupt Clear (p. 743) [Offset: 0x008] */
oC_REG_RESERVED(0);
    oC_Register_t       RIS;                            /**< Watchdog Raw Interrupt Status (p. 744) [Offset: 0x010] */
    oC_Register_t       MIS;                            /**< Watchdog Masked Interrupt Status (p. 745) [Offset: 0x014] */
oC_REG_RESERVED_ARRAY(1 , 0x14U , 0x418U);
    oC_Register_t       TEST;                           /**< Watchdog Test (p. 746) [Offset: 0x418] */
oC_REG_RESERVED_ARRAY(2 , 0x418U , 0xc00U);
    oC_Register_t       LOCK;                           /**< Watchdog Lock (p. 747) [Offset: 0xC00] */
oC_REG_RESERVED_ARRAY(3 , 0xc00U , 0xfd0U);
    oC_Register_t       PeriphID4;                      /**< Watchdog Peripheral Identification 4 (p. 748) [Offset: 0xFD0] */
    oC_Register_t       PeriphID5;                      /**< Watchdog Peripheral Identification 5 (p. 749) [Offset: 0xFD4] */
    oC_Register_t       PeriphID6;                      /**< Watchdog Peripheral Identification 6 (p. 750) [Offset: 0xFD8] */
    oC_Register_t       PeriphID7;                      /**< Watchdog Peripheral Identification 7 (p. 751) [Offset: 0xFDC] */
    oC_Register_t       PeriphID0;                      /**< Watchdog Peripheral Identification 0 (p. 752) [Offset: 0xFE0] */
    oC_Register_t       PeriphID1;                      /**< Watchdog Peripheral Identification 1 (p. 753) [Offset: 0xFE4] */
    oC_Register_t       PeriphID2;                      /**< Watchdog Peripheral Identification 2 (p. 754) [Offset: 0xFE8] */
    oC_Register_t       PeriphID3;                      /**< Watchdog Peripheral Identification 3 (p. 755) [Offset: 0xFEC] */
    oC_Register_t       PCellID0;                       /**< Watchdog PrimeCell Identification 0 (p. 756) [Offset: 0xFF0] */
    oC_Register_t       PCellID1;                       /**< Watchdog PrimeCell Identification 1 (p. 757) [Offset: 0xFF4] */
    oC_Register_t       PCellID2;                       /**< Watchdog PrimeCell Identification 2 (p. 758) [Offset: 0xFF8] */
    oC_Register_t       PCellID3;                       /**< Watchdog PrimeCell Identification 3 (p. 759) [Offset: 0xFFC] */
} oC_REG_WDG_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_WDG0_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_WDG_RegisterMap_t , oC_REG_WDG0_BASE)
#define oC_REG_WDG1_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_WDG_RegisterMap_t , oC_REG_WDG1_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Analog to Digital Converter (ADC) (p. 779)
//

/*
 *  BASES
 */
#define oC_REG_ADC0_BASE                            ( 0x40038000UL )    /**< Base of the Register Map */
#define oC_REG_ADC1_BASE                            ( 0x40039000UL )    /**< Base of the Register Map */

/**
 * Register Map for ADC (p. 779)
 */
typedef struct
{
    oC_Register_t       ACTSS;                          /**< ADC Active Sample Sequencer (p. 782) [Offset: 0x000] */
    oC_Register_t       RIS;                            /**< ADC Raw Interrupt Status (p. 784) [Offset: 0x004] */
    oC_Register_t       IM;                             /**< ADC Interrupt Mask (p. 786) [Offset: 0x008] */
    oC_Register_t       ISC;                            /**< ADC Interrupt Status and Clear (p. 789) [Offset: 0x00C] */
    oC_Register_t       OSTAT;                          /**< ADC Overflow Status (p. 792) [Offset: 0x010] */
    oC_Register_t       EMUX;                           /**< ADC Event Multiplexer Select (p. 794) [Offset: 0x014] */
    oC_Register_t       USTAT;                          /**< ADC Underflow Status (p. 799) [Offset: 0x018] */
oC_REG_RESERVED(0);
    oC_Register_t       SSPRI;                          /**< ADC Sample Sequencer Priority (p. 800) [Offset: 0x020] */
    oC_Register_t       SPC;                            /**< ADC Sample Phase Control (p. 802) [Offset: 0x024] */
    oC_Register_t       PSSI;                           /**< ADC Processor Sample Sequence Initiate (p. 804) [Offset: 0x028] */
oC_REG_RESERVED(1);
    oC_Register_t       SAC;                            /**< ADC Sample Averaging Control (p. 806) [Offset: 0x030] */
    oC_Register_t       DCISC;                          /**< ADC Digital Comparator Interrupt Status and Clear (p. 807) [Offset: 0x034] */
    oC_Register_t       CTL;                            /**< ADC Control (p. 809) [Offset: 0x038] */
oC_REG_RESERVED(2);
    oC_Register_t       SSMUX0;                         /**< ADC Sample Sequence Input Multiplexer Select 0 (p. 810) [Offset: 0x040] */
    oC_Register_t       SSCTL0;                         /**< ADC Sample Sequence Control 0 (p. 812) [Offset: 0x044] */
    oC_Register_t       SSFIFO0;                        /**< ADC Sample Sequence Result FIFO 0 (p. 819) [Offset: 0x048] */
    oC_Register_t       SSFSTAT0;                       /**< ADC Sample Sequence FIFO 0 Status (p. 820) [Offset: 0x04C] */
    oC_Register_t       SSOP0;                          /**< ADC Sample Sequence 0 Operation (p. 822) [Offset: 0x050] */
    oC_Register_t       SSDC0;                          /**< ADC Sample Sequence 0 Digital Comparator Select (p. 824) [Offset: 0x054] */
oC_REG_RESERVED_ARRAY(3 , 0x54U , 0x60U);
    oC_Register_t       SSMUX1;                         /**< ADC Sample Sequence Input Multiplexer Select 1 (p. 826) [Offset: 0x060] */
    oC_Register_t       SSCTL1;                         /**< ADC Sample Sequence Control 1 (p. 827) [Offset: 0x064] */
    oC_Register_t       SSFIFO1;                        /**< ADC Sample Sequence Result FIFO 1 (p. 819) [Offset: 0x068] */
    oC_Register_t       SSFSTAT1;                       /**< ADC Sample Sequence FIFO 1 Status (p. 820) [Offset: 0x06C] */
    oC_Register_t       SSOP1;                          /**< ADC Sample Sequence 1 Operation (p. 831) [Offset: 0x070] */
    oC_Register_t       SSDC1;                          /**< ADC Sample Sequence 1 Digital Comparator Select (p. 832) [Offset: 0x074] */
oC_REG_RESERVED_ARRAY(4 , 0x74U , 0x80U);
    oC_Register_t       SSMUX2;                         /**< ADC Sample Sequence Input Multiplexer Select 2 (p. 826) [Offset: 0x080] */
    oC_Register_t       SSCTL2;                         /**< ADC Sample Sequence Control 2 (p. 827) [Offset: 0x084] */
    oC_Register_t       SSFIFO2;                        /**< ADC Sample Sequence Result FIFO 2 (p. 819) [Offset: 0x088] */
    oC_Register_t       SSFSTAT2;                       /**< ADC Sample Sequence FIFO 2 Status (p. 820) [Offset: 0x08C] */
    oC_Register_t       SSOP2;                          /**< ADC Sample Sequence 2 Operation (p. 831) [Offset: 0x090] */
    oC_Register_t       SSDC2;                          /**< ADC Sample Sequence 2 Digital Comparator Select (p. 832) [Offset: 0x094] */
oC_REG_RESERVED_ARRAY(5 , 0x94U , 0xa0U);
    oC_Register_t       SSMUX3;                         /**< ADC Sample Sequence Input Multiplexer Select 3 (p. 834) [Offset: 0x0A0] */
    oC_Register_t       SSCTL3;                         /**< ADC Sample Sequence Control 3 (p. 835) [Offset: 0x0A4] */
    oC_Register_t       SSFIFO3;                        /**< ADC Sample Sequence Result FIFO 3 (p. 819) [Offset: 0x0A8] */
    oC_Register_t       SSFSTAT3;                       /**< ADC Sample Sequence FIFO 3 Status (p. 820) [Offset: 0x0AC] */
    oC_Register_t       SSOP3;                          /**< ADC Sample Sequence 3 Operation (p. 837) [Offset: 0x0B0] */
    oC_Register_t       SSDC3;                          /**< ADC Sample Sequence 3 Digital Comparator Select (p. 838) [Offset: 0x0B4] */
oC_REG_RESERVED_ARRAY(6 , 0xb4U , 0xd00U);
    oC_Register_t       DCRIC;                          /**< ADC Digital Comparator Reset Initial Conditions (p. 839) [Offset: 0xD00] */
oC_REG_RESERVED_ARRAY(7 , 0xd00U , 0xe00U);
    oC_Register_t       DCCTL0;                         /**< ADC Digital Comparator Control 0 (p. 844) [Offset: 0xE00] */
    oC_Register_t       DCCTL1;                         /**< ADC Digital Comparator Control 1 (p. 844) [Offset: 0xE04] */
    oC_Register_t       DCCTL2;                         /**< ADC Digital Comparator Control 2 (p. 844) [Offset: 0xE08] */
    oC_Register_t       DCCTL3;                         /**< ADC Digital Comparator Control 3 (p. 844) [Offset: 0xE0C] */
    oC_Register_t       DCCTL4;                         /**< ADC Digital Comparator Control 4 (p. 844) [Offset: 0xE10] */
    oC_Register_t       DCCTL5;                         /**< ADC Digital Comparator Control 5 (p. 844) [Offset: 0xE14] */
    oC_Register_t       DCCTL6;                         /**< ADC Digital Comparator Control 6 (p. 844) [Offset: 0xE18] */
    oC_Register_t       DCCTL7;                         /**< ADC Digital Comparator Control 7 (p. 844) [Offset: 0xE1C] */
oC_REG_RESERVED_ARRAY(8 , 0xe1cU , 0xe40U);
    oC_Register_t       DCCMP0;                         /**< ADC Digital Comparator Range 0 (p. 846) [Offset: 0xE40] */
    oC_Register_t       DCCMP1;                         /**< ADC Digital Comparator Range 1 (p. 846) [Offset: 0xE44] */
    oC_Register_t       DCCMP2;                         /**< ADC Digital Comparator Range 2 (p. 846) [Offset: 0xE48] */
    oC_Register_t       DCCMP3;                         /**< ADC Digital Comparator Range 3 (p. 846) [Offset: 0xE4C] */
    oC_Register_t       DCCMP4;                         /**< ADC Digital Comparator Range 4 (p. 846) [Offset: 0xE50] */
    oC_Register_t       DCCMP5;                         /**< ADC Digital Comparator Range 5 (p. 846) [Offset: 0xE54] */
    oC_Register_t       DCCMP6;                         /**< ADC Digital Comparator Range 6 (p. 846) [Offset: 0xE58] */
    oC_Register_t       DCCMP7;                         /**< ADC Digital Comparator Range 7 (p. 846) [Offset: 0xE5C] */
oC_REG_RESERVED_ARRAY(9 , 0xe5cU , 0xfc0U);
    oC_Register_t       PP;                             /**< ADC Peripheral Properties (p. 847) [Offset: 0xFC0] */
    oC_Register_t       PC;                             /**< ADC Peripheral Configuration (p. 849) [Offset: 0xFC4] */
    oC_Register_t       CC;                             /**< ADC Clock Configuration (p. 850) [Offset: 0xFC8] */
} oC_REG_ADC_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_ADC0_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_ADC_RegisterMap_t , oC_REG_ADC0_BASE)
#define oC_REG_ADC1_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_ADC_RegisterMap_t , oC_REG_ADC1_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Universal Asynchronous Receivers/Transmitters (UART) (p. 861)
//

/*
 *  BASES
 */
#define oC_REG_UART0_BASE                           ( 0x4000C000UL )    /**< Base of the Register Map */
#define oC_REG_UART1_BASE                           ( 0x4000D000UL )    /**< Base of the Register Map */
#define oC_REG_UART2_BASE                           ( 0x4000E000UL )    /**< Base of the Register Map */
#define oC_REG_UART3_BASE                           ( 0x4000F000UL )    /**< Base of the Register Map */
#define oC_REG_UART4_BASE                           ( 0x40010000UL )    /**< Base of the Register Map */
#define oC_REG_UART5_BASE                           ( 0x40011000UL )    /**< Base of the Register Map */
#define oC_REG_UART6_BASE                           ( 0x40012000UL )    /**< Base of the Register Map */
#define oC_REG_UART7_BASE                           ( 0x40013000UL )    /**< Base of the Register Map */

/**
 * Register Map for UART (p. 861)
 */
typedef struct
{
    oC_Register_t       DR;                             /**< UART Data (p. 864) [Offset: 0x000] */
oC_REG_RESERVED_ARRAY(0 , 0x0U , 0x18U);
    oC_Register_t       FR;                             /**< UART Flag (p. 869) [Offset: 0x018] */
oC_REG_RESERVED(1);
    oC_Register_t       ILPR;                           /**< UART IrDA Low-Power Register (p. 871) [Offset: 0x020] */
    oC_Register_t       IBRD;                           /**< UART Integer Baud-Rate Divisor (p. 872) [Offset: 0x024] */
    oC_Register_t       FBRD;                           /**< UART Fractional Baud-Rate Divisor (p. 873) [Offset: 0x028] */
    oC_Register_t       LCRH;                           /**< UART Line Control (p. 874) [Offset: 0x02C] */
    oC_Register_t       CTL;                            /**< UART Control (p. 876) [Offset: 0x030] */
    oC_Register_t       IFLS;                           /**< UART Interrupt FIFO Level Select (p. 880) [Offset: 0x034] */
    oC_Register_t       IM;                             /**< UART Interrupt Mask (p. 882) [Offset: 0x038] */
    oC_Register_t       RIS;                            /**< UART Raw Interrupt Status (p. 885) [Offset: 0x03C] */
    oC_Register_t       MIS;                            /**< UART Masked Interrupt Status (p. 888) [Offset: 0x040] */
    oC_Register_t       ICR;                            /**< UART Interrupt Clear (p. 891) [Offset: 0x044] */
    oC_Register_t       DMACTL;                         /**< UART DMA Control (p. 893) [Offset: 0x048] */
oC_REG_RESERVED_ARRAY(2 , 0x48U , 0xa4U);
    oC_Register_t       _9BITADDR;                      /**< UART 9-Bit Self Address (p. 894) [Offset: 0x0A4] */
    oC_Register_t       _9BITAMASK;                     /**< UART 9-Bit Self Address Mask (p. 895) [Offset: 0x0A8] */
oC_REG_RESERVED_ARRAY(3 , 0xa8U , 0xfc0U);
    oC_Register_t       PP;                             /**< UART Peripheral Properties (p. 896) [Offset: 0xFC0] */
oC_REG_RESERVED(4);
    oC_Register_t       CC;                             /**< UART Clock Configuration (p. 897) [Offset: 0xFC8] */
oC_REG_RESERVED(5);
    oC_Register_t       PeriphID4;                      /**< UART Peripheral Identification 4 (p. 898) [Offset: 0xFD0] */
    oC_Register_t       PeriphID5;                      /**< UART Peripheral Identification 5 (p. 899) [Offset: 0xFD4] */
    oC_Register_t       PeriphID6;                      /**< UART Peripheral Identification 6 (p. 900) [Offset: 0xFD8] */
    oC_Register_t       PeriphID7;                      /**< UART Peripheral Identification 7 (p. 901) [Offset: 0xFDC] */
    oC_Register_t       PeriphID0;                      /**< UART Peripheral Identification 0 (p. 902) [Offset: 0xFE0] */
    oC_Register_t       PeriphID1;                      /**< UART Peripheral Identification 1 (p. 903) [Offset: 0xFE4] */
    oC_Register_t       PeriphID2;                      /**< UART Peripheral Identification 2 (p. 904) [Offset: 0xFE8] */
    oC_Register_t       PeriphID3;                      /**< UART Peripheral Identification 3 (p. 905) [Offset: 0xFEC] */
    oC_Register_t       PCellID0;                       /**< UART PrimeCell Identification 0 (p. 906) [Offset: 0xFF0] */
    oC_Register_t       PCellID1;                       /**< UART PrimeCell Identification 1 (p. 907) [Offset: 0xFF4] */
    oC_Register_t       PCellID2;                       /**< UART PrimeCell Identification 2 (p. 908) [Offset: 0xFF8] */
    oC_Register_t       PCellID3;                       /**< UART PrimeCell Identification 3 (p. 909) [Offset: 0xFFC] */
} oC_REG_UART_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_UART0_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_UART_RegisterMap_t , oC_REG_UART0_BASE)
#define oC_REG_UART1_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_UART_RegisterMap_t , oC_REG_UART1_BASE)
#define oC_REG_UART2_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_UART_RegisterMap_t , oC_REG_UART2_BASE)
#define oC_REG_UART3_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_UART_RegisterMap_t , oC_REG_UART3_BASE)
#define oC_REG_UART4_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_UART_RegisterMap_t , oC_REG_UART4_BASE)
#define oC_REG_UART5_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_UART_RegisterMap_t , oC_REG_UART5_BASE)
#define oC_REG_UART6_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_UART_RegisterMap_t , oC_REG_UART6_BASE)
#define oC_REG_UART7_RMAP                           oC_CREATE_REGISTER_MAP_CAST( oC_REG_UART_RegisterMap_t , oC_REG_UART7_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Synchronous Serial Interface (SSI) (p. 923)
//

/*
 *  BASES
 */
#define oC_REG_SSI0_BASE                            ( 0x40008000UL )    /**< Base of the Register Map */
#define oC_REG_SSI1_BASE                            ( 0x40009000UL )    /**< Base of the Register Map */
#define oC_REG_SSI2_BASE                            ( 0x4000A000UL )    /**< Base of the Register Map */
#define oC_REG_SSI3_BASE                            ( 0x4000B000UL )    /**< Base of the Register Map */

/**
 * Register Map for SSI (p. 923)
 */
typedef struct
{
    oC_Register_t       CR0;                            /**< SSI Control 0 (p. 925) [Offset: 0x000] */
    oC_Register_t       CR1;                            /**< SSI Control 1 (p. 927) [Offset: 0x004] */
    oC_Register_t       DR;                             /**< SSI Data (p. 929) [Offset: 0x008] */
    oC_Register_t       SR;                             /**< SSI Status (p. 930) [Offset: 0x00C] */
    oC_Register_t       CPSR;                           /**< SSI Clock Prescale (p. 932) [Offset: 0x010] */
    oC_Register_t       IM;                             /**< SSI Interrupt Mask (p. 933) [Offset: 0x014] */
    oC_Register_t       RIS;                            /**< SSI Raw Interrupt Status (p. 934) [Offset: 0x018] */
    oC_Register_t       MIS;                            /**< SSI Masked Interrupt Status (p. 936) [Offset: 0x01C] */
    oC_Register_t       ICR;                            /**< SSI Interrupt Clear (p. 938) [Offset: 0x020] */
    oC_Register_t       DMACTL;                         /**< SSI DMA Control (p. 939) [Offset: 0x024] */
oC_REG_RESERVED_ARRAY(0 , 0x24U , 0xfc8U);
    oC_Register_t       CC;                             /**< SSI Clock Configuration (p. 940) [Offset: 0xFC8] */
oC_REG_RESERVED(1);
    oC_Register_t       PeriphID4;                      /**< SSI Peripheral Identification 4 (p. 941) [Offset: 0xFD0] */
    oC_Register_t       PeriphID5;                      /**< SSI Peripheral Identification 5 (p. 942) [Offset: 0xFD4] */
    oC_Register_t       PeriphID6;                      /**< SSI Peripheral Identification 6 (p. 943) [Offset: 0xFD8] */
    oC_Register_t       PeriphID7;                      /**< SSI Peripheral Identification 7 (p. 944) [Offset: 0xFDC] */
    oC_Register_t       PeriphID0;                      /**< SSI Peripheral Identification 0 (p. 945) [Offset: 0xFE0] */
    oC_Register_t       PeriphID1;                      /**< SSI Peripheral Identification 1 (p. 946) [Offset: 0xFE4] */
    oC_Register_t       PeriphID2;                      /**< SSI Peripheral Identification 2 (p. 947) [Offset: 0xFE8] */
    oC_Register_t       PeriphID3;                      /**< SSI Peripheral Identification 3 (p. 948) [Offset: 0xFEC] */
    oC_Register_t       PCellID0;                       /**< SSI PrimeCell Identification 0 (p. 949) [Offset: 0xFF0] */
    oC_Register_t       PCellID1;                       /**< SSI PrimeCell Identification 1 (p. 950) [Offset: 0xFF4] */
    oC_Register_t       PCellID2;                       /**< SSI PrimeCell Identification 2 (p. 951) [Offset: 0xFF8] */
    oC_Register_t       PCellID3;                       /**< SSI PrimeCell Identification 3 (p. 952) [Offset: 0xFFC] */
} oC_REG_SSI_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_SSI0_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_SSI_RegisterMap_t , oC_REG_SSI0_BASE)
#define oC_REG_SSI1_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_SSI_RegisterMap_t , oC_REG_SSI1_BASE)
#define oC_REG_SSI2_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_SSI_RegisterMap_t , oC_REG_SSI2_BASE)
#define oC_REG_SSI3_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_SSI_RegisterMap_t , oC_REG_SSI3_BASE)

/*
 *  Bits definitions
 */
#define oC_REG_SSI_CR0_SCR                          oC_BITS( 8 , 15 )
#define oC_REG_SSI_CR0_SPH                          oC_BIT_7
#define oC_REG_SSI_CR0_SPO                          oC_BIT_6
#define oC_REG_SSI_CR0_FRF                          oC_BITS( 4 , 5 )
#define oC_REG_SSI_CR0_DSS                          oC_BITS( 0 , 3 )

#define oC_REG_SSI_CR1_SSE                          oC_BIT_1
#define oC_REG_SSI_CR1_EOT                          oC_BIT_4
#define oC_REG_SSI_CR1_LBM                          oC_BIT_0

#define oC_REG_SSI_IM_TXIM                          oC_BIT_3
#define oC_REG_SSI_IM_RXIM                          oC_BIT_2
#define oC_REG_SSI_IM_RTIM                          oC_BIT_1
#define oC_REG_SSI_IM_RORIM                         oC_BIT_0

#define oC_REG_SSI_CPSR_CPSDVSR                     oC_BITS( 0 , 7 )

#define oC_REG_SSI_SR_TFE                           oC_BIT_0
#define oC_REG_SSI_SR_TNF                           oC_BIT_1
#define oC_REG_SSI_SR_RNE                           oC_BIT_2
#define oC_REG_SSI_SR_RFF                           oC_BIT_3
#define oC_REG_SSI_SR_BSY                           oC_BIT_4

/*
 *  Bits values
 */
#define oC_REG_SSI_CR0_FRF_SPI_FORMAT               ( 0U )

#define oC_REG_SSI_CR0_SPO_CLOCK_HIGH_WHEN_ACTIVE   ( 0U )
#define oC_REG_SSI_CR0_SPO_CLOCK_LOW_WHEN_ACTIVE    ( 1U << 6 )

#define oC_REG_SSI_CR0_SPH_CAPTURED_ON_FIRST_EDGE   ( 0U )
#define oC_REG_SSI_CR0_SPH_CAPTURED_ON_SECOND_EDGE  ( 1U << 7 )

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Inter-Integrated Circuit (I2C) (p. 973)
//

/*
 *  BASES
 */
#define oC_REG_I2C0_BASE                            ( 0x40020000UL )    /**< Base of the Register Map */
#define oC_REG_I2C1_BASE                            ( 0x40021000UL )    /**< Base of the Register Map */
#define oC_REG_I2C2_BASE                            ( 0x40022000UL )    /**< Base of the Register Map */
#define oC_REG_I2C3_BASE                            ( 0x40023000UL )    /**< Base of the Register Map */

/**
 * Register Map for I2C (p. 973)
 */
typedef struct
{
    oC_Register_t       MSA;                         /**< I2C Master Slave Address (p. 975) [Offset: 0x000] */
    oC_Register_t       MCS;                         /**< I2C Master Control/Status (p. 976) [Offset: 0x004] */
    oC_Register_t       MDR;                         /**< I2C Master Data (p. 981) [Offset: 0x008] */
    oC_Register_t       MTPR;                        /**< I2C Master Timer Period (p. 982) [Offset: 0x00C] */
    oC_Register_t       MIMR;                        /**< I2C Master Interrupt Mask (p. 983) [Offset: 0x010] */
    oC_Register_t       MRIS;                        /**< I2C Master Raw Interrupt Status (p. 984) [Offset: 0x014] */
    oC_Register_t       MMIS;                        /**< I2C Master Masked Interrupt Status (p. 985) [Offset: 0x018] */
    oC_Register_t       MICR;                        /**< I2C Master Interrupt Clear (p. 986) [Offset: 0x01C] */
    oC_Register_t       MCR;                         /**< I2C Master Configuration (p. 987) [Offset: 0x020] */
    oC_Register_t       MCLKOCNT;                    /**< I2C Master Clock Low Timeout Count (p. 989) [Offset: 0x024] */
oC_REG_RESERVED(0);
    oC_Register_t       MBMON;                       /**< I2C Master Bus Monitor (p. 990) [Offset: 0x02C] */
oC_REG_RESERVED_ARRAY(1 , 0x2cU , 0x38U);
    oC_Register_t       MCR2;                        /**< I2C Master Configuration 2 (p. 991) [Offset: 0x038] */
oC_REG_RESERVED_ARRAY(2 , 0x38U , 0x800U);
    oC_Register_t       SOAR;                        /**< I2C Slave Own Address (p. 992) [Offset: 0x800] */
    oC_Register_t       SCSR;                        /**< I2C Slave Control/Status (p. 993) [Offset: 0x804] */
    oC_Register_t       SDR;                         /**< I2C Slave Data (p. 995) [Offset: 0x808] */
    oC_Register_t       SIMR;                        /**< I2C Slave Interrupt Mask (p. 996) [Offset: 0x80C] */
    oC_Register_t       SRIS;                        /**< I2C Slave Raw Interrupt Status (p. 997) [Offset: 0x810] */
    oC_Register_t       SMIS;                        /**< I2C Slave Masked Interrupt Status (p. 998) [Offset: 0x814] */
    oC_Register_t       SICR;                        /**< I2C Slave Interrupt Clear (p. 999) [Offset: 0x818] */
    oC_Register_t       SOAR2;                       /**< I2C Slave Own Address 2 (p. 1000) [Offset: 0x81C] */
    oC_Register_t       SACKCTL;                     /**< I2C Slave ACK Control (p. 1001) [Offset: 0x820] */
oC_REG_RESERVED_ARRAY(3 , 0x820U , 0xfc0U);
    oC_Register_t       PP;                          /**< I2C Peripheral Properties (p. 1002) [Offset: 0xFC0] */
    oC_Register_t       PC;                          /**< I2C Peripheral Configuration (p. 1003) [Offset: 0xFC4] */
} oC_REG_I2C_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_I2C0_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_I2C_RegisterMap_t , oC_REG_I2C0_BASE)
#define oC_REG_I2C1_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_I2C_RegisterMap_t , oC_REG_I2C1_BASE)
#define oC_REG_I2C2_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_I2C_RegisterMap_t , oC_REG_I2C2_BASE)
#define oC_REG_I2C3_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_I2C_RegisterMap_t , oC_REG_I2C3_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Controller Area Network (CAN) (p. 1023)
//

/*
 *  BASES
 */
#define oC_REG_CAN0_BASE                            ( 0x40040000UL )    /**< Base of the Register Map */

/**
 * Register Map for CAN (p. 1023)
 */
typedef struct
{
    oC_Register_t       CTL;                            /**< CAN Control (p. 1025) [Offset: 0x000] */
    oC_Register_t       STS;                            /**< CAN Status (p. 1027) [Offset: 0x004] */
    oC_Register_t       ERR;                            /**< CAN Error Counter (p. 1030) [Offset: 0x008] */
    oC_Register_t       BIT;                            /**< CAN Bit Timing (p. 1031) [Offset: 0x00C] */
    oC_Register_t       INT;                            /**< CAN Interrupt (p. 1032) [Offset: 0x010] */
    oC_Register_t       TST;                            /**< CAN Test (p. 1033) [Offset: 0x014] */
    oC_Register_t       BRPE;                           /**< CAN Baud Rate Prescaler Extension (p. 1035) [Offset: 0x018] */
oC_REG_RESERVED(0);
    oC_Register_t       IF1CRQ;                         /**< CAN IF1 Command Request (p. 1036) [Offset: 0x020] */
    oC_Register_t       IF1CMSK;                        /**< CAN IF1 Command Mask (p. 1037) [Offset: 0x024] */
    oC_Register_t       IF1MSK1;                        /**< CAN IF1 Mask 1 (p. 1040) [Offset: 0x028] */
    oC_Register_t       IF1MSK2;                        /**< CAN IF1 Mask 2 (p. 1041) [Offset: 0x02C] */
    oC_Register_t       IF1ARB1;                        /**< CAN IF1 Arbitration 1 (p. 1043) [Offset: 0x030] */
    oC_Register_t       IF1ARB2;                        /**< CAN IF1 Arbitration 2 (p. 1044) [Offset: 0x034] */
    oC_Register_t       IF1MCTL;                        /**< CAN IF1 Message Control (p. 1046) [Offset: 0x038] */
    oC_Register_t       IF1DA1;                         /**< CAN IF1 Data A1 (p. 1049) [Offset: 0x03C] */
    oC_Register_t       IF1DA2;                         /**< CAN IF1 Data A2 (p. 1049) [Offset: 0x040] */
    oC_Register_t       IF1DB1;                         /**< CAN IF1 Data B1 (p. 1049) [Offset: 0x044] */
    oC_Register_t       IF1DB2;                         /**< CAN IF1 Data B2 (p. 1049) [Offset: 0x048] */
oC_REG_RESERVED_ARRAY(1 , 0x48U , 0x80U);
    oC_Register_t       IF2CRQ;                         /**< CAN IF2 Command Request (p. 1036) [Offset: 0x080] */
    oC_Register_t       IF2CMSK;                        /**< CAN IF2 Command Mask (p. 1037) [Offset: 0x084] */
    oC_Register_t       IF2MSK1;                        /**< CAN IF2 Mask 1 (p. 1040) [Offset: 0x088] */
    oC_Register_t       IF2MSK2;                        /**< CAN IF2 Mask 2 (p. 1041) [Offset: 0x08C] */
    oC_Register_t       IF2ARB1;                        /**< CAN IF2 Arbitration 1 (p. 1043) [Offset: 0x090] */
    oC_Register_t       IF2ARB2;                        /**< CAN IF2 Arbitration 2 (p. 1044) [Offset: 0x094] */
    oC_Register_t       IF2MCTL;                        /**< CAN IF2 Message Control (p. 1046) [Offset: 0x098] */
    oC_Register_t       IF2DA1;                         /**< CAN IF2 Data A1 (p. 1049) [Offset: 0x09C] */
    oC_Register_t       IF2DA2;                         /**< CAN IF2 Data A2 (p. 1049) [Offset: 0x0A0] */
    oC_Register_t       IF2DB1;                         /**< CAN IF2 Data B1 (p. 1049) [Offset: 0x0A4] */
    oC_Register_t       IF2DB2;                         /**< CAN IF2 Data B2 (p. 1049) [Offset: 0x0A8] */
oC_REG_RESERVED_ARRAY(2 , 0xa8U , 0x100U);
    oC_Register_t       TXRQ1;                          /**< CAN Transmission Request 1 (p. 1050) [Offset: 0x100] */
    oC_Register_t       TXRQ2;                          /**< CAN Transmission Request 2 (p. 1050) [Offset: 0x104] */
oC_REG_RESERVED_ARRAY(3 , 0x104U , 0x120U);
    oC_Register_t       NWDA1;                          /**< CAN New Data 1 (p. 1051) [Offset: 0x120] */
    oC_Register_t       NWDA2;                          /**< CAN New Data 2 (p. 1051) [Offset: 0x124] */
oC_REG_RESERVED_ARRAY(4 , 0x124U , 0x140U);
    oC_Register_t       MSG1INT;                        /**< CAN Message 1 Interrupt Pending (p. 1052) [Offset: 0x140] */
    oC_Register_t       MSG2INT;                        /**< CAN Message 2 Interrupt Pending (p. 1052) [Offset: 0x144] */
oC_REG_RESERVED_ARRAY(5 , 0x144U , 0x160U);
    oC_Register_t       MSG1VAL;                        /**< CAN Message 1 Valid (p. 1053) [Offset: 0x160] */
    oC_Register_t       MSG2VAL;                        /**< CAN Message 2 Valid (p. 1053) [Offset: 0x164] */
} oC_REG_CAN_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_CAN0_RMAP                            oC_CREATE_REGISTER_MAP_CAST( oC_REG_CAN_RegisterMap_t , oC_REG_CAN0_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Universal Serial Bus (USB) (p. 1064)
//

/*
 *  BASES
 */
#define oC_REG_USB_BASE                             ( 0x40050000UL )    /**< Base of the Register Map */

/**
 * Register Map for USB (p. 1062)
 */
typedef struct
{
    oC_Register_t       FADDR;                          /**< USB Device Functional Address (p. 1066) [Offset: 0x000] */
    oC_Register_t       POWER;                          /**< USB Power (p. 1067) [Offset: 0x001] */
    oC_Register_t       TXIS;                           /**< USB Transmit Interrupt Status (p. 1069) [Offset: 0x002] */
    oC_Register_t       RXIS;                           /**< USB Receive Interrupt Status (p. 1071) [Offset: 0x004] */
    oC_Register_t       TXIE;                           /**< USB Transmit Interrupt Enable (p. 1072) [Offset: 0x006] */
    oC_Register_t       RXIE;                           /**< USB Receive Interrupt Enable (p. 1074) [Offset: 0x008] */
    oC_Register_t       IS;                             /**< USB General Interrupt Status (p. 1075) [Offset: 0x00A] */
    oC_Register_t       IE;                             /**< USB Interrupt Enable (p. 1076) [Offset: 0x00B] */
    oC_Register_t       FRAME;                          /**< USB Frame Value (p. 1078) [Offset: 0x00C] */
    oC_Register_t       EPIDX;                          /**< USB Endpoint Index (p. 1079) [Offset: 0x00E] */
    oC_Register_t       TEST;                           /**< USB Test Mode (p. 1080) [Offset: 0x00F] */
oC_REG_RESERVED_ARRAY(0 , 0xfU , 0x20U);
    oC_Register_t       FIFO0;                          /**< USB FIFO Endpoint 0 (p. 1081) [Offset: 0x020] */
    oC_Register_t       FIFO1;                          /**< USB FIFO Endpoint 1 (p. 1081) [Offset: 0x024] */
    oC_Register_t       FIFO2;                          /**< USB FIFO Endpoint 2 (p. 1081) [Offset: 0x028] */
    oC_Register_t       FIFO3;                          /**< USB FIFO Endpoint 3 (p. 1081) [Offset: 0x02C] */
    oC_Register_t       FIFO4;                          /**< USB FIFO Endpoint 4 (p. 1081) [Offset: 0x030] */
    oC_Register_t       FIFO5;                          /**< USB FIFO Endpoint 5 (p. 1081) [Offset: 0x034] */
    oC_Register_t       FIFO6;                          /**< USB FIFO Endpoint 6 (p. 1081) [Offset: 0x038] */
    oC_Register_t       FIFO7;                          /**< USB FIFO Endpoint 7 (p. 1081) [Offset: 0x03C] */
oC_REG_RESERVED_ARRAY(1 , 0x3cU , 0x62U);
    oC_Register_t       TXFIFOSZ;                       /**< USB Transmit Dynamic FIFO Sizing (p. 1082) [Offset: 0x062] */
    oC_Register_t       RXFIFOSZ;                       /**< USB Receive Dynamic FIFO Sizing (p. 1082) [Offset: 0x063] */
    oC_Register_t       TXFIFOADD;                      /**< USB Transmit FIFO Start Address (p. 1083) [Offset: 0x064] */
    oC_Register_t       RXFIFOADD;                      /**< USB Receive FIFO Start Address (p. 1083) [Offset: 0x066] */
oC_REG_RESERVED_ARRAY(2 , 0x66U , 0x7aU);
    oC_Register_t       CONTIM;                         /**< USB Connect Timing (p. 1084) [Offset: 0x07A] */
    oC_Register_t       FSEOF;                          /**< USB Full-Speed Last Transaction to End of Frame Timing (p. 1085) [Offset: 0x07D] */
oC_REG_RESERVED_ARRAY(3 , 0x7dU , 0x102U);
    oC_Register_t       CSRL0;                          /**< USB Control and Status Endpoint 0 Low (p. 1088) [Offset: 0x102] */
    oC_Register_t       CSRH0;                          /**< USB Control and Status Endpoint 0 High (p. 1090) [Offset: 0x103] */
    oC_Register_t       COUNT0;                         /**< USB Receive Byte Count Endpoint 0 (p. 1091) [Offset: 0x108] */
oC_REG_RESERVED(4);
    oC_Register_t       TXMAXP1;                        /**< USB Maximum Transmit Data Endpoint 1 (p. 1087) [Offset: 0x110] */
    oC_Register_t       TXCSRL1;                        /**< USB Transmit Control and Status Endpoint 1 Low (p. 1092) [Offset: 0x112] */
    oC_Register_t       TXCSRH1;                        /**< USB Transmit Control and Status Endpoint 1 High (p. 1094) [Offset: 0x113] */
    oC_Register_t       RXMAXP1;                        /**< USB Maximum Receive Data Endpoint 1 (p. 1096) [Offset: 0x114] */
    oC_Register_t       RXCSRL1;                        /**< USB Receive Control and Status Endpoint 1 Low (p. 1097) [Offset: 0x116] */
    oC_Register_t       RXCSRH1;                        /**< USB Receive Control and Status Endpoint 1 High (p. 1100) [Offset: 0x117] */
    oC_Register_t       RXCOUNT1;                       /**< USB Receive Byte Count Endpoint 1 (p. 1102) [Offset: 0x118] */
oC_REG_RESERVED(5);
    oC_Register_t       TXMAXP2;                        /**< USB Maximum Transmit Data Endpoint 2 (p. 1087) [Offset: 0x120] */
    oC_Register_t       TXCSRL2;                        /**< USB Transmit Control and Status Endpoint 2 Low (p. 1092) [Offset: 0x122] */
    oC_Register_t       TXCSRH2;                        /**< USB Transmit Control and Status Endpoint 2 High (p. 1094) [Offset: 0x123] */
    oC_Register_t       RXMAXP2;                        /**< USB Maximum Receive Data Endpoint 2 (p. 1096) [Offset: 0x124] */
    oC_Register_t       RXCSRL2;                        /**< USB Receive Control and Status Endpoint 2 Low (p. 1097) [Offset: 0x126] */
    oC_Register_t       RXCSRH2;                        /**< USB Receive Control and Status Endpoint 2 High (p. 1100) [Offset: 0x127] */
    oC_Register_t       RXCOUNT2;                       /**< USB Receive Byte Count Endpoint 2 (p. 1102) [Offset: 0x128] */
oC_REG_RESERVED(6);
    oC_Register_t       TXMAXP3;                        /**< USB Maximum Transmit Data Endpoint 3 (p. 1087) [Offset: 0x130] */
    oC_Register_t       TXCSRL3;                        /**< USB Transmit Control and Status Endpoint 3 Low (p. 1092) [Offset: 0x132] */
    oC_Register_t       TXCSRH3;                        /**< USB Transmit Control and Status Endpoint 3 High (p. 1094) [Offset: 0x133] */
    oC_Register_t       RXMAXP3;                        /**< USB Maximum Receive Data Endpoint 3 (p. 1096) [Offset: 0x134] */
    oC_Register_t       RXCSRL3;                        /**< USB Receive Control and Status Endpoint 3 Low (p. 1097) [Offset: 0x136] */
    oC_Register_t       RXCSRH3;                        /**< USB Receive Control and Status Endpoint 3 High (p. 1100) [Offset: 0x137] */
    oC_Register_t       RXCOUNT3;                       /**< USB Receive Byte Count Endpoint 3 (p. 1102) [Offset: 0x138] */
oC_REG_RESERVED(7);
    oC_Register_t       TXMAXP4;                        /**< USB Maximum Transmit Data Endpoint 4 (p. 1087) [Offset: 0x140] */
    oC_Register_t       TXCSRL4;                        /**< USB Transmit Control and Status Endpoint 4 Low (p. 1092) [Offset: 0x142] */
    oC_Register_t       TXCSRH4;                        /**< USB Transmit Control and Status Endpoint 4 High (p. 1094) [Offset: 0x143] */
    oC_Register_t       RXMAXP4;                        /**< USB Maximum Receive Data Endpoint 4 (p. 1096) [Offset: 0x144] */
    oC_Register_t       RXCSRL4;                        /**< USB Receive Control and Status Endpoint 4 Low (p. 1097) [Offset: 0x146] */
    oC_Register_t       RXCSRH4;                        /**< USB Receive Control and Status Endpoint 4 High (p. 1100) [Offset: 0x147] */
    oC_Register_t       RXCOUNT4;                       /**< USB Receive Byte Count Endpoint 4 (p. 1102) [Offset: 0x148] */
oC_REG_RESERVED(8);
    oC_Register_t       TXMAXP5;                        /**< USB Maximum Transmit Data Endpoint 5 (p. 1087) [Offset: 0x150] */
    oC_Register_t       TXCSRL5;                        /**< USB Transmit Control and Status Endpoint 5 Low (p. 1092) [Offset: 0x152] */
    oC_Register_t       TXCSRH5;                        /**< USB Transmit Control and Status Endpoint 5 High (p. 1094) [Offset: 0x153] */
    oC_Register_t       RXMAXP5;                        /**< USB Maximum Receive Data Endpoint 5 (p. 1096) [Offset: 0x154] */
    oC_Register_t       RXCSRL5;                        /**< USB Receive Control and Status Endpoint 5 Low (p. 1097) [Offset: 0x156] */
    oC_Register_t       RXCSRH5;                        /**< USB Receive Control and Status Endpoint 5 High (p. 1100) [Offset: 0x157] */
    oC_Register_t       RXCOUNT5;                       /**< USB Receive Byte Count Endpoint 5 (p. 1102) [Offset: 0x158] */
oC_REG_RESERVED(9);
    oC_Register_t       TXMAXP6;                        /**< USB Maximum Transmit Data Endpoint 6 (p. 1087) [Offset: 0x160] */
    oC_Register_t       TXCSRL6;                        /**< USB Transmit Control and Status Endpoint 6 Low (p. 1092) [Offset: 0x162] */
    oC_Register_t       TXCSRH6;                        /**< USB Transmit Control and Status Endpoint 6 High (p. 1094) [Offset: 0x163] */
    oC_Register_t       RXMAXP6;                        /**< USB Maximum Receive Data Endpoint 6 (p. 1096) [Offset: 0x164] */
    oC_Register_t       RXCSRL6;                        /**< USB Receive Control and Status Endpoint 6 Low (p. 1097) [Offset: 0x166] */
    oC_Register_t       RXCSRH6;                        /**< USB Receive Control and Status Endpoint 6 High (p. 1100) [Offset: 0x167] */
    oC_Register_t       RXCOUNT6;                       /**< USB Receive Byte Count Endpoint 6 (p. 1102) [Offset: 0x168] */
oC_REG_RESERVED(10);
    oC_Register_t       TXMAXP7;                        /**< USB Maximum Transmit Data Endpoint 7 (p. 1087) [Offset: 0x170] */
    oC_Register_t       TXCSRL7;                        /**< USB Transmit Control and Status Endpoint 7 Low (p. 1092) [Offset: 0x172] */
    oC_Register_t       TXCSRH7;                        /**< USB Transmit Control and Status Endpoint 7 High (p. 1094) [Offset: 0x173] */
    oC_Register_t       RXMAXP7;                        /**< USB Maximum Receive Data Endpoint 7 (p. 1096) [Offset: 0x174] */
    oC_Register_t       RXCSRL7;                        /**< USB Receive Control and Status Endpoint 7 Low (p. 1097) [Offset: 0x176] */
    oC_Register_t       RXCSRH7;                        /**< USB Receive Control and Status Endpoint 7 High (p. 1100) [Offset: 0x177] */
    oC_Register_t       RXCOUNT7;                       /**< USB Receive Byte Count Endpoint 7 (p. 1102) [Offset: 0x178] */
oC_REG_RESERVED_ARRAY(11 , 0x178U , 0x340U);
    oC_Register_t       RXDPKTBUFDIS;                   /**< USB Receive Double Packet Buffer Disable (p. 1103) [Offset: 0x340] */
    oC_Register_t       TXDPKTBUFDIS;                   /**< USB Transmit Double Packet Buffer Disable (p. 1104) [Offset: 0x342] */
oC_REG_RESERVED_ARRAY(12 , 0x342U , 0x410U);
    oC_Register_t       DRRIS;                          /**< USB Device RESUME Raw Interrupt Status (p. 1105) [Offset: 0x410] */
    oC_Register_t       DRIM;                           /**< USB Device RESUME Interrupt Mask (p. 1106) [Offset: 0x414] */
    oC_Register_t       DRISC;                          /**< USB Device RESUME Interrupt Status and Clear (p. 1107) [Offset: 0x418] */
oC_REG_RESERVED_ARRAY(13 , 0x418U , 0x450U);
    oC_Register_t       DMASEL;                         /**< USB DMA Select (p. 1108) [Offset: 0x450] */
oC_REG_RESERVED_ARRAY(14 , 0x450U , 0xfc0U);
    oC_Register_t       PP;                             /**< USB Peripheral Properties (p. 1110) [Offset: 0xFC0] */
} oC_REG_USB_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_USB_RMAP                             oC_CREATE_REGISTER_MAP_CAST( oC_REG_USB_RegisterMap_t , oC_REG_USB_BASE)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Analog Comparators (ACM) (p. 1116)
//

/*
 *  BASES
 */
#define oC_REG_ACM_BASE                             ( 0x4003C000UL )    /**< Base of the Register Map */

/**
 * Register Map for ACM (p. 1116)
 */
typedef struct
{
    oC_Register_t       MIS;                            /**< Analog Comparator Masked Interrupt Status (p. 1118) [Offset: 0x000] */
    oC_Register_t       RIS;                            /**< Analog Comparator Raw Interrupt Status (p. 1119) [Offset: 0x004] */
    oC_Register_t       INTEN;                          /**< Analog Comparator Interrupt Enable (p. 1120) [Offset: 0x008] */
oC_REG_RESERVED(0);
    oC_Register_t       REFCTL;                         /**< Analog Comparator Reference Voltage Control (p. 1121) [Offset: 0x010] */
oC_REG_RESERVED_ARRAY(1 , 0x10U , 0x20U);
    oC_Register_t       STAT0;                          /**< Analog Comparator Status 0 (p. 1122) [Offset: 0x020] */
    oC_Register_t       CTL0;                           /**< Analog Comparator Control 0 (p. 1123) [Offset: 0x024] */
oC_REG_RESERVED_ARRAY(2 , 0x24U , 0x40U);
    oC_Register_t       STAT1;                          /**< Analog Comparator Status 1 (p. 1122) [Offset: 0x040] */
    oC_Register_t       CTL1;                           /**< Analog Comparator Control 1 (p. 1123) [Offset: 0x044] */
oC_REG_RESERVED_ARRAY(3 , 0x44U , 0xfc0U);
    oC_Register_t       MPPP;                           /**< Analog Comparator Peripheral Properties (p. 1125) [Offset: 0xFC0] */
} oC_REG_ACM_RegisterMap_t;

/*
 *  Registers Maps casting
 */
#define oC_REG_ACM_RMAP                             oC_CREATE_REGISTER_MAP_CAST( oC_REG_ACM_RegisterMap_t , oC_REG_ACM_BASE)


#endif /* OC_REG_DEFS_H_ */
