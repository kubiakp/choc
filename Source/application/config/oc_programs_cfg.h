/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_programs_cfg.h
 *
 *    @brief      The file with configuration of the programs.
 *
 *    @author     Patryk Kubiak - (Created on: 3 pa� 2014 09:13:39)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#ifndef OC_PROGRAMS_CFG_H_
#define OC_PROGRAMS_CFG_H_

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INCLUDES
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// User includes

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    SYSTEM PROGRAMS LIST
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * The macro with definition of the list of system programs. To turn add new system program just add the line USE( program_name ) \ at the
 * end of the list. To turn off selected program, replace USE to DONT_USE
 *
 *      USE( PROGRAM_NAME , STACK_SIZE )
 *              PROGRAM_NAME    - name of the program function
 *              STACK_SIZE      - size of the stack for program (in words)
 */
#define oC_SYSTEM_PROGRAMS_LIST( USE , DONT_USE )                                                                                           \
                        USE( drivers_tests , "driv-tests"   , oC_StackSize_Minimum) \
                        USE( Welcome       , "welcome-msg"  , 512 ) \
                        USE( gpio_test     , "gpio-test"    , 512 ) \
                        USE( spi_test      , "spi-test"     , oC_StackSize_Minimum ) \
                        USE( one_wire_test , "1wire test"     , 512 ) \
                        USE( spi_test2     , "spi-test2"    , oC_StackSize_Minimum ) \
                        USE( tim_test      , "tim-test"     , 512 ) \
                        USE( time_test     , "time-test"     , 512 ) \

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    USER PROGRAMS LIST
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * The macro with definition of the list of user programs. To turn add new user program just add the line USE( program_name ) \ at the
 * end of the list. To turn off selected program, replace USE to DONT_USE
 */
#define oC_USER_PROGRAMS_LIST( USE , DONT_USE )                                                                                             \
                        USE( HelloWorld , "hello-world" , 512)   \


/**
 * The macro with definition of the user program, which should be run by default.
 */
#define oC_DEFAULT_PROGRAM             one_wire_test

/**
 * Default priority of the program
 */
#define oC_DEFAULT_PRIORITY            oC_OS_Priority_Maximum

#endif /* OC_PROGRAMS_CFG_H_ */
