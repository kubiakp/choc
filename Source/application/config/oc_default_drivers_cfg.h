/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_default_drivers_cfg.h
 *
 *    @brief      The file with list of the default drivers configuration
 *
 *    @author     Patryk Kubiak - (Created on: 8 pa� 2014 13:17:08)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @addtogroup CFG
 * @{
 *
 * @page CFG
 * @section DEFAULT_LOAD_CFG Default drivers configurations
 *
 *
 ******************************************************************************************************************************************/


#ifndef APPLICATION_CONFIG_OC_DEFAULT_DRIVERS_CFG_H_
#define APPLICATION_CONFIG_OC_DEFAULT_DRIVERS_CFG_H_

#include <oc_cfg.h>

//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
#define CFG_LIST_DEFAULT_DRIVERS_CONFIGURATIONS( CONFIGURE ) \
    CONFIGURE( SYS , SYS_Config ) \
    CONFIGURE( UART, UART_Config ) \

/**
 * @}
 */

#endif /* APPLICATION_CONFIG_OC_DEFAULT_DRIVERS_CFG_H_ */
