/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_drivers_cfg.h
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 6 pa� 2014 16:32:47)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef OC_DRIVERS_CFG_H_
#define OC_DRIVERS_CFG_H_

#include <oc_cfg.h>

//==========================================================================================================================================
/**
 * @brief   List of drivers, that should be included to the system.
 *
 * The definition contains list of drivers, that will be used in the system. To make driver usable it must be registered and turned on before.
 * Drivers can be turned on automatically before start of the system, or manually by calling #oC_Drivers_TurnOnDriver function, but the
 * registration can be done only by using this definition. <br />
 * <br />
 * To add driver to list, use one of the options, that are listed below:<br />
 *<br />
 * OPTIONS:<br />
 *      Name     | Description                                  | Prototype
 *      ---------|----------------------------------------------|---------------------------------------------------
 *      REGISTER | Driver will be registered in the system.     | REGISTER( DRIVER_NAME )
 *      TURN_ON  | Driver will be registered and turned on.     | TURN_ON( DRIVER_NAME )
 *
 * REGISTER PARAMETERS:<br />
 *      Parameter name | Description                            | Example of value
 *      ---------------|----------------------------------------|---------------------------------------------------
 *      DRIVER_NAME    | Name of the driver to turn on          | GPIO
 *
 * TURN_ON PARAMETERS:<br />
 *      Parameter name | Description                            | Example of value
 *      ---------------|----------------------------------------|---------------------------------------------------
 *      DRIVER_NAME    | Name of the driver to turn on          | GPIO
 *
 *  The code below shows example of the list usage
 *  ~~~~~~~~~~~~~~{.c}
 *  #define CFG_LIST_INCLUDED_DRIVERS( REGISTER , TURN_ON ) \
 *  TURN_ON ( SYS )\
 *  REGISTER( GPIO )\
 *  TURN_ON ( SPI )\
 *  ~~~~~~~~~~~~~~
 */
//==========================================================================================================================================
#define CFG_LIST_INCLUDED_DRIVERS( REGISTER , TURN_ON ) \
    TURN_ON( SYS )\
    TURN_ON( GPIO )\
    TURN_ON( SPI )\
    TURN_ON( UART )\
    TURN_ON( TIM )\

#endif /* OC_DRIVERS_CFG_H_ */
