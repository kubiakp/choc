/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file       oc_default_drivers_cfg.c
 *
 *    @brief      The file with definitions of the default driver initializations
 *
 *    @author     Patryk Kubiak - (Created on: 8 pa� 2014 13:11:05)
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/


#include <oc_drivers.h>
#include <oc_general_cfg.h>
#include <oc_gen_types.h>

/*==========================================================================================================================================
//
//     DRIVERS CONFIGURATION STRUCTURES
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * Default configuration of the SYS driver. To use it, look at #DEFAULT_DRIVERS_CONFIGURATION_LIST in the oc_default_drivers_cfg.h header.
 */
//==========================================================================================================================================
const oC_SYS_Config_t SYS_Config    = {
                                       .WantedFrequency     = oC_CLOCK_HZ,
                                       .OscillatorFrequency = oC_Frequency_MHz(16),
                                       .OscillatorSource    = oC_SYS_OscillatorSource_External
};

//==========================================================================================================================================
/**
 * Configuration of UART for stdio
 */
//==========================================================================================================================================
const oC_UART_Config_t UART_Config  = {
                                       .BitRate     = 921600 ,
                                       .Channel     = oC_UART_Channel_UART1 ,
                                       .WordLength  = oC_UART_WordLength_8Bits ,
                                       .Parity      = oC_UART_Parity_None ,
                                       .StopBit     = oC_UART_StopBit_1Bit ,
                                       .BitOrder    = oC_UART_BitOrder_LSBFirst ,
                                       .Invert      = oC_UART_Invert_NotInverted ,
                                       .Pins        = {
                                                       .Rx  = &PC4,//oC_GPIO_PinLink_UART1_Rx_PC4 ,
                                                       .Tx  = &PC5,//oC_GPIO_PinLink_UART1_Tx_PC5
                                       }
};
