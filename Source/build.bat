@echo off
set OS=Unknown
IF %PROCESSOR_ARCHITECTURE% == x86   set OS=Win32
IF %PROCESSOR_ARCHITECTURE% == AMD64 set OS=Win64
IF %PROCESSOR_ARCHITECTURE% == amd64 set OS=Win64

IF %OS% == Win32 (
	echo "Building for Win32 ... "
	@set Path=%cd%/../Tools/compilers/make32/;%cd%/../Tools/compilers/arm_none_eabi_gcc/bin/
	REM @setx path "%cd%/../Tools/compilers/make32/;%cd%/../Tools/compilers/arm_none_eabi_gcc/bin/"
)
IF %OS% == Win64 (
	echo "Building for Win64 ... "
	@set Path "%cd%/../Tools/compilers/make64/;%cd%/../Tools/compilers/arm_none_eabi_gcc/bin/;"
	REM @setx path "%cd%/../Tools/compilers/make64/;%cd%/../Tools/compilers/arm_none_eabi_gcc/bin/"
)

IF %OS% == Unknown (
	echo "Unknown operating system"
) ELSE (
	
	@make %1
)
