#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QStringList>
#include <QSettings>

#define TEMPLATES_PATH                  "templates_path"
#define DRIVERS_PATH                    "drivers_path"
#define LLD_PATH                        "lld_path"
#define CONFIG_PATH                     "config_path"

#define DRIVER_FILES_ARRAY_NAME         "driver_files"
#define LLD_FILES_ARRAY_NAME            "lld_files"

#define FILE_TEMPLATE_TAG               "template"
#define FILE_OUTPUT_TAG                 "output"

#define DRIVER_NAME_REPLACE_TAG         "<dname>"

#define DRIVER_NAME_REG_EXP             "\\w+"

#define TMPLPARS_PROGRAM_NAME           "tmplpars.exe"

class Manager
{

    //==============================================================================================
    //
    //  PRIVATE VARIABLES SECTION
    //
    //==============================================================================================
    private:

        QString                 cv_DriverName;          /**< Name of the driver to create */
        bool                    cv_WithLLD;             /**< Flag if should create LLD layer also */
        QStringList             cv_Variables;           /**< Additional variables put into the template */

        QString                 cv_TemplatesPath;       /**< Path to the templates */
        QString                 cv_DriversPath;         /**< Path to the driver destination */
        QString                 cv_LLDPath;             /**< Path to the lld layer destination */
        QString                 cv_ConfigPath;          /**< Path to the configuration directory */

        QMap<QString,QString>   cv_DriverFiles;         /**< List of the files to create in driver layer */
        QMap<QString,QString>   cv_LLDFiles;            /**< List of the files to create in lld layer */

    //==============================================================================================
    //
    //  PUBLIC METHODS SECTION
    //
    //==============================================================================================
    public:

        Manager();
        bool                    ParseArguments          ( QStringList Arguments );
        bool                    LoadSettings            ( QString FileName );
        bool                    CreatePaths             ();
        bool                    CheckProgramFields      ();
        bool                    CreateDriverFiles       ();
        bool                    CreateLLDFiles          ();

    //==============================================================================================
    //
    //  PRIVATE METHODS SECTION
    //
    //==============================================================================================
    private:

        bool                    PrintHelp               ();
        bool                    CheckPathExists         ( QString Path , QString Msg );
        bool                    CheckDriverName         ( QString DriverName );
        bool                    CheckFilesList          ( QMap<QString,QString> & Files , QString Msg );
        bool                    CheckFileExists         ( QString FilePath );
        QString                 ReadValueFromSettings   ( QSettings & Settings , QString key );
        QMap<QString,QString>   ReadFilesFromSettings   ( QSettings & Settings , QString ArrayName );
        bool                    CreatePath              ( QString Path , QString Msg );
        bool                    RunProgram              ( QString Name , QStringList Arguments );
        bool                    CreateFileFromTemplate  ( QString TemplateFilePath , QString OutputFilePath );
        bool                    CreateFileFromTemplate  ( QString TemplateFileName , QString OutputFileName , QString OutputPath );
        bool                    CreateFiles             ( QMap<QString,QString> Files , QString OutputPath );
        QString                 ReplaceDriverNameTag    (QString String );

};

#endif // MANAGER_H
