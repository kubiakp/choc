#include <QCoreApplication>
#include <templateparser.h>
#include <console.h>

struct Properties_t
{
        QString ProgramName;
        QString TemplateFileName;
        QString OutputFileName;
        QMap<QString , QString> Variables;
        bool    Properties;
};

void ShowUsage()
{
    QFile HelpFile( ":/cmd/usage" );

    if ( HelpFile.open( QFile::ReadOnly ) )
    {
        QTextStream HelpStream(&HelpFile);

        PrintDefault( HelpStream.readAll().toStdString() );
    }
    else
    {
        PrintError("File with usage not found!");
    }
}

void ShowHelp()
{
    QFile HelpFile( ":/cmd/help" );

    if ( HelpFile.open( QFile::ReadOnly ) )
    {
        QTextStream HelpStream(&HelpFile);

        PrintDefault( HelpStream.readAll().toStdString() );
    }
    else
    {
        PrintError("File with help not found!");
    }
}

bool CheckProperties( Properties_t & Properties )
{
    bool result;
    if ( Properties.TemplateFileName.isEmpty() )
    {
        PrintError("The template path is not given! (type --help for more)");
        result = false;
    }
    else if ( Properties.OutputFileName.isEmpty() )
    {
        PrintError("The output path is not given! (type --help for more)");
        result = false;
    }
    else
    {
        result = true;
    }

    return result;
}


Properties_t ReadProperties( QStringList arguments )
{
    Properties_t Properties;

    if ( arguments.contains( "--help" ) )
    {
        ShowHelp();
    }
    else
    {
        Properties.ProgramName      = arguments.value(0);
        Properties.TemplateFileName = arguments.value(1);
        Properties.OutputFileName   = arguments.value(2);

        int cnt = arguments.count();

        for(int i = 3 ; i < cnt ; i++)
        {
            QStringList splitted    = arguments.value(i).split("=");

            if ( splitted.count() == 2 )
            {
                Properties.Variables[ splitted[0] ] = splitted[1];
            }
        }
    }

    return Properties;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Properties_t Properties = ReadProperties( a.arguments() );


    if ( CheckProperties( Properties ) )
    {
        TemplateParser * T = new TemplateParser( Properties.TemplateFileName , Properties.OutputFileName , Properties.Variables);
    }
    else
    {
        ShowUsage();
    }

    return 0;
}
